<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AutomationController extends Controller
{
    public function index()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Datatable"], ['name' => "Basic"]];
        return view('/content/13_automation/index', ['breadcrumbs' => $breadcrumbs]);
    }

    public function create()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/13_automation/create', ['pageConfigs' => $pageConfigs]);
    }

    public function edit()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/13_automation/edit', ['pageConfigs' => $pageConfigs]);
    }

    public function detail()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/13_automation/detail', ['pageConfigs' => $pageConfigs]);
    }

    public function launch()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/13_automation/launch', ['pageConfigs' => $pageConfigs]);
    }

    public function setting()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/13_automation/setting', ['pageConfigs' => $pageConfigs]);
    }

    public function stat()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/13_automation/stat', ['pageConfigs' => $pageConfigs]);
    }
}
