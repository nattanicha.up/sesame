<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ConsignmentController extends Controller
{
    public function index()
    {
        $pageConfigs = [
            'pageClass' => 'ecommerce-application',
        ];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['name' => "Checkout"]
        ];

        return view('/content/04_Consignment/sellcontract', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
//        return view('/content/03_buy_and_sale/buyreceive');
    }

    public function pay_installment()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Datatable"], ['name' => "Basic"]];
        return view('/content/04_Consignment/paycontract', ['breadcrumbs' => $breadcrumbs]);
    }

    public function pay_detail()
    {
        $pageConfigs = [
            'pageClass' => 'ecommerce-application',
        ];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['name' => "Checkout"]
        ];

        return view('/content/04_Consignment/paydetail', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
//        return view('/content/03_buy_and_sale/buyreceive');
    }

    public function pay_invoice()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/content/04_Consignment/payinvoice', ['pageConfigs' => $pageConfigs]);
    }

    public function redeem_invoice()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/content/04_Consignment/redeeminvoice', ['pageConfigs' => $pageConfigs]);
    }

    public function redeem_contract()
    {
        $pageConfigs = [
            'pageClass' => 'ecommerce-application',
        ];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['name' => "Checkout"]
        ];

        return view('/content/04_Consignment/redeemcontract', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
//        return view('/content/03_buy_and_sale/buyreceive');
    }

    public function sell_reportcons()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Datatable"], ['name' => "Basic"]];
        return view('/content/08_Report/sellconsreport', ['breadcrumbs' => $breadcrumbs]);
    }

    public function sell_reportcons_detail()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Datatable"], ['name' => "Basic"]];
        return view('/content/08_Report/sellconsreportdetail', ['breadcrumbs' => $breadcrumbs]);
    }
}
