<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CostController extends Controller
{
    public function index()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Datatable"], ['name' => "Basic"]];
        return view('/content/11_cost/index', ['breadcrumbs' => $breadcrumbs]);
    }

    public function create()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/11_cost/create', ['pageConfigs' => $pageConfigs]);
    }

    public function edit()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/11_cost/edit', ['pageConfigs' => $pageConfigs]);
    }

    public function detail()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/11_cost/detail', ['pageConfigs' => $pageConfigs]);
    }
}
