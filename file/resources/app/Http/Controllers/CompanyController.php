<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class CompanyController extends Controller
{
    public function settings()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Pages"], ['name' => "Account Settings"]];
        return view('/content/09_company/settings', ['breadcrumbs' => $breadcrumbs]);
    }

    public function user()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Datatable"], ['name' => "Basic"]];
        return view('/content/09_company/user', ['breadcrumbs' => $breadcrumbs]);
    }

    public function userStore(Request $request)
    {
        try{
            $rules = [
                'name' => 'required',
                'surname' => 'required',
                'id_card' => 'required|string|min:13|max:13|unique:100_USERS',
                'gender' => 'required',
                'permission_id' => 'required',
                'username' => 'required|unique:100_USERS',
                'password' => 'required',
            ];

            $message = [
                'id_card.numeric' => 'โปรดระบุเลขบัตรประชาชนให้ถูกต้อง',
                'id_card.min' => 'โปรดระบุเลขบัตรประชาชนให้ถูกต้อง',
                'id_card.max' => 'โปรดระบุเลขบัตรประชาชนให้ถูกต้อง',
                'id_card.unique' => 'เลขบัตรประชาชนซ้ำ',
                'username.unique' => 'ชื่อผู้ใช้งานซ้ำ',
                '*.required' => 'โปรดระบุ',
            ];

            if($request->accExpireStatus == "1") {
                $rules['acc_expire'] = 'required';
            }

            if($request->file('pic') != null) {
                $rules['pic'] = 'image|mimes:jpg,png,jpeg,gif,svg';
                $message['pic.*'] = 'การอัพโหลดไฟล์รองรับนามสกุล jpg,png,jpeg,gif,svg เท่านั้น';
            }

            $validator = Validator::make($request->all(), $rules, $message);

            if ($validator->fails()) {
                return response()->json([
                    'error' => 1,
                    'messages' => $validator->messages(),
                ], 200);
            }

            $user = User::create();
            $user->username = $request->username;
            $user->password = Hash::make($request->password);
            $user->name = $request->name;
            $user->surname = $request->surname;
            $user->gender = $request->gender;
            $user->id_card = $request->id_card;
            $user->permission_id = $request->permission_id;
            $user->facebook = $request->facebook;
            $user->line = $request->line;
            $user->email = $request->email;
            $user->status = 1;
            if ($request->file('pic') != null) {
                $extension = $request->file('pic')->guessExtension();
//                $name = 'USER' . Auth::user()->company_id . date('YmdHIs');
                $name = 'USER'. date('YmdHIs');
                $img = Image::make($_FILES['pic']['tmp_name']);
//                $img->fit(100, 100, function ($constraint) {
//                    $constraint->upsize();
//                });
                $img->save(public_path('_uploads/_users/') . $name . '.' . $extension);
                $user->pic = $name . '.' . $extension;
            }
            $user->user_id = Auth::user()->id;
            $user->update_by = Auth::user()->id;
            $user->company_id = 1;
            $user->notifi_price = Carbon::now();
            if($request->accExpireStatus == 1 && $request->acc_expire != null) {
                $user->acc_expire = $request->acc_expire;
            }
            $user->save();

            return response()->json([
                'error' => 0,
                'messages' => "เพิ่มข้อมูลเรียบร้อย",
            ],200);

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'messages' => $e,
            ], 200);
        }
    }

    public function userList(){
        try{
            $data = User::query()
                ->with('permission')
                ->where('company_id', Auth::user()->company_id)
                ->where('permission_id', '<>', '1')
                ->where('permission_id', '<>', '2')
                ->get();
            return response()->json([
                'error' => 0,
                'data' => $data,
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }

    public function userShow($id){
        try{
            $data = User::query()
                ->with('gender')
                ->with('permission')
                ->where('id', $id)
                ->first();
            return response()->json([
                'error' => 0,
                'data' => $data,
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }

    public function userUpdate(Request $request, $id)
    {
        try{
            $rules = [
                'name' => 'required',
                'surname' => 'required',
//                'id_card' => 'required|string|min:13|max:13|unique:100_USERS',
                'gender' => 'required',
                'permission_id' => 'required',
//                'username' => 'required|unique:100_USERS',
//                'password' => 'required',
            ];

            $message = [
//                'id_card.numeric' => 'โปรดระบุเลขบัตรประชาชนให้ถูกต้อง',
//                'id_card.min' => 'โปรดระบุเลขบัตรประชาชนให้ถูกต้อง',
//                'id_card.max' => 'โปรดระบุเลขบัตรประชาชนให้ถูกต้อง',
//                'id_card.unique' => 'เลขบัตรประชาชนซ้ำ',
//                'username.unique' => 'ชื่อผู้ใช้งานซ้ำ',
                '*.required' => 'โปรดระบุ',
            ];

            $id_card = User::query()
                ->where('id', $id)
                ->where('id_card', $request->id_card)
                ->first();
            if($id_card == null) {
                $rules['id_card'] = 'required|string|min:13|max:13|unique:100_USERS';
                $message['id_card.numeric'] = 'โปรดระบุเลขบัตรประชาชนให้ถูกต้อง';
                $message['id_card.min'] = 'โปรดระบุเลขบัตรประชาชนให้ถูกต้อง';
                $message['id_card.max'] = 'โปรดระบุเลขบัตรประชาชนให้ถูกต้อง';
                $message['id_card.unique'] = 'เลขบัตรประชาชนซ้ำ';
            }

            $username = User::query()
                ->where('id', $id)
                ->where('username', $request->username)
                ->first();
            if($username == null) {
                $rules['username'] = 'required|unique:100_USERS';
                $message['username.unique'] = 'ชื่อผู้ใช้งานซ้ำ';
            }

            if($request->accExpireStatus == "1") {
                $rules['acc_expire'] = 'required';
            }

            if($request->file('pic') != null) {
                $rules['pic'] = 'image|mimes:jpg,png,jpeg,gif,svg';
                $message['pic.*'] = 'การอัพโหลดไฟล์รองรับนามสกุล jpg,png,jpeg,gif,svg เท่านั้น';
            }

            $validator = Validator::make($request->all(), $rules, $message);

            if ($validator->fails()) {
                return response()->json([
                    'error' => 1,
                    'messages' => $validator->messages(),
                ], 200);
            }

            $user = User::query()->where('id', $id)->first();
            if($username == null) {
                $user->username = $request->username;
            }
            if($request->password != null) {
                $user->password = Hash::make($request->password);
            }
            $user->name = $request->name;
            $user->surname = $request->surname;
            $user->gender = $request->gender;
            if($id_card == null) {
                $user->id_card = $request->id_card;
            }
            $user->permission_id = $request->permission_id;
            $user->facebook = $request->facebook;
            $user->line = $request->line;
            $user->email = $request->email;
//            $user->status = 1;
            if ($request->file('pic') != null) {
                if($user->pic != null) {
                    @unlink(public_path('/_uploads/_users/'). $user->pic);
                }
                $extension = $request->file('pic')->guessExtension();
//                $name = 'USER' . Auth::user()->company_id . date('YmdHIs');
                $name = 'USER'. date('YmdHIs');
                $img = Image::make($_FILES['pic']['tmp_name']);
//                $img->fit(100, 100, function ($constraint) {
//                    $constraint->upsize();
//                });
                $img->save(public_path('_uploads/_users/') . $name . '.' . $extension);
                $user->pic = $name . '.' . $extension;
            }
//            $user->user_id = 1;
            $user->update_by = Auth::user()->id;
//            $user->company_id = 1;
            $user->notifi_price = Carbon::now();
            if($request->accExpireStatus == 1 && $request->acc_expire != null) {
                $user->acc_expire = $request->acc_expire;
            }
            else {
                $user->acc_expire = null;
            }
            $user->save();

            return response()->json([
                'error' => 0,
                'messages' => "เพิ่มข้อมูลเรียบร้อย",
            ],200);

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'messages' => $e,
            ], 200);
        }
    }

    public function userSuspend($id)
    {
        try {
            $user = User::query()->where('id', $id)->first();
            $user->status = 0;
            $user->save();

            return response()->json([
                'error' => 0,
                'message' => "ระงับบัญชีพนักงานเรียบร้อย",
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'messages' => $e,
            ], '200');
        }
    }

    public function userReSuspend($id)
    {
        try {
            $user = User::query()->where('id', $id)->first();
            $user->status = 1;
            $user->save();

            return response()->json([
                'error' => 0,
                'message' => "ระงับบัญชีพนักงานเรียบร้อย",
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'messages' => $e,
            ], '200');
        }
    }

    public function companyInfo(){
        try{
            $data = Company::query()->first();

            return response()->json([
                'error' => 0,
                'data' => $data,
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }

    public function settingsUpdate(Request $request)
    {
        try{
            $rules = [
                'company_name' => 'required',
                'company_tax' => 'required',
                'telephone' => 'required',
                'address' => 'required',
                'province_id' => 'required',
                'district_id' => 'required',
                'subdistrict_id' => 'required',
                'subdistrict_id' => 'required',
                'zipcode' => 'required',
            ];

            $message = [
                '*.required' => 'โปรดระบุ',
            ];

            if($request->file('pic') != null) {
                $rules['pic'] = 'image|mimes:jpg,png,jpeg,gif,svg|max:2048';
                $message['pic.*'] = 'รองรับไฟล์นามสกุล JPG, JPEG, PNG, GIF ขนาดไม่เกิน 2MB เท่านั้น';
            }

            $validator = Validator::make($request->all(), $rules, $message);

            if ($validator->fails()) {
                return response()->json([
                    'error' => 1,
                    'messages' => $validator->messages(),
                ], 200);
            }

            if($request->company_id == null) {
                $company = Company::create();
                $company->company_name = $request->company_name;
                $company->company_tax = $request->company_tax;
                $company->address = $request->address;
                $company->province_id = $request->province_id;
                $company->district_id = $request->district_id;
                $company->subdistrict_id = $request->subdistrict_id;
                $company->zipcode = $request->zipcode;
                $company->telephone = $request->telephone;
                $company->email = $request->email;
                $company->website = $request->website;
                $company->facebook = $request->facebook;
                $company->line = $request->line;

                if($request->file('pic') != null) {
                    $extension = $request->file('pic')->guessExtension();
                    $name = 'COMPANY'. date('YmdHIs');
                    $img = Image::make($_FILES['pic']['tmp_name']);
//                    $img->fit(100, 100, function ($constraint) {
//                        $constraint->upsize();
//                    });
                    $img->save(public_path('_uploads/_companys/') . $name . '.' . $extension);
                    $company->pic = $name . '.' . $extension;
                }
                $company->save();

                return response()->json([
                    'error' => 0,
                    'messages' => "บันทึกข้อมูลเรียบร้อย",
                ],200);
            }
            else {
                $company = Company::query()->where('id', $request->company_id)->first();
                $company->company_name = $request->company_name;
                $company->company_tax = $request->company_tax;
                $company->address = $request->address;
                $company->province_id = $request->province_id;
                $company->district_id = $request->district_id;
                $company->subdistrict_id = $request->subdistrict_id;
                $company->zipcode = $request->zipcode;
                $company->telephone = $request->telephone;
                $company->email = $request->email;
                $company->website = $request->website;
                $company->facebook = $request->facebook;
                $company->line = $request->line;

                if ($request->file('pic') != null) {
                    if($company->pic != null) {
                        @unlink(public_path('_uploads/_companys/'). $company->pic);
                    }
                    $extension = $request->file('pic')->guessExtension();
                    $name = 'COMPANY'. date('YmdHIs');
                    $img = Image::make($_FILES['pic']['tmp_name']);
//                    $img->fit(100, 100, function ($constraint) {
//                        $constraint->upsize();
//                    });
                    $img->save(public_path('_uploads/_companys/') . $name . '.' . $extension);
                    $company->pic = $name . '.' . $extension;
                }
                $company->save();

                return response()->json([
                    'error' => 0,
                    'messages' => "บันทึกข้อมูลเรียบร้อย",
                ],200);
            }

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'messages' => $e,
            ], 200);
        }
    }
}
