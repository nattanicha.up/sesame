<?php

namespace App\Http\Controllers;

use App\Models\Customers;
use App\Models\Gold\Gold;
use App\Models\Gold\GoldSubCategory;
use App\Models\Gold\GoldType;
use App\Models\Gold\GoldUnits;
use App\Models\Gold\TransectionStock;
use App\Models\GoldPercent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class ReceiveBuyController extends Controller
{
    public function index()
    {
        $pageConfigs = [
            'pageClass' => 'ecommerce-application',
        ];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['name' => "Checkout"]
        ];

        $dummy = Customers::query()->whereDummy(1)->first();

        return view('/content/03_buy_and_sale/buyreceive', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs,
            'dummy' => $dummy
        ]);
//        return view('/content/03_buy_and_sale/buyreceive');
    }

    public function buyinvoice()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/content/03_buy_and_sale/buy-invoice', ['pageConfigs' => $pageConfigs]);
    }

    public function goldStore(Request $request)
    {
        try{
            if($request->gold_category == 1) {
                if($request->gold_type == 1) {
                    $rules = [
                        'gold_pattern' => 'required',
//                        'gold_sku' => 'required',
                        'gold_weight' => 'required',
                        'gold_weight_actual' => 'required',
                        'gold_cost' => 'required',
//                        'gold_smith_charge' => 'required',
                        'gold_size' => 'required',
                    ];

                    $message = [
                        '*.required' => 'โปรดระบุ',
                    ];

                    if($request->file('gold_image') != null) {
                        $rules['gold_image'] = 'image|mimes:jpg,png,jpeg,gif,svg';
                        $message['gold_image.*'] = 'การอัพโหลดไฟล์รองรับนามสกุล jpg,png,jpeg,gif,svg เท่านั้น';
                    }

                    $validator = Validator::make($request->all(), $rules, $message);

                    if ($validator->fails()) {
                        return response()->json([
                            'error' => 1,
                            'messages' => $validator->messages(),
                        ], 200);
                    }

                    return response()->json([
                        'error' => 0,
                        'data' => $request->all(),
                        'gold_sku' => $this->generate(),
                        'gold_type' => GoldType::query()->select('title')->whereId($request->gold_type)->first(),
                        'gold_percent' => GoldPercent::query()->select('title')->whereId($request->gold_category)->first(),
                        'gold_category' => GoldSubCategory::query()->select('title')->whereId($request->gold_sub_category)->first(),
                        'gold_units' => GoldUnits::query()->whereId($request->gold_unit)->select('title')->first(),
                        'messages' => "เพิ่มข้อมูลเรียบร้อย",
                    ],200);
                }
                else if($request->gold_type == 2) {
                    $rules = [
                        'gold_pattern' => 'required',
//                        'gold_sku' => 'required',
                        'gold_weight' => 'required',
                        'gold_weight_actual' => 'required',
                        'gold_cost' => 'required',
                    ];

                    $message = [
                        '*.required' => 'โปรดระบุ',
                    ];

                    if($request->file('gold_image') != null) {
                        $rules['gold_image'] = 'image|mimes:jpg,png,jpeg,gif,svg';
                        $message['gold_image.*'] = 'การอัพโหลดไฟล์รองรับนามสกุล jpg,png,jpeg,gif,svg เท่านั้น';
                    }

                    $validator = Validator::make($request->all(), $rules, $message);

                    if ($validator->fails()) {
                        return response()->json([
                            'error' => 1,
                            'messages' => $validator->messages(),
                        ], 200);
                    }

                    return response()->json([
                        'error' => 0,
                        'data' => $request->all(),
                        'gold_sku' => $this->generate(),
                        'gold_type' => GoldType::query()->select('title')->whereId($request->gold_type)->first(),
                        'gold_percent' => GoldPercent::query()->select('title')->whereId($request->gold_category)->first(),
                        'gold_category' => null,
                        'gold_units' => GoldUnits::query()->whereId($request->gold_unit)->select('title')->first(),
                        'messages' => "เพิ่มข้อมูลเรียบร้อย",
                    ],200);
                }
            }
            else if($request->gold_category == 2) {
                if($request->gold_type == 1) {
                    $rules = [
                        'gold_pattern' => 'required',
//                        'gold_sku' => 'required',
//                        'gold_weight' => 'required',
                        'gold_weight_actual' => 'required',
                        'gold_cost' => 'required',
//                        'gold_smith_charge' => 'required',
                        'gold_size' => 'required',
                    ];

                    $message = [
                        '*.required' => 'โปรดระบุ',
                    ];

                    if($request->file('gold_image') != null) {
                        $rules['gold_image'] = 'image|mimes:jpg,png,jpeg,gif,svg';
                        $message['gold_image.*'] = 'การอัพโหลดไฟล์รองรับนามสกุล jpg,png,jpeg,gif,svg เท่านั้น';
                    }

                    $validator = Validator::make($request->all(), $rules, $message);

                    if ($validator->fails()) {
                        return response()->json([
                            'error' => 1,
                            'messages' => $validator->messages(),
                        ], 200);
                    }

                    return response()->json([
                        'error' => 0,
                        'data' => $request->all(),
                        'gold_sku' => $this->generate(),
                        'gold_type' => GoldType::query()->select('title')->whereId($request->gold_type)->first(),
                        'gold_percent' => GoldPercent::query()->select('title')->whereId($request->gold_category)->first(),
                        'gold_category' => GoldSubCategory::query()->select('title')->whereId($request->gold_sub_category)->first(),
                        'gold_units' => null,
                        'messages' => "เพิ่มข้อมูลเรียบร้อย",
                    ],200);
                }
                else if($request->gold_type == 2) {
                    $rules = [
                        'gold_pattern' => 'required',
//                        'gold_sku' => 'required',
//                        'gold_weight' => 'required',
                        'gold_weight_actual' => 'required',
                        'gold_cost' => 'required',
                    ];

                    $message = [
                        '*.required' => 'โปรดระบุ',
                    ];

                    if($request->file('gold_image') != null) {
                        $rules['gold_image'] = 'image|mimes:jpg,png,jpeg,gif,svg';
                        $message['gold_image.*'] = 'การอัพโหลดไฟล์รองรับนามสกุล jpg,png,jpeg,gif,svg เท่านั้น';
                    }

                    $validator = Validator::make($request->all(), $rules, $message);

                    if ($validator->fails()) {
                        return response()->json([
                            'error' => 1,
                            'messages' => $validator->messages(),
                        ], 200);
                    }

                    return response()->json([
                        'error' => 0,
                        'data' => $request->all(),
                        'gold_sku' => $this->generate(),
                        'gold_type' => GoldType::query()->select('title')->whereId($request->gold_type)->first(),
                        'gold_percent' => GoldPercent::query()->select('title')->whereId($request->gold_category)->first(),
                        'gold_category' => null,
                        'gold_units' => null,
                        'messages' => "เพิ่มข้อมูลเรียบร้อย",
                    ],200);
                }
            }
            else if($request->gold_category == 3) {
                if($request->gold_type == 1) {
                    $rules = [
                        'gold_pattern' => 'required',
//                        'gold_sku' => 'required',
//                        'gold_weight' => 'required',
                        'gold_weight_actual' => 'required',
                        'gold_cost' => 'required',
//                        'gold_smith_charge' => 'required',
                        'gold_size' => 'required',
                    ];

                    $message = [
                        '*.required' => 'โปรดระบุ',
                    ];

                    if($request->file('gold_image') != null) {
                        $rules['gold_image'] = 'image|mimes:jpg,png,jpeg,gif,svg';
                        $message['gold_image.*'] = 'การอัพโหลดไฟล์รองรับนามสกุล jpg,png,jpeg,gif,svg เท่านั้น';
                    }

                    $validator = Validator::make($request->all(), $rules, $message);

                    if ($validator->fails()) {
                        return response()->json([
                            'error' => 1,
                            'messages' => $validator->messages(),
                        ], 200);
                    }

                    return response()->json([
                        'error' => 0,
                        'data' => $request->all(),
                        'gold_sku' => $this->generate(),
                        'gold_type' => GoldType::query()->select('title')->whereId($request->gold_type)->first(),
                        'gold_percent' => GoldPercent::query()->select('title')->whereId($request->gold_category)->first(),
                        'gold_category' => GoldSubCategory::query()->select('title')->whereId($request->gold_sub_category)->first(),
                        'gold_units' => null,
                        'messages' => "เพิ่มข้อมูลเรียบร้อย",
                    ],200);
                }
                else if($request->gold_type == 2) {
                    $rules = [
                        'gold_pattern' => 'required',
//                        'gold_sku' => 'required',
//                        'gold_weight' => 'required',
                        'gold_weight_actual' => 'required',
                        'gold_cost' => 'required',
                    ];

                    $message = [
                        '*.required' => 'โปรดระบุ',
                    ];

                    if($request->file('gold_image') != null) {
                        $rules['gold_image'] = 'image|mimes:jpg,png,jpeg,gif,svg';
                        $message['gold_image.*'] = 'การอัพโหลดไฟล์รองรับนามสกุล jpg,png,jpeg,gif,svg เท่านั้น';
                    }

                    $validator = Validator::make($request->all(), $rules, $message);

                    if ($validator->fails()) {
                        return response()->json([
                            'error' => 1,
                            'messages' => $validator->messages(),
                        ], 200);
                    }

                    return response()->json([
                        'error' => 0,
                        'data' => $request->all(),
                        'gold_sku' => $this->generate(),
                        'gold_type' => GoldType::query()->select('title')->whereId($request->gold_type)->first(),
                        'gold_percent' => GoldPercent::query()->select('title')->whereId($request->gold_category)->first(),
                        'gold_category' => null,
                        'gold_units' => null,
                        'messages' => "เพิ่มข้อมูลเรียบร้อย",
                    ],200);
                }
            }
            else if($request->gold_category == 4) {
                $rules = [
                    'gold_pattern' => 'required',
//                        'gold_sku' => 'required',
//                        'gold_weight' => 'required',
                    'gold_weight_actual' => 'required',
                    'gold_cost' => 'required',
//                        'gold_smith_charge' => 'required',
                    'gold_size' => 'required',
                ];

                $message = [
                    '*.required' => 'โปรดระบุ',
                ];

                if($request->file('gold_image') != null) {
                    $rules['gold_image'] = 'image|mimes:jpg,png,jpeg,gif,svg';
                    $message['gold_image.*'] = 'การอัพโหลดไฟล์รองรับนามสกุล jpg,png,jpeg,gif,svg เท่านั้น';
                }

                $validator = Validator::make($request->all(), $rules, $message);

                if ($validator->fails()) {
                    return response()->json([
                        'error' => 1,
                        'messages' => $validator->messages(),
                    ], 200);
                }

                return response()->json([
                    'error' => 0,
                    'data' => $request->all(),
                    'gold_sku' => $this->generate(),
                    'gold_type' => null,
                    'gold_percent' => GoldPercent::query()->select('title')->whereId($request->gold_category)->first(),
                    'gold_category' => GoldSubCategory::query()->select('title')->whereId($request->gold_sub_category)->first(),
                    'gold_units' => null,
                    'messages' => "เพิ่มข้อมูลเรียบร้อย",
                ],200);
            }

            return response()->json([
                'error' => 0,
                'messages' => "เพิ่มข้อมูลเรียบร้อย",
            ],200);

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'messages' => $e,
            ], 200);
        }
    }

    public function generate(){
//        try{
            $generate = true;

            while($generate) {
                $barcode_number = 'G' . Auth::user()->company_id . date('ymdHis'). $this->randomBarcode(11);

                $barcode = TransectionStock::query()->where('sku',$barcode_number)->count();

                if ($barcode == 0){
                    $generate = false;
                }
            }

            return $barcode_number;

//            return response()->json([
//                'error' => 0,
//                'barcode_number' => $barcode_number
//            ], 200);
//
//        }catch (\Throwable $e) {
//            return response()->json([
//                'error' => 1,
//                'messages' => $e
//            ],200);
//        }
    }

    public function randomBarcode($num){
        srand((double)microtime()*10000000);
        $chars = "0123456789";
        $ret_str = "";
        //$num = strlen($chars);
        for($i = 0; $i < 2; $i++)
        {
            $ret_str.= $chars[rand()%$num];
            $ret_str.="";
        }
        return $ret_str;
    }

    public function buyGoldStore(Request $request)
    {
        $messages = [];
        $request->total_price ? '' : $messages['product'] = ['โปรดเพิ่มข้อมูลทอง'];
        $request->buyer_id ? '' : $messages['buyer'] = ['โปรดเพิ่มข้อมูลผู้ขาย'];
        if($messages) {
            return response()->json([
                'error' => 1,
                'messages' => $messages,
            ], 200);
        }

        try{
            return response()->json([
                'error' => 0,
                'messages' => "เพิ่มข้อมูลเรียบร้อย",
            ],200);

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'messages' => $e,
            ], 200);
        }
    }
}
