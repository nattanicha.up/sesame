<?php

namespace App\Models\Gold;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransectionStock extends Model
{
    use HasFactory;

    protected $table = '430_TRANSECTION_STOCK';
    protected $fillable = [
        'title',
        'gold_id',
        'sku',
        'price_income',
        'price_goldsmith',
        'status',
        'transection_id',
        'transection_price',
        'transection_smith',
        'user_id',
        'update_by',
        'company_id',
    ];
}
