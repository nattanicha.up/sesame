<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Title extends Model
{
    use HasFactory;

    protected $table = '104_USER_TITLE';
    protected $fillable = [
        'title_th',
        'title_en',
    ];
}
