<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    use HasFactory;

    protected $table = '002_PROVINCE';
    protected $fillable = [
        'code',
        'title_th',
        'title_en',
        'geo_id',
    ];
}
