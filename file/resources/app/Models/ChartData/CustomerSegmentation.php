<?php

namespace App\Models\ChartData;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerSegmentation extends Model
{
    use HasFactory;

    protected $table = '812_CustomerSegmentation';
    protected $fillable = [
        'CustomerID',
        'Segment',
    ];
}
