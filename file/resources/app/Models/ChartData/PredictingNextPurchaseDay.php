<?php

namespace App\Models\ChartData;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PredictingNextPurchaseDay extends Model
{
    use HasFactory;

    protected $table = '813_PredictingNextPurchaseDay';
    protected $fillable = [
        'CustomerID',
        'NextPurchaseDay',
        'Recency',
        'RecencyCluster',
        'Frequency',
        'FrequencyCluster',
        'Revenue',
        'RevenueCluster',
        'OverallScore',
        'DayDiff',
        'DayDiff2',
        'DayDiff3',
        'DayDiffMean',
        'DayDiffStd',
        'Segment_High-Value',
        'Segment_Low-Value',
        'Segment_Mid-Value',
        'NextPurchaseDayRange',
        'Cluster'
    ];
}
