<?php

namespace App\Models\ChartData;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PredictingSales extends Model
{
    use HasFactory;

    protected $table = '814_PredictingSales';
    protected $fillable = [
        'InvoiceDate',
        'UnitPrice',
        'Predicted',
    ];
}
