<?php

namespace App\Models\ChartData;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MonthlyTotalNumberOfOrder extends Model
{
    use HasFactory;

    protected $table = '804_MonthlyTotalNumberOfOrder';
    protected $fillable = [
        'InvoiceDate',
        'net',
        'pawn',
        'collect',
        'sell',
        'buy',
    ];
}
