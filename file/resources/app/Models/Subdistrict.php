<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subdistrict extends Model
{
    use HasFactory;

    protected $table = '004_SUB_DISTRICT';
    protected $fillable = [
        'code',
        'title_th',
        'title_en',
        'district_id',
        'province_id',
        'geo_id',
    ];

    public function districtCode(){
        return $this->hasOne(Zipcode::class,'district_code','code');
    }
}
