@extends('layouts/contentLayoutMasterMain')

@section('title', 'ภาพรวมแคมเปญ')

@section('vendor-style')
    {{-- vendor css files --}}
@endsection

@section('page-style')
    {{-- Page Css files --}}
@endsection

@section('content')
    <style>
        #conversionRatePieOverview {
            width: 100%;
            height: 300px;
            font-size: 10px;
        }
        #conversionRateLineOverview {
            width: 100%;
            height: 300px;
            font-size: 10px;
        }
        #conversionRatePieEmail {
            width: 100%;
            height: 300px;
            font-size: 10px;
        }
        #conversionRateLineEmail {
            width: 100%;
            height: 300px;
            font-size: 10px;
        }
        #conversionRatePieSMS {
            width: 100%;
            height: 300px;
            font-size: 10px;
        }
        #conversionRateLineSMS {
            width: 100%;
            height: 300px;
            font-size: 10px;
        }
        #conversionRatePieLine {
            width: 100%;
            height: 300px;
            font-size: 10px;
        }
        #conversionRateLineLine {
            width: 100%;
            height: 300px;
            font-size: 10px;
        }
    </style>
    <!-- Statistics card section -->
    <section id="statistics-card">
        <!-- Miscellaneous Charts -->
        <div class="row">
            <div class="col-lg-4 col-sm-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h2 class="font-weight-bolder mb-0">125</h2>
                            <p class="card-text">All Customers</p>
                        </div>
                        <div class="avatar bg-light-primary p-50 m-0">
                            <div class="avatar-content">
                                <i data-feather="users" class="font-medium-5"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h2 class="font-weight-bolder mb-0">7</h2>
                            <p class="card-text">All Campaigns</p>
                        </div>
                        <div class="avatar bg-light-primary p-50 m-0">
                            <div class="avatar-content">
                                <i data-feather="send" class="font-medium-5"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h2 class="font-weight-bolder mb-0">9.52%</h2>
                            <p class="card-text">Average Conversion Rate</p>
                        </div>
                        <div class="avatar bg-light-primary p-50 m-0">
                            <div class="avatar-content">
                                <i data-feather="pie-chart" class="font-medium-5"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-12">
                <!-- Basic and Outline Pills start -->
                <section id="basic-and-outline-pills">
                    <div class="row match-height">
                        <!-- Basic pills starts -->
                        <div class="col-xl-12 col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <ul class="nav nav-pills mb-0">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="home-tab" data-toggle="pill" href="#overvive" aria-expanded="true">Overview</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="profile-tab" data-toggle="pill" href="#email" aria-expanded="false">Email</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="profile-tab" data-toggle="pill" href="#sms" aria-expanded="false">SMS</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="about-tab" data-toggle="pill" href="#line" aria-expanded="false">Line</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- Basic pills ends -->
                    </div>
                </section>
                <!-- Basic and Outline Pills end -->
            </div>

            <div class="col-lg-12 col-12">
                <div class="card-body pt-0 pl-0 pr-0" >
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="overvive" aria-labelledby="home-tab" aria-expanded="true">
                            <div class="row">
                                <div class="col-lg-6 col-12">
                                    <div class="card" style="height: 500px;">
                                        <div class="card-header" style="border-bottom: #eeeeee 1px solid;">
                                            <div class="card-title d-flex align-items-center">
                                                <i data-feather='pie-chart' class="font-medium-5" style="color:#C1C1CB;"></i>
                                                <span style="color:#C1C1CB;margin-left:5px;"> Conversion Rate</span>
                                            </div>
                                        </div>
                                        <div class="card-body" >
                                            <div class="row">
                                                <div class="mt-4" id="conversionRatePieOverview"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div class="card" style="height: 500px;">
                                        <div class="card-header" style="border-bottom: #eeeeee 1px solid;">
                                            <div class="card-title d-flex align-items-center">
                                                <i data-feather='pie-chart' class="font-medium-5" style="color:#C1C1CB;"></i>
                                                <span style="color:#C1C1CB;margin-left:5px;"> Conversion Rate</span>
                                            </div>
                                        </div>
                                        <div class="card-body" >
                                            <div class="row">
                                                <div class="mt-4" id="conversionRateLineOverview"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="email" role="tabpanel" aria-labelledby="profile-tab" aria-expanded="false">
                            <div class="row">
                                <div class="col-lg-6 col-12">
                                    <div class="card" style="height: 500px;">
                                        <div class="card-header" style="border-bottom: #eeeeee 1px solid;">
                                            <div class="card-title d-flex align-items-center">
                                                <i data-feather='pie-chart' class="font-medium-5" style="color:#C1C1CB;"></i>
                                                <span style="color:#C1C1CB;margin-left:5px;"> Conversion Rate</span>
                                            </div>
                                        </div>
                                        <div class="card-body" >
                                            <div class="row">
                                                <div class="mt-4" id="conversionRatePieEmail"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div class="card" style="height: 500px;">
                                        <div class="card-header" style="border-bottom: #eeeeee 1px solid;">
                                            <div class="card-title d-flex align-items-center">
                                                <i data-feather='pie-chart' class="font-medium-5" style="color:#C1C1CB;"></i>
                                                <span style="color:#C1C1CB;margin-left:5px;"> Conversion Rate</span>
                                            </div>
                                        </div>
                                        <div class="card-body" >
                                            <div class="row">
                                                <div class="mt-4" id="conversionRateLineEmail"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="sms" role="tabpanel" aria-labelledby="dropdown1-tab" aria-expanded="false">
                            <div class="row">
                                <div class="col-lg-6 col-12">
                                    <div class="card" style="height: 500px;">
                                        <div class="card-header" style="border-bottom: #eeeeee 1px solid;">
                                            <div class="card-title d-flex align-items-center">
                                                <i data-feather='pie-chart' class="font-medium-5" style="color:#C1C1CB;"></i>
                                                <span style="color:#C1C1CB;margin-left:5px;"> Conversion Rate</span>
                                            </div>
                                        </div>
                                        <div class="card-body" >
                                            <div class="row">
                                                <div class="mt-4" id="conversionRatePieSMS"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div class="card" style="height: 500px;">
                                        <div class="card-header" style="border-bottom: #eeeeee 1px solid;">
                                            <div class="card-title d-flex align-items-center">
                                                <i data-feather='pie-chart' class="font-medium-5" style="color:#C1C1CB;"></i>
                                                <span style="color:#C1C1CB;margin-left:5px;"> Conversion Rate</span>
                                            </div>
                                        </div>
                                        <div class="card-body" >
                                            <div class="row">
                                                <div class="mt-4" id="conversionRateLineSMS"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="line" role="tabpanel" aria-labelledby="dropdown2-tab" aria-expanded="false">
                            <div class="row">
                                <div class="col-lg-6 col-12">
                                    <div class="card" style="height: 500px;">
                                        <div class="card-header" style="border-bottom: #eeeeee 1px solid;">
                                            <div class="card-title d-flex align-items-center">
                                                <i data-feather='pie-chart' class="font-medium-5" style="color:#C1C1CB;"></i>
                                                <span style="color:#C1C1CB;margin-left:5px;"> Conversion Rate</span>
                                            </div>
                                        </div>
                                        <div class="card-body" >
                                            <div class="row">
                                                <div class="mt-4" id="conversionRatePieLine"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div class="card" style="height: 500px;">
                                        <div class="card-header" style="border-bottom: #eeeeee 1px solid;">
                                            <div class="card-title d-flex align-items-center">
                                                <i data-feather='pie-chart' class="font-medium-5" style="color:#C1C1CB;"></i>
                                                <span style="color:#C1C1CB;margin-left:5px;"> Conversion Rate</span>
                                            </div>
                                        </div>
                                        <div class="card-body" >
                                            <div class="row">
                                                <div class="mt-4" id="conversionRateLineLine"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ Statistics Card section-->
@endsection

@section('vendor-script')
    {{-- vendor files --}}
@endsection

@section('page-script')
    {{-- Page js files --}}
    <script type='text/javascript' src='https://cdn.amcharts.com/lib/4/core.js?ver=20210901-5e9' id='-lib-4-core-js-js'></script>
    <script type='text/javascript' src='https://cdn.amcharts.com/lib/4/charts.js?ver=20210901-5e9' id='-lib-4-charts-js-js'></script>
    <script type='text/javascript' src='https://cdn.amcharts.com/lib/4/themes/animated.js?ver=20210901-5e9' id='-lib-4-themes-animated-js-js'></script>

    {{-- Page js cumtoms --}}
    <script>
        $(document).ready(function(){
            conversionRatePieOverview();
            conversionRateLineOverview();
            conversionRatePieEmail();
            conversionRateLineEmail();
            conversionRatePieSMS();
            conversionRateLineSMS();
            conversionRatePieLine();
            conversionRateLineLine();
        });

        function conversionRatePieOverview() {
            try {
                // Themes begin
                // am4core.useTheme(am4themes_kelly);
                am4core.useTheme(am4themes_animated);
                // Themes end
                /**
                 * Define data for each year
                 */

                // Create chart instance
                var chart = am4core.create("conversionRatePieOverview", am4charts.PieChart);
                chart.logo.disabled = true;
                // Add data
                chart.data = [
                    { "sector": "Email", "size": 50.0 },
                    { "sector": "SMS", "size": 30.0 },
                    { "sector": "Line", "size": 20.0 },

                ];

                // Add label
                chart.innerRadius = 60;

                // Add and configure Series
                var pieSeries = chart.series.push(new am4charts.PieSeries());
                pieSeries.dataFields.value = "size";
                pieSeries.dataFields.category = "sector";

                // Animate chart data
            }
            catch( e ) {
                console.log( e );
            }
        }

        function conversionRateLineOverview() {
            try {
                // Themes begin
                // Using default theme
                am4core.useTheme(am4themes_animated);
                // Themes end

                // Create chart instance
                var chart = am4core.create("conversionRateLineOverview", am4charts.XYChart);
                chart.logo.disabled = true;

                // Add data
                //chart.data = generatechartData();
                var chartData = [
                    {
                        date: new Date("2015-01-31"),
                        visits: 414662444,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-02-28"),
                        visits: 372150922,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-03-31"),
                        visits: 420382942,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-04-30"),
                        visits: 397943194,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-05-31"),
                        visits: 417188449,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-06-30"),
                        visits: 403219253,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-07-31"),
                        visits: 416871320,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-08-31"),
                        visits: 416298557,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-09-30"),
                        visits: 412549794,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-10-31"),
                        visits: 412726526,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-11-30"),
                        visits: 393763393,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-12-31"),
                        visits: 400372379,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-12-31"),
                        visits: 32787.788837713,
                        lineColor: "#c767dc",
                        fillColor: "#c767dc"
                    },
                    {
                        date: new Date("2016-01-31"),
                        visits: 32854.190410423,
                        lineColor: "#c767dc",
                        fillColor: "#c767dc"
                    },
                    {
                        date: new Date("2016-02-29"),
                        visits: 32912.496118312,
                        lineColor: "#c767dc",
                        fillColor: "#c767dc"
                    },
                    {
                        date: new Date("2016-03-31"),
                        visits: 32895.687888724,
                        lineColor: "#c767dc",
                        fillColor: "#c767dc"
                    },
                    {
                        date: new Date("2016-04-30"),
                        visits: 32843.845069298,
                        lineColor: "#c767dc",
                        fillColor: "#c767dc"
                    }
                ];

                chart.data = chartData;

                // Create axes
                var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
                dateAxis.startLocation = 0.5;
                dateAxis.endLocation = 0.5;

                // Create value axis
                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

                // Create series
                var series = chart.series.push(new am4charts.LineSeries());
                series.dataFields.valueY = "visits";
                series.dataFields.dateX = "date";
                series.strokeWidth = 3;
                series.tooltipText = "{valueY.value}";
                series.fillOpacity = 0.1;
                series.propertyFields.stroke = "lineColor";
                series.propertyFields.fill = "fillColor";

                // Add cursor
                chart.cursor = new am4charts.XYCursor();
                chart.cursor.xAxis = dateAxis;
                chart.scrollbarX = new am4core.Scrollbar();

                series.tooltip.getFillFromObject = false;
                series.tooltip.adapter.add("x", (x, target)=>{
                    if(series.tooltip.tooltipDataItem.valueY < 0){
                        series.tooltip.background.fill = chart.colors.getIndex(4);
                    }
                    else{
                        series.tooltip.background.fill = chart.colors.getIndex(0);
                    }
                    return x;
                })
            }
            catch( e ) {
                console.log( e );
            }
        }

        function conversionRatePieEmail() {
            try {
                // Themes begin
                // am4core.useTheme(am4themes_kelly);
                am4core.useTheme(am4themes_animated);
                // Themes end
                /**
                 * Define data for each year
                 */

                    // Create chart instance
                var chart = am4core.create("conversionRatePieEmail", am4charts.PieChart);
                chart.logo.disabled = true;
                // Add data
                chart.data = [
                    { "sector": "Email", "size": 50.0 },
                    { "sector": "SMS", "size": 30.0 },
                    { "sector": "Line", "size": 20.0 },

                ];

                // Add label
                chart.innerRadius = 60;

                // Add and configure Series
                var pieSeries = chart.series.push(new am4charts.PieSeries());
                pieSeries.dataFields.value = "size";
                pieSeries.dataFields.category = "sector";

                // Animate chart data
            }
            catch( e ) {
                console.log( e );
            }
        }

        function conversionRateLineEmail() {
            try {
                // Themes begin
                // Using default theme
                am4core.useTheme(am4themes_animated);
                // Themes end

                // Create chart instance
                var chart = am4core.create("conversionRateLineEmail", am4charts.XYChart);
                chart.logo.disabled = true;

                // Add data
                //chart.data = generatechartData();
                var chartData = [
                    {
                        date: new Date("2015-01-31"),
                        visits: 414662444,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-02-28"),
                        visits: 372150922,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-03-31"),
                        visits: 420382942,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-04-30"),
                        visits: 397943194,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-05-31"),
                        visits: 417188449,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-06-30"),
                        visits: 403219253,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-07-31"),
                        visits: 416871320,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-08-31"),
                        visits: 416298557,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-09-30"),
                        visits: 412549794,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-10-31"),
                        visits: 412726526,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-11-30"),
                        visits: 393763393,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-12-31"),
                        visits: 400372379,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-12-31"),
                        visits: 32787.788837713,
                        lineColor: "#c767dc",
                        fillColor: "#c767dc"
                    },
                    {
                        date: new Date("2016-01-31"),
                        visits: 32854.190410423,
                        lineColor: "#c767dc",
                        fillColor: "#c767dc"
                    },
                    {
                        date: new Date("2016-02-29"),
                        visits: 32912.496118312,
                        lineColor: "#c767dc",
                        fillColor: "#c767dc"
                    },
                    {
                        date: new Date("2016-03-31"),
                        visits: 32895.687888724,
                        lineColor: "#c767dc",
                        fillColor: "#c767dc"
                    },
                    {
                        date: new Date("2016-04-30"),
                        visits: 32843.845069298,
                        lineColor: "#c767dc",
                        fillColor: "#c767dc"
                    }
                ];

                chart.data = chartData;

                // Create axes
                var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
                dateAxis.startLocation = 0.5;
                dateAxis.endLocation = 0.5;

                // Create value axis
                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

                // Create series
                var series = chart.series.push(new am4charts.LineSeries());
                series.dataFields.valueY = "visits";
                series.dataFields.dateX = "date";
                series.strokeWidth = 3;
                series.tooltipText = "{valueY.value}";
                series.fillOpacity = 0.1;
                series.propertyFields.stroke = "lineColor";
                series.propertyFields.fill = "fillColor";

                // Add cursor
                chart.cursor = new am4charts.XYCursor();
                chart.cursor.xAxis = dateAxis;
                chart.scrollbarX = new am4core.Scrollbar();

                series.tooltip.getFillFromObject = false;
                series.tooltip.adapter.add("x", (x, target)=>{
                    if(series.tooltip.tooltipDataItem.valueY < 0){
                        series.tooltip.background.fill = chart.colors.getIndex(4);
                    }
                    else{
                        series.tooltip.background.fill = chart.colors.getIndex(0);
                    }
                    return x;
                })
            }
            catch( e ) {
                console.log( e );
            }
        }

        function conversionRatePieSMS() {
            try {
                // Themes begin
                // am4core.useTheme(am4themes_kelly);
                am4core.useTheme(am4themes_animated);
                // Themes end
                /**
                 * Define data for each year
                 */

                    // Create chart instance
                var chart = am4core.create("conversionRatePieSMS", am4charts.PieChart);
                chart.logo.disabled = true;
                // Add data
                chart.data = [
                    { "sector": "Email", "size": 50.0 },
                    { "sector": "SMS", "size": 30.0 },
                    { "sector": "Line", "size": 20.0 },

                ];

                // Add label
                chart.innerRadius = 60;

                // Add and configure Series
                var pieSeries = chart.series.push(new am4charts.PieSeries());
                pieSeries.dataFields.value = "size";
                pieSeries.dataFields.category = "sector";

                // Animate chart data
            }
            catch( e ) {
                console.log( e );
            }
        }

        function conversionRateLineSMS() {
            try {
                // Themes begin
                // Using default theme
                am4core.useTheme(am4themes_animated);
                // Themes end

                // Create chart instance
                var chart = am4core.create("conversionRateLineSMS", am4charts.XYChart);
                chart.logo.disabled = true;

                // Add data
                //chart.data = generatechartData();
                var chartData = [
                    {
                        date: new Date("2015-01-31"),
                        visits: 414662444,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-02-28"),
                        visits: 372150922,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-03-31"),
                        visits: 420382942,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-04-30"),
                        visits: 397943194,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-05-31"),
                        visits: 417188449,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-06-30"),
                        visits: 403219253,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-07-31"),
                        visits: 416871320,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-08-31"),
                        visits: 416298557,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-09-30"),
                        visits: 412549794,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-10-31"),
                        visits: 412726526,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-11-30"),
                        visits: 393763393,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-12-31"),
                        visits: 400372379,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-12-31"),
                        visits: 32787.788837713,
                        lineColor: "#c767dc",
                        fillColor: "#c767dc"
                    },
                    {
                        date: new Date("2016-01-31"),
                        visits: 32854.190410423,
                        lineColor: "#c767dc",
                        fillColor: "#c767dc"
                    },
                    {
                        date: new Date("2016-02-29"),
                        visits: 32912.496118312,
                        lineColor: "#c767dc",
                        fillColor: "#c767dc"
                    },
                    {
                        date: new Date("2016-03-31"),
                        visits: 32895.687888724,
                        lineColor: "#c767dc",
                        fillColor: "#c767dc"
                    },
                    {
                        date: new Date("2016-04-30"),
                        visits: 32843.845069298,
                        lineColor: "#c767dc",
                        fillColor: "#c767dc"
                    }
                ];

                chart.data = chartData;

                // Create axes
                var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
                dateAxis.startLocation = 0.5;
                dateAxis.endLocation = 0.5;

                // Create value axis
                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

                // Create series
                var series = chart.series.push(new am4charts.LineSeries());
                series.dataFields.valueY = "visits";
                series.dataFields.dateX = "date";
                series.strokeWidth = 3;
                series.tooltipText = "{valueY.value}";
                series.fillOpacity = 0.1;
                series.propertyFields.stroke = "lineColor";
                series.propertyFields.fill = "fillColor";

                // Add cursor
                chart.cursor = new am4charts.XYCursor();
                chart.cursor.xAxis = dateAxis;
                chart.scrollbarX = new am4core.Scrollbar();

                series.tooltip.getFillFromObject = false;
                series.tooltip.adapter.add("x", (x, target)=>{
                    if(series.tooltip.tooltipDataItem.valueY < 0){
                        series.tooltip.background.fill = chart.colors.getIndex(4);
                    }
                    else{
                        series.tooltip.background.fill = chart.colors.getIndex(0);
                    }
                    return x;
                })
            }
            catch( e ) {
                console.log( e );
            }
        }

        function conversionRatePieLine() {
            try {
                // Themes begin
                // am4core.useTheme(am4themes_kelly);
                am4core.useTheme(am4themes_animated);
                // Themes end
                /**
                 * Define data for each year
                 */

                    // Create chart instance
                var chart = am4core.create("conversionRatePieLine", am4charts.PieChart);
                chart.logo.disabled = true;
                // Add data
                chart.data = [
                    { "sector": "Email", "size": 50.0 },
                    { "sector": "SMS", "size": 30.0 },
                    { "sector": "Line", "size": 20.0 },

                ];

                // Add label
                chart.innerRadius = 60;

                // Add and configure Series
                var pieSeries = chart.series.push(new am4charts.PieSeries());
                pieSeries.dataFields.value = "size";
                pieSeries.dataFields.category = "sector";

                // Animate chart data
            }
            catch( e ) {
                console.log( e );
            }
        }

        function conversionRateLineLine() {
            try {
                // Themes begin
                // Using default theme
                am4core.useTheme(am4themes_animated);
                // Themes end

                // Create chart instance
                var chart = am4core.create("conversionRateLineLine", am4charts.XYChart);
                chart.logo.disabled = true;

                // Add data
                //chart.data = generatechartData();
                var chartData = [
                    {
                        date: new Date("2015-01-31"),
                        visits: 414662444,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-02-28"),
                        visits: 372150922,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-03-31"),
                        visits: 420382942,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-04-30"),
                        visits: 397943194,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-05-31"),
                        visits: 417188449,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-06-30"),
                        visits: 403219253,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-07-31"),
                        visits: 416871320,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-08-31"),
                        visits: 416298557,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-09-30"),
                        visits: 412549794,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-10-31"),
                        visits: 412726526,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-11-30"),
                        visits: 393763393,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-12-31"),
                        visits: 400372379,
                        lineColor: "#67b7dc",
                        fillColor: "#67b7dc"
                    },
                    {
                        date: new Date("2015-12-31"),
                        visits: 32787.788837713,
                        lineColor: "#c767dc",
                        fillColor: "#c767dc"
                    },
                    {
                        date: new Date("2016-01-31"),
                        visits: 32854.190410423,
                        lineColor: "#c767dc",
                        fillColor: "#c767dc"
                    },
                    {
                        date: new Date("2016-02-29"),
                        visits: 32912.496118312,
                        lineColor: "#c767dc",
                        fillColor: "#c767dc"
                    },
                    {
                        date: new Date("2016-03-31"),
                        visits: 32895.687888724,
                        lineColor: "#c767dc",
                        fillColor: "#c767dc"
                    },
                    {
                        date: new Date("2016-04-30"),
                        visits: 32843.845069298,
                        lineColor: "#c767dc",
                        fillColor: "#c767dc"
                    }
                ];

                chart.data = chartData;

                // Create axes
                var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
                dateAxis.startLocation = 0.5;
                dateAxis.endLocation = 0.5;

                // Create value axis
                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

                // Create series
                var series = chart.series.push(new am4charts.LineSeries());
                series.dataFields.valueY = "visits";
                series.dataFields.dateX = "date";
                series.strokeWidth = 3;
                series.tooltipText = "{valueY.value}";
                series.fillOpacity = 0.1;
                series.propertyFields.stroke = "lineColor";
                series.propertyFields.fill = "fillColor";

                // Add cursor
                chart.cursor = new am4charts.XYCursor();
                chart.cursor.xAxis = dateAxis;
                chart.scrollbarX = new am4core.Scrollbar();

                series.tooltip.getFillFromObject = false;
                series.tooltip.adapter.add("x", (x, target)=>{
                    if(series.tooltip.tooltipDataItem.valueY < 0){
                        series.tooltip.background.fill = chart.colors.getIndex(4);
                    }
                    else{
                        series.tooltip.background.fill = chart.colors.getIndex(0);
                    }
                    return x;
                })
            }
            catch( e ) {
                console.log( e );
            }
        }
    </script>
@endsection
