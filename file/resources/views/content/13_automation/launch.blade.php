@extends('layouts/contentLayoutMasterMain')

@section('title', 'ภาพรวมแคมเปญ')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap4.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/charts/chart-apex.css')) }}">
@endsection

@section('content')
    <style>
        #customerSegment {
            width: 100%;
            height: 300px;
            font-size: 10px;
        }
    </style>
    <!-- Statistics card section -->
    <section id="statistics-card">
        <!-- Miscellaneous Charts -->
        <div class="row">
            <div class="col-lg-4 col-sm-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h2 class="font-weight-bolder mb-0">125</h2>
                            <p class="card-text">All Customers</p>
                        </div>
                        <div class="avatar bg-light-primary p-50 m-0">
                            <div class="avatar-content">
                                <i data-feather="users" class="font-medium-5"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h2 class="font-weight-bolder mb-0">7</h2>
                            <p class="card-text">All Campaigns</p>
                        </div>
                        <div class="avatar bg-light-primary p-50 m-0">
                            <div class="avatar-content">
                                <i data-feather="send" class="font-medium-5"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h2 class="font-weight-bolder mb-0">9.52%</h2>
                            <p class="card-text">Average Conversion Rate</p>
                        </div>
                        <div class="avatar bg-light-primary p-50 m-0">
                            <div class="avatar-content">
                                <i data-feather="pie-chart" class="font-medium-5"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6 col-md-12 col-sm-12">
                <div class="card" style="height: 400px;">
                    <div class="card-header">
                        <div class="card-title d-flex align-items-center">
                            <i data-feather='pie-chart' class="font-medium-5" style="color:#C1C1CB;"></i>
                            <span style="color:#C1C1CB;margin-left:5px;"> Campaign Launch</span>
                        </div>
                    </div>
                    <div class="card-body" >
                        <div class="row">
                                <div class="mt-2" id="customerSegment"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6 col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title d-flex align-items-center">
                            <i data-feather='pie-chart' class="font-medium-5" style="color:#C1C1CB;"></i>
                            <span style="color:#C1C1CB;margin-left:5px;"> Campaign Launch</span>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <table class="datatables-basic table">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th>id</th>
                                            <th>NO</th>
                                            <th>Segment</th>
                                            <th>จำนวนคน</th>
                                            <th>จัดการข้อมูล</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ Statistics Card section-->
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/jszip.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.rowGroup.min.js')) }}"></script>
@endsection

@section('page-script')
    {{-- Page js files --}}
    <script type='text/javascript' src='https://cdn.amcharts.com/lib/4/core.js?ver=20210901-5e9' id='-lib-4-core-js-js'></script>
    <script type='text/javascript' src='https://cdn.amcharts.com/lib/4/charts.js?ver=20210901-5e9' id='-lib-4-charts-js-js'></script>
{{--    <script type='text/javascript' src='https://cdn.amcharts.com/lib/4/themes/dataviz.js?ver=20210901-5e9' id='-lib-4-themes-dataviz-js-js'></script>--}}
{{--    <script type='text/javascript' src='https://cdn.amcharts.com/lib/4/themes/material.js?ver=20210901-5e9' id='-lib-4-themes-material-js-js'></script>--}}
{{--    <script type='text/javascript' src='https://cdn.amcharts.com/lib/4/themes/kelly.js?ver=20210901-5e9' id='-lib-4-themes-kelly-js-js'></script>--}}
{{--    <script type='text/javascript' src='https://cdn.amcharts.com/lib/4/themes/dark.js?ver=20210901-5e9' id='-lib-4-themes-dark-js-js'></script>--}}
{{--    <script type='text/javascript' src='https://cdn.amcharts.com/lib/4/themes/frozen.js?ver=20210901-5e9' id='-lib-4-themes-frozen-js-js'></script>--}}
{{--    <script type='text/javascript' src='https://cdn.amcharts.com/lib/4/themes/moonrisekingdom.js?ver=20210901-5e9' id='-lib-4-themes-moonrisekingdom-js-js'></script>--}}
{{--    <script type='text/javascript' src='https://cdn.amcharts.com/lib/4/themes/spiritedaway.js?ver=20210901-5e9' id='-lib-4-themes-spiritedaway-js-js'></script>--}}
    <script type='text/javascript' src='https://cdn.amcharts.com/lib/4/themes/animated.js?ver=20210901-5e9' id='-lib-4-themes-animated-js-js'></script>

    {{-- Page js cumtoms --}}
    <script>
        $(document).ready(function(){
            customerSegment();
            $(function () {
                'use strict';
                var dt_basic_table = $('.datatables-basic'),
                    dt_date_table = $('.dt-date'),
                    assetPath = '../../../app-assets/';
                if ($('body').attr('data-framework') === 'laravel') {
                    assetPath = $('body').attr('data-asset-path');
                }

                // DataTable with buttons
                // --------------------------------------------------------------------
                if (dt_basic_table.length) {
                    var dt_basic = dt_basic_table.DataTable({
                        ajax: assetPath + 'data/table-datatable-segment.json',
                        columns: [
                            { data: 'responsive_id' },
                            { data: 'id' },
                            { data: 'id' }, // used for sorting so will hide this column
                            {
                                "data": "id",
                                render: function (data, type, row, meta) {
                                    return meta.row + meta.settings._iDisplayStart + 1;
                                }
                            },
                            { data: 'full_name' },
                            { data: 'email' },
                            { data: '' }
                        ],

                        columnDefs: [
                            {
                                // For Responsive
                                className: 'control',
                                orderable: false,
                                responsivePriority: 2,
                                targets: 0
                            },
                            {
                                // For Checkboxes
                                targets: 1,
                                orderable: false,
                                responsivePriority: 3,
                                render: function (data, type, full, meta) {
                                    return (
                                        '<div class="custom-control custom-checkbox"> <input class="custom-control-input dt-checkboxes" type="checkbox" value="" id="checkbox' +
                                        data +
                                        '" /><label class="custom-control-label" for="checkbox' +
                                        data +
                                        '"></label></div>'
                                    );
                                },
                                checkboxes: {
                                    selectAllRender:
                                        '<div class="custom-control custom-checkbox"> <input class="custom-control-input" type="checkbox" value="" id="checkboxSelectAll" /><label class="custom-control-label" for="checkboxSelectAll"></label></div>'
                                }
                            },
                            {
                                targets: 2,
                                visible: false
                            },
                            {
                                responsivePriority: 1,
                                targets: 4
                            },
                            {
                                // Actions
                                targets: -1,
                                title: 'จัดการข้อมูล',
                                orderable: false,
                                render: function (data, type, full, meta) {
                                    return (
                                        '<a href="{{ url('/automation/create') }}" class="item-edit">' +
                                        feather.icons['plus-square'].toSvg({ class: 'font-medium-5' }) +
                                        '</a>' + ' ' +
                                        '<a href="{{ url('/automation/setting') }}" class="item-edit">' +
                                        feather.icons['send'].toSvg({ class: 'font-medium-5' }) +
                                        '</a>'
                                    );
                                }
                            }
                        ],
                        order: [[2, 'asc']],
                        dom:
                            '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-right"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                        displayLength: 11,
                        lengthMenu: [11, 25, 50, 75, 100],
                        buttons: [],
                        responsive: {
                            details: {
                                display: $.fn.dataTable.Responsive.display.modal({
                                    header: function (row) {
                                        var data = row.data();
                                        return 'Details of ' + data['full_name'];
                                    }
                                }),
                                type: 'column',
                                renderer: function (api, rowIdx, columns) {
                                    var data = $.map(columns, function (col, i) {
                                        return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                                            ? '<tr data-dt-row="' +
                                            col.rowIndex +
                                            '" data-dt-column="' +
                                            col.columnIndex +
                                            '">' +
                                            '<td>' +
                                            col.title +
                                            ':' +
                                            '</td> ' +
                                            '<td>' +
                                            col.data +
                                            '</td>' +
                                            '</tr>'
                                            : '';
                                    }).join('');
                                    return data ? $('<table class="table"/>').append(data) : false;
                                }
                            }
                        },
                        language: {
                            paginate: {
                                // remove previous & next text from pagination
                                previous: '&nbsp;',
                                next: '&nbsp;'
                            }
                        }
                    });
                }
            });
        });

        function customerSegment() {
            try {
                // Themes begin
                // am4core.useTheme(am4themes_kelly);
                am4core.useTheme(am4themes_animated);
                // Themes end
                /**
                 * Define data for each year
                 */

                // Create chart instance
                var chart = am4core.create("customerSegment", am4charts.PieChart);
                chart.logo.disabled = true;
                // Add data
                chart.data = [
                    { "sector": "About To Sleep", "size": 80.0 },
                    { "sector": "At Risk", "size": 10.0 },
                    { "sector": "Cannot Lose Them", "size": 30.0 },
                    { "sector": "Champions", "size": 40.0 },
                    { "sector": "Hibernating", "size": 50.0 },
                    { "sector": "Loyal Customers", "size": 100.0 },
                    { "sector": "Need Attention", "size": 140.0 },
                    { "sector": "New Customers", "size": 170.0 },
                    { "sector": "Potential", "size": 200.0 },
                    { "sector": "Promising", "size": 210.0 },
                    { "sector": "Unknow", "size": 5.0 },

                ];

                // Add label
                chart.innerRadius = 60;

                // Add and configure Series
                var pieSeries = chart.series.push(new am4charts.PieSeries());
                pieSeries.dataFields.value = "size";
                pieSeries.dataFields.category = "sector";

                // Animate chart data
            }
            catch( e ) {
                console.log( e );
            }
        }
    </script>
@endsection
