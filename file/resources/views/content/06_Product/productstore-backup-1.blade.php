@extends('layouts/contentLayoutMasterMain')

@section('title', 'Checkout', 'Statistics Cards', 'Form Repeater')

@section('vendor-style')
    <!-- Vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset('js/dropify/dist/css/dropify.css') }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-ecommerce.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-number-input.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
@endsection

@section('content')

    <style>
        .subText{
            font-size: 14px;
        }
    </style>

        <div class="row">
            <div class="col-lg-12 col-12">
                <div class="card mb-0">
                    <div class="card-header">
                        <h4 class="card-title col-md-6 col-sm-12 col-6 pl-0">ระบบจัดการคลังสินค้า</h4>
                        <h4 class="card-title col-md-6 col-sm-12 col-6 pr-0 text-right">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_gold_add">
                                <i data-feather="plus" class="mr-25"></i>
                                <span>เพิ่มสินค้า</span>
                            </button>
                            <button type="button" class="btn btn-secondary">
                                <i data-feather="printer" class="mr-25"></i>
                                <span>พิมพ์บาร์โค๊ด</span>
                            </button>
                            <button type="button" class="btn btn-danger">
                                <i data-feather="trash-2" class="mr-25"></i>
                                <span>ลบทั้งหมด</span>
                            </button>
                        </h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-12 mb-2 mb-md-0">
                                <div class="form-group">
                                    <label for="basicSelect" class="flsize">ค้นหาสินค้า</label>
                                    <select class="form-control" id="basicSelect">
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-12">
                <div class="list-view">
                    <div class="checkout-items">
                        <div class="card ecommerce-card" style="grid-template-columns: 1fr 3fr 2fr;">
                            <div class="item-img ml-2">
                                <img src="{{asset('images/gold/gold1.jpg')}}" alt="img-placeholder" style="width: 160px;" />
                            </div>
                            <div class="card-body">
                                <div class="item-name">
                                    <h4 class="mb-0 text-body bold">ออสสิริส</h4>
                                    <span class="item-company text-primary">ทองคำแท่ง</span>
                                    <div class="badge badge-pill badge-warning">96.5 %</div>
                                </div>
                                <span class="item-company">หนัก: 1 บาท</span>
                                <span class="item-company">ขนาด/ความยาว: 1</span>
                            </div>
                            <div class="item-options text-center">
                                <div class="item-wrapper">
                                    <div class="item-cost">
                                        <h4 class="item-price">฿ 25,000</h4>
                                        <p class="card-text shipping">
                                            <span class="badge badge-pill badge-light-success">คงเหลือ 10 ชิ้น</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="demo-inline-spacing">
                                    <button type="button" class="btn btn-icon btn-warning btn-sm" data-toggle="modal" data-target="#edit_product" style="margin-right: 2px;">
                                        <i data-feather="edit-2"></i>
                                    </button>
                                    <button type="button" class="btn btn-icon btn-info btn-sm" data-toggle="modal" data-target="#default" style="margin-right: 2px;">
                                        <i data-feather="eye"></i>
                                    </button>
                                    <button type="button" class="btn btn-icon btn-danger btn-sm" data-toggle="modal" data-target="#delete_product" style="margin-right: 2px;">
                                        <i data-feather="trash-2"></i>
                                    </button>
                                    <button type="button" class="btn btn-icon btn-secondary btn-sm" style="margin-right: 2px;">
                                        <i data-feather="printer"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card ecommerce-card" style="grid-template-columns: 1fr 3fr 2fr;">
                            <div class="item-img ml-2">
                                <img src="{{asset('images/gold/gold2.jpg')}}" alt="img-placeholder" style="width: 160px;"  />
                            </div>
                            <div class="card-body">
                                <div class="item-name">
                                    <h4 class="mb-0 text-body bold">สปริงหลุยส์</h4>
                                    <span class="item-company text-primary">สร้อยคอ</span>
                                    <div class="badge badge-pill badge-warning">96.5 %</div>
                                </div>
                                <span class="item-company">หนัก: 2 บาท</span>
                                <span class="item-company">ขนาด/ความยาว: 8</span>
                            </div>
                            <div class="item-options text-center">
                                <div class="item-wrapper">
                                    <div class="item-cost">
                                        <h4 class="item-price">฿ 58,000</h4>
                                        <p class="card-text shipping">
                                            <span class="badge badge-pill badge-light-success">คงเหลือ 3 ชิ้น</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="demo-inline-spacing">
                                    <button type="button" class="btn btn-icon btn-warning btn-sm" style="margin-right: 2px;">
                                        <i data-feather="edit-2"></i>
                                    </button>
                                    <button type="button" class="btn btn-icon btn-info btn-sm" style="margin-right: 2px;">
                                        <i data-feather="eye"></i>
                                    </button>
                                    <button type="button" class="btn btn-icon btn-danger btn-sm" style="margin-right: 2px;">
                                        <i data-feather="trash-2"></i>
                                    </button>
                                    <button type="button" class="btn btn-icon btn-secondary btn-sm" style="margin-right: 2px;">
                                        <i data-feather="printer"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card ecommerce-card" style="grid-template-columns: 1fr 3fr 2fr;">
                            <div class="item-img ml-2">
                                <img src="{{asset('images/gold/gold3.jpg')}}" alt="img-placeholder" style="width: 160px;"  />
                            </div>
                            <div class="card-body">
                                <div class="item-name">
                                    <h4 class="mb-0 text-body bold">ดอกไม้ฝังพลอย</h4>
                                    <span class="item-company text-primary">แหวน</span>
                                    <div class="badge badge-pill badge-warning">96.5 %</div>
                                </div>
                                <span class="item-company">หนัก: 1 บาท</span>
                                <span class="item-company">ขนาด/ความยาว: 1</span>
                            </div>
                            <div class="item-options text-center">
                                <div class="item-wrapper">
                                    <div class="item-cost">
                                        <h4 class="item-price">฿ 25,000</h4>
                                        <p class="card-text shipping">
                                            <span class="badge badge-pill badge-light-success">คงเหลือ 2 ชิ้น</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="demo-inline-spacing">
                                    <button type="button" class="btn btn-icon btn-warning btn-sm" style="margin-right: 2px;">
                                        <i data-feather="edit-2"></i>
                                    </button>
                                    <button type="button" class="btn btn-icon btn-info btn-sm" style="margin-right: 2px;">
                                        <i data-feather="eye"></i>
                                    </button>
                                    <button type="button" class="btn btn-icon btn-danger btn-sm" style="margin-right: 2px;">
                                        <i data-feather="trash-2"></i>
                                    </button>
                                    <button type="button" class="btn btn-icon btn-secondary btn-sm" style="margin-right: 2px;">
                                        <i data-feather="printer"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-12">
                <div class="list-view">
                    <div class="checkout-items">
                        <div class="card ecommerce-card" style="grid-template-columns: 1fr 3fr 2fr;">
                            <div class="item-img ml-2">
                                <img src="{{asset('images/gold/gold1.jpg')}}" alt="img-placeholder" style="width: 160px;" />
                            </div>
                            <div class="card-body">
                                <div class="item-name">
                                    <h4 class="mb-0 text-body bold">ออสสิริส</h4>
                                    <span class="item-company text-primary">ทองคำแท่ง</span>
                                    <div class="badge badge-pill badge-warning">96.5 %</div>
                                </div>
                                <span class="item-company">หนัก: 1 บาท</span>
                                <span class="item-company">ขนาด/ความยาว: 1</span>
                            </div>
                            <div class="item-options text-center">
                                <div class="item-wrapper">
                                    <div class="item-cost">
                                        <h4 class="item-price">฿ 25,000</h4>
                                        <p class="card-text shipping">
                                            <span class="badge badge-pill badge-light-success">คงเหลือ 10 ชิ้น</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="demo-inline-spacing">
                                    <button type="button" class="btn btn-icon btn-warning btn-sm" style="margin-right: 2px;">
                                        <i data-feather="edit-2"></i>
                                    </button>
                                    <button type="button" class="btn btn-icon btn-info btn-sm" style="margin-right: 2px;">
                                        <i data-feather="eye"></i>
                                    </button>
                                    <button type="button" class="btn btn-icon btn-danger btn-sm" style="margin-right: 2px;">
                                        <i data-feather="trash-2"></i>
                                    </button>
                                    <button type="button" class="btn btn-icon btn-secondary btn-sm" style="margin-right: 2px;">
                                        <i data-feather="printer"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card ecommerce-card" style="grid-template-columns: 1fr 3fr 2fr;">
                            <div class="item-img ml-2">
                                <img src="{{asset('images/gold/gold2.jpg')}}" alt="img-placeholder" style="width: 160px;"  />
                            </div>
                            <div class="card-body">
                                <div class="item-name">
                                    <h4 class="mb-0 text-body bold">สปริงหลุยส์</h4>
                                    <span class="item-company text-primary">สร้อยคอ</span>
                                    <div class="badge badge-pill badge-warning">96.5 %</div>
                                </div>
                                <span class="item-company">หนัก: 2 บาท</span>
                                <span class="item-company">ขนาด/ความยาว: 8</span>
                            </div>
                            <div class="item-options text-center">
                                <div class="item-wrapper">
                                    <div class="item-cost">
                                        <h4 class="item-price">฿ 58,000</h4>
                                        <p class="card-text shipping">
                                            <span class="badge badge-pill badge-light-success">คงเหลือ 3 ชิ้น</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="demo-inline-spacing">
                                    <button type="button" class="btn btn-icon btn-warning btn-sm" style="margin-right: 2px;">
                                        <i data-feather="edit-2"></i>
                                    </button>
                                    <button type="button" class="btn btn-icon btn-info btn-sm" style="margin-right: 2px;">
                                        <i data-feather="eye"></i>
                                    </button>
                                    <button type="button" class="btn btn-icon btn-danger btn-sm" style="margin-right: 2px;">
                                        <i data-feather="trash-2"></i>
                                    </button>
                                    <button type="button" class="btn btn-icon btn-secondary btn-sm" style="margin-right: 2px;">
                                        <i data-feather="printer"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card ecommerce-card" style="grid-template-columns: 1fr 3fr 2fr;">
                            <div class="item-img ml-2">
                                <img src="{{asset('images/gold/gold3.jpg')}}" alt="img-placeholder" style="width: 160px;"  />
                            </div>
                            <div class="card-body">
                                <div class="item-name">
                                    <h4 class="mb-0 text-body bold">ดอกไม้ฝังพลอย</h4>
                                    <span class="item-company text-primary">แหวน</span>
                                    <div class="badge badge-pill badge-warning">96.5 %</div>
                                </div>
                                <span class="item-company">หนัก: 1 บาท</span>
                                <span class="item-company">ขนาด/ความยาว: 1</span>
                            </div>
                            <div class="item-options text-center">
                                <div class="item-wrapper">
                                    <div class="item-cost">
                                        <h4 class="item-price">฿ 25,000</h4>
                                        <p class="card-text shipping">
                                            <span class="badge badge-pill badge-light-success">คงเหลือ 2 ชิ้น</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="demo-inline-spacing">
                                    <button type="button" class="btn btn-icon btn-warning btn-sm" style="margin-right: 2px;">
                                        <i data-feather="edit-2"></i>
                                    </button>
                                    <button type="button" class="btn btn-icon btn-info btn-sm" style="margin-right: 2px;">
                                        <i data-feather="eye"></i>
                                    </button>
                                    <button type="button" class="btn btn-icon btn-danger btn-sm" style="margin-right: 2px;">
                                        <i data-feather="trash-2"></i>
                                    </button>
                                    <button type="button" class="btn btn-icon btn-secondary btn-sm" style="margin-right: 2px;">
                                        <i data-feather="printer"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-12">
                <div class="list-view">
                    <div class="checkout-items" id="gold_list_left">
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-12">
                <div class="list-view">
                    <div class="checkout-items" id="gold_list_right">
                    </div>
                </div>
            </div>
        </div>

    <div
        class="modal fade text-left"
        id="default"
        tabindex="-1"
        role="dialog"
        aria-labelledby="myModalLabel1"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">รายละเอียดสินค้า</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="col-md-12 mb-3 mt-1">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>หมวดหมู่สินค้า</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong>ทอง 96.5 %</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ประเภทสินค้า</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong>ทองคำแท่ง</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ลายทอง</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong>ออสสิริส</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>น้ำหนัก</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong>2 บาท</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>น้ำหนักจริง</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong>30.32 กรัม (g)</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ราคาต้นทุนเฉลี่ย</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong>30,800.00 บาท</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ค่ากำเหน็ดเฉลี่ย</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong>300.00 บาท</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ขนาด/ความยาว</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong>-</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ราคา</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong>21,000.00 บาท</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>จำนวนคงเหลือ</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong>2 ชิ้น</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="col-md-12 mt-1">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>รูปภาพ</span>
                                    </div>
                                    <div class="col-md-9">
                                        <img  style="width: 300px;" src="{{asset('images/gold/gold1.jpg')}}" alt="img-placeholder" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                </div>
            </div>
        </div>
    </div>

    <div
        class="modal fade text-left"
        id="modal_gold_add"
        tabindex="-1"
        role="dialog"
        aria-labelledby="myModalLabel1"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">เพิ่มรายการสินค้า</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="preview_gold_add"></div>
                    <form id="form_gold_add">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="col-md-12 mb-2 mt-1">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>หมวดหมู่สินค้า</span>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="custom-control custom-radio mr-1">
                                                <input onchange="gold_category_form(this)" type="radio" id="gold_category_ninety_six" name="gold_category" value="1" class="custom-control-input" checked/>
                                                <label class="custom-control-label" for="gold_category_ninety_six">ทอง 96.5 %</label>
                                            </div>
                                            <div class="custom-control custom-radio mr-1">
                                                <input onchange="gold_category_form(this)" type="radio" id="gold_category_ninety_nine" name="gold_category" value="2" class="custom-control-input"/>
                                                <label class="custom-control-label" for="gold_category_ninety_nine">ทอง 99.99 %</label>
                                            </div>
                                            <div class="custom-control custom-radio mr-1">
                                                <input onchange="gold_category_form(this)" type="radio" id="gold_category_ninety" name="gold_category" value="3" class="custom-control-input" />
                                                <label class="custom-control-label" for="gold_category_ninety">ทอง 90 %</label>
                                            </div>
                                            <div class="custom-control custom-radio">
                                                <input onchange="gold_category_form(this)" type="radio" id="gold_category_other" name="gold_category" value="4" class="custom-control-input"/>
                                                <label class="custom-control-label" for="gold_category_other">อื่น ๆ</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2" id="gold_type_input">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ประเภททอง</span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="custom-control custom-radio mr-1">
                                                <input onchange="gold_type_form(this)" type="radio" class="custom-control-input" name="gold_type" value="1"  id="gold_type_ornament_add" checked />
                                                <label class="custom-control-label" for="gold_type_ornament_add">ทองรูปพรรณ</label>
                                            </div>
                                            <div class="custom-control custom-radio mr-1">
                                                <input onchange="gold_type_form(this)" type="radio" class="custom-control-input" name="gold_type" value="2" id="gold_type_bar_add" />
                                                <label class="custom-control-label" for="gold_type_bar_add">ทองแท่ง</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2" id="gold_sub_category_input">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ประเภทสินค้า</span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select class="form-control" id="gold_sub_category" name="gold_sub_category">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ลายทอง</span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="gold_pattern_add" name="gold_pattern"/>
                                            <div id="gold_pattern_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2" id="gold_weight_input">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>น้ำหนัก</span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="number" class="form-control" id="gold_weight_add" name="gold_weight"/>
                                            <div id="gold_weight_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <select class="form-control" id="gold_unit" name="gold_unit">
                                                @foreach(\App\Models\Gold\GoldUnits::query()->get() as $row)
                                                    <option value="{{ $row->id }}">{{ $row->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>น้ำหนักจริง</span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="number" class="form-control" id="gold_weight_actual_add" name="gold_weight_actual"/>
                                            <div id="gold_weight_actual_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <span>กรัม (g)</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ราคาต้นทุน</span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="number" class="form-control" id="gold_cost_add" name="gold_cost"/>
                                            <div id="gold_cost_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <span>บาท</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ค่ากำเหน็ด</span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="number" class="form-control" id="gold_smith_charge_add" name="gold_smith_charge"/>
                                            <div id="gold_smith_charge_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <span>บาท</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>รูปภาพสินค้า</span>
                                    </div>
                                    <div class="col-md-9" id="gold_image_input">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="col-md-12 mt-1" id="gold_list_input">
                                <div class="invoice-repeater">
                                    <div data-repeater-list="gold_list">
                                        <div data-repeater-item>
                                            <div class="row d-flex align-items-end">
                                                <div class="col-md-5 col-12">
                                                    <div class="form-group">
                                                        <label for="gold_size">ขนาด/ความยาว</label>
                                                        <input
                                                            type="number"
                                                            class="form-control"
                                                            name="gold_size"
                                                            aria-describedby="gold_size"
                                                        />
                                                    </div>
                                                </div>

                                                <div class="col-md-5 col-12">
                                                    <div class="form-group">
                                                        <label for="gold_amount">จำนวน/ชิ้น</label>
                                                        <input
                                                            type="number"
                                                            class="form-control"
                                                            name="gold_amount"
                                                            aria-describedby="gold_amount"
                                                        />
                                                    </div>
                                                </div>

                                                <div class="col-md-2 col-12 mb-50">
                                                    <div class="form-group">
                                                        <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete type="button">
                                                            <i data-feather="x" class="mr-25"></i>
                                                            <span>ลบ</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div id="gold_list_error_add" class="invalid-feedback" style="display:block;"></div>
                                            <button class="btn btn-icon btn-primary" type="button" data-repeater-create>
                                                <i data-feather="plus" class="mr-25"></i>
                                                <span>เพิ่ม</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mt-1" id="gold_amount_input">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>จำนวน</span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="number" class="form-control" id="gold_amount_add" name="gold_amount"/>
                                            <div id="gold_amount_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <span>ชิ้น</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btn_gold_add">บันทึก</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                </div>
            </div>
        </div>
    </div>

    <div
        class="modal fade text-left"
        id="edit_product"
        tabindex="-1"
        role="dialog"
        aria-labelledby="myModalLabel1"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">แก้ไขรายการสินค้า</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="col-md-12 mb-2 mt-1">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>หมวดหมู่สินค้า</span>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="custom-control custom-radio mr-1">
                                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input" disabled checked/>
                                                <label class="custom-control-label" for="customRadio1">ทอง 96.5 %</label>
                                            </div>
                                            <div class="custom-control custom-radio mr-1">
                                                <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input" disabled/>
                                                <label class="custom-control-label" for="customRadio2">ทอง 99.99 %</label>
                                            </div>
                                            <div class="custom-control custom-radio mr-1">
                                                <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input" disabled/>
                                                <label class="custom-control-label" for="customRadio3">ทอง 90 %</label>
                                            </div>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio4" name="customRadio" class="custom-control-input" disabled/>
                                                <label class="custom-control-label" for="customRadio4">อื่น ๆ</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ประเภททอง</span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="custom-control custom-radio mr-1">
                                                <input type="radio" id="typeRadio1" name="typeRadio1" class="custom-control-input" disabled checked/>
                                                <label class="custom-control-label" for="typeRadio1">ทองรูปพรรณ</label>
                                            </div>
                                            <div class="custom-control custom-radio mr-1">
                                                <input type="radio" id="typeRadio2" name="typeRadio2" class="custom-control-input" disabled/>
                                                <label class="custom-control-label" for="typeRadio2">ทองแท่ง</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ประเภทสินค้า</span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select class="form-control" id="basicSelect">
                                                <option>สร้อยคอ</option>
                                                <option>แหวน</option>
                                                <option>ข้อมือ</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ลายทอง</span>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="floating-label-disable" placeholder="หัวใจโปร่ง"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>น้ำหนักจริง</span>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="floating-label-disable" placeholder="14.9"/>
                                    </div>
                                    <div class="col-md-3">
                                        <span>กรัม (g)</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ขนาด/ความยาว</span>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="floating-label-disable"
                                               placeholder="17"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="col-md-12 mt-1">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>รูปภาพสินค้า</span>
                                    </div>
                                    <div class="col-md-9">
                                        <form action="#" class="dropzone dropzone-area" id="dpz-single-file">
                                            <div class="dz-message">Drop files here or click to upload.</div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">บันทึก</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                </div>
            </div>
        </div>
    </div>

    <div
        class="modal fade text-left"
        id="delete_product"
        tabindex="-1"
        role="dialog"
        aria-labelledby="myModalLabel1"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">ลบสินค้า - <span>76</span></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <span>ช่วงเวลาที่เพิ่มสินค้าที่ต้องการลบ</span>
                        </div>
                        <div class="col-md-4">
                            <input type="text" id="fp-default" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                        </div>
                        <div class="col-md-4">
                            <input type="text" id="fp-default" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">แก้ไข</button>
                    <button type="button" class="btn btn-primary">บันทึก</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('vendor-script')
    <!-- Vendor js files -->
    <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/spinner/jquery.bootstrap-touchspin.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/repeater/jquery.repeater.min.js')) }}"></script>
@endsection

@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset('js/dropify/dist/js/dropify.js') }}"></script>
    <script src="{{ asset(mix('js/scripts/pages/app-ecommerce-checkout.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-repeater.js')) }}"></script>
    <script>
        $(document).ready(function () {
            $('#modal_gold_add').on('show.bs.modal', function() {
                gold_category_form({ value: '1' });
                gold_type_form({ value: '1' });
                gold_sub_category(1);

                $("#gold_image_input").html("<input type=\"file\" class=\"dropify\" name=\"gold_image\" id=\"gold_image_add\" data-width=\"200\" data-height=\"150\">\n" +
                    "<div id=\"gold_image_error_add\" class=\"invalid-feedback\" style=\"display:block;\"></div>");

                $('.dropify').dropify({
                    messages: {
                        'default': 'Drag and drop a file here or click',
                        'replace': 'Drag and drop or click to replace',
                        'remove': 'Remove',
                        'error': 'Ooops, something wrong happended.'
                    },
                    tpl: {
                        wrap:            '<div class="dropify-wrapper"></div>',
                        loader:          '<div class="dropify-loader"></div>',
                        message:         '<div class="dropify-message"><span class="file-icon" /> <p></p></div>',
                        preview:         '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message"></p></div></div></div>',
                        filename:        '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
                        clearButton:     '<button type="button" class="dropify-clear">ลบ</button>',
                        errorLine:       '<p class="dropify-error"></p>',
                        errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
                    }
                });
            });

            $('#modal_gold_add').on('hide.bs.modal', function() {
                $('#form_gold_add')[0].reset();

                removeValidation('add');
            });

            $('#btn_gold_add').click(function (e) {
                $('#btn_gold_add').empty();
                $('#btn_gold_add').attr("disabled", true);
                $('#btn_gold_add').append("<div class=\"spinner-border spinner-grow-sm text-light\" role=\"status\"><span class=\"sr-only\">Loading...</span></div> กำลังบันทึกข้อมูล");

                removeValidation('add');

                e.preventDefault();
                var form = $('#form_gold_add')[0];
                var data = new FormData(form);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{url('/gold-store')}}',
                    type: "POST",
                    enctype: 'multipart/form-data',
                    dataType: 'JSON',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (res) {

                        if(res.error == 0){
                            $('#preview_gold_add').append("<div class=\"alert alert-success\" role=\"alert\">\n" +
                                "<h4 class=\"alert-heading\">เพิ่มข้อมูลเรียบร้อย</h4>\n" +
                                "</div>");
                            setTimeout(function(){
                                $('#modal_gold_add').modal('hide');

                                // $('#table').dataTable().fnClearTable();
                                // $('#table').dataTable().fnDestroy();
                                // listUser();
                            }, 1000);
                        }else{
                            $('#preview_gold_add').append("<div class=\"alert alert-danger\" role=\"alert\">\n" +
                                "<h4 class=\"alert-heading\">พบข้อผิดพลาด</h4>\n" +
                                "</div>");
                            $.each(res.messages, function (k,v) {
                                $('#'+ k + '_add').addClass('is-invalid');
                                $('#'+ k + '_error_add').html(v);
                            });
                        }

                        $('#btn_gold_add').empty();
                        $('#btn_gold_add').append("บันทึก");
                        $('#btn_gold_add').attr("disabled",false);
                    }
                });
            });

            gold_stock();
        });

        function gold_category_form(el) {
            removeValidation('add');
            resetForm('add');

            switch (el.value) {
                case '1':
                    $('#gold_type_input').css('display', 'block');
                    $('#gold_weight_input').css('display', 'block');
                    gold_sub_category(1);
                    break;
                case '2':
                    $('#gold_type_input').css('display', 'block');
                    $('#gold_weight_input').css('display', 'none');
                    gold_sub_category(1);
                    break;
                case '3':
                    $('#gold_type_input').css('display', 'block');
                    $('#gold_weight_input').css('display', 'none');
                    gold_sub_category(1);
                    break;
                case '4':
                    $('#gold_type_input').css('display', 'none');
                    $('#gold_weight_input').css('display', 'none');
                    gold_sub_category(4);
                    break;
                default:
                    break;
            }
        }

        function gold_type_form(el) {
            removeValidation('add');

            switch (el.value) {
                case '1':
                    $('#gold_sub_category_input').css('display', 'block');
                    $('#gold_list_input').css('display', 'block');
                    $('#gold_amount_input').css('display', 'none');
                    break;
                case '2':
                    $('#gold_sub_category_input').css('display', 'none');
                    $('#gold_list_input').css('display', 'none');
                    $('#gold_amount_input').css('display', 'block');
                    break;
                default:
                    break;
            }
        }

        function gold_sub_category(type) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('gold-sub-category?type=') }}' + type,
                type: "GET",
                enctype: 'multipart/form-data',
                dataType: 'JSON',
                async: false,
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    $('#gold_sub_category').empty();
                    $.each(res.data, function (index, sub_category) {
                        $('#gold_sub_category').append('<option value="' + sub_category.id + '">' + sub_category.title + '</option>');
                    });
                }
            });
        }

        function removeValidation(status) {
            $('#preview_gold_' + status).empty();
            $('#gold_pattern_' + status).removeClass('is-invalid');
            $('#gold_weight_' + status).removeClass('is-invalid');
            $('#gold_weight_actual_' + status).removeClass('is-invalid');
            $('#gold_cost_' + status).removeClass('is-invalid');
            $('#gold_smith_charge_' + status).removeClass('is-invalid');
            $('#gold_image_' + status).removeClass('is-invalid');
            $('#gold_list_' + status).removeClass('is-invalid');
            $('#gold_amount_' + status).removeClass('is-invalid');

            $('#gold_pattern_error_' + status).html("");
            $('#gold_weight_error_' + status).html("");
            $('#gold_weight_actual_error_' + status).html("");
            $('#gold_cost_error_' + status).html("");
            $('#gold_smith_charge_error_' + status).html("");
            $('#gold_image_error_' + status).html("");
            $('#gold_list_error_' + status).html("");
            $('#gold_amount_error_' + status).html("");
        }

        function resetForm(status) {
            $('#gold_type_ornament_' + status).prop("checked", true);
            gold_type_form({ value: '1' });
        }

        function gold_stock() {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('gold-stock') }}',
                type: "GET",
                enctype: 'multipart/form-data',
                dataType: 'JSON',
                async: false,
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    $('#gold_list_left').empty();
                    $('#gold_list_right').empty();

                    $.each(res.data, function (index, item) {
                        let column = ((index % 2) === 0 ? '#gold_list_left' : '#gold_list_right');

                        $(column).append('<div class="card ecommerce-card" style="grid-template-columns: 1fr 3fr 2fr;">\n' +
                        '                            <div class="item-img ml-2">\n' +
                        '                                <img src="{{ asset('_uploads/_products') }}/' + item.pic + '" alt="img-placeholder" style="width: 160px;" />\n' +
                        '                            </div>\n' +
                        '                            <div class="card-body">\n' +
                        '                                <div class="item-name">\n' +
                        '                                    <h4 class="mb-0 text-body bold">' + item.title + '</h4>\n' +
                        '                                    <span class="item-company text-primary">' + item.subcategory.title + '</span>\n' +
                        '                                    <div class="badge badge-pill badge-warning">' + item.category.title + '</div>\n' +
                        '                                </div>\n' +
                        '                                <span class="item-company">หนัก: ' + item.weight + ' ' + item.units.title + '</span>\n' +
                        '                                <span class="item-company">ขนาด/ความยาว: ' + item.size + '</span>\n' +
                        '                            </div>\n' +
                        '                            <div class="item-options text-center">\n' +
                        '                                <div class="item-wrapper">\n' +
                        '                                    <div class="item-cost">\n' +
                        '                                        <h4 class="item-price">฿ ' + item.price.income + '</h4>\n' +
                        '                                        <p class="card-text shipping">\n' +
                        '                                            <span class="badge badge-pill badge-light-success">คงเหลือ ' + item.stock.count + ' ชิ้น</span>\n' +
                        '                                        </p>\n' +
                        '                                    </div>\n' +
                        '                                </div>\n' +
                        '                                <div class="demo-inline-spacing">\n' +
                        '                                    <button type="button" class="btn btn-icon btn-warning btn-sm" data-toggle="modal" data-target="#edit_product" style="margin-right: 2px;">\n' +
                        '                                        <i data-feather="edit-2"></i>\n' +
                        '                                    </button>\n' +
                        '                                    <button type="button" class="btn btn-icon btn-info btn-sm" data-toggle="modal" data-target="#default" style="margin-right: 2px;">\n' +
                        '                                        <i data-feather="eye"></i>\n' +
                        '                                    </button>\n' +
                        '                                    <button type="button" class="btn btn-icon btn-danger btn-sm" data-toggle="modal" data-target="#delete_product" style="margin-right: 2px;">\n' +
                        '                                        <i data-feather="trash-2"></i>\n' +
                        '                                    </button>\n' +
                        '                                    <button type="button" class="btn btn-icon btn-secondary btn-sm" style="margin-right: 2px;">\n' +
                        '                                        <i data-feather="printer"></i>\n' +
                        '                                    </button>\n' +
                        '                                </div>\n' +
                        '                            </div>\n' +
                        '                        </div>');
                    });
                }
            });
        }
    </script>
@endsection
