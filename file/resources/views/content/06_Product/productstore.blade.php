@extends('layouts/contentLayoutMasterMain')

@section('title', 'Checkout', 'Statistics Cards', 'Form Repeater')

@section('vendor-style')
    <!-- Vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset('js/dropify/dist/css/dropify.css') }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-ecommerce.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-number-input.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
        
@endsection

@section('content')

    <style>
        .subText{
            font-size: 14px;
        }
        label.error {
            text-align: left;
            color: #e64141;
        }
    </style>

<div id="content-page" class="content-page">
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12 col-12">
                <div class="card mb-0">
                    <div class="card-header">
                        <h4 class="card-title col-md-6 col-sm-12 col-6 pl-0">ระบบจัดการคลังสินค้า</h4>
                        <h4 class="card-title col-md-6 col-sm-12 col-6 pr-0 text-right">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_gold_add">
                                <i data-feather="plus" class="mr-25"></i>
                                <span>เพิ่มสินค้า</span>
                            </button>
                            <button type="button" class="btn btn-secondary">
                                <i data-feather="printer" class="mr-25"></i>
                                <span>พิมพ์บาร์โค๊ด</span>
                            </button>
                            <button type="button" class="btn btn-danger">
                                <i data-feather="trash-2" class="mr-25"></i>
                                <span>ลบทั้งหมด</span>
                            </button>
                        </h4>
                    </div>
                    <div class="card-body">
                        
                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <div class="iq-card bg-transparent shadow-none iq-card-block iq-card-stretch iq-card-height text-primary mb-0">
                                    <div class="col-12 iq-card-body pt-3 pb-0">
                                        <form id="gold_filter">
                                            <div class="row">
                                                <div class="col-sm-2 pl-0">
                                                    <div class="form-group">
                                                        <select name="category" id="category" class="form-control" onchange="Searchgold()">
                                                            <option value="">หมวดหมู่</option>
                                                            <option value="1">ทอง 96.5 %</option>
                                                            <option value="2">ทอง 99.99 %</option>
                                                            <option value="3">ทอง 90%</option>
                                                            <option value="4">อื่น ๆ</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2 pl-0">
                                                    <div class="form-group">
                                                        <select name="subcategory" id="subcategory" class="form-control" onchange="Searchgold()">
                                                            <option value="">ประเภททอง</option>
                                                            @foreach(App\Models\Gold\GoldSubCategory::where('id', '<>', 0)->get() as $item)
                                                                <option value="{{ $item->id }}"
                                                                        @if(!is_null(request()->subcategory) && request()->subcategory == $item->id) selected @endif>
                                                                    {{ $item->title }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-sm-2 pl-sm-0">
                                                    
                                                    <div class="form-group">
                                                        <input class="form-control" placeholder="ลายทอง"
                                                            @if(!is_null(request()->txt))value="{{ request()->txt }}"@endif name="txt" type="text" id="txt" onkeyup="Searchgold()">
                                                    </div>
                                                </div>

                                                <div class="col-sm-1 pl-sm-0">
                                                    
                                                    <div class="form-group">
                                                        <input class="form-control" placeholder="น้ำหนัก"
                                                            @if(!is_null(request()->weight))value="{{ request()->weight }}"@endif name="weight" type="number" id="weight" onkeyup="Searchgold()">
                                                    </div>
                                                </div>

                                                <div class="col-sm-1 pl-sm-0">
                                                    <div class="form-group">
                                                        <select name="units" id="units" class="form-control" onchange="Searchgold()">
                                                            <option value="">หน่วย</option>
                                                            @foreach(App\Models\Gold\GoldUnits::all() as $item)
                                                                <option value="{{ $item->id }}"
                                                                        @if(!is_null(request()->units) && request()->units == $item->id) selected @endif>
                                                                    {{ $item->title }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2 pl-sm-0">
                                                    
                                                    <div class="form-group">
                                                        <input class="form-control" placeholder="ขนาด/ความยาว"
                                                            @if(!is_null(request()->size))value="{{ request()->size }}"@endif name="size" type="number" id="size" onkeyup="Searchgold()">
                                                    </div>
                                                </div>

                                                {{--<div class="col-sm-1">
                                                    <div class="form-group">
                                                        <select name="in_stock" id="in_stock" class="form-control">
                                                            <option value="1"
                                                                    @if(!is_null(request()->in_stock) && request()->in_stock == 1) selected @endif>มีสินค้า</option>
                                                            <option value="0"
                                                                    @if(!is_null(request()->in_stock) && request()->in_stock == 0) selected @endif>ขายแล้ว</option>
                                                        </select>
                                                    </div>
                                                </div>--}}
                                                {{--<div class="col-sm-1">
                                                    <div class="form-group">
                                                        <select name="price" id="price" class="form-control">
                                                            <option value="buy"
                                                                    @if(!is_null(request()->price) && request()->price == 'buy') selected @endif>ราคาซื้อ</option>
                                                            <option value="goldsmith"
                                                                    @if(!is_null(request()->price) && request()->price == 'goldsmith') selected @endif>ค่ากำเหน็ด</option>
                                                        </select>
                                                    </div>
                                                </div>--}}
                                                {{--<div class="col-sm-2">
                                                    <div class="form-group">
                                                        <input class="form-control form-control-sm" placeholder="ราคาขั้นต่ำ"
                                                            @if(!is_null(request()->min)) value="{{ request()->min }}" @endif name="min" type="number" id="input">
                                                    </div>
                                                </div>--}}
                                                {{--<div class="col-sm-2">
                                                    <div class="form-group">
                                                        <input class="form-control form-control-sm" placeholder="ราคาไม่เกิน"
                                                            @if(!is_null(request()->max)) value="{{ request()->max }}" @endif name="max" type="number" id="input">
                                                    </div>
                                                </div>--}}

                                                <div class="col-1 pr-sm-0">
                                                    <div class="form-group">
                                                        <button onclick="Searchgold()" type="button" class="btn btn-secondary waves-effect waves-float waves-light"><i class="ri-filter-2-line"></i>ค้นหา</button>
                                                    </div>
                                                </div>
                                                <div class="col-1 pr-sm-0">
                                                    <div class="form-group">
                                                        <button onclick="resetSearchgold()" type="button" class="btn btn-secondary waves-effect waves-float waves-light"><i class="ri-refresh-line"></i>รีเซ็ท</button>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-12">
                <div class="list-view">
                    <div class="checkout-items" id="gold_list_left">
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-12">
                <div class="list-view">
                    <div class="checkout-items" id="gold_list_right">
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-sm-12 col-12">
                <ul class="pagination start-links justify-content-center" id="pagination">
                </ul>
            </div>
        </div>

    </div>
</div>

        

    <div
        class="modal fade text-left"
        id="modal_view"
        tabindex="-1"
        role="dialog"
        aria-labelledby="myModalLabel1"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">รายละเอียดสินค้า</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="col-md-12 mb-3 mt-1">
                                <div class="row">
                                    <div class="col-md-3">
                                        <!-- get -->
                                        <span>หมวดหมู่สินค้า</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong id="category_id_view"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <!-- get -->
                                        <span>ประเภทสินค้า</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong id="subcategory_id_view"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <!-- get -->
                                        <span>ลายทอง</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong id='title_view'></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3" id="hideweight">
                                <div class="row">
                                    <div class="col-md-3">
                                        <!-- get -->
                                        <span>น้ำหนัก</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong id="weight_view"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>น้ำหนักจริง</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong id="weight_real_view"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ราคาต้นทุนเฉลี่ย</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong id="price_view"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ค่ากำเหน็ดเฉลี่ย</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong id="smith_view"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <!-- get -->
                                        <span>ขนาด/ความยาว</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong id='size_view'></strong>
                                    </div>
                                </div>
                            </div>                            
                            <div class="col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <!-- get -->
                                        <span>จำนวนคงเหลือ</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong id='amount_view'></strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="col-md-12 mt-1">
                                <div class="row">
                                    <div class="col-md-3">
                                        <!-- get -->
                                        <span>รูปภาพ</span>
                                    </div>                                    
                                    <div class="col-md-9" id="pic_view">
                                        <img/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                </div>
            </div>
        </div>
    </div>

    <div
        class="modal fade text-left"
        id="modal_gold_add"
        tabindex="-1"
        role="dialog"
        aria-labelledby="myModalLabel1"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">เพิ่มรายการสินค้า</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="preview_gold_add"></div>
                    <form id="form_gold_add">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="col-md-12 mb-2 mt-1">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>หมวดหมู่สินค้า</span>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="custom-control custom-radio mr-1">
                                                <input onchange="gold_category_form(this)" type="radio" id="gold_category_ninety_six" name="gold_category" value="1" class="custom-control-input" checked/>
                                                <label class="custom-control-label" for="gold_category_ninety_six">ทอง 96.5 %</label>
                                            </div>
                                            <div class="custom-control custom-radio mr-1">
                                                <input onchange="gold_category_form(this)" type="radio" id="gold_category_ninety_nine" name="gold_category" value="2" class="custom-control-input"/>
                                                <label class="custom-control-label" for="gold_category_ninety_nine">ทอง 99.99 %</label>
                                            </div>
                                            <div class="custom-control custom-radio mr-1">
                                                <input onchange="gold_category_form(this)" type="radio" id="gold_category_ninety" name="gold_category" value="3" class="custom-control-input" />
                                                <label class="custom-control-label" for="gold_category_ninety">ทอง 90 %</label>
                                            </div>
                                            <div class="custom-control custom-radio">
                                                <input onchange="gold_category_form(this)" type="radio" id="gold_category_other" name="gold_category" value="4" class="custom-control-input"/>
                                                <label class="custom-control-label" for="gold_category_other">อื่น ๆ</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2" id="gold_type_input">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ประเภททอง</span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="custom-control custom-radio mr-1">
                                                <input onchange="gold_type_form(this)" type="radio" class="custom-control-input" name="gold_type" value="1"  id="gold_type_ornament_add" checked />
                                                <label class="custom-control-label" for="gold_type_ornament_add">ทองรูปพรรณ</label>
                                            </div>
                                            <div class="custom-control custom-radio mr-1">
                                                <input onchange="gold_type_form(this)" type="radio" class="custom-control-input" name="gold_type" value="2" id="gold_type_bar_add" />
                                                <label class="custom-control-label" for="gold_type_bar_add">ทองแท่ง</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2" id="gold_sub_category_input">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ประเภทสินค้า</span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select class="form-control" id="gold_sub_category" name="gold_sub_category">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ลายทอง</span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="gold_pattern_add" name="gold_pattern"/>
                                            <div id="gold_pattern_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2" id="gold_weight_input">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>น้ำหนัก</span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="number" class="form-control" id="gold_weight_add" name="gold_weight"/>
                                            <div id="gold_weight_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <select class="form-control" id="gold_unit" name="gold_unit">
                                                @foreach(\App\Models\Gold\GoldUnits::query()->get() as $row)
                                                    <option value="{{ $row->id }}">{{ $row->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>น้ำหนักจริง</span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="number" class="form-control" id="gold_weight_actual_add" name="gold_weight_actual"/>
                                            <div id="gold_weight_actual_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <span>กรัม (g)</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ราคาต้นทุน</span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="number" class="form-control" id="gold_cost_add" name="gold_cost"/>
                                            <div id="gold_cost_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <span>บาท</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ค่ากำเหน็ด</span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="number" class="form-control" id="gold_smith_charge_add" name="gold_smith_charge"/>
                                            <div id="gold_smith_charge_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <span>บาท</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>รูปภาพสินค้า</span>
                                    </div>
                                    <div class="col-md-9" id="gold_image_input">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="col-md-12 mt-1" id="gold_list_input">
                                <div class="invoice-repeater">
                                    <div data-repeater-list="gold_list">
                                        <div data-repeater-item>
                                            <div class="row d-flex align-items-end">
                                                <div class="col-md-5 col-12">
                                                    <div class="form-group">
                                                        <label for="gold_size">ขนาด/ความยาว</label>
                                                        <input
                                                            type="number"
                                                            class="form-control"
                                                            name="gold_size"
                                                            aria-describedby="gold_size"
                                                        />
                                                    </div>
                                                </div>

                                                <div class="col-md-5 col-12">
                                                    <div class="form-group">
                                                        <label for="gold_amount">จำนวน/ชิ้น</label>
                                                        <input
                                                            type="number"
                                                            class="form-control"
                                                            name="gold_amount"
                                                            aria-describedby="gold_amount"
                                                        />
                                                    </div>
                                                </div>

                                                <div class="col-md-2 col-12 mb-50">
                                                    <div class="form-group">
                                                        <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete type="button">
                                                            <i data-feather="x" class="mr-25"></i>
                                                            <span>ลบ</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div id="gold_list_error_add" class="invalid-feedback" style="display:block;"></div>
                                            <button class="btn btn-icon btn-primary" type="button" data-repeater-create>
                                                <i data-feather="plus" class="mr-25"></i>
                                                <span>เพิ่ม</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mt-1" id="gold_amount_input">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>จำนวน</span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="number" class="form-control" id="gold_amount_add" name="gold_amount"/>
                                            <div id="gold_amount_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <span>ชิ้น</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btn_gold_add">บันทึก</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                </div>
            </div>
        </div>
    </div>

        <div class="modal fade gold_add" id="modal_add" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">เพิ่มจำนวนสินค้า</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="result_add"></div>
                    <form id="product_add" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-sm-12 col-lg-12">
                                <div class="iq-card">
                                    <div class="iq-card-body">
                                        @csrf
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group row">
                                                    <label for="price_income" class="control-label col-sm-3 align-self-center mb-0">
                                                        ราคาต้นทุน
                                                    </label>
                                                    <div class="col-sm-6">
                                                        <input type="number" id="price_income" name="price_income" min="0" step="0.01" class="form-control"
                                                            @if(isset($gold)) value="{{ $gold->price_income }}" @else value="{{ old('price_income') }}" @endif
                                                            placeholder=""  autocomplete="off">
                                                        @error('price_income')
                                                        <div class="invalid-feedback text-danger">
                                                            {{ $message }}
                                                        </div>
                                                        @enderror
                                                    </div>
                                                    <div class="col-sm-3 text-left">
                                                        บาท
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="price_goldsmith" class="control-label col-sm-3 align-self-center mb-0">
                                                        ค่ากำเหน็ด
                                                    </label>
                                                    <div class="col-sm-6">
                                                        <input type="number" id="price_goldsmith" name="price_goldsmith" min="0" step="0.01" class="form-control"
                                                            @if(isset($gold)) value="{{ $gold->price_goldsmith }}" @else value="{{ old('price_goldsmith') }}" @endif
                                                            placeholder=""  autocomplete="off">
                                                        @error('price_goldsmith')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>
                                                        @enderror
                                                    </div>
                                                    <div class="col-sm-3 text-left">
                                                        บาท
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="amount" class="control-label col-sm-3 align-self-center mb-0">
                                                        จำนวน
                                                    </label>
                                                    <div class="col-sm-6">
                                                        <input  type="number" id="amount" name="amount" min="0" step="1" class="form-control"
                                                                @if(isset($gold)) value="{{ $gold->amount_gold }}" @else value="{{ old('amount_gold') }}" @endif
                                                                placeholder=""  autocomplete="off">
                                                    </div>
                                                    <div class="col-sm-3 text-left">
                                                        ชิ้น
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="add" onclick="submitAdd()">บันทึก</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                </div>
            </div>
        </div>
    </div>

    <div
        class="modal fade text-left"
        id="edit_product"
        tabindex="-1"
        role="dialog"
        aria-labelledby="myModalLabel1"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">แก้ไขรายการสินค้า</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="result_edit"></div>
                    <form id="product_edit" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="col-md-12 mb-2 mt-1">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <span>หมวดหมู่สินค้า</span>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="custom-control custom-radio mr-1">
                                                    <input type="radio" id="customRadio" name="customRadio" value="1" class="custom-control-input" disabled=""/>
                                                    <label class="custom-control-label" for="customRadio1">ทอง 96.5 %</label>
                                                </div>
                                                <div class="custom-control custom-radio mr-1">
                                                    <input type="radio" id="customRadio" name="customRadio" value="2" class="custom-control-input" disabled=""/>
                                                    <label class="custom-control-label" for="customRadio2">ทอง 99.99 %</label>
                                                </div>
                                                <div class="custom-control custom-radio mr-1">
                                                    <input type="radio" id="customRadio" name="customRadio" value="3" class="custom-control-input" disabled=""/>
                                                    <label class="custom-control-label" for="customRadio3">ทอง 90 %</label>
                                                </div>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" id="customRadio" name="customRadio" value="4" class="custom-control-input" disabled=""/>
                                                    <label class="custom-control-label" for="customRadio4">อื่น ๆ</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-2">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <span>ประเภททอง</span>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="custom-control custom-radio mr-1">
                                                    <input type="radio" id="gold_type_edit" name="gold_type_edit" value="1" class="custom-control-input" disabled=""/>
                                                    <label class="custom-control-label" for="typeRadio1">ทองรูปพรรณ</label>
                                                </div>
                                                <div class="custom-control custom-radio mr-1">
                                                    <input type="radio" id="gold_type_edit" name="gold_type_edit" value="2" class="custom-control-input" disabled=""/>
                                                    <label class="custom-control-label" for="typeRadio2">ทองแท่ง</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 mb-2">
                                    <div class="row" id="divsub">
                                        <!-- <div class="col-md-3">
                                            <span>ประเภทสินค้า</span>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select id="subcategory_id_edit" name="subcategory_id_edit" class="form-control" >
                                                        @foreach(App\Models\Gold\GoldSubCategory::where('id', '<>', '0')->get() as $item)
                                                            <option value="{{ $item->id }}"
                                                                    @if(isset($gold) && $item->id == $gold->subcategory_id) selected @endif>
                                                                {{ $item->title }}</option>
                                                        @endforeach
                                                </select>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>

                                <div class="col-md-12 mb-2">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <span>ลายทอง</span>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" id="title_edit" name="title_edit"
                                                @if(isset($gold)) value="{{ $gold->title }}" @else value="{{ old('title') }}" @endif
                                                placeholder="หัวใจโปร่ง"  autocomplete="off" />
                                            @error('title')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 mb-2" id="divweight">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <span>น้ำหนัก</span>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" id="weight_edit" name="weight_edit" value="" placeholder="" autocomplete="off"/>
                                        </div>
                                        <div class="col-sm-3">
                                            <!-- <select id="unit_id_edit" name="unit_id_edit" class="form-control">
                                                <option value="1">
                                                    สลึง</option>
                                                <option value="2">
                                                    บาท</option>
                                            </select> -->
                                            <select id="unit_id_edit" name="unit_id_edit" class="form-control" >
                                                @foreach(DB::table('204_GOLDS_UNIT')->get() as $item)
                                                    <option value="{{ $item->id }}"
                                                            @if(isset($gold) && $item->id == $gold->unit_id) selected @endif>
                                                        {{ $item->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 mb-2">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <span>น้ำหนักจริง</span>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" id="weight_real_edit" name="weight_real_edit"/>
                                        </div>
                                        <div class="col-md-3">
                                            <span>กรัม (g)</span>
                                        </div>
                                    </div>
                                </div>                  

                                <div class="col-md-12 mb-2">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <span>ขนาด/ความยาว</span>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" id="size_edit" name="size_edit"
                                                placeholder=""/>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="iq-card">
                                    <div class="iq-card-body">
                                        <div class="row">
                                            <div id="display_gold" name="display_gold" style="width: 90%;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="edit" onclick="submitEdit()">บันทึก</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                </div>
            </div>
        </div>
    </div>

    <div
        class="modal fade text-left"
        id="delete_product"
        tabindex="-1"
        role="dialog"
        aria-labelledby="myModalLabel1"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">ลบสินค้า - <span id="data_title"></span></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="result_delete"></div>
                    <form id="product_delete">
                        @csrf
                        <div class="iq-card-body">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <span>ช่วงเวลาที่เพิ่มสินค้าที่ต้องการลบ</span>
                                </div>
                                <div class="col-sm-4">
                                    <input type="date" id="date_delete_start" name="date_delete_start" class="form-control basic" value="" placeholder="YYYY-MM-DD" autocomplete="off" onchange="getGoldDelete()" />
                                </div>
                                <div class="col-sm-4">
                                    <input type="date" id="date_delete_end" name="date_delete_end" class="form-control basic" value="" placeholder="YYYY-MM-DD" autocomplete="off" onchange="getGoldDelete()" />
                                </div>
                                <input type="hidden" id="gold_id_delete" name="gold_id_delete" value="" />

                                <div class="container">                                
                                    <div id="result_sku">                                
                                    </div>                                
                                </div>

                            </div>
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-primary" id="update">แก้ไข</button> -->
                    <button type="button" class="btn btn-primary" id="delete">บันทึก</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('vendor-script')
    <!-- Vendor js files -->
    <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/spinner/jquery.bootstrap-touchspin.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/repeater/jquery.repeater.min.js')) }}"></script>
@endsection

@section('page-script')
    <!-- Page js files -->
    
    <script src="{{ asset('js/dropify/dist/js/dropify.js') }}"></script>
    <script src="{{ asset(mix('js/scripts/pages/app-ecommerce-checkout.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-repeater.js')) }}"></script>
    <script src="{{ asset('js/validate/jquery.validate.js') }}"></script>
    <!-- <script src="{{ asset('js/validate/jquery.validate.js') }}"></script> -->
    
    <script>
        $(document).ready(function () {
            $('#modal_gold_add').on('show.bs.modal', function() {
                gold_category_form({ value: '1' });
                gold_type_form({ value: '1' });
                gold_sub_category(1);

                $("#gold_image_input").html("<input type=\"file\" class=\"dropify\" name=\"gold_image\" id=\"gold_image_add\" data-width=\"200\" data-height=\"150\">\n" +
                    "<div id=\"gold_image_error_add\" class=\"invalid-feedback\" style=\"display:block;\"></div>");

                $('.dropify').dropify({
                    messages: {
                        'default': 'Drag and drop a file here or click',
                        'replace': 'Drag and drop or click to replace',
                        'remove': 'Remove',
                        'error': 'Ooops, something wrong happended.'
                    },
                    tpl: {
                        wrap:            '<div class="dropify-wrapper"></div>',
                        loader:          '<div class="dropify-loader"></div>',
                        message:         '<div class="dropify-message"><span class="file-icon" /> <p></p></div>',
                        preview:         '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message"></p></div></div></div>',
                        filename:        '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
                        clearButton:     '<button type="button" class="dropify-clear">ลบ</button>',
                        errorLine:       '<p class="dropify-error"></p>',
                        errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
                    }
                });
            });

            $('#modal_gold_add').on('hide.bs.modal', function() {
                $('#form_gold_add')[0].reset();

                removeValidation('add');
            });

            $('#btn_gold_add').click(function (e) {
                $('#btn_gold_add').empty();
                $('#btn_gold_add').attr("disabled", true);
                $('#btn_gold_add').append("<div class=\"spinner-border spinner-grow-sm text-light\" role=\"status\"><span class=\"sr-only\">Loading...</span></div> กำลังบันทึกข้อมูล");

                removeValidation('add');

                e.preventDefault();
                var form = $('#form_gold_add')[0];
                var data = new FormData(form);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{url('/gold-store')}}',
                    type: "POST",
                    enctype: 'multipart/form-data',
                    dataType: 'JSON',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (res) {

                        if(res.error == 0){
                            $('#preview_gold_add').append("<div class=\"alert alert-success\" role=\"alert\">\n" +
                                "<h4 class=\"alert-heading\">เพิ่มข้อมูลเรียบร้อย</h4>\n" +
                                "</div>");
                            setTimeout(function(){
                                $('#modal_gold_add').modal('hide');

                                // $('#table').dataTable().fnClearTable();
                                // $('#table').dataTable().fnDestroy();
                                // listUser();
                            }, 1000);
                        }else{
                            $('#preview_gold_add').append("<div class=\"alert alert-danger\" role=\"alert\">\n" +
                                "<h4 class=\"alert-heading\">พบข้อผิดพลาด</h4>\n" +
                                "</div>");
                            $.each(res.messages, function (k,v) {
                                $('#'+ k + '_add').addClass('is-invalid');
                                $('#'+ k + '_error_add').html(v);
                            });
                        }

                        $('#btn_gold_add').empty();
                        $('#btn_gold_add').append("บันทึก");
                        $('#btn_gold_add').attr("disabled",false);
                    }
                });
            });

            

            $('#modal_view').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget);
                $.get('{{ url('/get-gold?id=') }}' + button.data('id'), function(res) {
                    // alert(' Id: '+res.data.id + ' Title: '+ res.data.title + ' Size: '+ res.data.size + ' price: '+ res.price);
                    //alert(' Id: '+res.data.id + ' Title: '+ res.data.title + ' Size: '+ res.data.size + ' price: '+ res.price);
                    // console.log('ราคา : ' + res.price);
                    // alert(res.price);

                    $('#title_view').html(res.data.title);
                    $('#weight_real_view').html(res.data.weight_real + ' กรัม (g)');
                    // $('#weight_view').html(res.data.weight + ' บาท');

                    if(res.data.category_id == 1){
                        $('#hideweight').attr("hidden",false);
                        $('#weight_view').html(res.data.weight + ' ' + res.data.units.title);
                        // + ' ' + res.unit.title
                    }else{
                        $('#hideweight').attr("hidden",true);
                    }

                    $('#price_view').html(res.price + ' บาท');
                    $('#amount_view').html(res.amount + ' ชิ้น');
                    $('#smith_view').html(res.smith + ' บาท');

                    // $('#size_view').html(res.data.size);

                    // $('#category_id_view').html(res.data.category_id);

                    if(res.data.category_id == 1){
                            $('#category_id_view').html("ทอง 96.5 %");
                        }else if(res.data.category_id == 2){
                            $('#category_id_view').html("ทอง 99.99 %");
                        }else if(res.data.category_id == 3){
                            $('#category_id_view').html("ทอง 90 %");
                        }else {
                            $('#category_id_view').html("อื่น ๆ");
                        }

                        if (res.data.gold_type == 1){
                            $('#subcategory_id_view').html(res.subcategory.title + ' / ทองรูปพรรณ');
                        }else if (res.data.gold_type == 2){
                            $('#subcategory_id_view').html('ทองคำแท่ง');
                        }else {
                            $('#subcategory_id_view').html(res.subcategory.title);
                        }

                    if(res.data.pic != null){
                            $('#pic_view').html('<img src="{{ asset("_uploads/_products") }}/' + res.data.pic + '" style="width: 300px;" />');
                        }else{
                            $('#pic_view').html('<img src="{{ asset("assets/images/default.jpg") }}" style="width: 300px;" />');
                        }

                    // if(res.data.size != null){
                    //     $('#size_view').html(res.data.size);
                    // }else{
                    //     $('#size_view').html("-");
                    // }

                    res.data.size != null ? $('#size_view').html(res.data.size) : $('#size_view').html("-");

                    // alert(res.data.weight);
                    // if(res.data.weight != null){
                    //     if(res.data.unit_id == 1){
                    //         $('#hideweight').attr("hidden",false);
                    //         $('#weight_view').html(res.data.weight + ' สลิง');
                    //     }else if(res.data.unit_id == 2){
                    //         $('#hideweight').attr("hidden",false);
                    //         $('#weight_view').html(res.data.weight + ' บาท');
                    //     }else   {
                    //         $('#hideweight').attr("hidden",false);
                    //         $('#weight_view').html(res.data.weight + ' g');
                    //     }
                    //     // + ' ' + res.unit.title
                    // }else{
                    //     $('#hideweight').attr("hidden",true);
                    // }

                    // $('#price_income_view').html(res.price + ' บาท');
                    

                    


                        

                //     if(res.error == false){
                //         if(res.data.category_id == 1){
                //             $('#category_id_view').html("ทอง 96.5 %");
                //         }else if(res.data.category_id == 2){
                //             $('#category_id_view').html("ทอง 99.99 %");
                //         }else if(res.data.category_id == 3){
                //             $('#category_id_view').html("ทอง 90 %");
                //         }else {
                //             $('#category_id_view').html("อื่น ๆ");
                //         }

                //         if (res.data.gold_type == 1){
                //             $('#subcategory_id_view').html(res.subcategory.title + ' / ทองรูปพรรณ');
                //         }else if (res.data.gold_type == 2){
                //             $('#subcategory_id_view').html('ทองคำแท่ง');
                //         }else {
                //             $('#subcategory_id_view').html(res.subcategory.title);
                //         }

                //         // $('#subcategory_id_view').html(res.subcategory + ' / ' + res.category);
                //         $('#title_view').html(res.data.title);

                //         if(res.data.weight != null){
                //             $('#hideweight').attr("hidden",false);
                //             $('#weight_view').html(res.data.weight + ' ' + res.unit.title);
                //         }else{
                //             $('#hideweight').attr("hidden",true);
                //         }

                //         $('#weight_real_view').html(res.data.weight_real + ' กรัม (g)');
                //         $('#price_income_view').html(res.price + ' บาท');
                //         $('#price_goldsmith_view').html(res.smith + ' บาท');
                //         $('#size_view').html(res.data.size);
                //         $('#amount_view').html(res.amount + ' ชิ้น');

                //         if(res.data.pic != null){
                //             $('#pic_view').html('<img src="{{ asset("_Uploads/_Products") }}/' + res.data.pic + '" style="max-width:100%;" />');
                //         }else{
                //             $('#pic_view').html('<img src="{{ asset("assets/images/default.jpg") }}" style="max-width:100%;" />');
                //         }

                //     }
                
                });
            });

            $('#edit_product').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget);
                $('#edit').val(button.data('id'));
                $('#result_edit').empty();
                $('#type').attr("hidden", false);
                $.get('{{ url('/get-gold?id=') }}' + button.data('id'), function(res) {
                    if(res.error == false){

                        $('input[name^="customRadio"][value="'+res.data.category_id+'"').prop('checked',true);

                        $('input[name^="gold_type_edit"][value="'+res.data.gold_type+'"').prop('checked',true);
                        
                        if (res.data.category_id != 1){
                            $('#divweight').attr("hidden",true);

                            if(res.data.gold_type == 2) {
                                $('#divweight').attr("hidden", true);
                                $('#divsub').attr("hidden", true);
                            }else if(res.data.gold_type == null){
                                $('#type').attr("hidden", true);
                            }
                        }else {
                            if(res.data.gold_type == 2) {
                                // $('#divweight').attr("hidden", true);
                                $('#divsub').attr("hidden", true);
                            }
                        }

                        var html = "<div class=\"col-md-3\">\n" +
                            "<span>ประเภทสินค้า</span>\n" +
                            "</div>\n" +
                            "<div class=\"col-md-6\">\n" +
                            "<div class=\"form-group\">\n" +
                            "          <select id=\"subcategory_id_edit\" name=\"subcategory_id_edit\" class=\"form-control\" >\n" ;
                                    if(res.data.category_id != 4){
                                        $.each(res.category1, function (index, row) {
                                            html += "<option value=\"" + row.id + "\">" + row.title + "</option>\n";
                                        });
                                    }else {
                                        $.each(res.category4, function (index, row) {
                                            html += "<option value=\"" + row.id + "\">" + row.title + "</option>\n";
                                        });
                                    }
                            html += "</select>\n" +
                            "</div>\n" +
                            "</div>";

                        $('#divsub').html(html);

                        $('#subcategory_id_edit').val(res.data.subcategory_id);
                        $('#title_edit').val(res.data.title);
                        $('#weight_edit').val(res.data.weight);
                        $('#unit_id_edit').val(res.data.unit_id);
                        $('#weight_real_edit').val(res.data.weight_real);
                        $('#size_edit').val(res.data.size);
                        $('#pic_edit').addClass('dropify');
                        $('#pic_edit').attr('data-default-file', '{{ asset("_Uploads/_Products") }}/' + res.data.pic);
                        $('.dropify').dropify();

                        if(res.data.pic != null) {
                            {{--$("#display").html("<label for=\"pic\" class=\"control-label col-sm-3 align-self-center mb-0\">รูปภาพ</label>\n" +--}}
                            {{--    "<input type=\"file\" class=\"dropify\" name=\"pic\" id=\"pic_edit\" data-default-file=\"{{ asset('_Uploads/_Products') }}/" + res.data.pic + "\">");--}}

                            $("#display_gold").html(
                                "<div class=\"col-sm-12\">\n" +
                                " <div class=\"form-group row\">" +
                                "<label for=\"title\" class=\"control-label col-sm-4 align-self-center mb-0\">\n" +
                                "   รูปภาพสินค้า\n" +
                                "</label>\n" +
                                "<div class=\"col-sm-8\">\n" +
                                "  <div class=\"custom-file\">\n" +
                                "    <input type=\"file\" class=\"dropify\" id=\"pic_edit\"  name=\"pic\" data-default-file=\"{{ asset('_uploads/_products') }}/" + res.data.pic + "\">\n" +
                                "  </div>\n" + 
                                "</div>" +
                                "</div>\n" +
                                "</div>"
                            );
                            $('.dropify').dropify();
                        }
                        else {
                            $("#display_gold").html(
                                "<div class=\"col-sm-12\">\n" +
                                " <div class=\"form-group row\">" +
                                "<label for=\"title\" class=\"control-label col-sm-4 align-self-center mb-0\">\n" +
                                "   รูปภาพสินค้า\n" +
                                "</label>\n" +
                                "<div class=\"col-sm-8\">\n" +
                                "  <div class=\"custom-file\">\n" +
                                "    <input type=\"file\" class=\"dropify\" id=\"pic_edit\"  name=\"pic\">\n" +
                                "  </div>\n" +
                                "</div>" +
                                "</div>\n" +
                                "</div>"
                            );
                            $('.dropify').dropify();
                        }
                    }
                });
            });

            $("#product_edit").validate({
                submitHandler: function(form) {
                    $('#result_edit').empty();
                    //e.preventDefault();
                    var form = $('#product_edit')[0];
                    var data = new FormData(form);
                    // alert($('#edit').val());
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{{ url("gold-edit") }}/' +$('#edit').val(),
                        type: 'POST',
                        enctype: 'multipart/form-data',
                        dataType: 'JSON',
                        data: data,
                        processData: false,
                        contentType: false,
                        cache: false,
                        success: function (res) {
                            if (res.error == false) {
                                $('#result_edit').append('<div class="alert alert-success" role="alert"><h4 class="alert-heading">' + res.messages + '</h4></div>');
                                location.reload(true);
                            } else {
                                $('#result_edit').append('<div class="alert alert-danger" role="alert"><h4 class="alert-heading">' + res.messages + '</h4></div>');
                            }
                        }
                    });
                },
                rules: {
                    title: {
                        required: true
                    },
                    weight: {
                        required: true,
                        number: true,
                        min: 0
                    },
                    weight_real: {
                        required: true,
                        number: true,
                        min: 0
                    },
                    size: {
                        required: true,
                        number: true,
                        min: 0
                    }
                },
                messages: {
                    title: {
                        required: "โปรดระบุลายทอง"
                    },
                    weight: {
                        required: "โปรดระบุน้ำหนัก",
                        number: "โปรดระบุน้ำหนักให้ถูกต้อง",
                        min: "โปรดระบุน้ำหนักให้ถูกต้อง"
                    },
                    weight_real: {
                        required: "โปรดระบุน้ำหนักจริง",
                        number: "โปรดระบุน้ำหนักจริงให้ถูกต้อง",
                        min: "โปรดระบุน้ำหนักจริงให้ถูกต้อง"
                    },
                    size: {
                        required: "โปรดระบุขนาด",
                        number: "โปรดระบุขนาดให้ถูกต้อง",
                        min: "โปรดระบุขนาดให้ถูกต้อง"
                    }
                }
            });

            $('#edit_product').on('hide.bs.modal', function() {
                // location.reload(true);
                $('#divweight').attr("hidden",false);
                $('#divsub').attr("hidden",false);
            });

            $('#modal_add').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget);
                $('#product_add')[0].reset();
                $('#add').val(button.data('id'));
                $('#result_add').empty();
            });

            $("#product_add").validate({
                submitHandler: function(form) {
                    $('#result_add').empty();
                    //e.preventDefault();
                    var form = $('#product_add')[0];
                    var data = new FormData(form);
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{{ url('gold-add') }}/' +$('#add').val(),
                        
                        type: 'POST',
                        enctype: 'multipart/form-data',
                        dataType: 'JSON',
                        data: data,
                        processData: false,
                        contentType: false,
                        cache: false,
                        success: function (res) {
                            if (res.error == false) {
                                $('#result_add').append('<div class="alert alert-success" role="alert"><h4 class="alert-heading">' + res.messages + '</h4></div>');
                                $('#product_add')[0].reset();
                                location.reload(true);
                            } else {
                                $('#result_add').append('<div class="alert alert-danger" role="alert"><h4 class="alert-heading">' + res.messages+ '</h4></div>');
                            }
                        }
                    });
                },
                rules: {
                    price_income: {
                        required: true,
                        number: true,
                        min: 0
                    },
                    price_goldsmith: {
                        required: true,
                        number: true,
                        min: 0
                    },
                    amount: {
                        required: true,
                        number: true,
                        min: 0
                    }
                },
                messages: {
                    price_income: {
                        required: "โปรดระบุราคาต้นทุน",
                        number: "โปรดระบุราคาต้นทุนให้ถูกต้อง",
                        min: "โปรดระบุราคาต้นทุนให้ถูกต้อง"
                    },
                    price_goldsmith: {
                        required: "โปรดระบุค่ากำเหน็ด",
                        number: "โปรดระบุค่ากำเหน็ดให้ถูกต้อง",
                        min: "โปรดระบุค่ากำเหน็ดให้ถูกต้อง"
                    },
                    amount: {
                        required: "โปรดระบุจำนวน",
                        number: "โปรดระบุจำนวนให้ถูกต้อง",
                        min: "โปรดระบุให้ถูกต้อง",
                        step: "โปรดระบุจำนวนให้ถูกต้อง"
                    }
                }
            });

            $('#delete_product').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget);
                $('#product_delete')[0].reset();
                $('#gold_id_delete').val(button.data('id'));
                $('#result_delete').empty();
                $('#result_sku').empty();
                $('#data_title').html(button.data('title'));
            });

            $('#delete').click(function (e) {
                $('#result_delete').empty();
                e.preventDefault();
                var form = $('#product_delete')[0];
                var data = new FormData(form);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url('get-delete') }}',
                    type: 'POST',
                    enctype: 'multipart/form-data',
                    dataType: 'JSON',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (res) {
                        if (res.error == false) {
                            $('#result_delete').append('<div class="alert alert-success" role="alert"><h4 class="alert-heading">' + res.message + '</h4></div>');
                            $('#product_delete')[0].reset();
                            location.reload(true);
                        } else {
                            $('#result_delete').append('<div class="alert alert-danger" role="alert"><h4 class="alert-heading">' + res.message + '</h4></div>');
                        }
                    }
                });
        });


            gold_stock(1, '', '', '', '', '', '');
        });


        function submitAdd() {
            $("#product_add").submit();
        }        

        function submitEdit() {
            $("#product_edit").submit();
        }

        function gold_category_form(el) {
            removeValidation('add');
            resetForm('add');

            switch (el.value) {
                case '1':
                    $('#gold_type_input').css('display', 'block');
                    $('#gold_weight_input').css('display', 'block');
                    gold_sub_category(1);
                    break;
                case '2':
                    $('#gold_type_input').css('display', 'block');
                    $('#gold_weight_input').css('display', 'none');
                    gold_sub_category(1);
                    break;
                case '3':
                    $('#gold_type_input').css('display', 'block');
                    $('#gold_weight_input').css('display', 'none');
                    gold_sub_category(1);
                    break;
                case '4':
                    $('#gold_type_input').css('display', 'none');
                    $('#gold_weight_input').css('display', 'none');
                    gold_sub_category(4);
                    break;
                default:
                    break;
            }
        }

        function gold_type_form(el) {
            removeValidation('add');

            switch (el.value) {
                case '1':
                    $('#gold_sub_category_input').css('display', 'block');
                    $('#gold_list_input').css('display', 'block');
                    $('#gold_amount_input').css('display', 'none');
                    break;
                case '2':
                    $('#gold_sub_category_input').css('display', 'none');
                    $('#gold_list_input').css('display', 'none');
                    $('#gold_amount_input').css('display', 'block');
                    break;
                default:
                    break;
            }
        }

        function gold_sub_category(type) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('/gold-sub-category?type=') }}' + type,
                type: "GET",
                enctype: 'multipart/form-data',
                dataType: 'JSON',
                async: false,
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    $('#gold_sub_category').empty();
                    $.each(res.data, function (index, sub_category) {
                        $('#gold_sub_category').append('<option value="' + sub_category.id + '">' + sub_category.title + '</option>');
                    });
                }
            });
        }

        function removeValidation(status) {
            $('#preview_gold_' + status).empty();
            $('#gold_pattern_' + status).removeClass('is-invalid');
            $('#gold_weight_' + status).removeClass('is-invalid');
            $('#gold_weight_actual_' + status).removeClass('is-invalid');
            $('#gold_cost_' + status).removeClass('is-invalid');
            $('#gold_smith_charge_' + status).removeClass('is-invalid');
            $('#gold_image_' + status).removeClass('is-invalid');
            $('#gold_list_' + status).removeClass('is-invalid');
            $('#gold_amount_' + status).removeClass('is-invalid');

            $('#gold_pattern_error_' + status).html("");
            $('#gold_weight_error_' + status).html("");
            $('#gold_weight_actual_error_' + status).html("");
            $('#gold_cost_error_' + status).html("");
            $('#gold_smith_charge_error_' + status).html("");
            $('#gold_image_error_' + status).html("");
            $('#gold_list_error_' + status).html("");
            $('#gold_amount_error_' + status).html("");
        }

        function resetForm(status) {
            $('#gold_type_ornament_' + status).prop("checked", true);
            gold_type_form({ value: '1' });
        }

        function Searchgold() {
            const category = $('#category').val();
            const subcategory = $('#subcategory').val();
            const txt = $('#txt').val();
            const weight = $('#weight').val();
            const units = $('#units').val();
            const size = $('#size').val();            
            gold_stock(1, category, subcategory, txt, weight, units, size);
        }

        function resetSearchgold() {
            $('#gold_filter')[0].reset();
            Searchgold();
        }

        function gold_stock(page, category, subcategory, txt, weight, units, size) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('/gold-stock?page=') }}' + page + '&category=' + category + '&subcategory=' + subcategory + '&txt=' + txt + '&units=' + units + '&weight=' + weight + '&size=' + size ,
                type: "GET",
                enctype: 'multipart/form-data',
                dataType: 'JSON',
                async: false,
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    $('#gold_list_left').empty();
                    $('#gold_list_right').empty();

                    $.each(res.data.data, function (index, item) {
                        let column = ((index % 2) === 0 ? '#gold_list_left' : '#gold_list_right');

                        if(item.category_id === 1 && item.gold_type === 1) {
                            $(column).append('<div class="card ecommerce-card" style="grid-template-columns: 1fr 3fr 2fr;">\n' +
                                '                            <div class="item-img ml-2">\n' +
                                '                                <img src="{{ asset('_uploads/_products') }}/' + item.pic + '" alt="img-placeholder" style="width: 160px;" />\n' +
                                '                            </div>\n' +
                                '                            <div class="card-body">\n' +
                                '                                <div class="item-name">\n' +
                                '                                    <h4 class="mb-0 text-body bold">' + item.title + '</h4>\n' +
                                '                                    <span class="item-company text-primary">' + item.subcategory.title + '</span>\n' +
                                '                                    <div class="badge badge-pill badge-warning">' + item.category.title + '</div>\n' +
                                '                                </div>\n' +
                                '                                <span class="item-company">หนัก: ' + item.weight + ' ' + item.units.title + ' </span>\n' +
                                '                                <span class="item-company">ขนาด/ความยาว: ' + item.size + '</span>\n' +
                                '                            </div>\n' +
                                '                            <div class="item-options text-center">\n' +
                                '                                <div class="item-wrapper">\n' +
                                '                                    <div class="item-cost">\n' +
                                '                                        <h4 class="item-price">฿ ' + item.price.income + '</h4>\n' +
                                '                                        <p class="card-text shipping">\n' +
                                '                                            <span class="badge badge-pill badge-light-success">คงเหลือ ' + item.stock.count + ' ชิ้น</span>\n' +
                                '                                        </p>\n' +
                                '                                    </div>\n' +
                                '                                </div>\n' +
                                '                                <div class="demo-inline-spacing">\n' +
                                '                                    <button type="button" class="btn btn-icon btn-warning btn-sm" data-toggle="modal" data-id="' + item.id + '" data-target="#edit_product"  style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="edit-2"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-info btn-sm" data-toggle="modal" data-id="' + item.id + '" data-target="#modal_view" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="eye"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-danger btn-sm" data-toggle="modal" data-id="' + item.id + '" data-target="#delete_product" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="trash-2"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-secondary btn-sm" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="printer"></i>\n' +
                                '                                    </button>\n' +

                                '                                    <button type="button" class="btn btn-icon btn-info btn-sm" data-toggle="modal" data-id="' + item.id + '"  data-target="#modal_add" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="plus-circle"></i>\n' +
                                '                                    </button>\n' +

                                '                                </div>\n' +
                                '                            </div>\n' +
                                '                        </div>');
                        }
                        else if(item.category_id === 1 && item.gold_type === 2) {
                            $(column).append('<div class="card ecommerce-card" style="grid-template-columns: 1fr 3fr 2fr;">\n' +
                                '                            <div class="item-img ml-2">\n' +
                                '                                <img src="{{ asset('_uploads/_products') }}/' + item.pic + '" alt="img-placeholder" style="width: 160px;" />\n' +
                                '                            </div>\n' +
                                '                            <div class="card-body">\n' +
                                '                                <div class="item-name">\n' +
                                '                                    <h4 class="mb-0 text-body bold">' + item.title + '</h4>\n' +
                                '                                    <span class="item-company text-primary">ทองแท่ง</span>\n' +
                                '                                    <div class="badge badge-pill badge-warning">' + item.category.title + '</div>\n' +
                                '                                </div>\n' +
                                '                                <span class="item-company">หนัก: ' + item.weight_real + ' กรัม(g)</span>\n' +
                                '                                <span class="item-company">ขนาด/ความยาว: -</span>\n' +
                                '                            </div>\n' +
                                '                            <div class="item-options text-center">\n' +
                                '                                <div class="item-wrapper">\n' +
                                '                                    <div class="item-cost">\n' +
                                '                                        <h4 class="item-price">฿ ' + item.price.income + '</h4>\n' +
                                '                                        <p class="card-text shipping">\n' +
                                '                                            <span class="badge badge-pill badge-light-success">คงเหลือ ' + item.stock.count + ' ชิ้น</span>\n' +
                                '                                        </p>\n' +
                                '                                    </div>\n' +
                                '                                </div>\n' +
                                '                                <div class="demo-inline-spacing">\n' +
                                '                                    <button type="button" class="btn btn-icon btn-warning btn-sm" data-toggle="modal" data-id="' + item.id + '" data-target="#edit_product"  style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="edit-2"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-info btn-sm" data-toggle="modal" data-id="' + item.id + '" data-target="#modal_view" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="eye"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-danger btn-sm" data-toggle="modal" data-id="' + item.id + '" data-target="#delete_product" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="trash-2"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-secondary btn-sm" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="printer"></i>\n' +
                                '                                    </button>\n' +

                                '                                    <button type="button" class="btn btn-icon btn-info btn-sm" data-toggle="modal" data-id="' + item.id + '"  data-target="#modal_add" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="plus-circle"></i>\n' +
                                '                                    </button>\n' +

                                '                                </div>\n' +
                                '                            </div>\n' +
                                '                        </div>');
                        }
                        else if((item.category_id === 2 || item.category_id === 3) && item.gold_type === 1) {
                            $(column).append('<div class="card ecommerce-card" style="grid-template-columns: 1fr 3fr 2fr;">\n' +
                                '                            <div class="item-img ml-2">\n' +
                                '                                <img src="{{ asset('_uploads/_products') }}/' + item.pic + '" alt="img-placeholder" style="width: 160px;" />\n' +
                                '                            </div>\n' +
                                '                            <div class="card-body">\n' +
                                '                                <div class="item-name">\n' +
                                '                                    <h4 class="mb-0 text-body bold">' + item.title + '</h4>\n' +
                                '                                    <span class="item-company text-primary">' + item.subcategory.title + '</span>\n' +
                                '                                    <div class="badge badge-pill badge-warning">' + item.category.title + '</div>\n' +
                                '                                </div>\n' +
                                '                                <span class="item-company">หนัก: ' + item.weight_real + ' กรัม(g)</span>\n' +
                                '                                <span class="item-company">ขนาด/ความยาว: ' + item.size + '</span>\n' +
                                '                            </div>\n' +
                                '                            <div class="item-options text-center">\n' +
                                '                                <div class="item-wrapper">\n' +
                                '                                    <div class="item-cost">\n' +
                                '                                        <h4 class="item-price">฿ ' + item.price.income + '</h4>\n' +
                                '                                        <p class="card-text shipping">\n' +
                                '                                            <span class="badge badge-pill badge-light-success">คงเหลือ ' + item.stock.count + ' ชิ้น</span>\n' +
                                '                                        </p>\n' +
                                '                                    </div>\n' +
                                '                                </div>\n' +
                                '                                <div class="demo-inline-spacing">\n' +
                                '                                    <button type="button" class="btn btn-icon btn-warning btn-sm" data-toggle="modal" data-id="' + item.id + '"  data-target="#edit_product"  style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="edit-2"></i>\n' +
                                '                                    </button>\n' +
                                // '                                    <button type="button" class="btn btn-icon btn-warning btn-sm" data-toggle="modal" data-id="' + item.id + '"  data-target="#edit_product"  style="margin-right: 2px;">\n' +
                                // '                                        <i data-feather="edit-2"></i>\n' +
                                // '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-info btn-sm" data-toggle="modal" data-id="' + item.id + '" data-target="#modal_view" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="eye"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-danger btn-sm" data-toggle="modal" data-id="' + item.id + '" data-target="#delete_product" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="trash-2"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-secondary btn-sm" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="printer"></i>\n' +
                                '                                    </button>\n' +

                                '                                    <button type="button" class="btn btn-icon btn-info btn-sm" data-toggle="modal" data-id="' + item.id + '"  data-target="#modal_add" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="plus-circle"></i>\n' +
                                '                                    </button>\n' +


                                '                                </div>\n' +
                                '                            </div>\n' +
                                '                        </div>');
                        }
                        else if((item.category_id === 2 || item.category_id === 3) && item.gold_type === 2) {
                            $(column).append('<div class="card ecommerce-card" style="grid-template-columns: 1fr 3fr 2fr;">\n' +
                                '                            <div class="item-img ml-2">\n' +
                                '                                <img src="{{ asset('_uploads/_products') }}/' + item.pic + '" alt="img-placeholder" style="width: 160px;" />\n' +
                                '                            </div>\n' +
                                '                            <div class="card-body">\n' +
                                '                                <div class="item-name">\n' +
                                '                                    <h4 class="mb-0 text-body bold">' + item.title + '</h4>\n' +
                                '                                    <span class="item-company text-primary">ทองแท่ง</span>\n' +
                                '                                    <div class="badge badge-pill badge-warning">' + item.category.title + '</div>\n' +
                                '                                </div>\n' +
                                '                                <span class="item-company">หนัก: ' + item.weight_real + ' กรัม(g)</span>\n' +
                                '                                <span class="item-company">ขนาด/ความยาว: -</span>\n' +
                                '                            </div>\n' +
                                '                            <div class="item-options text-center">\n' +
                                '                                <div class="item-wrapper">\n' +
                                '                                    <div class="item-cost">\n' +
                                '                                        <h4 class="item-price">฿ ' + item.price.income + '</h4>\n' +
                                '                                        <p class="card-text shipping">\n' +
                                '                                            <span class="badge badge-pill badge-light-success">คงเหลือ ' + item.stock.count + ' ชิ้น</span>\n' +
                                '                                        </p>\n' +
                                '                                    </div>\n' +
                                '                                </div>\n' +
                                '                                <div class="demo-inline-spacing">\n' +
                                '                                    <button type="button" class="btn btn-icon btn-warning btn-sm" data-toggle="modal" data-id="' + item.id + '"  data-target="#edit_product"  style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="edit-2"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-info btn-sm" data-toggle="modal" data-id="' + item.id + '" data-target="#modal_view" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="eye"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-danger btn-sm" data-toggle="modal" data-id="' + item.id + '" data-target="#delete_product" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="trash-2"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-secondary btn-sm" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="printer"></i>\n' +
                                '                                    </button>\n' +

                                '                                    <button type="button" class="btn btn-icon btn-info btn-sm" data-toggle="modal" data-id="' + item.id + '"  data-target="#modal_add" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="plus-circle"></i>\n' +
                                '                                    </button>\n' +

                                '                                </div>\n' +
                                '                            </div>\n' +
                                '                        </div>');
                        }
                        else if(item.category_id === 4) {
                            $(column).append('<div class="card ecommerce-card" style="grid-template-columns: 1fr 3fr 2fr;">\n' +
                                '                            <div class="item-img ml-2">\n' +
                                '                                <img src="{{ asset('_uploads/_products') }}/' + item.pic + '" alt="img-placeholder" style="width: 160px;" />\n' +
                                '                            </div>\n' +
                                '                            <div class="card-body">\n' +
                                '                                <div class="item-name">\n' +
                                '                                    <h4 class="mb-0 text-body bold">' + item.title + '</h4>\n' +
                                '                                    <span class="item-company text-primary">' + item.subcategory.title + '</span>\n' +
                                '                                    <div class="badge badge-pill badge-warning">อื่นๆ</div>\n' +
                                '                                </div>\n' +
                                '                                <span class="item-company">หนัก: ' + item.weight_real + ' กรัม(g)</span>\n' +
                                '                                <span class="item-company">ขนาด/ความยาว: ' + item.size + '</span>\n' +
                                '                            </div>\n' +
                                '                            <div class="item-options text-center">\n' +
                                '                                <div class="item-wrapper">\n' +
                                '                                    <div class="item-cost">\n' +
                                '                                        <h4 class="item-price">฿ ' + item.price.income + '</h4>\n' +
                                '                                        <p class="card-text shipping">\n' +
                                '                                            <span class="badge badge-pill badge-light-success">คงเหลือ ' + item.stock.count + ' ชิ้น</span>\n' +
                                '                                        </p>\n' +
                                '                                    </div>\n' +
                                '                                </div>\n' +
                                '                                <div class="demo-inline-spacing">\n' +
                                '                                    <button type="button" class="btn btn-icon btn-warning btn-sm" data-toggle="modal" data-id="' + item.id + '"  data-target="#edit_product"  style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="edit-2"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-info btn-sm" data-toggle="modal" data-id="' + item.id + '" data-target="#modal_view" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="eye"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-danger btn-sm" data-toggle="modal" data-id="' + item.id + '" data-target="#delete_product" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="trash-2"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-secondary btn-sm" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="printer"></i>\n' +
                                '                                    </button>\n' +

                                '                                    <button type="button" class="btn btn-icon btn-info btn-sm" data-toggle="modal" data-id="' + item.id + '"  data-target="#modal_add" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="plus-circle"></i>\n' +
                                '                                    </button>\n' +

                                '                                </div>\n' +
                                '                            </div>\n' +
                                '                        </div>');
                        }
                    });

                    gold_stock_pagination(res.data);
                    feather.replace();
                }
            });
        }

        function gold_stock_pagination(p) {
            const category = $('#category').val();
            const subcategory = $('#subcategory').val();
            const txt = $('#txt').val();
            const weight = $('#weight').val();
            const units = $('#units').val();
            const size = $('#size').val();            
            // gold_stock(1, category, subcategory, txt, weight, units, size);
            
            let first_page = p.first_page_url.replace(/.+page=/, '');
            let last_page = p.last_page_url.replace(/.+page=/, '');

            $('#pagination').empty();

            if(p.last_page > 1) {
                $('#pagination').append('<li class="page-item first">' +
                    '<a href="javascript:gold_stock(' + first_page + ',\'' + category + '\',\'' + subcategory + '\',\'' + txt + '\',\'' + weight + '\',\'' + units + '\',\'' + size + '\');" class="page-link">First</a>' +
                    '</li>');
            }

            if(p.prev_page_url !== null) {
                let prev_page = p.prev_page_url.replace(/.+page=/, '');
                $('#pagination').append('<li class="page-item prev">' +
                    '<a href="javascript:gold_stock(' + prev_page + ',\'' + category + '\',\'' + subcategory + '\',\'' + txt + '\',\'' + weight + '\',\'' + units + '\',\'' + size + '\');" class="page-link">Prev</a>' +
                    '</li>');
            }

            $.each(p.links, function (index, link) {
                if(link.label.indexOf('Previous') === -1 && link.label.indexOf('Next ')) {
                    let page_link = link.url.replace(/.+page=/, '');
                    $('#pagination').append('<li class="page-item' + (link.active ? ' active' : '') + '">' +
                        '<a href="javascript:gold_stock(' + page_link + ',\'' + category + '\',\'' + subcategory + '\',\'' + txt + '\',\'' + weight + '\',\'' + units + '\',\'' + size + '\');" class="page-link">' + page_link + '</a>' +
                        '</li>');
                }
            });

            if(p.next_page_url !== null) {
                let next_page = p.next_page_url.replace(/.+page=/, '');
                $('#pagination').append('<li class="page-item next">' +
                    '<a href="javascript:gold_stock(' + next_page + ',\'' + category + '\',\'' + subcategory + '\',\'' + txt + '\',\'' + weight + '\',\'' + units + '\',\'' + size + '\');" class="page-link">Next</a>' +
                    '</li>');
            }

            if(p.last_page > 1) {
                $('#pagination').append('<li class="page-item first">' +
                    '<a href="javascript:gold_stock(' + last_page + ',\'' + category + '\',\'' + subcategory + '\',\'' + txt + '\',\'' + weight + '\',\'' + units + '\',\'' + size + '\');" class="page-link">Last</a>' +
                    '</li>');
            }
        }

        function getGoldDelete() {
            $('#result_sku').empty();
            var date_start = $("#date_delete_start").val();
            var date_end = $("#date_delete_end").val();
            if(date_start != "" && date_end != "") {
                var gold_id = $("#gold_id_delete").val();
                $.get('{{ url('/gold-delete?gold_id=') }}' + gold_id + "&date_start=" + date_start + "&date_end=" + date_end, function(res) {
                    if(res.error == false){
                        var table = ""
                            + "<table class=\"table table-bordered table-responsive-md table-striped\">"
                            + "<thead>"
                            + "<tr>"
                            + "<th>"
                            + "<div class=\"custom-control custom-checkbox custom-checkbox-color-check custom-control-inline\">"
                            + "<input type=\"checkbox\" class=\"custom-control-input bg-danger\" id=\"check_all\" onclick=\"checkAll()\">"
                            + "<label class=\"custom-control-label\" for=\"check_all\"><strong>เลือกทั้งหมด</strong></label>"
                            + "</div>"
                            + "</th>"
                            + "<th>วันที่บันทึก</th>"
                            + "</tr>"
                            + "</thead>"
                            + "<tbody>";
                        $("#result_sku").append("");
                        $.each(res.data, function(index, value) {
                            var datetime_add = value.created_at.substring(0, value.created_at.lastIndexOf(".")).replace("T", " ").split(" ");
                            var date_add = datetime_add[0].split("-");
                            var time_add = datetime_add[1].split(":");
                            table += "<tr>"
                                + " <td>"
                                + "<div class=\"custom-control custom-checkbox custom-checkbox-color-check\">"
                                + "<input type=\"checkbox\" class=\"custom-control-input bg-danger\" id=\"" + value.sku + "\" name=\"sku_delete[]\" value=\"" + value.id + "\">"
                                + "<label class=\"custom-control-label\" for=\"" + value.sku + "\">" + value.sku + "</label>"
                                + "</div>"
                                + "</td>"
                                + "<td>"
                                + date_add[2] + "/" + date_add[1] + "/" + date_add[0] + " " + time_add[0] + ":" + time_add[1]
                                + "</td>"
                                + "</tr>";
                        });
                        table += "</tbody>"
                            + "</table>";
                        $("#result_sku").append(table);
                        checkAll();
                    }
                });
            }
        }

        function submitDel() {
            $("#product_delete").submit();
        }

        function checkAll() {
            $('#check_all').change(function() {
                var checkboxes = $(this).closest('form').find(':checkbox');
                checkboxes.prop('checked', $(this).is(':checked'));
            });
        }
        function gold_filter() {
            $("#gold_filter").submit();
        }

    </script>
@endsection
