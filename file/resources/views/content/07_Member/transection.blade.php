
@extends('layouts/contentLayoutMasterMain')

@section('title', 'ประวัติการทำรายการ (คุณพงศกร สนชาวไพร)')

@section('vendor-style')
  {{-- vendor css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap4.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap4.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
@endsection

@section('content')

<!-- Basic table -->
    <section id="basic-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <table class="datatables-basic table">
                        <thead>
                        <tr>
                            <th></th>
{{--                            <th></th>--}}
                            <th>#</th>
                            <th>#</th>
                            <th>หมายเลขธุรกรรม</th>
                            <th>ราคา</th>
                            <th>สถานะ</th>
{{--                            <th>พนักงาน</th>--}}
                            <th>วันที่</th>
{{--                            <th>...</th>--}}
                            <th>จัดการข้อมูล</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <div
        class="modal fade text-left"
        id="default"
        tabindex="-1"
        role="dialog"
        aria-labelledby="myModalLabel1"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">รายละเอียดสินค้า</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="col-md-12 mb-3 mt-1">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>หมวดหมู่สินค้า</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong>ทอง 96.5 %</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ประเภทสินค้า</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong>สร้อยคอ / ทองรูปพรรณ</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ลายทอง</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong>สร้อยคอ</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>น้ำหนัก</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong>1 บาท</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>น้ำหนักจริง</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong>15.44 กรัม (g)</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ขนาด/ความยาว</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong>8</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ราคา</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong>21,000.00 บาท</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>สถานะ</span>
                                    </div>
                                    <div class="col-md-9">
                                        <strong>มีสินค้า</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="col-md-12 mt-1">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>รูปภาพ</span>
                                    </div>
                                    <div class="col-md-9">
                                        <img  style="width: 300px;" src="{{asset('images/gold/gold1.jpg')}}" alt="img-placeholder" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                </div>
            </div>
        </div>
    </div>

    <div
        class="modal fade text-left"
        id="edit"
        tabindex="-1"
        role="dialog"
        aria-labelledby="myModalLabel1"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">แก้ไขสรุปรายการรับซื้อทอง - <span>TB2021-1004101625</span></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="col-md-12 mb-2 mt-1">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>หมวดหมู่สินค้า</span>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="custom-control custom-radio mr-1">
                                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input" checked disabled />
                                                <label class="custom-control-label" for="customRadio1">ทอง 96.5 %</label>
                                            </div>
                                            <div class="custom-control custom-radio mr-1">
                                                <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input" disabled />
                                                <label class="custom-control-label" for="customRadio2">ทอง 99.99 %</label>
                                            </div>
                                            <div class="custom-control custom-radio mr-1">
                                                <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input" disabled />
                                                <label class="custom-control-label" for="customRadio3">ทอง 90 %</label>
                                            </div>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio4" name="customRadio" class="custom-control-input" disabled />
                                                <label class="custom-control-label" for="customRadio4">อื่น ๆ</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2 mt-1">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ประเภททอง</span>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="custom-control custom-radio mr-1">
                                                <input type="radio" id="typeRadio1" name="typeRadio1" class="custom-control-input" checked disabled />
                                                <label class="custom-control-label" for="typeRadio1">ทองรูปพรรณ</label>
                                            </div>
                                            <div class="custom-control custom-radio mr-1">
                                                <input type="radio" id="typeRadio2" name="typeRadio2" class="custom-control-input" disabled />
                                                <label class="custom-control-label" for="typeRadio2">ทองแท่ง</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ประเภทสินค้า</span>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <select class="form-control" id="basicSelect">
                                                <option>สร้อยคอ</option>
                                                <option>แหวน</option>
                                                <option>ข้อมือ</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

{{--                            <div class="col-md-12 mb-3">--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-md-3">--}}
{{--                                        <span>สถานะ</span>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-9">--}}
{{--                                        <strong>มีสินค้า</strong>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="col-md-12 mb-2">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ลายทอง</span>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="floating-label-disable"
                                               placeholder="สร้อยคอ"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>น้ำหนัก</span>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="floating-label-disable"
                                               placeholder="1"
                                        />
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <select class="form-control" id="basicSelect">
                                                <option>บาท</option>
                                                <option>สลึง</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>น้ำหนักจริง</span>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="floating-label-disable"
                                               placeholder="15.44"
                                        />
                                    </div>
                                    <div class="col-md-3">
                                        <span>กรัม (g)</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>ราคา</span>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="floating-label-disable"
                                               placeholder="21000"
                                        />
                                    </div>
                                    <div class="col-md-3">
                                        <span>บาท</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">บันทึก</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/jszip.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.rowGroup.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
@endsection

@section('page-script')
    {{-- Page js files --}}
    {{--  <script src="{{ asset(mix('js/scripts/tables/table-datatables-basic.js')) }}"></script>--}}
    <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
    <script>
        /**
         * DataTables Basic
         */

        $(function () {
            'use strict';

            var dt_basic_table = $('.datatables-basic'),
                dt_date_table = $('.dt-date'),
                assetPath = '../../../app-assets/';

            if ($('body').attr('data-framework') === 'laravel') {
                assetPath = $('body').attr('data-asset-path');
            }

            // DataTable with buttons
            // --------------------------------------------------------------------

            if (dt_basic_table.length) {
                var dt_basic = dt_basic_table.DataTable({
                    ajax: assetPath + 'data/table-datatable-transection.json',
                    columns: [
                        { data: 'responsive_id' },
                        { data: 'id' },
                        // { data: 'id' },
                        { data: 'full_name' },
                        { data: 'email' },
                        { data: 'pattern' },
                        { data: 'start_date' },
                        // { data: 'price' },
                        { data: 'salary' },
                        // { data: 'id' },
                        { data: '' }
                    ],
                    columnDefs: [
                        {
                            // For Responsive
                            className: 'control',
                            orderable: false,
                            responsivePriority: 2,
                            targets: 0
                        },
                        // {
                        //     // For Checkboxes
                        //     targets: 1,
                        //     orderable: false,
                        //     responsivePriority: 3,
                        //     render: function (data, type, full, meta) {
                        //         return (
                        //             '<div class="custom-control custom-checkbox"> <input class="custom-control-input dt-checkboxes" type="checkbox" value="" id="checkbox' +
                        //             data +
                        //             '" /><label class="custom-control-label" for="checkbox' +
                        //             data +
                        //             '"></label></div>'
                        //         );
                        //     },
                        //     checkboxes: {
                        //         selectAllRender:
                        //             '<div class="custom-control custom-checkbox"> <input class="custom-control-input" type="checkbox" value="" id="checkboxSelectAll" /><label class="custom-control-label" for="checkboxSelectAll"></label></div>'
                        //     }
                        // },
                        {
                            targets: 2,
                            visible: false
                        },
                        {
                            responsivePriority: 1,
                            targets: 4
                        },
                        // {
                        //     // Label
                        //     targets: -2,
                        //     render: function (data, type, full, meta) {
                        //         var $status_number = full['status'];
                        //         var $status = {
                        //             1: { title: 'มีสินค้า', class: 'badge-light-success' },
                        //             2: { title: 'ไม่มีสินค้า', class: 'badge-light-danger ' }
                        //         };
                        //         if (typeof $status[$status_number] === 'undefined') {
                        //             return data;
                        //         }
                        //         return (
                        //             '<span class="badge badge-pill ' +
                        //             $status[$status_number].class +
                        //             '">' +
                        //             $status[$status_number].title +
                        //             '</span>'
                        //         );
                        //     }
                        // },
                        {
                            // Actions
                            targets: -1,
                            title: 'จัดการข้อมูล',
                            orderable: false,
                            render: function (data, type, full, meta) {
                                return (
                                    '<a href="{{url('/buy_report_detail')}}" target="_blank" type="button" class="text-primary">' +
                                    feather.icons['eye'].toSvg({ class: 'font-medium-5 mr-1' }) +
                                    '</a>' /*+
                                    '<a type="button" class="text-primary" data-toggle="modal" data-target="#edit">' +
                                    feather.icons['edit'].toSvg({ class: 'font-medium-5' }) +
                                    '</a>'*/
                                );
                            }
                        }
                    ],
                    order: [[2, 'desc']],
                    dom:
                        '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-right"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                    displayLength: 7,
                    lengthMenu: [7, 10, 25, 50, 75, 100],
                    buttons: [
                        {
                            extend: 'collection',
                            className: 'btn btn-outline-secondary dropdown-toggle',
                            text: feather.icons['share'].toSvg({ class: 'font-small-4 mr-50' }) + 'Export',
                            buttons: [
                                {
                                    extend: 'print',
                                    text: feather.icons['printer'].toSvg({ class: 'font-small-4 mr-50' }) + 'Print',
                                    className: 'dropdown-item',
                                    exportOptions: { columns: [3, 4, 5, 6, 7] }
                                },
                                {
                                    extend: 'csv',
                                    text: feather.icons['file-text'].toSvg({ class: 'font-small-4 mr-50' }) + 'Csv',
                                    className: 'dropdown-item',
                                    exportOptions: { columns: [3, 4, 5, 6, 7] }
                                },
                                {
                                    extend: 'excel',
                                    text: feather.icons['file'].toSvg({ class: 'font-small-4 mr-50' }) + 'Excel',
                                    className: 'dropdown-item',
                                    exportOptions: { columns: [3, 4, 5, 6, 7] }
                                },
                                {
                                    extend: 'pdf',
                                    text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 mr-50' }) + 'Pdf',
                                    className: 'dropdown-item',
                                    exportOptions: { columns: [3, 4, 5, 6, 7] }
                                },
                                {
                                    extend: 'copy',
                                    text: feather.icons['copy'].toSvg({ class: 'font-small-4 mr-50' }) + 'Copy',
                                    className: 'dropdown-item',
                                    exportOptions: { columns: [3, 4, 5, 6, 7] }
                                }
                            ],
                            init: function (api, node, config) {
                                $(node).removeClass('btn-secondary');
                                $(node).parent().removeClass('btn-group');
                                setTimeout(function () {
                                    $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                                }, 50);
                            }
                        }
                    ],
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Details of ' + data['full_name'];
                                }
                            }),
                            type: 'column',
                            renderer: function (api, rowIdx, columns) {
                                var data = $.map(columns, function (col, i) {
                                    return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                                        ? '<tr data-dt-row="' +
                                        col.rowIndex +
                                        '" data-dt-column="' +
                                        col.columnIndex +
                                        '">' +
                                        '<td>' +
                                        col.title +
                                        ':' +
                                        '</td> ' +
                                        '<td>' +
                                        col.data +
                                        '</td>' +
                                        '</tr>'
                                        : '';
                                }).join('');

                                return data ? $('<table class="table"/>').append(data) : false;
                            }
                        }
                    },
                    language: {
                        paginate: {
                            // remove previous & next text from pagination
                            previous: '&nbsp;',
                            next: '&nbsp;'
                        }
                    }
                });
                $('div.head-label').html('<h6 class="card-title mb-0">ประวัติการทำรายการ (คุณพงศกร สนชาวไพร)</h6>');
            }

            // Flat Date picker
            if (dt_date_table.length) {
                dt_date_table.flatpickr({
                    monthSelectorType: 'static',
                    dateFormat: 'm/d/Y'
                });
            }
        });

    </script>

@endsection
