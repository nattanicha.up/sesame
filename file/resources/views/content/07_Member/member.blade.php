
@extends('layouts/contentLayoutMasterMain')

@section('title', 'รายการสมาชิก')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap4.min.css')) }}">
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
@endsection

@section('content')
    <style>
        .flsize{
            font-size: 16px;
        }

        .dataTables_filter{
            display: none;
        }
    </style>
    {{--<div class="row">--}}
    {{--  <div class="col-12">--}}
    {{--    <p>Read full documnetation <a href="https://datatables.net/" target="_blank">here</a></p>--}}
    {{--  </div>--}}
    {{--</div>--}}
    <!-- Basic table -->
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">ค้นหาสมาชิก</h4>
                </div>
                <div class="card-body">
                    <div class="row">
{{--                        <div class="col-md-12 col-sm-6 col-12 mb-2 mb-md-0">--}}
{{--                            <div class="form-group">--}}
{{--                                <input type="text" id="helperText" class="form-control" placeholder="เลขบัตรประชาชน, ชื่อ, เบอร์โทรศัพท์" />--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="col-md-4 col-sm-6 col-12 mb-2 mb-md-0">
                            <div class="form-group">
                                <label for="basicSelect" class="flsize">เลขบัตรประชาชน</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="id_card_filter" placeholder="" onchange="filter()"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-12 mb-2 mb-md-0">
                            <div class="form-group">
                                <label for="basicSelect" class="flsize">ชื่อ</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="name_filter" placeholder="" onchange="filter()"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-12 mb-2 mb-md-0">
                            <div class="form-group">
                                <label for="basicSelect" class="flsize">เบอร์โทรศัพท์</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="phone_filter" placeholder="" onchange="filter()"/>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <section id="basic-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <table class="datatables-basic table" id="table">
                        <thead>
                        <tr>
                            <th>ลำดับ</th>
                            <th>เลขบัตรประชาชน</th>
                            <th>ชื่อ-นามสกุล</th>
                            <th>เบอร์โทรศัพท์</th>
                            <th>วันที่สมัคร</th>
                            <th>จัดการข้อมูล</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <div
        class="modal fade text-left"
        id="dummyModal"
{{--        tabindex="-1"--}}
        role="dialog"
        aria-labelledby="myModalLabel1"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">เพิ่มข้อมูล DUMMY</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="previewDummy"></div>
                    <form id="dummyForm">
                    @csrf
                        <div class="row">
                            <div class="col-xl-4 col-md-6 col-12 mb-1">
                                <label for="basicInput" class="subText">เลขบัตรประชาชน <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-xl-8 col-md-6 col-12 mb-1">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="id_cardDummy" name="id_card" placeholder="เฉพาะตัวเลขเท่านั้น"/>
                                    <div id="id_cardErrorDummy" class="invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-md-6 col-12 mb-1">
                                <label for="basicInput" class="subText">เบอร์โทร <span class="text-danger">*</span></label>
                            </div>
                            <div class="col-xl-8 col-md-6 col-12 mb-1">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="phoneDummy" name="phone" placeholder="เฉพาะตัวเลขเท่านั้น"/>
                                    <div id="phoneErrorDummy" class="invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-md-6 col-12 mb-1">
                                <label for="basicInput" class="subText">ชื่อ</label>
                            </div>
                            <div class="col-xl-8 col-md-6 col-12 mb-1">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="nameDummy" name="name" placeholder=""/>
                                    <div id="nameErrorDummy" class="invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-md-6 col-12 mb-1">
                                <label for="basicInput" class="subText">นามสกุล</label>
                            </div>
                            <div class="col-xl-8 col-md-6 col-12 mb-1">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="surnameDummy" name="surname" placeholder=""/>
                                    <div id="surnameErrorDummy" class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="id_dummy" name="id_dummy" value=""/>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="addDummySubmit">บันทึก</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">ปิด</button>
                </div>
            </div>
        </div>
    </div>
    <div
            class="modal fade text-left"
            id="addModal"
{{--            tabindex="-1"--}}
            role="dialog"
            aria-labelledby="myModalLabel17"
            aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel17">ข้อมูลสมาชิก</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="previewAdd"></div>
                    <form id="addUserForm">
                    @csrf
                    <div class="row">
                        <div class="col-6">
                            <div class="row">
                                {{--                        <div class="col-xl-12 col-md-12 col-12 mb-1">--}}
                                {{--                            <label for="basicInput" class="subText text-danger">**สำหรับสมาชิกเก่ากรอกเพียงหมายเลขบัตรประชาชน หรือ เบอร์โทร</label>--}}
                                {{--                        </div>--}}
                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">เลขบัตรประชาชน <sapn class="text-danger">*</sapn></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="id_cardAdd" name="id_card" placeholder="เฉพาะตัวเลขเท่านั้น"/>
                                        <div id="id_cardErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">เบอร์โทร <sapn class="text-danger">*</sapn></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="phoneAdd" name="phone" placeholder="เฉพาะตัวเลขเท่านั้น"/>
                                        <div id="phoneErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">คำนำหน้าชื่อ <sapn class="text-danger">*</sapn></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <select class="form-control" id="titleAdd" name="title">
                                            <option value="">กรุณาเลือกคำนำหน้าชื่อ</option>
                                            @foreach(\App\Models\Title::query()->get() as $item)
                                                <option value="{{ $item->id }}">{{ $item->title_th }}</option>
                                            @endforeach
                                        </select>
                                        <div id="titleErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">ชื่อ <sapn class="text-danger">*</sapn></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="nameAdd" name="name"/>
                                        <div id="nameErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">นามสกุล <sapn class="text-danger">*</sapn></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="surnameAdd" name="surname"/>
                                        <div id="surnameErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">บ้านเลขที่ <sapn class="text-danger">*</sapn></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="addressAdd" name="address"/>
                                        <div id="addressErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">จังหวัด <sapn class="text-danger">*</sapn></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <select class="form-control" id="province_idAdd" name="province_id">
                                        </select>
                                        <div id="province_idErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">อำเภอ <sapn class="text-danger">*</sapn></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <select class="form-control" id="district_idAdd" name="district_id">
                                        </select>
                                        <div id="district_idErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">ตำบล <sapn class="text-danger">*</sapn></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <select class="form-control" id="subdistrict_idAdd" name="subdistrict_id">
                                        </select>
                                        <div id="subdistrict_idErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">รหัสไปรษณีย์ <sapn class="text-danger">*</sapn></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="zipcodeAdd" name="zipcode" readonly/>
                                        <div id="zipcodeErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-6">
                            <div class="row">

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">อีเมล</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="emailAdd" name="email" placeholder=""/>
                                        <div id="emailErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">ไลน์ไอดี</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="lineAdd" name="line" placeholder=""/>
                                        <div id="lineErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">เฟสบุ๊ค URL</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="facebookAdd" name="facebook" placeholder=""/>
                                        <div id="facebookErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">คำนำหน้าชื่ออังกฤษ</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <select class="form-control" id="title_enAdd" name="title_en">
                                            <option value="">กรุณาเลือกคำนำหน้าชื่อ</option>
                                            @foreach(\App\Models\Title::query()->get() as $item)
                                                <option value="{{ $item->id }}">{{ $item->title_en }}</option>
                                            @endforeach
                                        </select>
                                        <div id="title_enErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">ชื่ออังกฤษ</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="name_enAdd" name="name_en"/>
                                        <div id="name_enErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">นามสกุลอังกฤษ</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="surname_enAdd" name="surname_en"/>
                                        <div id="surname_enErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">วันเกิด</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" id="birthdayAdd" name="birthday" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                    </div>
                                    <div id="birthdayErrorAdd" class="invalid-feedback"></div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">เพศ</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <select class="form-control" id="genderAdd" name="gender">
                                            <option value="">กรุณาเลือกเพศ</option>
                                            @foreach(\App\Models\Gender::query()->get() as $item)
                                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                        <div id="genderErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">วันที่ออกบัตร</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" id="issueAdd" name="issue" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                        <div id="issueErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">วันที่บัตรหมดอายุ</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" id="expireAdd" name="expire" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                        <div id="expireErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary"  data-dismiss="modal" id="addUserSubmit">บันทึก</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">ปิด</button>
                </div>
            </div>
        </div>
    </div>
    <div
        class="modal fade text-left"
        id="editModal"
        {{--            tabindex="-1"--}}
        role="dialog"
        aria-labelledby="myModalLabel17"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel17">แก้ไขข้อมูลสมาชิก</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="previewEdit"></div>
                    <form id="editUserForm">
                        @csrf
                        <div class="row">
                            <div class="col-6">
                                <div class="row">
                                    {{--                        <div class="col-xl-12 col-md-12 col-12 mb-1">--}}
                                    {{--                            <label for="basicInput" class="subText text-danger">**สำหรับสมาชิกเก่ากรอกเพียงหมายเลขบัตรประชาชน หรือ เบอร์โทร</label>--}}
                                    {{--                        </div>--}}
                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">เลขบัตรประชาชน <sapn class="text-danger">*</sapn></label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="id_cardEdit" name="id_card" placeholder="เฉพาะตัวเลขเท่านั้น"/>
                                            <div id="id_cardErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">เบอร์โทร <sapn class="text-danger">*</sapn></label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="phoneEdit" name="phone" placeholder="เฉพาะตัวเลขเท่านั้น"/>
                                            <div id="phoneErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">คำนำหน้าชื่อ <sapn class="text-danger">*</sapn></label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <select class="form-control" id="titleEdit" name="title">
                                                <option value="">กรุณาเลือกคำนำหน้าชื่อ</option>
                                                @foreach(\App\Models\Title::query()->get() as $item)
                                                    <option value="{{ $item->id }}">{{ $item->title_th }}</option>
                                                @endforeach
                                            </select>
                                            <div id="titleErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">ชื่อ <sapn class="text-danger">*</sapn></label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="nameEdit" name="name"/>
                                            <div id="nameErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">นามสกุล <sapn class="text-danger">*</sapn></label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="surnameEdit" name="surname"/>
                                            <div id="surnameErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">บ้านเลขที่ <sapn class="text-danger">*</sapn></label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="addressEdit" name="address"/>
                                            <div id="addressErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">จังหวัด <sapn class="text-danger">*</sapn></label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <select class="form-control" id="province_idEdit" name="province_id">
                                            </select>
                                            <div id="province_idErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">อำเภอ <sapn class="text-danger">*</sapn></label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <select class="form-control" id="district_idEdit" name="district_id">
                                            </select>
                                            <div id="district_idErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">ตำบล <sapn class="text-danger">*</sapn></label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <select class="form-control" id="subdistrict_idEdit" name="subdistrict_id">
                                            </select>
                                            <div id="subdistrict_idErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">รหัสไปรษณีย์ <sapn class="text-danger">*</sapn></label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="zipcodeEdit" name="zipcode" readonly/>
                                            <div id="zipcodeErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-6">
                                <div class="row">

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">อีเมล</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="emailEdit" name="email" placeholder=""/>
                                            <div id="emailErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">ไลน์ไอดี</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="lineEdit" name="line" placeholder=""/>
                                            <div id="lineErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">เฟสบุ๊ค URL</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="facebookEdit" name="facebook" placeholder=""/>
                                            <div id="facebookErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">คำนำหน้าชื่ออังกฤษ</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <select class="form-control" id="title_enEdit" name="title_en">
                                                <option value="">กรุณาเลือกคำนำหน้าชื่อ</option>
                                                @foreach(\App\Models\Title::query()->get() as $item)
                                                    <option value="{{ $item->id }}">{{ $item->title_en }}</option>
                                                @endforeach
                                            </select>
                                            <div id="title_enErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">ชื่ออังกฤษ</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="name_enEdit" name="name_en"/>
                                            <div id="name_enErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">นามสกุลอังกฤษ</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="surname_enEdit" name="surname_en"/>
                                            <div id="surname_enErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">วันเกิด</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" id="birthdayEdit" name="birthday" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                            <div id="birthdayErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">เพศ</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <select class="form-control" id="genderEdit" name="gender">
                                                <option value="">กรุณาเลือกเพศ</option>
                                                @foreach(\App\Models\Gender::query()->get() as $item)
                                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                            <div id="genderErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">วันที่ออกบัตร</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" id="issueEdit" name="issue" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                            <div id="issueErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">วันที่บัตรหมดอายุ</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" id="expireEdit" name="expire" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                            <div id="expireErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary"  data-dismiss="modal" id="editUserSubmit">บันทึก</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">ปิด</button>
                </div>
            </div>
        </div>
    </div>
    <div
        class="modal fade text-left"
        id="detailModal"
        {{--            tabindex="-1"--}}
        role="dialog"
        aria-labelledby="myModalLabel17"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel17">ข้อมูลสมาชิก</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="previewEdit"></div>
                    <form id="editUserForm">
                        @csrf
                        <div class="row">
                            <div class="col-6">
                                <div class="row">
                                    {{--                        <div class="col-xl-12 col-md-12 col-12 mb-1">--}}
                                    {{--                            <label for="basicInput" class="subText text-danger">**สำหรับสมาชิกเก่ากรอกเพียงหมายเลขบัตรประชาชน หรือ เบอร์โทร</label>--}}
                                    {{--                        </div>--}}
                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">เลขบัตรประชาชน</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="id_cardDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">เบอร์โทร</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="phoneDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">คำนำหน้าชื่อ</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="titleDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">ชื่อ</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="nameDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">นามสกุล</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="surnameDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">บ้านเลขที่</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="addressDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">จังหวัด</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="province_idDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">อำเภอ</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="district_idDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">ตำบล</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="subdistrict_idDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">รหัสไปรษณีย์</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="zipcodeDetail"></strong>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-6">
                                <div class="row">

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">อีเมล</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="emailDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">ไลน์ไอดี</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="lineDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">เฟสบุ๊ค URL</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="facebookDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">คำนำหน้าชื่ออังกฤษ</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="title_enDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">ชื่ออังกฤษ</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="name_enDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">นามสกุลอังกฤษ</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="surname_enDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">วันเกิด</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="birthdayDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">เพศ</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="genderDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">วันที่ออกบัตร</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="issueDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">วันที่บัตรหมดอายุ</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="expireDetail"></strong>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
{{--                    <button type="button" class="btn btn-primary"  data-dismiss="modal" id="editUserSubmit">บันทึก</button>--}}
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">ปิด</button>
                </div>
            </div>
        </div>
    </div>
    {{--<div
            class="modal fade text-left"
            id="editModal"
            tabindex="-1"
            role="dialog"
            aria-labelledby="myModalLabel17"
            aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel17">ข้อมูลสมาชิก</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-12 mb-1">
                            <label for="basicInput" class="subText text-danger">**สำหรับสมาชิกเก่ากรอกเพียงหมายเลขบัตรประชาชน หรือ เบอร์โทร</label>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เลขบัตรประชาชน</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="id_card" placeholder="เฉพาะตัวเลขเท่านั้น"/>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เบอร์โทร<label class="text-danger">*</label></label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="telephone" placeholder="เฉพาะตัวเลขเท่านั้น"/>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">คำนำหน้าชื่อ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <select class="form-control" id="prefix">
                                    <option>นาย</option>
                                    <option>นาง</option>
                                    <option>นางสาว</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">ไลน์ไอดี</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="lineID" placeholder="@abc"/>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">ชื่อ<label class="text-danger">*</label></label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="name_th"/>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เฟสบุ๊ค URL</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="facebook" placeholder="www.facebook.com/abc"/>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">นามสกุล</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="last_name"/>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">คำนำหน้าชื่ออังกฤษ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <select class="form-control" id="prefix_en">
                                    <option></option>
                                </select>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">บ้านเลขที่</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="number"/>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">ชื่ออังกฤษ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="name_en"/>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">จังหวัด</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <select class="form-control" id="province">
                                    <option>กรุณาเลือกจังหวัด</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">นามสกุลอังกฤษ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="lastname_en"/>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">อำเภอ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <select class="form-control" id="district">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">วันเกิด</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" id="birthday" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">ตำบล</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <select class="form-control" id="sub_district">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เพศ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <select class="form-control" id="sex">
                                    <option>ชาย</option>
                                    <option>หญิง</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">รหัสไปรษณีย์</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="zipcode" disabled/>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">วันที่ออกบัตร</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" id="date_start" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">อีเมล</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="email" placeholder="abc@gmail.com"/>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">วันที่บัตรหมดอายุ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" id="date_exp" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                            </div>
                        </div>

                        --}}{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}{{--
                        --}}{{--                                    <label for="basicInput" class="subText">รหัสสินค้า</label>--}}{{--
                        --}}{{--                                </div>--}}{{--
                        --}}{{--                                <div class="col-xl-10 col-md-6 col-12 mb-1">--}}{{--
                        --}}{{--                                    <span>G121102111243840</span>--}}{{--
                        --}}{{--                                </div>--}}{{--

                        --}}{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}{{--
                        --}}{{--                                    <label for="basicInput" class="subText">น้ำหนัก</label>--}}{{--
                        --}}{{--                                </div>--}}{{--
                        --}}{{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}{{--
                        --}}{{--                                    <div class="form-group">--}}{{--
                        --}}{{--                                        <input type="text" class="form-control" id="basicInput"/>--}}{{--
                        --}}{{--                                    </div>--}}{{--
                        --}}{{--                                </div>--}}{{--
                        --}}{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}{{--
                        --}}{{--                                    <div class="form-group">--}}{{--
                        --}}{{--                                        <select class="form-control" id="basicSelect">--}}{{--
                        --}}{{--                                            <option>สลึง</option>--}}{{--
                        --}}{{--                                            <option>บาท</option>--}}{{--
                        --}}{{--                                        </select>--}}{{--
                        --}}{{--                                    </div>--}}{{--
                        --}}{{--                                </div>--}}{{--

                        --}}{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}{{--
                        --}}{{--                                    <label for="basicInput" class="subText">น้ำหนักจริง</label>--}}{{--
                        --}}{{--                                </div>--}}{{--
                        --}}{{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}{{--
                        --}}{{--                                    <div class="form-group">--}}{{--
                        --}}{{--                                        <input type="text" class="form-control" id="basicInput"/>--}}{{--
                        --}}{{--                                    </div>--}}{{--
                        --}}{{--                                </div>--}}{{--
                        --}}{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}{{--
                        --}}{{--                                    <label for="basicInput" class="subText">กรัม (g)</label>--}}{{--
                        --}}{{--                                </div>--}}{{--

                        --}}{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}{{--
                        --}}{{--                                    <label for="basicInput" class="subText">ราคารับซื้อ</label>--}}{{--
                        --}}{{--                                </div>--}}{{--
                        --}}{{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}{{--
                        --}}{{--                                    <div class="form-group">--}}{{--
                        --}}{{--                                        <input type="text" class="form-control" id="basicInput"/>--}}{{--
                        --}}{{--                                    </div>--}}{{--
                        --}}{{--                                </div>--}}{{--
                        --}}{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}{{--
                        --}}{{--                                    <label for="basicInput" class="subText">บาท</label>--}}{{--
                        --}}{{--                                </div>--}}{{--

                        --}}{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}{{--
                        --}}{{--                                    <label for="basicInput" class="subText">ขนาด/ความยาว</label>--}}{{--
                        --}}{{--                                </div>--}}{{--
                        --}}{{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}{{--
                        --}}{{--                                    <div class="form-group">--}}{{--
                        --}}{{--                                        <input type="text" class="form-control" id="basicInput"/>--}}{{--
                        --}}{{--                                    </div>--}}{{--
                        --}}{{--                                </div>--}}{{--
                        --}}{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}{{--
                        --}}{{--                                </div>--}}{{--
                        --}}{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}{{--
                        --}}{{--                                    <label for="basicInput" class="subText">รูปสินค้า</label>--}}{{--
                        --}}{{--                                </div>--}}{{--
                        --}}{{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}{{--
                        --}}{{--                                    <form action="#" class="dropzone dropzone-area" id="dpz-single-file">--}}{{--
                        --}}{{--                                        <div class="dz-message">Drop files here or click to upload.</div>--}}{{--
                        --}}{{--                                    </form>--}}{{--
                        --}}{{--                                </div>--}}{{--
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary"  data-dismiss="modal">บันทึก</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">ปิด</button>
                </div>
            </div>
        </div>
    </div>--}}
    {{--<div
            class="modal fade text-left"
            id="detailModal"
            tabindex="-1"
            role="dialog"
            aria-labelledby="myModalLabel17"
            aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel17">ข้อมูลสมาชิก</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-12 mb-1">
--}}{{--                            <label for="basicInput" class="subText text-danger">**สำหรับสมาชิกเก่ากรอกเพียงหมายเลขบัตรประชาชน หรือ เบอร์โทร</label>--}}{{--
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เลขบัตรประชาชน</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
--}}{{--                                <input type="text" class="form-control" id="id_card" placeholder="เฉพาะตัวเลขเท่านั้น"/>--}}{{--
                                <span id="id_card">1 5044 00412 65 5<strong></strong></span>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เบอร์โทร</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
--}}{{--                                <input type="text" class="form-control" id="telephone" placeholder="เฉพาะตัวเลขเท่านั้น"/>--}}{{--
                                <span id="telephone">095 455 3031<strong></strong></span>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">คำนำหน้าชื่อ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
--}}{{--                                <select class="form-control" id="prefix">--}}{{--
--}}{{--                                    <option>นาย</option>--}}{{--
--}}{{--                                    <option>นาง</option>--}}{{--
--}}{{--                                    <option>นางสาว</option>--}}{{--
--}}{{--                                </select>--}}{{--
                                <span id="prefix">นาย<strong></strong></span>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">ไลน์ไอดี</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
--}}{{--                                <input type="text" class="form-control" id="lineID" placeholder="@abc"/>--}}{{--
                                <span id="prefix">pongston<strong></strong></span>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">ชื่อ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
--}}{{--                                <input type="text" class="form-control" id="name_th"/>--}}{{--
                                <span id="name_th">พงศธร<strong></strong></span>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เฟสบุ๊ค URL</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
--}}{{--                                <input type="text" class="form-control" id="facebook" placeholder="www.facebook.com/abc"/>--}}{{--
                                <span id="facebook">facebook.com/pongston<strong></strong></span>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">นามสกุล</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
--}}{{--                                <input type="text" class="form-control" id="last_name"/>--}}{{--
                                <span id="last_name">สนชาวไพร<strong></strong></span>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">คำนำหน้าชื่ออังกฤษ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
--}}{{--                                <select class="form-control" id="prefix_en">--}}{{--
--}}{{--                                    <option></option>--}}{{--
--}}{{--                                </select>--}}{{--
                                <span id="prefix_en">MR.<strong></strong></span>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">บ้านเลขที่</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
--}}{{--                                <input type="text" class="form-control" id="number"/>--}}{{--
                                <span id="address">149 หมู่ 5<strong></strong></span>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">ชื่ออังกฤษ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
--}}{{--                                <input type="text" class="form-control" id="name_en"/>--}}{{--
                                <span id="name_en">Pongston<strong></strong></span>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">จังหวัด</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
--}}{{--                                <select class="form-control" id="province">--}}{{--
--}}{{--                                    <option>กรุณาเลือกจังหวัด</option>--}}{{--
--}}{{--                                </select>--}}{{--
                                <span id="province">เชียงใหม่<strong></strong></span>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">นามสกุลอังกฤษ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
--}}{{--                            <div class="form-group">--}}{{--
--}}{{--                                <input type="text" class="form-control" id="lastname_en"/>--}}{{--
--}}{{--                            </div>--}}{{--
                            <span id="lastname_en">Sonchawpai<strong></strong></span>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">อำเภอ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
--}}{{--                                <select class="form-control" id="district">--}}{{--
--}}{{--                                    <option></option>--}}{{--
--}}{{--                                </select>--}}{{--
                                <span id="district">หางดง<strong></strong></span>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">วันเกิด</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
--}}{{--                                <input type="text" id="birthday" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />--}}{{--
                                <span id="birthday">1992-11-11<strong></strong></span>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">ตำบล</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
--}}{{--                                <select class="form-control" id="sub_district">--}}{{--
--}}{{--                                    <option></option>--}}{{--
--}}{{--                                </select>--}}{{--
                                <span id="sub_district">หนองควาย<strong></strong></span>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เพศ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
--}}{{--                                <select class="form-control" id="sex">--}}{{--
--}}{{--                                    <option>ชาย</option>--}}{{--
--}}{{--                                    <option>หญิง</option>--}}{{--
--}}{{--                                </select>--}}{{--
                                <span id="sex">ชาย<strong></strong></span>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">รหัสไปรษณีย์</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
--}}{{--                            <div class="form-group">--}}{{--
--}}{{--                                <input type="text" class="form-control" id="zipcode" disabled/>--}}{{--
--}}{{--                            </div>--}}{{--
                            <span id="zipcode">50230<strong></strong></span>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">วันที่ออกบัตร</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
--}}{{--                                <input type="text" id="date_start" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />--}}{{--
                                <span id="date_start">2021-11-11<strong></strong></span>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">อีเมล</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
--}}{{--                                <input type="text" class="form-control" id="email" placeholder="abc@gmail.com"/>--}}{{--
                                <span id="email">pongston@gmail.com<strong></strong></span>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">วันที่บัตรหมดอายุ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
--}}{{--                                <input type="text" id="date_exp" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />--}}{{--
                                <span id="date_exp">2026-11-11<strong></strong></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary"  data-dismiss="modal">บันทึก</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">ปิด</button>
                </div>
            </div>
        </div>
    </div>--}}
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/jszip.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.rowGroup.min.js')) }}"></script>
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
    {{-- Page js files --}}
    {{--  <script src="{{ asset(mix('js/scripts/tables/table-datatables-basic.js')) }}"></script>--}}
    <script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>
    <script>
        $(document).ready(function () {
            pdfMake.fonts = {
                THSarabun: {
                    normal: 'THSarabun.ttf',
                    bold: 'THSarabun-Bold.ttf',
                    italics: 'THSarabun-Italic.ttf',
                    bolditalics: 'THSarabun-BoldItalic.ttf'
                }
            };

            listUser();

            province('Add');

            $('#province_idAdd').change(function (e) {
                e.preventDefault();
                district('Add');

                $('#subdistrict_idAdd').empty();
                $('#zipcodeAdd').val('');
            });

            $('#district_idAdd').change(function (e) {
                e.preventDefault();
                subdistrict('Add');

                $('#zipcodeAdd').val('');
            });

            $('#subdistrict_idAdd').change(function (e) {
                e.preventDefault();
                zipcode('Add');
            });

            province('Edit');

            $('#province_idEdit').change(function (e) {
                e.preventDefault();
                district('Edit');

                $('#subdistrict_idEdit').empty();
                $('#zipcodeEdit').val('');
            });

            $('#district_idEdit').change(function (e) {
                e.preventDefault();
                subdistrict('Edit');

                $('#zipcodeEdit').val('');
            });

            $('#subdistrict_idEdit').change(function (e) {
                e.preventDefault();
                zipcode('Edit');
            });

            $('#addModal').on('hide.bs.modal', function() {
                $('#addUserForm')[0].reset();

                removeValidation('Add');
            });

            $('#editModal').on('hide.bs.modal', function() {
                $('#editUserForm')[0].reset();

                removeValidation('Edit');
            });

            $('#dummyModal').on('hide.bs.modal', function() {
                $('#dummyForm')[0].reset();

                removeValidationDummy('Dummy');
            });

            $('#addUserSubmit').click(function (e) {
                $('#addUserSubmit').empty();
                $('#addUserSubmit').attr("disabled", true);
                $('#addUserSubmit').append("<div class=\"spinner-border spinner-grow-sm text-light\" role=\"status\"><span class=\"sr-only\">Loading...</span></div> กำลังบันทึกข้อมูล");

                removeValidation('Add');

                e.preventDefault();
                var form = $('#addUserForm')[0];
                var data = new FormData(form);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{url('/member-store')}}',
                    type: "POST",
                    enctype: 'multipart/form-data',
                    dataType: 'JSON',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (res) {

                        if(res.error == 0){
                            $('#previewAdd').append("<div class=\"alert alert-success\" role=\"alert\">\n" +
                                "<h4 class=\"alert-heading\">เพิ่มข้อมูลเรียบร้อย</h4>\n" +
                                "</div>");
                            setTimeout(function(){
                                $('#addModal').modal('hide');

                                $('#table').dataTable().fnClearTable();
                                $('#table').dataTable().fnDestroy();
                                listUser();
                            }, 1000);
                        }else{
                            $('#previewAdd').append("<div class=\"alert alert-danger\" role=\"alert\">\n" +
                                "<h4 class=\"alert-heading\">พบข้อผิดพลาด</h4>\n" +
                                "</div>");
                            $.each(res.messages, function (k,v) {
                                $('#'+ k + 'Add').addClass('is-invalid');
                                $('#'+ k + 'ErrorAdd').html(v);
                            });
                        }

                        $('#addUserSubmit').empty();
                        $('#addUserSubmit').append("บันทึก");
                        $('#addUserSubmit').attr("disabled",false);

                    }
                });
            });

            $('#editUserSubmit').click(function (e) {
                $('#editUserSubmit').empty();
                $('#editUserSubmit').attr("disabled", true);
                $('#editUserSubmit').append("<div class=\"spinner-border spinner-grow-sm text-light\" role=\"status\"><span class=\"sr-only\">Loading...</span></div> กำลังบันทึกข้อมูล");

                removeValidation('Edit');

                e.preventDefault();
                var id = $('#editUserSubmit').val();
                var form = $('#editUserForm')[0];
                var data = new FormData(form);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{url('/member-update')}}/' + id,
                    type: "POST",
                    enctype: 'multipart/form-data',
                    dataType: 'JSON',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (res) {

                        if(res.error == 0){
                            $('#previewEdit').append("<div class=\"alert alert-success\" role=\"alert\">\n" +
                                "<h4 class=\"alert-heading\">เพิ่มข้อมูลเรียบร้อย</h4>\n" +
                                "</div>");
                            setTimeout(function(){
                                $('#editModal').modal('hide');

                                $('#table').dataTable().fnClearTable();
                                $('#table').dataTable().fnDestroy();
                                listUser();
                            }, 1000);
                        }else{
                            $('#previewEdit').append("<div class=\"alert alert-danger\" role=\"alert\">\n" +
                                "<h4 class=\"alert-heading\">พบข้อผิดพลาด</h4>\n" +
                                "</div>");
                            $.each(res.messages, function (k,v) {
                                $('#'+ k + 'Edit').addClass('is-invalid');
                                $('#'+ k + 'ErrorEdit').html(v);
                            });
                        }

                        $('#editUserSubmit').empty();
                        $('#editUserSubmit').append("บันทึก");
                        $('#editUserSubmit').attr("disabled",false);

                    }
                });
            });

            $('#addDummySubmit').click(function (e) {
                $('#addDummySubmit').empty();
                $('#addDummySubmit').attr("disabled", true);
                $('#addDummySubmit').append("<div class=\"spinner-border spinner-grow-sm text-light\" role=\"status\"><span class=\"sr-only\">Loading...</span></div> กำลังบันทึกข้อมูล");

                removeValidationDummy('Dummy');

                e.preventDefault();
                var form = $('#dummyForm')[0];
                var data = new FormData(form);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{url('/dummy-store')}}',
                    type: "POST",
                    enctype: 'multipart/form-data',
                    dataType: 'JSON',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (res) {

                        if(res.error == 0){
                            $('#previewDummy').append("<div class=\"alert alert-success\" role=\"alert\">\n" +
                                "<h4 class=\"alert-heading\">เพิ่มข้อมูลเรียบร้อย</h4>\n" +
                                "</div>");
                            setTimeout(function(){
                                $('#dummyModal').modal('hide');

                                // $('#table').dataTable().fnClearTable();
                                // $('#table').dataTable().fnDestroy();
                                // listUser();
                            }, 1000);
                        }else{
                            $('#previewDummy').append("<div class=\"alert alert-danger\" role=\"alert\">\n" +
                                "<h4 class=\"alert-heading\">พบข้อผิดพลาด</h4>\n" +
                                "</div>");
                            $.each(res.messages, function (k,v) {
                                $('#'+ k + 'Dummy').addClass('is-invalid');
                                $('#'+ k + 'ErrorDummy').html(v);
                            });
                        }

                        $('#addDummySubmit').empty();
                        $('#addDummySubmit').append("บันทึก");
                        $('#addDummySubmit').attr("disabled",false);

                    }
                });
            });
        });

        function province(status) {
            /*$.get('{{ url('location/province') }}', function (res) {
                $('#province_id').empty();
                $('#province_id').append('<option value="">กรุณาเลือกจังหวัด</option>');
                $.each(res.data, function (index, province) {
                    $('#province_id').append('<option value="' + province.id + '">' + province.title_th + '</option>');
                });
            });*/

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('location/province') }}',
                type: "GET",
                enctype: 'multipart/form-data',
                dataType: 'JSON',
                async: false,
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    $('#province_id' + status).empty();
                    $('#province_id' + status).append('<option value="">กรุณาเลือกจังหวัด</option>');
                    $.each(res.data, function (index, province) {
                        $('#province_id' + status).append('<option value="' + province.id + '">' + province.title_th + '</option>');
                    });
                }
            });
        }

        function district(status) {
            var province_id = $('#province_id' + status).val();
            /*$.get('{{ url('location/district?province_id=') }}' + province_id, function (res) {
                $('#district_id').empty();
                $('#district_id').append('<option value="">กรุณาเลือกอำเภอ/เขต</option>');
                $.each(res.data, function (index, district) {
                    $('#district_id').append('<option value="' + district.id + '">' + district.title_th + '</option>');
                });
            });*/

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('location/district?province_id=') }}' + province_id,
                type: "GET",
                enctype: 'multipart/form-data',
                dataType: 'JSON',
                async: false,
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    $('#district_id' + status).empty();
                    $('#district_id' + status).append('<option value="">กรุณาเลือกอำเภอ/เขต</option>');
                    $.each(res.data, function (index, district) {
                        $('#district_id' + status).append('<option value="' + district.id + '">' + district.title_th + '</option>');
                    });
                }
            });
        }

        function subdistrict(status) {
            var district_id = $('#district_id' + status).val();
            /*$.get('{{ url('location/subdistrict?district_id=') }}' + district_id, function (res) {
                $('#subdistrict_id').empty();
                $('#subdistrict_id').append('<option value="">กรุณาเลือกตำบล/แขวง</option>');
                $.each(res.data, function (index, subdistrict) {
                    $('#subdistrict_id').append('<option value="' + subdistrict.id + '">' + subdistrict.title_th + '</option>');
                });
            });*/

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('location/subdistrict?district_id=') }}' + district_id,
                type: "GET",
                enctype: 'multipart/form-data',
                dataType: 'JSON',
                async: false,
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    $('#subdistrict_id' + status).empty();
                    $('#subdistrict_id' + status).append('<option value="">กรุณาเลือกตำบล/แขวง</option>');
                    $.each(res.data, function (index, subdistrict) {
                        $('#subdistrict_id' + status).append('<option value="' + subdistrict.id + '">' + subdistrict.title_th + '</option>');
                    });
                }
            });
        }

        function zipcode(status) {
            var subdistrict_id = $('#subdistrict_id' + status).val();
            /*$.get('{{ url('location/zipcode?subdistrict_id=') }}' + subdistrict_id, function (res) {
                $('#zipcode').val(res.data.zipcode);
            });*/

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('location/zipcode?subdistrict_id=') }}' + subdistrict_id,
                type: "GET",
                enctype: 'multipart/form-data',
                dataType: 'JSON',
                // async: false,
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    $('#zipcode' + status).val(res.data.zipcode);
                }
            });
        }

        function removeValidation(status) {
            $('#preview' + status).empty();
            $('#id_card' + status).removeClass('is-invalid');
            $('#phone' + status).removeClass('is-invalid');
            $('#title' + status).removeClass('is-invalid');
            $('#name' + status).removeClass('is-invalid');
            $('#surname' + status).removeClass('is-invalid');
            $('#address' + status).removeClass('is-invalid');
            $('#province_id' + status).removeClass('is-invalid');
            $('#district_id' + status).removeClass('is-invalid');
            $('#subdistrict_id' + status).removeClass('is-invalid');
            $('#zipcode' + status).removeClass('is-invalid');
            $('#email' + status).removeClass('is-invalid');
            $('#line' + status).removeClass('is-invalid');
            $('#facebook' + status).removeClass('is-invalid');
            $('#title_en' + status).removeClass('is-invalid');
            $('#name_en' + status).removeClass('is-invalid');
            $('#surname_en' + status).removeClass('is-invalid');
            $('#birthday' + status).removeClass('is-invalid');
            $('#gender' + status).removeClass('is-invalid');
            $('#issue' + status).removeClass('is-invalid');
            $('#expire' + status).removeClass('is-invalid');

            $('#id_cardError' + status).html("");
            $('#phoneError' + status).html("");
            $('#titleError' + status).html("");
            $('#nameError' + status).html("");
            $('#surnameError' + status).html("");
            $('#addressError' + status).html("");
            $('#province_idError' + status).html("");
            $('#district_idError' + status).html("");
            $('#subdistrict_idError' + status).html("");
            $('#zipcodeError' + status).html("");
            $('#emailError' + status).html("");
            $('#lineError' + status).html("");
            $('#facebookError' + status).html("");
            $('#title_enError' + status).html("");
            $('#name_enError' + status).html("");
            $('#surname_enError' + status).html("");
            $('#birthdayError' + status).html("");
            $('#genderError' + status).html("");
            $('#issueError' + status).html("");
            $('#expireError' + status).html("");
        }

        function removeValidationDummy(status) {
            $('#preview' + status).empty();
            $('#id_card' + status).removeClass('is-invalid');
            $('#phone' + status).removeClass('is-invalid');
            $('#name' + status).removeClass('is-invalid');
            $('#surname' + status).removeClass('is-invalid');

            $('#id_cardError' + status).html("");
            $('#phoneError' + status).html("");
            $('#nameError' + status).html("");
            $('#surnameError' + status).html("");
        }

        /**
         * DataTables Basic
         */
        function listUser() {
            'use strict';

            var dt_basic_table = $('.datatables-basic'),
                dt_date_table = $('.dt-date'),
                assetPath = '../../../app-assets/';
            if ($('body').attr('data-framework') === 'laravel') {
                assetPath = $('body').attr('data-asset-path');
            }

            // DataTable with buttons
            // --------------------------------------------------------------------
            if (dt_basic_table.length) {
                var dt_basic = dt_basic_table.DataTable({
                    ajax: '{{ url('member-list') }}',
                    columns: [
                        {
                            "data": "id",
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        { data: 'id_card' },
                        { data: null },
                        { data: 'phone' },
                        { data: 'created_at' },
                        { data: null }
                    ],

                    columnDefs: [
                        {
                            responsivePriority: 1,
                            targets: 2,
                            render: function (data, type, full, meta) {
                                return (
                                    data.name + ' ' + data.surname
                                );
                            }
                        },
                        {
                            targets: 4,
                            render: function (data, type, full, meta) {
                                const pos = data.indexOf("T");
                                const str = data.substr(0, pos);
                                const [year, month, day] = str.split("-")
                                return (
                                    day + '/' + month + '/' + year
                                );
                            }
                        },
                        {
                            // Actions
                            targets: -1,
                            title: 'จัดการข้อมูล',
                            orderable: false,
                            render: function (data, type, full, meta) {
                                return (
                                    '<a href="javascript:;" class="item-edit" data-toggle="modal" data-target="#editModal" data-id="' + data.id + '" onclick="getEditData(' + data.id + ')">' +
                                    feather.icons['edit'].toSvg({ class: 'font-medium-5' }) +
                                    '</a>' + ' ' +
                                    '<a href="javascript:;" class="item-edit" data-toggle="modal" data-target="#detailModal" data-id="' + data.id + '" onclick="getDetailData(' + data.id + ')">' +
                                    feather.icons['eye'].toSvg({ class: 'font-medium-5' }) +
                                    '</a>' + ' ' +
                                    '<a href="{{ url('/get-transection-by-member') }}" target="_blank" class="item-edit">' +
                                    feather.icons['list'].toSvg({ class: 'font-medium-5' }) +
                                    '</a>'
                                );
                            }
                        }
                    ],
                    order: [[0, 'asc']],
                    dom:
                        '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-right"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                    displayLength: 10,
                    lengthMenu: [10, 25, 50, 75, 100],
                    buttons: [
                        {
                            text: 'เพิมข้อมูล DUMMY',
                            className: 'create-new btn btn-danger mr-50',
                            attr: {
                                'data-toggle': 'modal',
                                'data-target': '#dummyModal',
                                'onclick': 'getDummyData()'
                            },
                            init: function (api, node, config) {
                                $(node).removeClass('btn-secondary');
                            }
                        },
                        {
                            text: 'เพิ่มสมาชิกใหม่',
                            className: 'create-new btn btn-primary mr-50',
                            attr: {
                                'data-toggle': 'modal',
                                'data-target': '#addModal'
                            },
                            init: function (api, node, config) {
                                $(node).removeClass('btn-secondary');
                            }
                        },
                        {
                            extend: 'collection',
                            className: 'btn btn-outline-secondary dropdown-toggle',
                            text: feather.icons['share'].toSvg({ class: 'font-small-4 mr-50' }) + 'Export',
                            buttons: [
                                {
                                    extend: 'print',
                                    text: feather.icons['printer'].toSvg({ class: 'font-small-4 mr-50' }) + 'Print',
                                    className: 'dropdown-item',
                                    exportOptions: { columns: [0, 1, 2, 3, 4] }
                                },
                                {
                                    extend: 'csv',
                                    text: feather.icons['file-text'].toSvg({ class: 'font-small-4 mr-50' }) + 'Csv',
                                    className: 'dropdown-item',
                                    exportOptions: { columns: [0, 1, 2, 3, 4] }
                                },
                                {
                                    extend: 'excel',
                                    text: feather.icons['file'].toSvg({ class: 'font-small-4 mr-50' }) + 'Excel',
                                    className: 'dropdown-item',
                                    exportOptions: { columns: [0, 1, 2, 3, 4] }
                                },
                                {
                                    extend: 'pdf',
                                    text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 mr-50' }) + 'Pdf',
                                    className: 'dropdown-item',
                                    exportOptions: { columns: [0, 1, 2, 3, 4] },
                                    customize: function(doc) {
                                        doc.defaultStyle = {
                                            font:'THSarabun',
                                        };
                                        doc.content[1].table.widths =
                                            Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                                        doc.defaultStyle.alignment = 'left';
                                        doc.styles.tableHeader.alignment = 'left';
                                    }
                                },
                                {
                                    extend: 'copy',
                                    text: feather.icons['copy'].toSvg({ class: 'font-small-4 mr-50' }) + 'Copy',
                                    className: 'dropdown-item',
                                    exportOptions: { columns: [0, 1, 2, 3, 4] }
                                }
                            ],
                            init: function (api, node, config) {
                                $(node).removeClass('btn-secondary');
                                $(node).parent().removeClass('btn-group');
                                setTimeout(function () {
                                    $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                                }, 50);
                            },
                        }
                    ],
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Details of ' + data['full_name'];
                                }
                            }),
                            type: 'column',
                            renderer: function (api, rowIdx, columns) {
                                var data = $.map(columns, function (col, i) {
                                    return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                                        ? '<tr data-dt-row="' +
                                        col.rowIndex +
                                        '" data-dt-column="' +
                                        col.columnIndex +
                                        '">' +
                                        '<td>' +
                                        col.title +
                                        ':' +
                                        '</td> ' +
                                        '<td>' +
                                        col.data +
                                        '</td>' +
                                        '</tr>'
                                        : '';
                                }).join('');
                                return data ? $('<table class="table"/>').append(data) : false;
                            }
                        }
                    },
                    language: {
                        paginate: {
                            // remove previous & next text from pagination
                            previous: '&nbsp;',
                            next: '&nbsp;'
                        }
                    }
                });
                $('div.head-label').html('<h6 class="card-title mb-0">จัดการสมาชิก</h6>');
            }

            // Flat Date picker
            if (dt_date_table.length) {
                dt_date_table.flatpickr({
                    monthSelectorType: 'static',
                    dateFormat: 'm/d/Y'
                });
            }
        }

        function filter() {
            const id_card = $('#id_card_filter').val();
            const name = $('#name_filter').val();
            const phone = $('#phone_filter').val();
            const table = $('#table').DataTable();

            table.columns(1).search(id_card).draw();
            table.columns(2).search(name).draw();
            table.columns(3).search(phone).draw();
        }

        function getEditData(id) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('member-show') }}/' + id,
                type: "GET",
                enctype: 'multipart/form-data',
                dataType: 'JSON',
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    $('#editUserSubmit').val(res.data.id);
                    $('#id_cardEdit').val(res.data.id_card);
                    $('#phoneEdit').val(res.data.phone);
                    $('#titleEdit').val(res.data.title.id);
                    $('#nameEdit').val(res.data.name);
                    $('#surnameEdit').val(res.data.surname);
                    $('#addressEdit').val(res.data.address);
                    $('#province_idEdit').val(res.data.province.id);
                    district('Edit');
                    $('#district_idEdit').val(res.data.district.id);
                    subdistrict('Edit');
                    $('#subdistrict_idEdit').val(res.data.subdistrict.id);
                    zipcode('Edit');
                    // $('#zipcodeEdit').val(res.data.zipcode);
                    $('#emailEdit').val(res.data.email);
                    $('#lineEdit').val(res.data.line);
                    $('#facebookEdit').val(res.data.facebook);
                    $('#title_enEdit').val(res.data.title.id);
                    $('#name_enEdit').val(res.data.name_en);
                    $('#surname_enEdit').val(res.data.surname_en);

                    let pos = res.data.birthday.indexOf(" ");
                    let str = res.data.birthday.substr(0, pos);
                    let [year, month, day] = str.split("-")
                    $("#birthdayEdit").flatpickr({
                        defaultDate: new Date(year + '-' + month + '-' + day),
                    });

                    $('#genderEdit').val(res.data.gender.id);

                    pos = res.data.issue.indexOf(" ");
                    str = res.data.issue.substr(0, pos);
                    [year, month, day] = str.split("-")
                    $("#issueEdit").flatpickr({
                        defaultDate: new Date(year + '-' + month + '-' + day),
                    });

                    pos = res.data.expire.indexOf(" ");
                    str = res.data.expire.substr(0, pos);
                    [year, month, day] = str.split("-")
                    $("#expireEdit").flatpickr({
                        defaultDate: new Date(year + '-' + month + '-' + day),
                    });
                }
            });
        }

        function getDetailData(id) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('member-show') }}/' + id,
                type: "GET",
                enctype: 'multipart/form-data',
                dataType: 'JSON',
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    $('#id_cardDetail').html(res.data.id_card);
                    $('#phoneDetail').html(res.data.phone);
                    $('#titleDetail').html(res.data.title.title_th);
                    $('#nameDetail').html(res.data.name);
                    $('#surnameDetail').html(res.data.surname);
                    $('#addressDetail').html(res.data.address);
                    $('#province_idDetail').html(res.data.province.title_th);
                    $('#district_idDetail').html(res.data.district.title_th);
                    $('#subdistrict_idDetail').html(res.data.subdistrict.title_th);
                    $('#zipcodeDetail').html(res.data.zipcode);
                    $('#emailDetail').html(res.data.email);
                    $('#lineDetail').html(res.data.line);
                    $('#facebookDetail').html(res.data.facebook);
                    $('#title_enDetail').html(res.data.title.title_en);
                    $('#name_enDetail').html(res.data.name_en);
                    $('#surname_enDetail').html(res.data.surname_en);

                    let pos = res.data.birthday.indexOf(" ");
                    let str = res.data.birthday.substr(0, pos);
                    let [year, month, day] = str.split("-");
                    $("#birthdayDetail").html(day + '/' + month + '/' + year);

                    $('#genderDetail').html(res.data.gender.name);

                    pos = res.data.issue.indexOf(" ");
                    str = res.data.issue.substr(0, pos);
                    [year, month, day] = str.split("-");
                    $("#issueDetail").html(day + '/' + month + '/' + year);

                    pos = res.data.expire.indexOf(" ");
                    str = res.data.expire.substr(0, pos);
                    [year, month, day] = str.split("-");
                    $("#expireDetail").html(day + '/' + month + '/' + year);
                }
            });
        }

        function getDummyData() {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('dummy-show') }}',
                type: "GET",
                enctype: 'multipart/form-data',
                dataType: 'JSON',
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    if(res.data != null) {
                        $('#id_dummy').val(res.data.id);
                        $('#id_cardDummy').val(res.data.id_card);
                        $('#phoneDummy').val(res.data.phone);
                        $('#nameDummy').val(res.data.name);
                        $('#surnameDummy').val(res.data.surname);
                    }
                }
            });
        }
    </script>
@endsection
