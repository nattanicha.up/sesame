@extends('layouts/contentLayoutMasterMain')

@section('title', 'ตั้งค่าห้างร้าน')

@section('vendor-style')
  <!-- vendor css files -->
  <link rel='stylesheet' href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
  <link rel='stylesheet' href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection
@section('page-style')
  <!-- Page css files -->
  <link rel="stylesheet" href="{{ asset('js/dropify/dist/css/dropify.css') }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
@endsection

@section('content')
<!-- account setting page -->
<section id="page-account-settings">
  <div class="row">
    <!-- right content section -->
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <div class="tab-content">
            <!-- general tab -->
            <div
              role="tabpanel"
              class="tab-pane active"
              id="account-vertical-general"
              aria-labelledby="account-pill-general"
              aria-expanded="true"
            >
              <!-- header media -->
                <div class="col-12 ml-0 mb-2 pl-0">
                    <div id="preview"></div>
                    <h4><strong>ตั้งค่าห้างร้าน</strong></h4>
                </div>
                <div class="col-12 ml-0 mb-1 pl-0">
                    <i data-feather='archive'></i> ข้อมูลห้างร้าน
                </div>

                <!-- form -->
              <form id="company_info" class="validate-form mt-2">
              @csrf
              <div class="media">
                {{--<a href="javascript:void(0);" class="mr-25">
                  <img
                    src="{{asset('images/logo_sesame_rect.png')}}"
                    id="account-upload-img"
                    class="rounded mr-50"
                    alt="profile image"
                    height="80"
                    width="80"
                  />
                </a>--}}
                <div id="logo">
                </div>
                <!-- upload and reset button -->
                <div class="media-body mt-75 ml-1">
{{--                  <label for="account-upload" class="btn btn-sm btn-success mb-75 mr-75">อัพโหลด</label>--}}
                  <input type="file" id="account-upload" hidden accept="image/*" />
{{--                  <button class="btn btn-sm btn-outline-secondary mb-75">Reset</button>--}}
                  <p>Allowed JPG, JPEG, PNG or GIF. Max size of 2MB</p>
                  <div id="pic_error" class="invalid-feedback" style="display:block;"></div>
                </div>
                <!--/ upload and reset button -->
              </div>
              <!--/ header media -->
                <div class="row mt-1">
                  <div class="col-12 col-sm-4">
                    <div class="form-group">
                      <label for="company-name">ชื่อห้างร้าน <span class="text-danger">*</span></label>
                      <input
                        type="text"
                        class="form-control"
                        id="company_name"
                        name="company_name"
                        placeholder=""
                        value=""
                      />
                      <div id="company_name_error" class="invalid-feedback"></div>
                    </div>
                  </div>
                  <div class="col-12 col-sm-4">
                    <div class="form-group">
                      <label for="tax-number">เลขประจำตัวผู้เสียภาษี <span class="text-danger">*</span></label>
                      <input
                        type="text"
                        class="form-control"
                        id="company_tax"
                        name="company_tax"
                        placeholder=""
                        value=""
                      />
                        <div id="company_tax_error" class="invalid-feedback"></div>
                    </div>
                  </div>
                  <div class="col-12 col-sm-4">
                    <div class="form-group">
                      <label for="phone">เบอร์โทรศัพท์ <span class="text-danger">*</span></label>
                      <input
                        type="phone"
                        class="form-control"
                        id="telephone"
                        name="telephone"
                        placeholder=""
                        value=""
                      />
                        <div id="telephone_error" class="invalid-feedback"></div>
                    </div>
                  </div>
                  <div class="col-12 col-sm-4">
                    <div class="form-group">
                      <label for="email">อีเมล</label>
                      <input
                        type="text"
                        class="form-control"
                        id="email"
                        name="email"
                        placeholder=""
                        value=""
                      />
                        <div id="email_error" class="invalid-feedback"></div>
                    </div>
                  </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="website">เว็บไซต์</label>
                            <input
                                type="text"
                                class="form-control"
                                id="website"
                                name="website"
                                placeholder=""
                                value=""
                            />
                        </div>
                        <div id="website_error" class="invalid-feedback"></div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="facebook">Facebook</label>
                            <input
                                type="text"
                                class="form-control"
                                id="facebook"
                                name="facebook"
                                placeholder=""
                                value=""
                            />
                            <div id="facebook_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="lineid">Line ID</label>
                            <input
                                type="text"
                                class="form-control"
                                id="line"
                                name="line"
                                placeholder=""
                                value=""
                            />
                            <div id="line_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-12 mt-2 mb-1">
                        <i data-feather='map-pin'></i> ที่ตั้ง
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="address">ที่อยู่ <span class="text-danger">*</span></label>
                            <input
                                type="text"
                                class="form-control"
                                id="address"
                                name="address"
                                placeholder=""
                                value=""
                            />
                            <div id="address_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="province">จังหวัด <span class="text-danger">*</span></label>
                            <select class="form-control" id="province_id"name="province_id"></select>
                            <div id="province_id_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="district">อำเภอ <span class="text-danger">*</span></label>
                            <select class="form-control" id="district_id"name="district_id"></select>
                            <div id="district_id_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="subdistrict">ตำบล <span class="text-danger">*</span></label>
                            <select class="form-control" id="subdistrict_id"name="subdistrict_id"></select>
                            <div id="subdistrict_id_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="zipcode">รหัสไปรษณีย์</label>
                            <input
                                type="text"
                                class="form-control"
                                id="zipcode"
                                name="zipcode"
                                placeholder=""
                                value=""
                                readonly
                            />
                            <div id="zipcode_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                  <div class="col-12 text-right">
                    <input type="hidden" id="company_id" name="company_id" value="" />
                    <button type="button" class="btn btn-primary mt-2 mr-1" id="submit">แก้ไขข้อมูล</button>
{{--                    <button type="button" class="btn btn-outline-secondary mt-2" id="reset">ยกเลิก</button>--}}
                  </div>
                </div>
              </form>
              <!--/ form -->
            </div>
            <!--/ general tab -->
          </div>
        </div>
      </div>
    </div>
    <!--/ right content section -->
  </div>
</section>
<!-- / account setting page -->
@endsection

@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset('js/dropify/dist/js/dropify.js') }}"></script>
    <script src="{{ asset(mix('js/scripts/pages/page-account-settings.js')) }}"></script>
    <script>
        $(document).ready(function () {
            province();
            companyInfo();

            $('#submit').click(function (e) {
                $('#submit').empty();
                $('#submit').attr("disabled", true);
                $('#submit').append("<div class=\"spinner-border spinner-grow-sm text-light\" role=\"status\"><span class=\"sr-only\">Loading...</span></div> กำลังบันทึกข้อมูล");

                $('#preview').empty();
                $('#company_name').removeClass('is-invalid');
                $('#company_tax').removeClass('is-invalid');
                $('#telephone').removeClass('is-invalid');
                $('#address').removeClass('is-invalid');
                $('#province_id').removeClass('is-invalid');
                $('#district_id').removeClass('is-invalid');
                $('#subdistrict_id').removeClass('is-invalid');
                $('#zipcode').removeClass('is-invalid');

                $('#pic_error').html("");
                $('#company_name_error').html("");
                $('#company_tax_error').html("");
                $('#telephone_error').html("");
                $('#address_error').html("");
                $('#province_id_error').html("");
                $('#district_id_error').html("");
                $('#subdistrict_id_error').html("");
                $('#zipcode_error').html("");

                e.preventDefault();
                var form = $('#company_info')[0];
                var data = new FormData(form);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{url('/company-settings-update')}}',
                    type: "POST",
                    enctype: 'multipart/form-data',
                    dataType: 'JSON',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (res) {

                        if(res.error == 0){
                            $('#preview').append("<div class=\"alert alert-success text-center\" role=\"alert\">\n" +
                                "<h4 class=\"alert-heading\">บันทึกข้อมูลเรียบร้อย</h4>\n" +
                                "</div>");
                            setTimeout(function(){
                                $('#preview').empty();
                                companyInfo();
                            }, 1000);
                        }else{
                            $('#preview').append("<div class=\"alert alert-danger text-center\" role=\"alert\">\n" +
                                "<h4 class=\"alert-heading\">พบข้อผิดพลาด</h4>\n" +
                                "</div>");
                            $.each(res.messages, function (k,v) {
                                $('#'+ k).addClass('is-invalid');
                                $('#'+ k + '_error').html(v);
                            });
                        }

                        $('#submit').empty();
                        $('#submit').append("บันทึก");
                        $('#submit').attr("disabled",false);
                    }
                });
            });

            $('#province_id').change(function (e) {
                e.preventDefault();
                district();

                $('#subdistrict_id').empty();
                $('#zipcode').val('');
            });

            $('#district_id').change(function (e) {
                e.preventDefault();
                subdistrict();

                $('#zipcode').val('');
            });

            $('#subdistrict_id').change(function (e) {
                e.preventDefault();
                zipcode();
            });
        });


        function province() {
            /*$.get('{{ url('location/province') }}', function (res) {
                $('#province_id').empty();
                $('#province_id').append('<option value="">กรุณาเลือกจังหวัด</option>');
                $.each(res.data, function (index, province) {
                    $('#province_id').append('<option value="' + province.id + '">' + province.title_th + '</option>');
                });
            });*/

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('location/province') }}',
                type: "GET",
                enctype: 'multipart/form-data',
                dataType: 'JSON',
                async: false,
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    $('#province_id').empty();
                    $('#province_id').append('<option value="">กรุณาเลือกจังหวัด</option>');
                    $.each(res.data, function (index, province) {
                        $('#province_id').append('<option value="' + province.id + '">' + province.title_th + '</option>');
                    });
                }
            });
        }

        function district() {
            var province_id = $('#province_id').val();
            /*$.get('{{ url('location/district?province_id=') }}' + province_id, function (res) {
                $('#district_id').empty();
                $('#district_id').append('<option value="">กรุณาเลือกอำเภอ/เขต</option>');
                $.each(res.data, function (index, district) {
                    $('#district_id').append('<option value="' + district.id + '">' + district.title_th + '</option>');
                });
            });*/

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('location/district?province_id=') }}' + province_id,
                type: "GET",
                enctype: 'multipart/form-data',
                dataType: 'JSON',
                async: false,
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    $('#district_id').empty();
                    $('#district_id').append('<option value="">กรุณาเลือกอำเภอ/เขต</option>');
                    $.each(res.data, function (index, district) {
                        $('#district_id').append('<option value="' + district.id + '">' + district.title_th + '</option>');
                    });
                }
            });
        }

        function subdistrict() {
            var district_id = $('#district_id').val();
            /*$.get('{{ url('location/subdistrict?district_id=') }}' + district_id, function (res) {
                $('#subdistrict_id').empty();
                $('#subdistrict_id').append('<option value="">กรุณาเลือกตำบล/แขวง</option>');
                $.each(res.data, function (index, subdistrict) {
                    $('#subdistrict_id').append('<option value="' + subdistrict.id + '">' + subdistrict.title_th + '</option>');
                });
            });*/

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('location/subdistrict?district_id=') }}' + district_id,
                type: "GET",
                enctype: 'multipart/form-data',
                dataType: 'JSON',
                async: false,
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    $('#subdistrict_id').empty();
                    $('#subdistrict_id').append('<option value="">กรุณาเลือกตำบล/แขวง</option>');
                    $.each(res.data, function (index, subdistrict) {
                        $('#subdistrict_id').append('<option value="' + subdistrict.id + '">' + subdistrict.title_th + '</option>');
                    });
                }
            });
        }

        function zipcode() {
            var subdistrict_id = $('#subdistrict_id').val();
            /*$.get('{{ url('location/zipcode?subdistrict_id=') }}' + subdistrict_id, function (res) {
                $('#zipcode').val(res.data.zipcode);
            });*/

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('location/zipcode?subdistrict_id=') }}' + subdistrict_id,
                type: "GET",
                enctype: 'multipart/form-data',
                dataType: 'JSON',
                // async: false,
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    $('#zipcode').val(res.data.zipcode);
                }
            });
        }

        function loadImg(pic) {
            var html = '';
            if(pic != null && pic != '') {
                html = "<input type=\"file\" class=\"dropify\" name=\"pic\" id=\"pic\" data-width=\"80\" data-height=\"80\" data-default-file=\"{{asset('/_uploads/_companys')}}/" + pic + "\">";
            }
            else {
                html = "<input type=\"file\" class=\"dropify\" name=\"pic\" id=\"pic\" data-width=\"80\" data-height=\"80\">";
            }

            $('#logo').html(html);

            $('.dropify').dropify({
                messages: {
                    'default': 'Drag and drop a file here or click',
                    'replace': 'Drag and drop or click to replace',
                    'remove': 'Remove',
                    'error': 'Ooops, something wrong happended.'
                },
                tpl: {
                    wrap:            '<div class="dropify-wrapper"></div>',
                    loader:          '<div class="dropify-loader"></div>',
                    message:         '<div class="dropify-message"><span class="file-icon" /> <p></p></div>',
                    preview:         '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message"></p></div></div></div>',
                    filename:        '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
                    clearButton:     '<button type="button" class="dropify-clear">ลบ</button>',
                    errorLine:       '<p class="dropify-error"></p>',
                    errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
                }
            });
        }

        function companyInfo() {
            $.get('{{ url('company-settings-info') }}', function (res) {
                if (res.data != undefined) {
                    loadImg(res.data.pic);

                    $('#company_name').val(res.data.company_name);
                    $('#company_tax').val(res.data.company_tax);
                    $('#telephone').val(res.data.telephone);
                    $('#email').val(res.data.email);
                    $('#website').val(res.data.website);
                    $('#facebook').val(res.data.facebook);
                    $('#line').val(res.data.line);
                    $('#address').val(res.data.address);
                    $('#province_id').val(res.data.province_id);
                    district();
                    $('#district_id').val(res.data.district_id);
                    subdistrict();
                    $('#subdistrict_id').val(res.data.subdistrict_id);
                    zipcode();
                    $('#company_id').val(res.data.id);
                }
                else {
                    loadImg(null);
                }
            });
        }
    </script>
@endsection
