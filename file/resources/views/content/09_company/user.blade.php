@extends('layouts/contentLayoutMasterMain')

@section('title', 'จัดการผู้ใช้งาน')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection

@section('page-style')
    <link rel="stylesheet" href="{{ asset('js/dropify/dist/css/dropify.css') }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
    <link rel="stylesheet" href="{{asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css'))}}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
@endsection

@section('content')
    <style>
        .flsize{
            font-size: 16px;
        }
    </style>

    <section id="basic-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <table class="datatables-basic table" id="table">
                        <thead>
                        <tr>
{{--                            <th></th>--}}
{{--                            <th></th>--}}
                            <th>id</th>
                            <th>ลำดับ</th>
                            <th>ชื่อ-นามสกุล</th>
                            <th>อีเมล</th>
                            <th>ระดับการใช้งาน</th>
                            <th>สถานะ</th>
                            <th>วันที่เพิ่มข้อมูล</th>
                            <th>จัดการข้อมูล</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <div
            class="modal fade text-left"
            id="addModal"
{{--            tabindex="-1"--}}
            role="dialog"
            aria-labelledby="myModalLabel17"
            aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel17">เพิ่มผู้ใช้งานใหม่</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="previewAdd"></div>
                    <form id="addUserForm">
                    @csrf
                    <div class="row">
                        <div class="col-6">
                            <div class="row">
                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">ชื่อจริง <span class="text-danger">*</span></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="nameAdd" name="name" placeholder=""/>
                                        <div id="nameErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">นามสกุล <span class="text-danger">*</span></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="surnameAdd" name="surname" placeholder=""/>
                                        <div id="surnameErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">เลขบัตรประชาชน <span class="text-danger">*</span></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="number" class="form-control" id="id_cardAdd" name="id_card"/>
                                        <div id="id_cardErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">เพศ <span class="text-danger">*</span></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <select class="form-control" id="genderAdd" name="gender">
                                            <option value="">กรุณาระบุเพศ</option>
                                            <option value="1">ชาย</option>
                                            <option value="2">หญิง</option>
                                        </select>
                                        <div id="genderErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">Facebook</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="facebookAdd" name="facebook"/>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">LINE ID</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="lineAdd" name="line"/>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">อีเมล</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="emailAdd" name="email"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="row">
                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">สิทธิ์การใช้งาน <span class="text-danger">*</span></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <select class="form-control" id="permission_idAdd" name="permission_id">
                                            <option value="">กรุณาเระบุสิทธิ์การใช้งาน</option>
                                            @foreach(DB::table('101_USER_PERMISSION')->where('id', '<>', 1)->where('id', '<>', 2)->get() as $item)
                                                <option value="{{ $item->id }}">{{ $item->title }}</option>
                                            @endforeach
                                        </select>
                                        <div id="permission_idErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">ชื่อผู้ใช้งาน <span class="text-danger">*</span></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="usernameAdd" name="username" placeholder=""/>
                                        <div id="usernameErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">รหัสผ่าน <span class="text-danger">*</span></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="passwordAdd" name="password" placeholder=""/>
                                        <div id="passwordErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">กำหนดอายุบัญชี <span class="text-danger">*</span></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <div class="form-check form-check-inline">
                                            <input
                                                class="form-check-input"
                                                type="radio"
                                                name="accExpireStatus"
                                                id="accExpireStatusNone"
                                                value="0"
                                                onclick="showAccExpireDateInput(this.value, 'add')"
                                                checked
                                            />
                                            <label class="form-check-label" for="inlineRadio1">ไม่กำหนด</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input
                                                class="form-check-input"
                                                type="radio"
                                                name="accExpireStatus"
                                                id="accExpireStatusSet"
                                                onclick="showAccExpireDateInput(this.value, 'add')"
                                                value="1"
                                            />
                                            <label class="form-check-label" for="inlineRadio2">กำหนด</label>
                                        </div>
                                    </div>
                                    <div id="accExpireDateInput" class="form-group" hidden>
                                        <input type="text" id="acc_expireAdd" name="acc_expire" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                        <div id="acc_expireErrorAdd" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-xl-12 col-md-12 col-12 mb-1">
                                    <label for="basicInput" class="subText">รูปประจำตัว</label>
                                </div>
                                <div class="col-xl-12 col-md-12 col-12 mb-1">
                                    <div class="form-group" id="profileUploadAdd">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="addUserSubmit">บันทึก</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">ปิด</button>
                </div>
            </div>
        </div>
    </div>
    <div
            class="modal fade text-left"
            id="editModal"
{{--            tabindex="-1"--}}
            role="dialog"
            aria-labelledby="myModalLabel17"
            aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel17">แก้ไขผู้ใช้งาน</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="previewEdit"></div>
                    <form id="editUserForm">
                        @csrf
                        <div class="row">
                            <div class="col-6">
                                <div class="row">
                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">ชื่อจริง <span class="text-danger">*</span></label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="nameEdit" name="name" placeholder=""/>
                                            <div id="nameErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">นามสกุล <span class="text-danger">*</span></label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="surnameEdit" name="surname" placeholder=""/>
                                            <div id="surnameErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">เลขบัตรประชาชน <span class="text-danger">*</span></label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="number" class="form-control" id="id_cardEdit" name="id_card"/>
                                            <div id="id_cardErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">เพศ <span class="text-danger">*</span></label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <select class="form-control" id="genderEdit" name="gender">
                                                <option value="">กรุณาระบุเพศ</option>
                                                <option value="1">ชาย</option>
                                                <option value="2">หญิง</option>
                                            </select>
                                            <div id="genderErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">Facebook</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="facebookEdit" name="facebook"/>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">LINE ID</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="lineEdit" name="line"/>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">อีเมล</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="emailEdit" name="email"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="row">
                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">สิทธิ์การใช้งาน <span class="text-danger">*</span></label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <select class="form-control" id="permission_idEdit" name="permission_id">
                                                <option value="">กรุณาเระบุสิทธิ์การใช้งาน</option>
                                                @foreach(DB::table('101_USER_PERMISSION')->where('id', '<>', 1)->where('id', '<>', 2)->get() as $item)
                                                    <option value="{{ $item->id }}">{{ $item->title }}</option>
                                                @endforeach
                                            </select>
                                            <div id="permission_idErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">ชื่อผู้ใช้งาน <span class="text-danger">*</span></label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="usernameEdit" name="username" placeholder=""/>
                                            <div id="usernameErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">รหัสผ่าน <span class="text-danger">*</span></label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="passwordEdit" name="password" placeholder="********"/>
                                            <div id="passwordErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">กำหนดอายุบัญชี <span class="text-danger">*</span></label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <div class="form-check form-check-inline">
                                                <input
                                                        class="form-check-input"
                                                        type="radio"
                                                        name="accExpireStatus"
                                                        id="accExpireStatusNoneEdit"
                                                        value="0"
                                                        onclick="showAccExpireDateInput(this.value, 'edit')"
                                                />
                                                <label class="form-check-label" for="inlineRadio1">ไม่กำหนด</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input
                                                        class="form-check-input"
                                                        type="radio"
                                                        name="accExpireStatus"
                                                        id="accExpireStatusSetEdit"
                                                        onclick="showAccExpireDateInput(this.value, 'edit')"
                                                        value="1"
                                                />
                                                <label class="form-check-label" for="inlineRadio2">กำหนด</label>
                                            </div>
                                        </div>
                                        <div id="accExpireDateInputEdit" class="form-group" hidden>
                                            <input type="text" id="acc_expireEdit" name="acc_expire" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                            <div id="acc_expireErrorEdit" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-12 col-md-12 col-12 mb-1">
                                        <label for="basicInput" class="subText">รูปประจำตัว</label>
                                    </div>
                                    <div class="col-xl-12 col-md-12 col-12 mb-1">
                                        <div class="form-group" id="profileUploadEdit">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="editUserSubmit">บันทึก</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">ปิด</button>
                </div>
            </div>
        </div>
    </div>
    <div
            class="modal fade text-left"
            id="detailModal"
            tabindex="-1"
            role="dialog"
            aria-labelledby="myModalLabel17"
            aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel17">ข้อมูลผู้ใช้งาน</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="previewEdit"></div>
                    <form id="editUserForm">
                        @csrf
                        <div class="row">
                            <div class="col-6">
                                <div class="row">
                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">ชื่อจริง</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="nameDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">นามสกุล</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="surnameDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">เลขบัตรประชาชน</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="id_cardDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">เพศ</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="genderDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">Facebook</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="facebookDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">LINE ID</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="lineDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">อีเมล</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="emailDetail"></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="row">
                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">สิทธิ์การใช้งาน</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="permission_idDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">ชื่อผู้ใช้งาน</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="usernameDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">รหัสผ่าน</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="passwordDetail">********</strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">กำหนดอายุบัญชี</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <strong id="accExpireStatusDetail"></strong>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">รูปประจำตัว</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group" id="profileUploadDetail">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">ปิด</button>
                </div>
            </div>
        </div>
    </div>
    {{--<div
        class="modal fade text-left"
        id="editModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="myModalLabel17"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel17">แก้ไขข้อมูลผู้ใช้งาน</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="row">
                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">ชื่อจริง <span class="text-danger">*</span></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="fname" placeholder="" value="ณัฐติชา"/>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">นามสกุล <span class="text-danger">*</span></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="lname" placeholder="" value="แสนแก้ว"/>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">เลขบัตรประชาชน <span class="text-danger">*</span></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="card-id" value="1234567891234" />
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">เพศ <span class="text-danger">*</span></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <select class="form-control" id="gender">
                                            <option>กรุณาระบุเพศ</option>
                                            <option>ชาย</option>
                                            <option selected>หญิง</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">Facebook</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="facebook" value="facebook.com/nutticha"/>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">LINE ID</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="lineid" value="@nutticha"/>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">อีเมล</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="email" value="nutticha@sesamepos.com"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="row">
                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">สิทธิ์การใช้งาน <span class="text-danger">*</span></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <select class="form-control" id="province">
                                            <option>กรุณาเระบุสิทธิ์การใช้งาน</option>
                                            <option>ผู้จัดการ</option>
                                            <option>บัญชี</option>
                                            <option selected>พนักงานขาย</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">ชื่อผู้ใช้งาน <span class="text-danger">*</span></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="username" placeholder="" value="nutticha"/>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">รหัสผ่าน <span class="text-danger">*</span></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="password" placeholder="*********"/>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">กำหนดอายุบัญชี <span class="text-danger">*</span></label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <div class="form-check form-check-inline">
                                            <input
                                                class="form-check-input"
                                                type="radio"
                                                name="inlineRadioOptions"
                                                id="inlineRadio1"
                                                value="option1"
                                            />
                                            <label class="form-check-label" for="inlineRadio1">ไม่กำหนด</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input
                                                class="form-check-input"
                                                type="radio"
                                                name="inlineRadioOptions"
                                                id="inlineRadio2"
                                                value="option2"
                                                checked
                                            />
                                            <label class="form-check-label" for="inlineRadio2">กำหนด</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" id="fp-default" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" value="2022-11-11" />
                                    </div>
                                </div>

                                <div class="col-xl-12 col-md-12 col-12 mb-1">
                                    <label for="basicInput" class="subText">รูปประจำตัว</label>
                                </div>
                                <div class="col-xl-12 col-md-12 col-12 mb-1">
                                    <div class="form-group">
                                        <form action="#" class="dropzone dropzone-area" id="dpz-multiple-files">
                                            <div class="dz-message" style="font-size: 10pt;">โปรดลากรูปภาพที่ต้องการมาวางที่นี่</div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary"  data-dismiss="modal">บันทึก</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">ปิด</button>
                </div>
            </div>
        </div>
    </div>--}}
    {{--<div
        class="modal fade text-left"
        id="detailModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="myModalLabel17"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel17">ข้อมูลผู้ใช้งาน</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="row">
                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">ชื่อจริง</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
--}}{{--                                        <input type="text" class="form-control" id="fname" placeholder="" value="ณัฐติชา"/>--}}{{--
                                        <strong>ณัฐติชา</strong>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">นามสกุล</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
--}}{{--                                        <input type="text" class="form-control" id="lname" placeholder="" value="แสนแก้ว"/>--}}{{--
                                        <strong>แสนแก้ว</strong>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">เลขบัตรประชาชน</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
--}}{{--                                        <input type="text" class="form-control" id="card-id" value="1234567891234" />--}}{{--
                                        <strong>1234567891234</strong>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">เพศ</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        --}}{{--<select class="form-control" id="gender">
                                            <option>กรุณาระบุเพศ</option>
                                            <option>ชาย</option>
                                            <option selected>หญิง</option>
                                        </select>--}}{{--
                                        <strong>หญิง</strong>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">Facebook</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
--}}{{--                                        <input type="text" class="form-control" id="facebook" value="facebook.com/nutticha"/>--}}{{--
                                        <strong>facebook.com/nutticha</strong>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">LINE ID</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
--}}{{--                                        <input type="text" class="form-control" id="lineid" value="@nutticha"/>--}}{{--
                                        <strong>@nutticha</strong>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">อีเมล</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
--}}{{--                                        <input type="text" class="form-control" id="email" value="nutticha@sesamepos.com"/>--}}{{--
                                        <strong>nutticha@sesamepos.com</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="row">
                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">สิทธิ์การใช้งาน</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        --}}{{--<select class="form-control" id="province">
                                            <option>กรุณาเระบุสิทธิ์การใช้งาน</option>
                                            <option>ผู้จัดการ</option>
                                            <option>บัญชี</option>
                                            <option selected>พนักงานขาย</option>
                                        </select>--}}{{--
                                        <strong>พนักงานขาย</strong>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">ชื่อผู้ใช้งาน</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
--}}{{--                                        <input type="text" class="form-control" id="username" placeholder="" value="nutticha"/>--}}{{--
                                        <strong>nutticha</strong>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">รหัสผ่าน</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
--}}{{--                                        <input type="text" class="form-control" id="password" placeholder="*********"/>--}}{{--
                                        <strong>*********</strong>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">กำหนดอายุบัญชี</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
--}}{{--                                        <div class="form-check form-check-inline">--}}{{--
--}}{{--                                            <input--}}{{--
--}}{{--                                                class="form-check-input"--}}{{--
--}}{{--                                                type="radio"--}}{{--
--}}{{--                                                name="inlineRadioOptions"--}}{{--
--}}{{--                                                id="inlineRadio1"--}}{{--
--}}{{--                                                value="option1"--}}{{--
--}}{{--                                            />--}}{{--
--}}{{--                                            <label class="form-check-label" for="inlineRadio1">ไม่กำหนด</label>--}}{{--
--}}{{--                                        </div>--}}{{--
--}}{{--                                        <div class="form-check form-check-inline">--}}{{--
--}}{{--                                            <input--}}{{--
--}}{{--                                                class="form-check-input"--}}{{--
--}}{{--                                                type="radio"--}}{{--
--}}{{--                                                name="inlineRadioOptions"--}}{{--
--}}{{--                                                id="inlineRadio2"--}}{{--
--}}{{--                                                value="option2"--}}{{--
--}}{{--                                                checked--}}{{--
--}}{{--                                            />--}}{{--
--}}{{--                                            <label class="form-check-label" for="inlineRadio2">กำหนด</label>--}}{{--
--}}{{--                                        </div>--}}{{--
                                        <strong>กำหนด</strong>
                                    </div>
                                    <div class="form-group">
--}}{{--                                        <input type="text" id="fp-default" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" value="2022-11-11" />--}}{{--
                                        <strong>2022-11-11</strong>
                                    </div>
                                </div>

                                <div class="col-xl-12 col-md-12 col-12 mb-1">
                                    <label for="basicInput" class="subText">รูปประจำตัว</label>
                                </div>
                                <div class="col-xl-12 col-md-12 col-12 mb-1">
                                    <div class="form-group">
                                        --}}{{--<form action="#" class="dropzone dropzone-area" id="dpz-multiple-files">
                                            <div class="dz-message" style="font-size: 10pt;">โปรดลากรูปภาพที่ต้องการมาวางที่นี่</div>
                                        </form>--}}{{--
                                        <img src="{{ asset('/images/profile/user-uploads/user.jpg') }}" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
--}}{{--                    <button type="button" class="btn btn-primary"  data-dismiss="modal">บันทึก</button>--}}{{--
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">ปิด</button>
                </div>
            </div>
        </div>
    </div>--}}
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/jszip.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.rowGroup.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
@endsection

@section('page-script')
    <script src="{{ asset('js/dropify/dist/js/dropify.js') }}"></script>
    {{-- Page js files --}}
    {{--  <script src="{{ asset(mix('js/scripts/tables/table-datatables-basic.js')) }}"></script>--}}
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
    <script>
        $(document).ready(function () {
            pdfMake.fonts = {
                THSarabun: {
                    normal: 'THSarabun.ttf',
                    bold: 'THSarabun-Bold.ttf',
                    italics: 'THSarabun-Italic.ttf',
                    bolditalics: 'THSarabun-BoldItalic.ttf'
                }
            };

            listUser();

            $('#addModal').on('show.bs.modal', function() {
                $("#profileUploadAdd").html("<input type=\"file\" class=\"dropify\" name=\"pic\" id=\"picAdd\" data-width=\"200\" data-height=\"150\">\n" +
                    "<div id=\"picErrorAdd\" class=\"invalid-feedback\" style=\"display:block;\"></div>");

                $('.dropify').dropify({
                    messages: {
                        'default': 'Drag and drop a file here or click',
                        'replace': 'Drag and drop or click to replace',
                        'remove': 'Remove',
                        'error': 'Ooops, something wrong happended.'
                    },
                    tpl: {
                        wrap:            '<div class="dropify-wrapper"></div>',
                        loader:          '<div class="dropify-loader"></div>',
                        message:         '<div class="dropify-message"><span class="file-icon" /> <p></p></div>',
                        preview:         '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message"></p></div></div></div>',
                        filename:        '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
                        clearButton:     '<button type="button" class="dropify-clear">ลบ</button>',
                        errorLine:       '<p class="dropify-error"></p>',
                        errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
                    }
                });
            });

            $('#addModal').on('hide.bs.modal', function() {
                $('#addUserForm')[0].reset();

                removeValidation('Add');
            });

            $('#editModal').on('hide.bs.modal', function() {
                $('#editUserForm')[0].reset();

                removeValidation('Edit');
            });

            $('#addUserSubmit').click(function (e) {
                $('#addUserSubmit').empty();
                $('#addUserSubmit').attr("disabled", true);
                $('#addUserSubmit').append("<div class=\"spinner-border spinner-grow-sm text-light\" role=\"status\"><span class=\"sr-only\">Loading...</span></div> กำลังบันทึกข้อมูล");

                removeValidation('Add');

                e.preventDefault();
                var form = $('#addUserForm')[0];
                var data = new FormData(form);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{url('/company-user-store')}}',
                    type: "POST",
                    enctype: 'multipart/form-data',
                    dataType: 'JSON',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (res) {

                        if(res.error == 0){
                            $('#previewAdd').append("<div class=\"alert alert-success\" role=\"alert\">\n" +
                                "<h4 class=\"alert-heading\">เพิ่มข้อมูลเรียบร้อย</h4>\n" +
                                "</div>");
                            setTimeout(function(){
                                $('#addModal').modal('hide');

                                $('#table').dataTable().fnClearTable();
                                $('#table').dataTable().fnDestroy();
                                listUser();
                            }, 1000);
                        }else{
                            $('#previewAdd').append("<div class=\"alert alert-danger\" role=\"alert\">\n" +
                                "<h4 class=\"alert-heading\">พบข้อผิดพลาด</h4>\n" +
                                "</div>");
                            $.each(res.messages, function (k,v) {
                                $('#'+ k + 'Add').addClass('is-invalid');
                                $('#'+ k + 'ErrorAdd').html(v);
                            });
                        }

                        $('#addUserSubmit').empty();
                        $('#addUserSubmit').append("บันทึก");
                        $('#addUserSubmit').attr("disabled",false);

                    }
                });
            });

            $('#editUserSubmit').click(function (e) {
                $('#editUserSubmit').empty();
                $('#editUserSubmit').attr("disabled", true);
                $('#editUserSubmit').append("<div class=\"spinner-border spinner-grow-sm text-light\" role=\"status\"><span class=\"sr-only\">Loading...</span></div> กำลังบันทึกข้อมูล");

                removeValidation('Edit');

                e.preventDefault();
                var id = $('#editUserSubmit').val();
                var form = $('#editUserForm')[0];
                var data = new FormData(form);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{url('/company-user-update')}}/' + id,
                    type: "POST",
                    enctype: 'multipart/form-data',
                    dataType: 'JSON',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (res) {

                        if(res.error == 0){
                            $('#previewEdit').append("<div class=\"alert alert-success\" role=\"alert\">\n" +
                                "<h4 class=\"alert-heading\">เพิ่มข้อมูลเรียบร้อย</h4>\n" +
                                "</div>");
                            setTimeout(function(){
                                $('#editModal').modal('hide');

                                $('#table').dataTable().fnClearTable();
                                $('#table').dataTable().fnDestroy();
                                listUser();
                            }, 1000);
                        }else{
                            $('#previewEdit').append("<div class=\"alert alert-danger\" role=\"alert\">\n" +
                                "<h4 class=\"alert-heading\">พบข้อผิดพลาด</h4>\n" +
                                "</div>");
                            $.each(res.messages, function (k,v) {
                                $('#'+ k + 'Edit').addClass('is-invalid');
                                $('#'+ k + 'ErrorEdit').html(v);
                            });
                        }

                        $('#editUserSubmit').empty();
                        $('#editUserSubmit').append("บันทึก");
                        $('#editUserSubmit').attr("disabled",false);

                    }
                });
            });
        });
        /**
         * DataTables Basic
         */
        function listUser() {
            'use strict';
            var dt_basic_table = $('.datatables-basic'),
                dt_date_table = $('.dt-date'),
                assetPath = '../../../app-assets/';
            if ($('body').attr('data-framework') === 'laravel') {
                assetPath = $('body').attr('data-asset-path');
            }

            // DataTable with buttons
            // --------------------------------------------------------------------
            if (dt_basic_table.length) {
                var dt_basic = dt_basic_table.DataTable({
                    // ajax: assetPath + 'data/table-datatable-user.json',
                    ajax: '{{ url('/company-user-list') }}',
                    columns: [
                        // { data: 'id' },
                        // { data: 'id' },
                        { data: 'id' }, // used for sorting so will hide this column
                        {
                            "data": "id",
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        { data: null},
                        { data: 'email' },
                        { data: 'permission' },
                        { data: 'status' },
                        { data: 'created_at' },
                        // { data: 'id' },
                        { data: null }
                    ],

                    columnDefs: [
                        /*{
                            // For Responsive
                            className: 'control',
                            orderable: false,
                            responsivePriority: 2,
                            targets: 0
                        },
                        {
                            // For Checkboxes
                            targets: 1,
                            orderable: false,
                            responsivePriority: 3,
                            render: function (data, type, full, meta) {
                                return (
                                    '<div class="custom-control custom-checkbox"> <input class="custom-control-input dt-checkboxes" type="checkbox" value="" id="checkbox' +
                                    data +
                                    '" /><label class="custom-control-label" for="checkbox' +
                                    data +
                                    '"></label></div>'
                                );
                            },
                            checkboxes: {
                                selectAllRender:
                                    '<div class="custom-control custom-checkbox"> <input class="custom-control-input" type="checkbox" value="" id="checkboxSelectAll" /><label class="custom-control-label" for="checkboxSelectAll"></label></div>'
                            }
                        },*/
                        {
                            targets: 0,
                            visible: false
                        },
                        {
                            responsivePriority: 1,
                            targets: 2,
                            render: function (data, type, full, meta) {
                                return (
                                    data.name + ' ' + data.surname
                                );
                            }
                        },
                        {
                            responsivePriority: 1,
                            targets: 4,
                            render: function (data, type, full, meta) {
                                return (
                                    data.title
                                );
                            }
                        },
                        /*{
                            // Label
                            targets: -2,
                            render: function (data, type, full, meta) {
                                var $status_number = full['status'];
                                var $status = {
                                    1: { title: 'อยู่ในสัญญา', class: 'badge-light-success' },
                                    2: { title: 'เลยกำหนดชำระ', class: 'badge-light-danger ' },
                                    3: { title: 'ไถ่ถอนแล้ว', class: ' badge-light-warning' },
                                    4: { title: 'Resigned', class: ' badge-light-primary' },
                                    5: { title: 'Applied', class: ' badge-light-info' }
                                };
                                if (typeof $status[$status_number] === 'undefined') {
                                    return data;
                                }
                                return (
                                    '<span class="badge badge-pill ' +
                                    $status[$status_number].class +
                                    '">' +
                                    $status[$status_number].title +
                                    '</span>'
                                );
                            }
                        },*/
                        {
                            // Label
                            targets: 5,
                            render: function (data, type, full, meta) {
                                var $status_number = full['status'];
                                var $status = {
                                    1: { title: 'ปกติ', class: 'badge-light-success' },
                                    0: { title: 'ระงับ', class: 'badge-light-danger ' }
                                };
                                if (typeof $status[$status_number] === 'undefined') {
                                    return data;
                                }
                                return (
                                    '<span class="badge badge-pill ' +
                                    $status[$status_number].class +
                                    '">' +
                                    $status[$status_number].title +
                                    '</span>'
                                );
                            }
                        },
                        {
                            responsivePriority: 1,
                            targets: 6,
                            render: function (data, type, full, meta) {
                                const pos = data.indexOf("T");
                                const str = data.substr(0, pos);
                                const [year, month, day] = str.split("-")
                                return (
                                    day + '/' + month + '/' + year
                                );
                            }
                        },
                        {
                            // Actions
                            targets: -1,
                            title: 'จัดการข้อมูล',
                            orderable: false,
                            render: function (data, type, full, meta) {
                                let btn = '<a href="javascript:;" class="item-edit" data-toggle="modal" data-target="#editModal" data-id="' + data.id + '" onclick="getEditData(' + data.id + ')">' +
                                    feather.icons['edit'].toSvg({ class: 'font-medium-5' }) +
                                    '</a>' + ' ' +
                                    '<a href="javascript:;" class="item-edit" data-toggle="modal" data-target="#detailModal" data-id="' + data.id + '" onclick="getDetailData(' + data.id + ')">' +
                                    feather.icons['eye'].toSvg({ class: 'font-medium-5' }) +
                                    '</a>' + ' ';

                                if(data.status == 1) {
                                    btn += '<a href="javascript:;" class="item-edit" onclick="confirmSuspend(' + data.id + ')">' +
                                    feather.icons['slash'].toSvg({ class: 'font-medium-5' }) +
                                    '</a href="javascript:;">';
                                }
                                else if(data.status == 0) {
                                    btn += '<a href="javascript:;" class="item-edit" onclick="confirmReSuspend(' + data.id + ')">' +
                                        feather.icons['rotate-ccw'].toSvg({ class: 'font-medium-5' }) +
                                        '</a href="javascript:;">';
                                }

                                return (btn);
                            }
                        }
                    ],

                    order: [[0, 'asc']],
                    dom:
                        '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-right"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                    displayLength: 10,
                    lengthMenu: [10, 25, 50, 75, 100],
                    buttons: [
                        {
                            text: 'เพิ่มผู้ใช้งานใหม่',
                            className: 'create-new btn btn-primary mr-50',
                            attr: {
                                'data-toggle': 'modal',
                                'data-target': '#addModal'
                            },
                            init: function (api, node, config) {
                                $(node).removeClass('btn-secondary');
                            }
                        },
                        {
                            extend: 'collection',
                            className: 'btn btn-outline-secondary dropdown-toggle',
                            text: feather.icons['share'].toSvg({ class: 'font-small-4 mr-50' }) + 'Export',
                            buttons: [
                                {
                                    extend: 'print',
                                    text: feather.icons['printer'].toSvg({ class: 'font-small-4 mr-50' }) + 'Print',
                                    className: 'dropdown-item',
                                    exportOptions: { columns: [1, 2, 3, 4, 5, 6] }
                                },
                                {
                                    extend: 'csv',
                                    text: feather.icons['file-text'].toSvg({ class: 'font-small-4 mr-50' }) + 'Csv',
                                    className: 'dropdown-item',
                                    exportOptions: { columns: [1, 2, 3, 4, 5, 6] }
                                },
                                {
                                    extend: 'excel',
                                    text: feather.icons['file'].toSvg({ class: 'font-small-4 mr-50' }) + 'Excel',
                                    className: 'dropdown-item',
                                    exportOptions: { columns: [1, 2, 3, 4, 5, 6] }
                                },
                                {
                                    extend: 'pdf',
                                    text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 mr-50' }) + 'Pdf',
                                    className: 'dropdown-item',
                                    exportOptions: { columns: [1, 2, 3, 4, 5, 6] },
                                    customize: function(doc) {
                                        doc.defaultStyle = {
                                            font:'THSarabun',
                                        };
                                        doc.content[1].table.widths =
                                            Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                                        doc.defaultStyle.alignment = 'left';
                                        doc.styles.tableHeader.alignment = 'left';
                                    }
                                },
                                {
                                    extend: 'copy',
                                    text: feather.icons['copy'].toSvg({ class: 'font-small-4 mr-50' }) + 'Copy',
                                    className: 'dropdown-item',
                                    exportOptions: { columns: [1, 2, 3, 4, 5, 6] }
                                }
                            ],
                            init: function (api, node, config) {
                                $(node).removeClass('btn-secondary');
                                $(node).parent().removeClass('btn-group');
                                setTimeout(function () {
                                    $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                                }, 50);
                            },
                        }
                    ],

                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Details of ' + data['full_name'];
                                }
                            }),
                            type: 'column',
                            renderer: function (api, rowIdx, columns) {
                                var data = $.map(columns, function (col, i) {
                                    return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                                        ? '<tr data-dt-row="' +
                                        col.rowIndex +
                                        '" data-dt-column="' +
                                        col.columnIndex +
                                        '">' +
                                        '<td>' +
                                        col.title +
                                        ':' +
                                        '</td> ' +
                                        '<td>' +
                                        col.data +
                                        '</td>' +
                                        '</tr>'
                                        : '';
                                }).join('');
                                return data ? $('<table class="table"/>').append(data) : false;
                            }
                        }
                    },

                    language: {
                        paginate: {
                            // remove previous & next text from pagination
                            previous: '&nbsp;',
                            next: '&nbsp;'
                        }
                    }
                });
                $('div.head-label').html('<h6 class="card-title mb-0">จัดการผู้ใช้งานระบบ</h6>');
            }

            // Flat Date picker
            if (dt_date_table.length) {
                dt_date_table.flatpickr({
                    monthSelectorType: 'static',
                    dateFormat: 'm/d/Y'
                });
            }
        }

        function getEditData(id) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('company-user-show') }}/' + id,
                type: "GET",
                enctype: 'multipart/form-data',
                dataType: 'JSON',
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    $('#editUserSubmit').val(res.data.id);
                    $('#nameEdit').val(res.data.name);
                    $('#surnameEdit').val(res.data.surname);
                    $('#id_cardEdit').val(res.data.id_card);
                    $('#genderEdit').val(res.data.gender.id);
                    $('#facebookEdit').val(res.data.facebook);
                    $('#lineEdit').val(res.data.line);
                    $('#emailEdit').val(res.data.email);
                    $('#permission_idEdit').val(res.data.permission.id);
                    $('#usernameEdit').val(res.data.username);

                    if(res.data.acc_expire == null || res.data.acc_expire == '') {
                        $('#accExpireStatusNoneEdit').prop('checked', true);
                        showAccExpireDateInput(0, 'edit');
                    }
                    else {
                        $('#accExpireStatusSetEdit').prop('checked', true);
                        showAccExpireDateInput(1, 'edit');

                        const pos = res.data.acc_expire.indexOf(" ");
                        const str = res.data.acc_expire.substr(0, pos);
                        const [year, month, day] = str.split("-")

                        $("#acc_expireEdit").flatpickr({
                            defaultDate: new Date(year + '-' + month + '-' + day),
                        });
                    }

                    if(res.data.pic != null && res.data.pic != '') {
                        $("#profileUploadEdit").html("<input type=\"file\" class=\"dropify\" name=\"pic\" id=\"picEdit\" data-width=\"200\" data-height=\"150\" data-default-file=\"{{asset('/_uploads/_users')}}/" + res.data.pic + "\" />\n" +
                            "<div id=\"picErrorEdit\" class=\"invalid-feedback\" style=\"display:block;\"></div>");
                    }
                    else {
                        $("#profileUploadEdit").html("<input type=\"file\" class=\"dropify\" name=\"pic\" id=\"picEdit\" data-width=\"200\" data-height=\"150\" />\n" +
                            "<div id=\"picErrorEdit\" class=\"invalid-feedback\" style=\"display:block;\"></div>");
                    }

                    $('.dropify').dropify({
                        messages: {
                            'default': 'Drag and drop a file here or click',
                            'replace': 'Drag and drop or click to replace',
                            'remove': 'Remove',
                            'error': 'Ooops, something wrong happended.'
                        },
                        tpl: {
                            wrap:            '<div class="dropify-wrapper"></div>',
                            loader:          '<div class="dropify-loader"></div>',
                            message:         '<div class="dropify-message"><span class="file-icon" /> <p></p></div>',
                            preview:         '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message"></p></div></div></div>',
                            filename:        '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
                            clearButton:     '<button type="button" class="dropify-clear">ลบ</button>',
                            errorLine:       '<p class="dropify-error"></p>',
                            errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
                        }
                    });
                }
            });
        }

        function getDetailData(id) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('company-user-show') }}/' + id,
                type: "GET",
                enctype: 'multipart/form-data',
                dataType: 'JSON',
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    $('#nameDetail').html(res.data.name);
                    $('#surnameDetail').html(res.data.surname);
                    $('#id_cardDetail').html(res.data.id_card);
                    $('#genderDetail').html(res.data.gender.name);
                    $('#facebookDetail').html(res.data.facebook);
                    $('#lineDetail').html(res.data.line);
                    $('#emailDetail').html(res.data.email);
                    $('#permission_idDetail').html(res.data.permission.title);
                    $('#usernameDetail').html(res.data.username);

                    if(res.data.acc_expire == null || res.data.acc_expire == '') {
                        $('#accExpireStatusDetail').html("บัญชีนี้ไม่มีการกำหนดอายุการใช้งาน");
                    }
                    else {
                        const pos = res.data.acc_expire.indexOf(" ");
                        const str = res.data.acc_expire.substr(0, pos);
                        const [year, month, day] = str.split("-")
                        // $('#acc_expireDetail').html(year + '-' + month + '-' + day);
                        $('#accExpireStatusDetail').html("บัญชีนี้สามารถใช้งานได้ถึงวันที่ " + day + '/' + month + '/' + year);
                    }

                    if(res.data.pic != null && res.data.pic != '') {
                        $("#profileUploadDetail").html("<img src=\"{{asset('/_uploads/_users')}}/" + res.data.pic + "\" width=\"100%\" />");
                    }
                    else {
                        $("#profileUploadDetail").html("<strong>ยังไม่ได้ทำการอัพโหลด</strong>");
                    }
                }
            });
        }

        function confirmSuspend(id) {
            //--------------- Confirm Options ---------------
            Swal.fire({
                title: 'คุณแน่ใจหรือไม่ที่จะระงับผู้ใช้งานรายนี้?',
                text: "เมื่อดำเนินการแล้วผู้ใช้งานรายดังกล่าวจะไม่สามารถเข้ามาใช้งานระบบได้!",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'ยกเลิก',
                confirmButtonText: 'ยืนยัน',
                customClass: {
                    confirmButton: 'btn btn-primary',
                    cancelButton: 'btn btn-outline-danger ml-1'
                },
                buttonsStyling: false
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{{url('/company-user-suspend')}}/' + id,
                        type: "POST",
                        enctype: 'multipart/form-data',
                        dataType: 'JSON',
                        // data: data,
                        processData: false,
                        contentType: false,
                        cache: false,
                        success: function (res) {
                            if(res.error == 0){
                                Swal.fire({
                                    icon: 'success',
                                    title: 'ระงับผู้ใช้งานเรียบร้อย!',
                                    text: 'ผู้ใช้งานรายดังกล่าวจะไม่สามารถเข้ามาใช้งานระบบได้แล้ว',
                                    customClass: {
                                        confirmButton: 'btn btn-success'
                                    }
                                });

                                $('#table').dataTable().fnClearTable();
                                $('#table').dataTable().fnDestroy();
                                listUser();
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'ระงับผู้ใช้งานเไม่สำเร็จ!',
                                    text: 'เกิดข้อผิดพลาดในการระงับผู้ใช้งานดังกล่าว',
                                    customClass: {
                                        confirmButton: 'btn btn-primary'
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }

        function confirmReSuspend(id) {
            //--------------- Confirm Options ---------------
            Swal.fire({
                title: 'คุณแน่ใจหรือไม่ที่จะกู้คืนผู้ใช้งานรายนี้?',
                text: "เมื่อดำเนินการแล้วผู้ใช้งานรายนี้จะสามารถกลับเข้ามาใช้งานระบบได้!",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'ยกเลิก',
                confirmButtonText: 'ยืนยัน',
                customClass: {
                    confirmButton: 'btn btn-primary',
                    cancelButton: 'btn btn-outline-danger ml-1'
                },
                buttonsStyling: false
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{{url('/company-user-resuspend')}}/' + id,
                        type: "POST",
                        enctype: 'multipart/form-data',
                        dataType: 'JSON',
                        // data: data,
                        processData: false,
                        contentType: false,
                        cache: false,
                        success: function (res) {
                            if(res.error == 0){
                                Swal.fire({
                                    icon: 'success',
                                    title: 'กู้คืนผู้ใช้งานเรียบร้อย!',
                                    text: 'ผู้ใช้งานรายดังกล่าวสามารถกลับเข้ามาใช้งานระบบได้แล้ว',
                                    customClass: {
                                        confirmButton: 'btn btn-success'
                                    }
                                });

                                $('#table').dataTable().fnClearTable();
                                $('#table').dataTable().fnDestroy();
                                listUser();
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'ระงับผู้ใช้งานเไม่สำเร็จ!',
                                    text: 'เกิดข้อผิดพลาดในการระงับผู้ใช้งานดังกล่าว',
                                    customClass: {
                                        confirmButton: 'btn btn-primary'
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }

        function showAccExpireDateInput(type, mode) {
            if(mode == 'add') {
                if(type == 0) {
                    $("#accExpireDateInput").attr("hidden", true);
                }
                else if(type == 1) {
                    $("#accExpireDateInput").attr("hidden", false);
                }
            }
            else if(mode == 'edit') {
                if(type == 0) {
                    $("#accExpireDateInputEdit").attr("hidden", true);

                    $("#acc_expireEdit").flatpickr({
                        defaultDate: "today",
                    });
                }
                else if(type == 1) {
                    $("#accExpireDateInputEdit").attr("hidden", false);
                }
            }
        }

        function removeValidation(status) {
            $('#preview' + status).empty();
            $('#name' + status).removeClass('is-invalid');
            $('#surname' + status).removeClass('is-invalid');
            $('#id_card' + status).removeClass('is-invalid');
            $('#gender' + status).removeClass('is-invalid');
            $('#permission_id' + status).removeClass('is-invalid');
            $('#username' + status).removeClass('is-invalid');
            $('#password' + status).removeClass('is-invalid');
            $('#acc_expire' + status).removeClass('is-invalid');
            $('#pic' + status).removeClass('is-invalid');

            $('#nameError' + status).html("");
            $('#surnameError' + status).html("");
            $('#id_cardError' + status).html("");
            $('#genderError' + status).html("");
            $('#permission_idError' + status).html("");
            $('#usernameError' + status).html("");
            $('#passwordError' + status).html("");
            $('#acc_expireError' + status).html("");
            $('#picError' + status).html("");
        }
    </script>
@endsection
