@extends('layouts/contentLayoutMasterMain')
{{--@section('title', ', 'Progress')--}}

@section('vendor-style')
    {{-- Vendor Css files --}}
{{--    <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{asset('vendors/animate-css/animate.css')}}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{asset('vendors/chartist-js/chartist.min.css')}}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{asset('vendors/chartist-js/chartist-plugin-tooltip.css')}}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{asset('js/jquery-ui/jquery-ui.css')}}">--}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/charts/chart-apex.css')) }}">
@endsection

@section('content')
    <style>
        #backsercus {
            width: 100%;
            height: 300px;
            font-size: 14px;
        }
        #backsercusbar {
            width: 100%;
            height: 300px;
            font-size: 14px;
        }
        #chartdivIncome {
            width: 100%;
            height: 300px;
            font-size: 14px;
        }
        #average-income {
            width: 100%;
            height: 300px;
            font-size: 14px;
        }
        #Newcustomer {
            width: 100%;
            height: 350px;
            font-size: 14px;
        }
        #Sercustomer {
            width: 100%;
            height: 350px;
            font-size: 14px;
        }
        #Numbercustomer {
            width: 100%;
            height: 1000px;
            font-size: 14px;
        }
        #monthlyorder {
            width: 100%;
            height: 400px;
            font-size: 14px;
        }
        .empty-data {
             height: inherit;
             display: flex;
             justify-content: center;
             align-items: center;
             flex-direction: column;
         }

    </style>
    <!-- Statistics card section -->
    <div class="card">
        <div class="card-body">
            <ul class="nav nav-pills mb-0" role="tablist">
                <li class="nav-item">
                    <a
                        class="nav-link d-flex align-items-center active"
                        id="account-tab"
                        data-toggle="tab"
                        href="#account"
                        aria-controls="account"
                        role="tab"
                        aria-selected="true"
                    ><span class="d-none d-sm-block">รายได้</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a
                        class="nav-link d-flex align-items-center"
                        id="information-tab"
                        data-toggle="tab"
                        href="#information"
                        aria-controls="information"
                        role="tab"
                        aria-selected="false"
                    >
                        <span class="d-none d-sm-block">ลูกค้า</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a
                        class="nav-link d-flex align-items-center"
                        id="social-tab"
                        data-toggle="tab"
                        href="#social"
                        aria-controls="social"
                        role="tab"
                        aria-selected="false"
                    >
                        <span class="d-none d-sm-block">คำสั่งซื้อ</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="tab-content">
        <div class="tab-pane active" id="account" aria-labelledby="account-tab" role="tabpanel">
            <div class="row">
                <div class="col-lg-8 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">รายรับต่อเดือน</h4>
                            <div class="d-flex align-items-center">
                                <div class="badge badge-light-primary">บาท/เดือน</div>
                            </div>
                        </div>
                        <div class="card-body" >
                            <div class="row">
                                <div id="chartdivIncome" class="empty-data"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12">
                    <div class="card text-center" style="height: 385px;">
                        <div class="card-body">
                            <div class="btn-group mt-5">
                                <button
                                    id="predicting_sales_btn"
                                    type="button"
                                    class="btn btn-outline-success btn-sm dropdown-toggle budget-dropdown"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                >
                                    <span id="predicting_sales_label"></span>
                                </button>
                                <div class="dropdown-menu" id="predicting_sales_list">
                                </div>
                            </div>
                            <h2 class="mb-25 mt-2">฿<span id="predicting_sales_value"></span></h2>
                            <div class="d-flex justify-content-center mb-3">
                                <span>การพยากรณ์ยอดขาย</span>
                            </div>
                            <div class="mb-2" id="budget-chart"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">รายได้เฉลี่ยต่อการสั่งซื้อ</h4>
                            <div class="d-flex align-items-center">
                                <div class="badge badge-light-primary">บาท/เดือน</div>
                            </div>
                        </div>
                        <div class="card-body" >
                            <div class="row">
                                <div id="average-income" class="empty-data"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12">
                    <div class="card text-center" style="height: 385px;">
                        <div class="card-body">
                            <div class="avatar bg-light-danger p-50 mb-3 mt-5">
                                <div class="avatar-content">
                                    <i data-feather="dollar-sign" class="font-medium-5"></i>
                                </div>
                            </div>
                            <br>
                            <span class="font-weight-bolder" style="font-size: 28px;" id="customer-lifetime-value"></span> <span class="badge badge-danger">บาท/เดือน</span>
                            <p class="card-text">มูลค่าการใช้จ่ายเฉลี่ย<br>
                                ตลอดการใช้บริการ</p>
                        </div>
                    </div>
                </div>
            </div>

            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <input type="hidden" id="maxSpender" value="{{ $maxSpender }}"/>
                            <table id="TableCost" class="datatables-basic table">
                                <thead>
                                <tr>
                                    <th>ลำดับ</th>
                                    <th>เลขบัตรประชาชน</th>
                                    <th>รายได้</th>
                                    <th></th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <div class="tab-pane" id="information" aria-labelledby="information-tab" role="tabpanel">
            <div class="row">
                <div class="col-lg-8 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">อัตราส่วนลูกค้าใหม่</h4>
                            <div class="d-flex align-items-center">
                                <div class="badge badge-light-primary">%/เดือน</div>
                            </div>
                        </div>
                        <div class="card-body" >
                            <div class="row">
                                <div id="Newcustomer" class="empty-data"></div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">อัตราของลูกค้าที่มีโอกาสจะเลิกใช้บริการ</h4>
                            <div class="d-flex align-items-center">
                                <div class="badge badge-light-primary">%/เดือน</div>
                            </div>
                        </div>
                        <div class="card-body" >
                            <div class="row">
                                <div id="Sercustomer" class="empty-data"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12">
                    <div class="card" style="height: 898px;">
                        <div class="card-header">
                            <h4 class="card-title">การแบ่งกลุ่มลูกค้าตามมูลค่า</h4>
                        </div>
                        <div class="card-body" >
{{--                            <div class="row">--}}
                                <div class="demo-vertical-spacing">
                                    <div class="progress-wrapper">
                                        <div id="example-caption-1">New Customers</div>
                                        <div class="progress progress-bar-primary">
                                            <div
                                                id="new_customers"
                                                class="progress-bar"
                                                role="progressbar"
                                                aria-valuenow="0"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                                style="width: 0%"
                                            ></div>
                                        </div>
                                    </div>
                                    <div class="progress-wrapper mt-2">
                                        <div id="example-caption-2">Promising</div>
                                        <div class="progress progress-bar-secondary">
                                            <div
                                                id="promising"
                                                class="progress-bar"
                                                role="progressbar"
                                                aria-valuenow="0"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                                style="width: 0%"
                                            ></div>
                                        </div>
                                    </div>
                                    <div class="progress-wrapper mt-2">
                                        <div id="example-caption-3">Loyal Customers</div>
                                        <div class="progress progress-bar-success">
                                            <div
                                                id="loyal_customers"
                                                class="progress-bar"
                                                role="progressbar"
                                                aria-valuenow="0"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                                style="width: 0%"
                                            ></div>
                                        </div>
                                    </div>
                                    <div class="progress-wrapper mt-2">
                                        <div id="example-caption-4">About To Sleep</div>
                                        <div class="progress progress-bar-danger">
                                            <div
                                                id="about_to_sleep"
                                                class="progress-bar"
                                                role="progressbar"
                                                aria-valuenow="0"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                                style="width: 0%"
                                            ></div>
                                        </div>
                                    </div>
                                    <div class="progress-wrapper mt-2">
                                        <div id="example-caption-5">Hibernating</div>
                                        <div class="progress progress-bar-warning">
                                            <div
                                                id="hibernating"
                                                class="progress-bar"
                                                role="progressbar"
                                                aria-valuenow="0"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                                style="width: 0%"
                                            ></div>
                                        </div>
                                    </div>
                                    <div class="progress-wrapper mt-2">
                                        <div id="example-caption-5">Potential Loyailst</div>
                                        <div class="progress progress-bar-info">
                                            <div
                                                id="potential_loyailst"
                                                class="progress-bar"
                                                role="progressbar"
                                                aria-valuenow="0"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                                style="width: 0%"
                                            ></div>
                                        </div>
                                    </div>
                                    <div class="progress-wrapper mt-2">
                                        <div id="example-caption-5">Cannot Lose Them</div>
                                        <div class="progress progress-bar-dark">
                                            <div
                                                id="cannot_lose_them"
                                                class="progress-bar"
                                                role="progressbar"
                                                aria-valuenow="0"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                                style="width: 0%"
                                            ></div>
                                        </div>
                                    </div>
                                    <div class="progress-wrapper mt-2">
                                        <div id="example-caption-5">Need Attention</div>
                                        <div class="progress progress-bar-primary">
                                            <div
                                                id="need_attention"
                                                class="progress-bar"
                                                role="progressbar"
                                                aria-valuenow="0"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                                style="width: 0%"
                                            ></div>
                                        </div>
                                    </div>
                                    <div class="progress-wrapper mt-2">
                                        <div id="example-caption-5">Champoions</div>
                                        <div class="progress progress-bar-secondary">
                                            <div
                                                id="champoions"
                                                class="progress-bar"
                                                role="progressbar"
                                                aria-valuenow="0"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                                style="width: 0%"
                                            ></div>
                                        </div>
                                    </div>
                                    <div class="progress-wrapper mt-2">
                                        <div id="example-caption-5">At Risk</div>
                                        <div class="progress progress-bar-success">
                                            <div
                                                id="at_risk"
                                                class="progress-bar"
                                                role="progressbar"
                                                aria-valuenow="0"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                                style="width: 0%"
                                            ></div>
                                        </div>
                                    </div>
                                    <div class="progress-wrapper mt-2">
                                        <div id="example-caption-5">Unknow</div>
                                        <div class="progress progress-bar-danger">
                                            <div
                                                id="unknow"
                                                class="progress-bar"
                                                role="progressbar"
                                                aria-valuenow="40"
                                                aria-valuemin="40"
                                                aria-valuemax="100"
                                                style="width: 0%"
                                            ></div>
                                        </div>
                                    </div>
                                </div>
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">จำนวนลูกค้าที่ใช้บริการต่อเดือน</h4>
                            <div class="d-flex align-items-center">
                                <div class="badge badge-light-primary">คน/เดือน</div>
                            </div>
                        </div>
                        <div class="card-body" >
                            <div class="row">
                                <div id="Numbercustomer" class="empty-data"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-pane" id="social" aria-labelledby="social-tab" role="tabpanel">
            <div class="row">
                <div class="col-lg-6 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">การพยากรณ์การกลับมาใช้บริการของกลุ่มลูกค้า</h4>
                        </div>
                        <div class="card-body" >
                            <div class="row">
                                <div id="backsercus" class="empty-data"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="card text-center" style="height: 385px;">
                        <div class="card-header">
                            <h4 class="card-title">การพยากรณ์การกลับมาใช้บริการของกลุ่มลูกค้า</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div id="backsercusbar" class="empty-data"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-12">
                    <div class="card text-center">
                        <div class="card-header">
                            <h4 class="card-title">จำนวนการสั่งซื้อต่อเดือน</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div id="monthlyorder" class="empty-data"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <table id="TablebackserCustomer" class="datatables-basic table">
                                <thead>
                                <tr>
                                    <th>ลำดับ</th>
                                    <th>เลขบัตรประชาชน</th>
                                    <th>มูลค่า</th>
                                    <th>การกลับมาใช้บริการ</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!--/ Statistics Card section-->
@endsection

@section('vendor-script')
    {{-- Vendor js files --}}
{{--    <script src="{{ asset(mix('vendors/js/jquery/jquery.min.js')) }}"></script>--}}
{{--        <script src="{{ asset('vendors/js/jquery/jquery.min.js') }}"></script>--}}
{{--    <script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>--}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/jszip.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.rowGroup.min.js')) }}"></script>
@endsection
@section('page-script')
    {{-- Page js files --}}
{{--    <script src="{{ asset(mix('js/scripts/cards/card-statistics.js')) }}"></script>--}}
{{--    <script src="{{ asset(mix('js/scripts/cards/card-analytics.js')) }}"></script>--}}
@endsection
<script src="{{ asset('vendors/js/jquery/jquery.min.js') }}"></script>
<script type='text/javascript' src='https://cdn.amcharts.com/lib/4/core.js?ver=20210901-5e9' id='-lib-4-core-js-js'></script>
<script type='text/javascript' src='https://cdn.amcharts.com/lib/4/charts.js?ver=20210901-5e9' id='-lib-4-charts-js-js'></script>
<script type='text/javascript' src='https://cdn.amcharts.com/lib/4/themes/dataviz.js?ver=20210901-5e9' id='-lib-4-themes-dataviz-js-js'></script>
<script type='text/javascript' src='https://cdn.amcharts.com/lib/4/themes/material.js?ver=20210901-5e9' id='-lib-4-themes-material-js-js'></script>
<script type='text/javascript' src='https://cdn.amcharts.com/lib/4/themes/kelly.js?ver=20210901-5e9' id='-lib-4-themes-kelly-js-js'></script>
<script type='text/javascript' src='https://cdn.amcharts.com/lib/4/themes/dark.js?ver=20210901-5e9' id='-lib-4-themes-dark-js-js'></script>
<script type='text/javascript' src='https://cdn.amcharts.com/lib/4/themes/frozen.js?ver=20210901-5e9' id='-lib-4-themes-frozen-js-js'></script>
<script type='text/javascript' src='https://cdn.amcharts.com/lib/4/themes/moonrisekingdom.js?ver=20210901-5e9' id='-lib-4-themes-moonrisekingdom-js-js'></script>
<script type='text/javascript' src='https://cdn.amcharts.com/lib/4/themes/spiritedaway.js?ver=20210901-5e9' id='-lib-4-themes-spiritedaway-js-js'></script>
<script type='text/javascript' src='https://cdn.amcharts.com/lib/4/themes/animated.js?ver=20210901-5e9' id='-lib-4-themes-animated-js-js'></script>
<script src="https://cdn.amcharts.com/lib/5/index.js"></script>
<script src="https://cdn.amcharts.com/lib/5/xy.js"></script>
<script src="https://cdn.amcharts.com/lib/5/themes/Kelly.js"></script>

<script>
    $(document).ready(function(){
        pdfMake.fonts = {
            THSarabun: {
                normal: 'THSarabun.ttf',
                bold: 'THSarabun-Bold.ttf',
                italics: 'THSarabun-Italic.ttf',
                bolditalics: 'THSarabun-BoldItalic.ttf'
            }
        };

        MonthlyIncome();
        averageIncome();
        NewCustomer();
        SerCustomer();
        NumberCustomer();
        BackSerCustomer();
        BackSerCusBar();
        MonthlyOrder();
        TableBackService();

        PredictingSalesList();
        customerLifeTimeValue();
        highLowSpender();
        customerSegmentation();
    });

    function customerSegmentation() {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url('chart-data/customer-segmentation') }}',
            type: "GET",
            enctype: 'multipart/form-data',
            dataType: 'JSON',
            processData: false,
            contentType: false,
            cache: false,
            success: function (res) {
                if(res.data.length != 0) {
                    $.each(res.data, function (i, v) {
                        switch(v.Segment) {
                            case 'New Customers':
                                customerSegmentationVal(v.total, res.max, 'new_customers');
                                break;
                            case 'Promising':
                                customerSegmentationVal(v.total, res.max, 'promising');
                                break;
                            case 'Loyal Customers':
                                customerSegmentationVal(v.total, res.max, 'loyal_customers');
                                break;
                            case 'About To Sleep':
                                customerSegmentationVal(v.total, res.max, 'about_to_sleep');
                                break;
                            case 'Hibernating':
                                customerSegmentationVal(v.total, res.max, 'hibernating');
                                break;
                            case 'Potential Loyalist':
                                customerSegmentationVal(v.total, res.max, 'potential_loyailst');
                                break;
                            case 'Cannot Lose Them':
                                customerSegmentationVal(v.total, res.max, 'cannot_lose_them');
                                break;
                            case 'Need Attention':
                                customerSegmentationVal(v.total, res.max, 'need_attention');
                                break;
                            case 'Champions':
                                customerSegmentationVal(v.total, res.max, 'champoions');
                                break;
                            case 'At Risk':
                                customerSegmentationVal(v.total, res.max, 'at_risk');
                                break;
                            case 'Unknow':
                                customerSegmentationVal(v.total, res.max, 'unknow');
                                break;
                            default:
                                break;
                        }
                    });
                }
            }
        });
    }

    function customerSegmentationVal(total, max, segment) {
        const val = Math.floor((total*100)/max);
        $('#' + segment).attr('aria-valuenow', val);
        $('#' + segment).attr('style', 'width: 100%');
        $('#' + segment).html(val + '%');
    }

    function PredictingSalesList() {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url('chart-data/predicting-sales') }}',
            type: "GET",
            enctype: 'multipart/form-data',
            dataType: 'JSON',
            processData: false,
            contentType: false,
            cache: false,
            success: function (res) {
                if(res.data.length != 0) {
                    $('#predicting_sales_btn').attr("disabled", false);
                    $('#predicting_sales_list').empty();
                    $.each(res.data, function (i, v) {
                        if(i == 0) {
                            $('#predicting_sales_label').html(v.date);
                            $('#predicting_sales_value').html(v.predicted);
                        }
                        $('#predicting_sales_list').append('<a class="dropdown-item" href="javascript:PredictingSales(\'' + v.predicted + '\');">' + v.date + '</a>\n');
                    });
                }
                else {
                    $('#predicting_sales_btn').attr("disabled", true);
                    $('#predicting_sales_label').html("ข้อมูลไม่เพียงพอ");
                    $('#predicting_sales_list').empty();
                    $('#predicting_sales_value').html(0);
                }
            }
        });
    }

    function PredictingSales(value) {
        $('#predicting_sales_value').html(value);
    }

    function MonthlyIncome() {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url('chart-data/monthly-revenue') }}',
            type: "GET",
            enctype: 'multipart/form-data',
            dataType: 'JSON',
            processData: false,
            contentType: false,
            cache: false,
            success: function (res) {
                if (res.data.length > 0){
                    // Themes begin
                    am4core.useTheme(am4themes_frozen);
                    // Themes end

                    // Create chart instance
                    var chart = am4core.create("chartdivIncome", am4charts.XYChart);
                    chart.logo.disabled = true;

                    // Add Data
                    let data = [];
                    $.each(res.data, function (i, v) {
                        data.push({
                            country: v.date,
                            visits: v.revenue
                        });
                    });
                    chart.data = data;

                    // Create axes
                    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                    categoryAxis.dataFields.category = "country";
                    // categoryAxis.renderer.labels.template.rotation = 270;
                    categoryAxis.renderer.grid.template.location = 0;
                    categoryAxis.renderer.minGridDistance = 30;

                    categoryAxis.renderer.labels.template.adapter.add("dy", function (dy, target) {
                        if (target.dataItem && target.dataItem.index & 2 == 2) {
                            return dy + 25;
                        }
                        return dy;
                    });

                    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

                    // Create series
                    var series = chart.series.push(new am4charts.ColumnSeries());
                    series.dataFields.valueY = "visits";
                    series.dataFields.categoryX = "country";
                    series.name = "Visits";
                    series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
                    series.columns.template.fillOpacity = .8;

                    var columnTemplate = series.columns.template;
                    columnTemplate.strokeWidth = 2;
                    columnTemplate.strokeOpacity = 1;

                }else {
                    $('#chartdivIncome').html("- ข้อมูลไม่เพียงพอ -");
                }
            }
        });
    }

    function averageIncome() {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url('chart-data/average-revenue') }}',
            type: "GET",
            enctype: 'multipart/form-data',
            dataType: 'JSON',
            processData: false,
            contentType: false,
            cache: false,
            success: function (res) {
                if (res.data.length > 0){
                    try {
                        // Themes begin
                        am4core.useTheme(am4themes_dataviz);
                        // Themes end

                        // Create chart instance
                        var chart = am4core.create("average-income", am4charts.XYChart);
                        chart.logo.disabled = true;

                        // Add Data
                        let data = [];
                        $.each(res.data, function (i, v) {
                            data.push({
                                year: v.date,
                                income: v.net,
                                expenses: v.order.net
                            });
                        });

                        /* Create axes */
                        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                        categoryAxis.dataFields.category = "year";
                        categoryAxis.renderer.minGridDistance = 30;

                        categoryAxis.renderer.labels.template.adapter.add("dy", function (dy, target) {
                            if (target.dataItem && target.dataItem.index & 2 == 2) {
                                return dy + 25;
                            }
                            return dy;
                        });

                        /* Create value axis */
                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

                        /* Create series */
                        var columnSeries = chart.series.push(new am4charts.ColumnSeries());
                        columnSeries.name = "Order Average";
                        columnSeries.dataFields.valueY = "income";
                        columnSeries.dataFields.categoryX = "year";

                        columnSeries.columns.template.tooltipText = "[#fff font-size: 10px]{name} in {categoryX}:\n[/][#fff font-size: 10px]{valueY}[/] [#fff]{additional}[/]"
                        columnSeries.columns.template.propertyFields.fillOpacity = "fillOpacity";
                        columnSeries.columns.template.propertyFields.stroke = "stroke";
                        columnSeries.columns.template.propertyFields.strokeWidth = "strokeWidth";
                        columnSeries.columns.template.propertyFields.strokeDasharray = "columnDash";
                        columnSeries.tooltip.label.textAlign = "middle";

                        var lineSeries = chart.series.push(new am4charts.LineSeries());
                        lineSeries.name = "Total Number of Order";
                        lineSeries.dataFields.valueY = "expenses";
                        lineSeries.dataFields.categoryX = "year";

                        lineSeries.stroke = am4core.color("#fdd400");
                        lineSeries.strokeWidth = 3;
                        lineSeries.propertyFields.strokeDasharray = "lineDash";
                        lineSeries.tooltip.label.textAlign = "middle";

                        var bullet = lineSeries.bullets.push(new am4charts.Bullet());
                        bullet.fill = am4core.color("#fdd400"); // tooltips grab fill from parent by default
                        bullet.tooltipText = "[#fff font-size: 10px]{name} in {categoryX}:\n[/][#fff font-size: 10px]{valueY}[/] [#fff]{additional}[/]"
                        var circle = bullet.createChild(am4core.Circle);
                        circle.radius = 4;
                        circle.fill = am4core.color("#fff");
                        circle.strokeWidth = 3;

                        chart.data = data;
                    } catch (e) {
                        console.log(e);
                    }
                }else {
                    $('#average-income').html("- ข้อมูลไม่เพียงพอ -");
                }
            }
        });
    }

    function customerLifeTimeValue() {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url('chart-data/customer-lifetime') }}',
            type: "GET",
            enctype: 'multipart/form-data',
            dataType: 'JSON',
            processData: false,
            contentType: false,
            cache: false,
            success: function (res) {
                $('#customer-lifetime-value').html(res.data);
            }
        });
    }

    function highLowSpender() {
        'use strict';

        var dt_basic_table = $('#TableCost'),
            dt_date_table = $('.dt-date'),
            assetPath = '../../../app-assets/';

        if ($('body').attr('data-framework') === 'laravel') {
            assetPath = $('body').attr('data-asset-path');
        }

        // DataTable with buttons
        // --------------------------------------------------------------------
        if (dt_basic_table.length) {
            var dt_basic = dt_basic_table.DataTable({
                ajax: '{{ url('chart-data/lowhigh-spender') }}',
                columns: [
                    {
                        "data": "id",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { data: 'CustomerID' },
                    { data: 'spend' },
                    { data: 'Revenue'},
                ],
                columnDefs: [
                    {
                        // Label
                        targets: -2,
                        render: function (data, type, full, meta) {
                            var $status = {
                                1: { title: '680,478.00', class: 'badge-light-success' }
                            };

                            return (
                                '<span class="badge badge-pill badge-light-success">' + data + '</span>'
                            );
                        }
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'จัดการข้อมูล',
                        orderable: false,
                        render: function (data, type, full, meta) {
                            const max = $('#maxSpender').val();
                            const val = Math.floor((data*100)/max);
                            return (
                                '<div class="progress progress-bar-warning">' +
                                '<div class="progress-bar" role="progressbar" aria-valuenow="' + val+ '" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
                                val + '%' +
                               '</div>' +
                                '</div>'
                            );
                        }
                    }
                ],
                // order: [[2, 'desc']],
                dom:
                    '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-right"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 mr-50' }) + 'Export',
                        buttons: [
                            {
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({ class: 'font-small-4 mr-50' }) + 'Print',
                                className: 'dropdown-item',
                                exportOptions: { columns: [0, 1, 2, 3] }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({ class: 'font-small-4 mr-50' }) + 'Csv',
                                className: 'dropdown-item',
                                exportOptions: { columns: [0, 1, 2, 3] }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({ class: 'font-small-4 mr-50' }) + 'Excel',
                                className: 'dropdown-item',
                                exportOptions: { columns: [0, 1, 2, 3] }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 mr-50' }) + 'Pdf',
                                className: 'dropdown-item',
                                exportOptions: { columns: [0, 1, 2, 3] },
                                customize: function(doc) {
                                    doc.defaultStyle = {
                                        font:'THSarabun',
                                    };
                                    doc.content[1].table.widths =
                                        Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                                    doc.defaultStyle.alignment = 'left';
                                    doc.styles.tableHeader.alignment = 'left';
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({ class: 'font-small-4 mr-50' }) + 'Copy',
                                className: 'dropdown-item',
                                exportOptions: { columns: [0, 1, 2, 3] }
                            }
                        ],

                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function (row) {
                                var data = row.data();
                                return 'Details of ' + data['full_name'];
                            }
                        }),
                        type: 'column',
                        renderer: function (api, rowIdx, columns) {
                            var data = $.map(columns, function (col, i) {
                                return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                                    ? '<tr data-dt-row="' +
                                    col.rowIndex +
                                    '" data-dt-column="' +
                                    col.columnIndex +
                                    '">' +
                                    '<td>' +
                                    col.title +
                                    ':' +
                                    '</td> ' +
                                    '<td>' +
                                    col.data +
                                    '</td>' +
                                    '</tr>'
                                    : '';
                            }).join('');
                            return data ? $('<table class="table"/>').append(data) : false;
                        }
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="card-title mb-0">การใช้จ่ายสูงสุดและต่ำสุด</h6>');
        }

        // Flat Date picker
        if (dt_date_table.length) {
            dt_date_table.flatpickr({
                monthSelectorType: 'static',
                dateFormat: 'm/d/Y'
            });
        }
    }

    function NewCustomer() {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url('chart-data/customer-ratio') }}',
            type: "GET",
            enctype: 'multipart/form-data',
            dataType: 'JSON',
            processData: false,
            contentType: false,
            cache: false,
            success: function (res) {
                if (res.data.length > 0){
                    try {
                        am4core.useTheme(am4themes_kelly);

                        var chart = am4core.create("Newcustomer", am4charts.XYChart);
                        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
                        chart.logo.disabled = true;

                        // Add Data
                        let data = [];
                        $.each(res.data, function (i, v) {
                            data.push({
                                category: v.date,
                                value1: v.buy,
                                value2: v.sell,
                                value3: v.pawn,
                                value4: v.collect
                            });
                        });
                        chart.data = data;

                        chart.colors.step = 2;
                        chart.padding(30, 30, 10, 30);
                        chart.legend = new am4charts.Legend();

                        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                        categoryAxis.dataFields.category = "category";
                        categoryAxis.renderer.grid.template.location = 0;

                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                        valueAxis.min = 0;
                        valueAxis.max = 100;
                        valueAxis.strictMinMax = true;
                        valueAxis.calculateTotals = true;
                        valueAxis.renderer.minWidth = 50;


                        var series1 = chart.series.push(new am4charts.ColumnSeries());
                        series1.columns.template.width = am4core.percent(80);
                        series1.columns.template.tooltipText =
                            "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
                        series1.name = "รับซื้อ";
                        series1.dataFields.categoryX = "category";
                        series1.dataFields.valueY = "value1";
                        series1.dataFields.valueYShow = "totalPercent";
                        series1.dataItems.template.locations.categoryX = 0.5;
                        series1.stacked = true;
                        series1.tooltip.pointerOrientation = "vertical";

                        var bullet1 = series1.bullets.push(new am4charts.LabelBullet());
                        bullet1.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
                        bullet1.label.fill = am4core.color("#ffffff");
                        bullet1.locationY = 0.5;

                        var series2 = chart.series.push(new am4charts.ColumnSeries());
                        series2.columns.template.width = am4core.percent(80);
                        series2.columns.template.tooltipText =
                            "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
                        series2.name = "ขายออก";
                        series2.dataFields.categoryX = "category";
                        series2.dataFields.valueY = "value2";
                        series2.dataFields.valueYShow = "totalPercent";
                        series2.dataItems.template.locations.categoryX = 0.5;
                        series2.stacked = true;
                        series2.tooltip.pointerOrientation = "vertical";

                        var bullet2 = series2.bullets.push(new am4charts.LabelBullet());
                        bullet2.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
                        bullet2.locationY = 0.5;
                        bullet2.label.fill = am4core.color("#ffffff");

                        var series3 = chart.series.push(new am4charts.ColumnSeries());
                        series3.columns.template.width = am4core.percent(80);
                        series3.columns.template.tooltipText =
                            "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
                        series3.name = "ขายฝาก";
                        series3.dataFields.categoryX = "category";
                        series3.dataFields.valueY = "value3";
                        series3.dataFields.valueYShow = "totalPercent";
                        series3.dataItems.template.locations.categoryX = 0.5;
                        series3.stacked = true;
                        series3.tooltip.pointerOrientation = "vertical";

                        var bullet3 = series3.bullets.push(new am4charts.LabelBullet());
                        bullet3.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
                        bullet3.locationY = 0.5;
                        bullet3.label.fill = am4core.color("#ffffff");

                        var series4 = chart.series.push(new am4charts.ColumnSeries());
                        series4.columns.template.width = am4core.percent(80);
                        series4.columns.template.tooltipText =
                            "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
                        series4.name = "ออมทอง";
                        series4.dataFields.categoryX = "category";
                        series4.dataFields.valueY = "value4";
                        series4.dataFields.valueYShow = "totalPercent";
                        series4.dataItems.template.locations.categoryX = 0.5;
                        series4.stacked = true;
                        series4.tooltip.pointerOrientation = "vertical";

                        var bullet4 = series4.bullets.push(new am4charts.LabelBullet());
                        bullet4.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
                        bullet4.locationY = 0.5;
                        bullet4.label.fill = am4core.color("#ffffff");

                        chart.scrollbarX = new am4core.Scrollbar();

                    } catch (e) {
                        console.log(e);
                    }
                }else {
                    $('#Newcustomer').html("- ข้อมูลไม่เพียงพอ -");
                }
            }
        });
    }

    function SerCustomer() {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url('chart-data/churn-rate') }}',
            type: "GET",
            enctype: 'multipart/form-data',
            dataType: 'JSON',
            processData: false,
            contentType: false,
            cache: false,
            success: function (res) {
                if (res.data.length > 0){
                    try {
                        am4core.unuseTheme(am4themes_kelly);
                        am4core.useTheme(am4themes_dataviz);

                        // Create chart instance
                        var chart = am4core.create("Sercustomer", am4charts.XYChart);
                        chart.logo.disabled = true;

                        // Add data
                        let data = [];
                        $.each(res.data, function (i, v) {
                            let [y, m, d] = v.date.split("-");
                            data.push({
                                date: new Date(y, m, d),
                                value: v.ChurnRate_net
                            });
                        });
                        chart.data = data;

                        // Create axes
                        var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
                        dateAxis.renderer.grid.template.location = 0;
                        dateAxis.renderer.minGridDistance = 30;

                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

                        // Create series
                        function createSeries(field, name) {
                            var series = chart.series.push(new am4charts.LineSeries());
                            series.dataFields.valueY = field;
                            series.dataFields.dateX = "date";
                            series.name = name;
                            series.tooltipText = "{dateX}: [b]{valueY}[/]";
                            series.strokeWidth = 2;
                            series.stroke = "#E9493C";

                            series.smoothing = "monotoneX";

                            var bullet = series.bullets.push(new am4charts.CircleBullet());
                            bullet.circle.stroke = am4core.color("#E9493C");
                            bullet.circle.strokeWidth = 2;
                            bullet.circle.fill = am4core.color("#fff");

                            return series;
                        }

                        createSeries("value", "");

                        /* chart.legend = new am4charts.Legend(); */
                        chart.cursor = new am4charts.XYCursor();
                    } catch (e) {
                        console.log(e);
                    }
                }else {
                    $('#Sercustomer').html("- ข้อมูลไม่เพียงพอ -");
                }
            }
        });
    }

    function NumberCustomer() {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url('chart-data/monthly-active') }}',
            type: "GET",
            enctype: 'multipart/form-data',
            dataType: 'JSON',
            processData: false,
            contentType: false,
            cache: false,
            success: function (res) {
                if (res.data.length > 0){
                    try {
                        // Themes begin
                        am4core.unuseTheme(am4themes_dataviz);
                        // Themes end

                        // Create chart instance
                        var chart = am4core.create("Numbercustomer", am4charts.XYChart);
                        chart.logo.disabled = true;

                        // Add Data
                        let data = [];
                        $.each(res.data, function (i, v) {
                            data.push({
                                year: v.date,
                                buy: v.buy,
                                sell: v.sell,
                                pawn: v.pawn,
                                collect: v.collect
                            });
                        });
                        chart.data = data;

                        // Create axes
                        var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
                        categoryAxis.dataFields.category = "year";
                        categoryAxis.numberFormatter.numberFormat = "#";
                        categoryAxis.renderer.inversed = true;
                        categoryAxis.renderer.grid.template.location = 0;
                        categoryAxis.renderer.cellStartLocation = 0.1;
                        categoryAxis.renderer.cellEndLocation = 0.9;

                        var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
                        valueAxis.renderer.opposite = true;

                        // Create series
                        function createSeries(field, name) {
                            var series = chart.series.push(new am4charts.ColumnSeries());
                            series.dataFields.valueX = field;
                            series.dataFields.categoryY = "year";
                            series.name = name;
                            series.columns.template.tooltipText = "{name}: [bold]{valueX}[/]";
                            series.columns.template.height = am4core.percent(100);
                            series.sequencedInterpolation = true;

                            var valueLabel = series.bullets.push(new am4charts.LabelBullet());
                            // valueLabel.label.text = "{valueX}";
                            valueLabel.label.horizontalCenter = "left";
                            valueLabel.label.dx = 10;
                            valueLabel.label.hideOversized = false;
                            valueLabel.label.truncate = false;

                            var categoryLabel = series.bullets.push(new am4charts.LabelBullet());
                            categoryLabel.label.text = "{name}";
                            categoryLabel.label.horizontalCenter = "right";
                            categoryLabel.label.dx = -10;
                            categoryLabel.label.fill = am4core.color("#000");
                            categoryLabel.label.hideOversized = false;
                            categoryLabel.label.truncate = false;
                        }

                        createSeries("buy", "รับซื้อ");
                        createSeries("sell", "ขายออก");
                        createSeries("pawn", "ขายฝาก");
                        createSeries("collect", "ออมทอง");

                        chart.legend = new am4charts.Legend();
                        chart.legend.position = 'top'
                        chart.legend.paddingBottom = 20
                    } catch (e) {
                        console.log(e);
                    }
                }else {
                    $('#Numbercustomer').html("- ข้อมูลไม่เพียงพอ -");
                }
            }
        });
    }

    function BackSerCustomer() {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url('chart-data/predicting-next-purchase-day-pie') }}',
            type: "GET",
            enctype: 'multipart/form-data',
            dataType: 'JSON',
            processData: false,
            contentType: false,
            cache: false,
            success: function (res) {
                if (res.data.length > 0){
                    try {
                        // Themes begin
                        am4core.useTheme(am4themes_kelly);
                        am4core.useTheme(am4themes_animated);
                        // Themes end

                        /**
                         * Define data for each year
                         */
                        // Create chart instance
                        var chart = am4core.create("backsercus", am4charts.PieChart);
                        chart.logo.disabled = true;

                        // Add data
                        let data = [];
                        $.each(res.data, function (i, v) {
                            data.push({
                                cluster: v.Cluster,
                                total: v.total
                            });
                        });
                        chart.data = data;

                        // Add label
                        chart.innerRadius = 50;

                        // Add and configure Series
                        var pieSeries = chart.series.push(new am4charts.PieSeries());
                        pieSeries.dataFields.value = "total";
                        pieSeries.dataFields.category = "cluster";

                        // Animate chart data
                        chart.legend = new am4charts.Legend();
                    }
                    catch( e ) {
                        console.log( e );
                    }
                }else {
                    $('#backsercus').html("- ข้อมูลไม่เพียงพอ -");
                }
            }
        });
    }

    function BackSerCusBar() {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url('chart-data/predicting-next-purchase-day-stacked') }}',
            type: "GET",
            enctype: 'multipart/form-data',
            dataType: 'JSON',
            processData: false,
            contentType: false,
            cache: false,
            success: function (res) {
                if (res.count > 0){
                    try {
                        am4core.unuseTheme(am4themes_kelly);
                        am4core.useTheme(am4themes_animated);

                        var chart = am4core.create("backsercusbar", am4charts.XYChart);
                        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
                        chart.logo.disabled = true;

                        // Add data
                        let data = [];
                        $.each(res.data, function (i, v) {
                            data.push({
                                category: v.name,
                                value1: (v.low !== undefined ? v.low : 0),
                                value2: (v.mid !== undefined ? v.mid : 0),
                                value3: (v.high !== undefined ? v.high : 0)
                            });
                        });
                        chart.data = data;

                        chart.colors.step = 2;
                        chart.padding(30, 30, 10, 30);
                        chart.legend = new am4charts.Legend();

                        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                        categoryAxis.dataFields.category = "category";
                        categoryAxis.renderer.grid.template.location = 0;

                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                        valueAxis.min = 0;
                        valueAxis.max = 100;
                        valueAxis.strictMinMax = true;
                        valueAxis.calculateTotals = true;
                        valueAxis.renderer.minWidth = 50;

                        var series1 = chart.series.push(new am4charts.ColumnSeries());
                        series1.columns.template.width = am4core.percent(80);
                        series1.columns.template.tooltipText =
                            "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
                        series1.name = "Low Value";
                        series1.dataFields.categoryX = "category";
                        series1.dataFields.valueY = "value1";
                        series1.dataFields.valueYShow = "totalPercent";
                        series1.dataItems.template.locations.categoryX = 0.5;
                        series1.stacked = true;
                        series1.tooltip.pointerOrientation = "vertical";

                        var bullet1 = series1.bullets.push(new am4charts.LabelBullet());
                        bullet1.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
                        bullet1.label.fill = am4core.color("#ffffff");
                        bullet1.locationY = 0.5;

                        var series2 = chart.series.push(new am4charts.ColumnSeries());
                        series2.columns.template.width = am4core.percent(80);
                        series2.columns.template.tooltipText =
                            "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
                        series2.name = "Mid Value";
                        series2.dataFields.categoryX = "category";
                        series2.dataFields.valueY = "value2";
                        series2.dataFields.valueYShow = "totalPercent";
                        series2.dataItems.template.locations.categoryX = 0.5;
                        series2.stacked = true;
                        series2.tooltip.pointerOrientation = "vertical";

                        var bullet2 = series2.bullets.push(new am4charts.LabelBullet());
                        bullet2.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
                        bullet2.locationY = 0.5;
                        bullet2.label.fill = am4core.color("#ffffff");

                        var series3 = chart.series.push(new am4charts.ColumnSeries());
                        series3.columns.template.width = am4core.percent(80);
                        series3.columns.template.tooltipText =
                            "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
                        series3.name = "High Value";
                        series3.dataFields.categoryX = "category";
                        series3.dataFields.valueY = "value3";
                        series3.dataFields.valueYShow = "totalPercent";
                        series3.dataItems.template.locations.categoryX = 0.5;
                        series3.stacked = true;
                        series3.tooltip.pointerOrientation = "vertical";

                        var bullet3 = series3.bullets.push(new am4charts.LabelBullet());
                        bullet3.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
                        bullet3.locationY = 0.5;
                        bullet3.label.fill = am4core.color("#ffffff");

                        chart.scrollbarX = new am4core.Scrollbar();
                    } catch (e) {
                        console.log(e);
                    }
                }else {
                    $('#backsercusbar').html("- ข้อมูลไม่เพียงพอ -");
                }
            }
        });
    }

    function MonthlyOrder() {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url('chart-data/monthly-order-count') }}',
            type: "GET",
            enctype: 'multipart/form-data',
            dataType: 'JSON',
            processData: false,
            contentType: false,
            cache: false,
            success: function (res) {
                if (res.data.length > 0){
                    // Themes begin
                    am4core.unuseTheme(am4themes_animated);
                    am4core.useTheme(am4themes_material);
                    // Themes end

                    // Create chart instance
                    var chart = am4core.create("monthlyorder", am4charts.XYChart);

                    chart.logo.disabled = true;

                    // Add data
                    let data = [];
                    $.each(res.data, function (i, v) {
                        data.push({
                            country: v.date,
                            visits: v.net
                        });
                    });
                    chart.data = data;

                    // Create axes
                    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                    categoryAxis.dataFields.category = "country";
                    // categoryAxis.renderer.labels.template.rotation = 270;
                    categoryAxis.renderer.grid.template.location = 0;
                    categoryAxis.renderer.minGridDistance = 30;

                    categoryAxis.renderer.labels.template.adapter.add("dy", function (dy, target) {
                        if (target.dataItem && target.dataItem.index & 2 == 2) {
                            return dy + 25;
                        }
                        return dy;
                    });

                    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

                    // Create series
                    var series = chart.series.push(new am4charts.ColumnSeries());
                    series.dataFields.valueY = "visits";
                    series.dataFields.categoryX = "country";
                    series.name = "Visits";
                    series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
                    series.columns.template.fillOpacity = .8;

                    var columnTemplate = series.columns.template;
                    columnTemplate.strokeWidth = 2;
                    columnTemplate.strokeOpacity = 1;
                }else {
                    $('#monthlyorder').html("- ข้อมูลไม่เพียงพอ -");
                }
            }
        });
    }
    
    function TableBackService() {
        'use strict';
        var dt_basic_table = $('#TablebackserCustomer'),
            dt_date_table = $('.dt-date'),
            assetPath = '../../../app-assets/';
        if ($('body').attr('data-framework') === 'laravel') {
            assetPath = $('body').attr('data-asset-path');
        }

        // DataTable with buttons
        // --------------------------------------------------------------------
        if (dt_basic_table.length) {
            var dt_basic = dt_basic_table.DataTable({
                ajax: '{{ url('chart-data/predicting-next-purchase-day-list') }}',
                columns: [
                    {
                        "data": "id",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { data: 'CustomerID' },
                    { data: null },
                    { data: 'Cluster' }
                ],
                columnDefs: [
                    {
                        // Label
                        targets: -2,
                        render: function (data, type, full, meta) {
                            let status = {};
                            if(data.low == 1) { status = { title: 'Low Value', class: 'badge-light-danger' }; }
                            else if(data.mid == 1) { status = { title: 'Middle Value', class: 'badge-light-warning' }; }
                            else if(data.high == 1) { status = { title: 'High Value', class: 'badge-light-success' }; }

                            return (
                                '<span class="badge badge-pill ' +
                                status.class +
                                '">' +
                                status.title +
                                '</span>'
                            );
                        }
                    },
                    {
                        // Actions
                        targets: -1,
                        orderable: false,
                        render: function (data, type, full, meta) {
                            let status = '';
                            switch (data) {
                                case '1-20 days':
                                    status = 'success';
                                    break;
                                case '21-49 days':
                                    status = 'warning';
                                    break;
                                case '> 50 days':
                                    status = 'danger';
                                    break;
                                default:
                                    break;
                            }

                            return (
                                '<div class="progress progress-bar-' + status + '">' +
                                '<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="100" aria-valuemax="100" style="width: 100%">' +
                                data +
                                '</div>' +
                                '</div>'
                            );
                        }
                    }
                ],
                order: [[0, 'asc']],
                dom:
                    '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-right"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 mr-50' }) + 'Export',
                        buttons: [
                            {
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({ class: 'font-small-4 mr-50' }) + 'Print',
                                className: 'dropdown-item',
                                exportOptions: { columns: [0, 1, 2, 3] }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({ class: 'font-small-4 mr-50' }) + 'Csv',
                                className: 'dropdown-item',
                                exportOptions: { columns: [0, 1, 2, 3] }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({ class: 'font-small-4 mr-50' }) + 'Excel',
                                className: 'dropdown-item',
                                exportOptions: { columns: [0, 1, 2, 3] }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 mr-50' }) + 'Pdf',
                                className: 'dropdown-item',
                                exportOptions: { columns: [0, 1, 2, 3] },
                                customize: function(doc) {
                                    doc.defaultStyle = {
                                        font:'THSarabun',
                                    };
                                    doc.content[1].table.widths =
                                        Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                                    doc.defaultStyle.alignment = 'left';
                                    doc.styles.tableHeader.alignment = 'left';
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({ class: 'font-small-4 mr-50' }) + 'Copy',
                                className: 'dropdown-item',
                                exportOptions: { columns: [0, 1, 2, 3] }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                                $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function (row) {
                                var data = row.data();
                                return 'Details of ' + data['full_name'];
                            }
                        }),
                        type: 'column',
                        renderer: function (api, rowIdx, columns) {
                            var data = $.map(columns, function (col, i) {
                                return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                                    ? '<tr data-dt-row="' +
                                    col.rowIndex +
                                    '" data-dt-column="' +
                                    col.columnIndex +
                                    '">' +
                                    '<td>' +
                                    col.title +
                                    ':' +
                                    '</td> ' +
                                    '<td>' +
                                    col.data +
                                    '</td>' +
                                    '</tr>'
                                    : '';
                            }).join('');
                            return data ? $('<table class="table"/>').append(data) : false;
                        }
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="card-title mb-0">การใช้จ่ายสูงสุดและต่ำสุด</h6>');
        }

        // Flat Date picker
        if (dt_date_table.length) {
            dt_date_table.flatpickr({
                monthSelectorType: 'static',
                dateFormat: 'm/d/Y'
            });
        }
    }
</script>
@section('vendor-script')

@endsection
