@extends('layouts/contentLayoutMasterMain')

@section('title', 'สร้างรายการเงินเดือน')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('vendors/css/pickers/flatpickr/flatpickr.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/forms/select/select2.min.css')}}">
@endsection
@section('page-style')
<link rel="stylesheet" href="{{asset('css/base/plugins/forms/pickers/form-flat-pickr.css')}}">
<link rel="stylesheet" href="{{asset('css/base/pages/app-invoice.css')}}">
@endsection

@section('content')
<section class="invoice-add-wrapper">
  <div class="row invoice-add">
    <!-- Invoice Add Left starts -->
    <div class="col-xl-9 col-md-8 col-12">
      <div class="card invoice-preview-card">
        <!-- Header starts -->
        <div class="card-body invoice-padding pb-0">
            <div class="row">
                <div class="col-12 mb-2"><h4><i data-feather='edit'></i> สร้างรายการเงินเดือน (1 - 31 ตุลาคม 2564)</h4><hr /></div>
            </div>
          <div class="d-flex justify-content-between flex-md-row flex-column invoice-spacing mt-0">
            <div>
                <div class="invoice-number-date mt-md-0 mt-2">
                    <div class="d-flex align-items-center justify-content-md-end mb-1">
                        <span class="title">วันที่ทำรายการ :</span>
                        <input type="text" class="form-control invoice-edit-input date-picker" />
                    </div>
                    <div class="d-flex align-items-center mb-1">
                        <span class="title">ช่วงวันที่ :</span>
                        <input type="text" class="form-control invoice-edit-input date-picker" />
                    </div>
                    <div class="d-flex align-items-center mb-1">
                        <span class="title">ถึงวันที่ :</span>
                        <input type="text" class="form-control invoice-edit-input date-picker" />
                    </div>
                    <div class="d-flex align-items-center mb-1">
                        <span class="title">วันที่ชำระ :</span>
                        <input type="text" class="form-control invoice-edit-input date-picker" />
                    </div>
                </div>
            </div>
            <div class="invoice-number-date mt-md-0 mt-2">
              <div class="d-flex align-items-center mb-1">
                <span class="invoice-title">จำนวนเงินทั้งสิ้น</span>
              </div>
                <div class="d-flex align-items-center mb-1">
                    <h1 class="text-primary">0.00 บาท</h1>
                </div>
            </div>
          </div>
        </div>
        <!-- Header ends -->

        <hr class="invoice-spacing" />

        <!-- Address and Contact starts -->
        <div class="card-body invoice-padding pt-0">
          <div class="row row-bill-to invoice-spacing mb-0 mt-1">
              <div class="col-xl-12 pr-0 mt-xl-0 mt-2">
                  <div class="row">
                      <div class="d-flex justify-content-between flex-md-row flex-column invoice-spacing mt-0 mb-0">
                          <div class="invoice-number-date mt-md-0 mt-2">
                              <div class="d-flex align-items-center justify-content-md-end mb-1">
                                  <span class="title">ประกันสังคม :</span>
                                  <input type="text" class="form-control invoice-edit-input mr-2" />
                                  <span class="title">%</span>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>

        <!-- Address and Contact ends -->
          <div class="table-responsive mt-0">
              <table class="table">
                  <thead>
                  <tr>
                      <th class="py-1" style="font-size: 16px;">ลำดับ</th>
                      <th class="py-1" style="font-size: 16px;">ชื่อ - สกุล พนักงาน</th>
                      <th class="py-1" style="font-size: 16px;">ช่องทางรับชำระ</th>
                      <th class="py-1" style="font-size: 16px;">เงินเดือน</th>
                      <th class="py-1" style="font-size: 16px;">ประกันสังคม</th>
                      <th class="py-1" style="font-size: 16px;">ยอดจ่ายสุทธิ</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr class="border-bottom">
                      <td class="py-1">
                          <p class="card-text font-weight-bold mb-25">1</p>
                      </td>
                      <td class="py-1">
                          <input type="text" class="form-control invoice-edit-input" />
                      </td>
                      <td class="py-1">
                          <select class="form-control" id="basicSelect">
                              <option>โปรดระบุช่องทางรับชำระ</option>
                              <option>โอนผ่านธนาคาร</option>
                          </select>
                      </td>
                      <td class="py-1">
                          <input type="text" class="form-control invoice-edit-input" />
                      </td>
                      <td class="py-1">
{{--                          <input type="text" class="form-control invoice-edit-input" />--}}
                          <span class="invoice-edit-input">0</span>
                      </td>
                      <td class="py-1">
                          <span class="font-weight-bold">0.00</span>
                      </td>
                  </tr>
                  <tr class="border-bottom">
                      <td class="py-1">
                          <p class="card-text font-weight-bold mb-25">2</p>
                      </td>
                      <td class="py-1">
                          <input type="text" class="form-control invoice-edit-input" />
                      </td>
                      <td class="py-1">
                          <select class="form-control" id="basicSelect">
                              <option>โปรดระบุช่องทางรับชำระ</option>
                              <option>โอนผ่านธนาคาร</option>
                          </select>
                      </td>
                      <td class="py-1">
                          <input type="text" class="form-control invoice-edit-input" />
                      </td>
                      <td class="py-1">
{{--                          <input type="text" class="form-control invoice-edit-input" />--}}
                          <span class="invoice-edit-input">0</span>
                      </td>
                      <td class="py-1">
                          <span class="font-weight-bold">0.00</span>
                      </td>
                  </tr>
                  <tr class="border-bottom">
                      <td class="py-1">
                          <p class="card-text font-weight-bold mb-25">3</p>
                      </td>
                      <td class="py-1">
                          <input type="text" class="form-control invoice-edit-input" />
                      </td>
                      <td class="py-1">
                          <select class="form-control" id="basicSelect">
                              <option>โปรดระบุช่องทางรับชำระ</option>
                              <option>โอนผ่านธนาคาร</option>
                          </select>
                      </td>
                      <td class="py-1">
                          <input type="text" class="form-control invoice-edit-input" />
                      </td>
                      <td class="py-1">
{{--                          <input type="text" class="form-control invoice-edit-input" />--}}
                          <span class="invoice-edit-input">0</span>
                      </td>
                      <td class="py-1">
                          <span class="font-weight-bold">0.00</span>
                      </td>
                  </tr>
                  </tbody>
              </table>
          </div>
          <div class="row mt-1 ml-3">
              <div class="col-12 px-0">
                  <button type="button" class="btn btn-primary btn-sm btn-add-new" data-repeater-create>
                      <i data-feather="plus" class="mr-25"></i>
                      <span class="align-middle">เพิ่มรายชื่อพนักงาน</span>
                  </button>
              </div>
          </div>

        <!-- Invoice Total starts -->
        <div class="card-body invoice-padding">
          <div class="row invoice-sales-total-wrapper">
            <div class="col-md-6 order-md-1 order-2 mt-md-0 mt-3">
              <div class="d-flex align-items-center mb-1">
                  <div class="row">
                      <div class="col-md-4">
                          <label for="salesperson" class="form-label">หมายเหตุ :</label>
                      </div>
                      <div class="col-md-8">
                          <textarea
                              class="form-control"
                              id="exampleFormControlTextarea1"
                              rows="5"
                              placeholder=""
                          ></textarea>
                      </div>
                  </div>
              </div>
            </div>
              <div class="col-md-6 justify-content-end order-md-2 order-1">
                  <div class="row">
                      <div class="col-7 invoice-total-title text-right">จำนวนพนักงาน :</div>
                      <div class="col-5 invoice-total-amount text-right">0</div>
                  </div>
                  <hr  align="right" class="my-50" style="max-width: 350px;" />
                  <div class="row">
                      <div class="col-7 invoice-total-title text-right">ยอดรวมเงินเดือน :</div>
                      <div class="col-5 invoice-total-amount text-right">0.00</div>
                  </div>
                  <div class="row">
                      <div class="col-7 invoice-total-title text-right">ประกันสังคม :</div>
                      <div class="col-5 invoice-total-amount text-right">0.00</div>
                  </div>
                  <div class="row">
                      <span class="col-7 invoice-total-title text-right">รวมจ่ายสุทธิ :</span>
                      <span class="col-5 invoice-total-amount text-right">0.00</span>
                  </div>
                  <hr  align="right" class="my-50" style="max-width: 350px;" />
                  <div class="row">
                      <span class="col-7 invoice-total-title text-right">ชำระผ่านการโอน :</span>
                      <span class="col-5 invoice-total-amount text-right">0.00</span>
                  </div>
                  <div class="row">
                      <span class="col-7 invoice-total-title text-right">ชำระผ่านเงินสด :</span>
                      <span class="col-5 invoice-total-amount text-right">0.00</span>
                  </div>
              </div>
          </div>
        </div>
        <!-- Invoice Total ends -->

        {{--<hr class="invoice-spacing mt-0" />

        <div class="card-body invoice-padding pt-0">
          <!-- Invoice Note starts -->
          <div class="row">
            <div class="col-12">
                <span class="font-weight-bold">Note:</span>
                <span>โปรดรักษาเอกสารฉบับนี้สำหรับเป็นหลักฐานการชำระเงิน</span>
            </div>
          </div>
          <!-- Invoice Note ends -->
        </div>--}}
      </div>
    </div>
    <!-- Invoice Add Left ends -->

    <!-- Invoice Add Right starts -->
    <div class="col-xl-3 col-md-4 col-12">
      <div class="card">
        <div class="card-body">
          <button type="button" class="btn btn-primary btn-block mb-75">บันทึก</button>
          <button type="button" class="btn btn-success btn-block" onclick="javascript:window.location.href = '{{ url('/salary') }}';">ปิด</button>
        </div>
      </div>
    </div>
    <!-- Invoice Add Right ends -->
  </div>

  <!-- Add New Customer Sidebar -->
  <div class="modal modal-slide-in fade" id="add-new-customer-sidebar" aria-hidden="true">
    <div class="modal-dialog sidebar-lg">
      <div class="modal-content p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
        <div class="modal-header mb-1">
          <h5 class="modal-title">
            <span class="align-middle">Add Customer</span>
          </h5>
        </div>
        <div class="modal-body flex-grow-1">
          <form>
            <div class="form-group">
              <label for="customer-name" class="form-label">Customer Name</label>
              <input type="text" class="form-control" id="customer-name" placeholder="John Doe" />
            </div>
            <div class="form-group">
              <label for="customer-email" class="form-label">Email</label>
              <input type="email" class="form-control" id="customer-email" placeholder="example@domain.com" />
            </div>
            <div class="form-group">
              <label for="customer-address" class="form-label">Customer Address</label>
              <textarea
                class="form-control"
                id="customer-address"
                cols="2"
                rows="2"
                placeholder="1307 Lady Bug Drive New York"
              ></textarea>
            </div>
            <div class="form-group position-relative">
              <label for="customer-country" class="form-label">Country</label>
              <select class="form-control" id="customer-country" name="customer-country">
                <option label="select country"></option>
                <option value="Australia">Australia</option>
                <option value="Canada">Canada</option>
                <option value="Russia">Russia</option>
                <option value="Saudi Arabia">Saudi Arabia</option>
                <option value="Singapore">Singapore</option>
                <option value="Sweden">Sweden</option>
                <option value="Switzerland">Switzerland</option>
                <option value="United Kingdom">United Kingdom</option>
                <option value="United Arab Emirates">United Arab Emirates</option>
                <option value="United States of America">United States of America</option>
              </select>
            </div>
            <div class="form-group">
              <label for="customer-contact" class="form-label">Contact</label>
              <input type="number" class="form-control" id="customer-contact" placeholder="763-242-9206" />
            </div>
            <div class="form-group d-flex flex-wrap mt-2">
              <button type="button" class="btn btn-primary mr-1" data-dismiss="modal">Add</button>
              <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- /Add New Customer Sidebar -->
</section>
@endsection

@section('vendor-script')
<script src="{{asset('vendors/js/forms/repeater/jquery.repeater.min.js')}}"></script>
<script src="{{asset('vendors/js/forms/select/select2.full.min.js')}}"></script>
<script src="{{asset('vendors/js/pickers/flatpickr/flatpickr.min.js')}}"></script>
@endsection

@section('page-script')
<script src="{{asset('js/scripts/pages/app-invoice.js')}}"></script>
@endsection
