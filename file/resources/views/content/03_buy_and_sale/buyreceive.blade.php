@extends('layouts/contentLayoutMasterMain')

@section('title', 'ร้านรับซื้อทอง', 'Statistics Cards', 'Pill Badges')

@section('vendor-style')
    <!-- Vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset('js/dropify/dist/css/dropify.css') }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-ecommerce.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-number-input.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
@endsection

@section('content')

    <style>
        .subText{
            font-size: 14px;
        }
    </style>

        <div class="row">
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h2 class="font-weight-bolder mb-0" id="goldbarBuyPriceBox"></h2>
                            <p class="card-text">ซื้อทองคำแท่ง</p>
                        </div>
                        <div class="avatar bg-light-danger p-50 m-0">
                            <div class="avatar-content">
                                <i data-feather='shopping-bag' class="font-medium-5"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h2 class="font-weight-bolder mb-0" id="goldbarSellPriceBox"></h2>
                            <p class="card-text">ขายทองคำแท่ง</p>
                        </div>
                        <div class="avatar bg-light-success p-50 m-0">
                            <div class="avatar-content">
                                <i data-feather="shopping-cart" class="font-medium-5"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h2 class="font-weight-bolder mb-0" id="goldBuyPriceBox"></h2>
                            <p class="card-text">ซื้อทองรูปพรรณ</p>
                        </div>
                        <div class="avatar bg-light-danger p-50 m-0">
                            <div class="avatar-content">
                                <i data-feather="shopping-bag" class="font-medium-5"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h2 class="font-weight-bolder mb-0" id="goldSellPriceBox"></h2>
                            <p class="card-text">ขายทองรูปพรรณ</p>
                        </div>
                        <div class="avatar bg-light-success p-50 m-0">
                            <div class="avatar-content">
                                <i data-feather="shopping-cart" class="font-medium-5"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-sm-6 col-12">
                <div id="preview_gold_buy_add"></div>
                <form id="form_gold_buy">
                    @csrf
                    <div class="list-view product-checkout">
                        <div class="checkout-items">
                            <div class="text-center mb-5">
                                <button type="button" class="btn btn-success mt-1" data-toggle="modal" data-target="#modal_gold_add">
                                    <i data-feather="plus" class="align-middle mr-25"></i>
                                    <span>เพิ่ม</span>
                                </button>
                                <div id="product_error_add" class="invalid-feedback" style="display:block;"></div>
                            </div>
                            <form>
                                <div id="gold_buy_list">
                                </div>
                            </form>
                        </div>

                        <div class="checkout-options">
                            <div class="card user-card">
                                <div class="card-body">
                                    <label class="section-label" style="font-size: 16px;">ผู้ขาย</label>
                                    <hr />
                                    <div class="row">
                                        <div class="col-xl-12 col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                            <div class="user-avatar-section">
                                                <div class="text-center" id="buyer_add_input" style="display: block;">
                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#buyerModal">
                                                        <i data-feather="user" class="mr-25"></i>
                                                        <span>เพิ่มข้อมูลผู้ขาย</span>
                                                    </button>
                                                    <div id="buyer_error_add" class="invalid-feedback" style="display:block;"></div>
                                                </div>
                                                <div id="buyer_profile" style="display: none;">
                                                    <div class="d-flex justify-content-start">
{{--                                                        <img--}}
{{--                                                            class="avatar"--}}
{{--                                                            src="{{asset('images/avatars/7.png')}}"--}}
{{--                                                            height="104"--}}
{{--                                                            width="104"--}}
{{--                                                            alt="User avatar"--}}
{{--                                                        />--}}
                                                        <div class="ml-1">
                                                            <div class="user-info mb-1">
                                                                <h4 class="mb-0"><i data-feather='user'></i> <span id="buyer_name"></span></h4>
                                                                <span class="card-text" style="font-size: 10px;"><i data-feather='map-pin'></i> <span id="buyer_address"></span></span>
                                                                <div class="d-flex flex-wrap mt-0">
                                                                    <span class="card-text" style="font-size: 10px;"><i data-feather='phone'></i> <span id="buyer_phone"></span></span>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" id="buyer_id" name="buyer_id" value="" />

                                                            <div class="d-flex flex-wrap">
                                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#buyerModal">แก้ไข</button>
                                                                <button type="button" class="btn btn-outline-danger ml-1" id="btn_buyer_remove">ลบ</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <label class="section-label" style="font-size: 16px;">ชำระเงิน</label>
                                    <hr />
                                    <div class="price-details">
{{--                                        <h6 class="price-title">Price Details</h6>--}}
                                        <div class="form-group row">
                                            <label for="colFormLabelLg" class="col-sm-5 col-form-label col-form-label-lg">ราคารับซื้อ</label>
                                            <div class="col-sm-7 col-form-label col-form-label-lg"><strong id="total_price_text"></strong></div>
                                            <input type="hidden" id="total_price" name="total_price" value="" />
{{--                                            <div class="col-sm-2 col-form-label col-form-label-lg"></div>--}}
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelLg" class="col-sm-5 col-form-label col-form-label-lg">จ่ายเงิน</label>
                                            <div class="col-sm-7 col-form-label col-form-label-lg"><strong id="pay_text"></strong></div>
                                            <input type="hidden" id="pay" name="pay" value="" />
{{--                                            <div class="col-sm-2 col-form-label col-form-label-lg"></div>--}}
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelLg" class="col-sm-5 col-form-label col-form-label-lg">ช่องทางการขาย</label>
                                            <div class="col-sm-7 col-form-label col-form-label-lg">
                                                <div class="demo-inline-spacing">
                                                    <div class="custom-control custom-radio mt-0">
                                                        <input type="radio" id="purchase_channel_front" name="purchase_channel" class="custom-control-input" value="0" checked />
                                                        <label class="custom-control-label" for="purchase_channel_front">หน้าร้าน</label>
                                                    </div>
                                                    <div class="custom-control custom-radio mt-0">
                                                        <input type="radio" id="purchase_channel_online" name="purchase_channel" class="custom-control-input" value="1" />
                                                        <label class="custom-control-label" for="purchase_channel_online">ออนไลน์</label>
                                                    </div>
                                                    <div class="custom-control custom-radio mt-0">
                                                        <input type="radio" id="purchase_channel_agent" name="purchase_channel" class="custom-control-input" value="2" />
                                                        <label class="custom-control-label" for="purchase_channel_agent">ตัวแทน</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelLg" class="col-sm-5 col-form-label col-form-label-lg">ช่องทางการชำระเงิน</label>
                                            <div class="col-sm-7 col-form-label col-form-label-lg">
                                                <div class="demo-inline-spacing">
                                                    <div class="custom-control custom-radio mt-0">
                                                        <input onclick="getBankList(this.value)" type="radio" id="payment_channel_cash" name="payment_channel" class="custom-control-input" value="0" checked />
                                                        <label class="custom-control-label" for="payment_channel_cash">เงินสด</label>
                                                    </div>
                                                    <div class="custom-control custom-radio mt-0">
                                                        <input onclick="getBankList(this.value)" type="radio" id="payment_channel_credit" name="payment_channel" class="custom-control-input" value="1" />
                                                        <label class="custom-control-label" for="payment_channel_credit">เครดิต</label>
                                                    </div>
                                                    <div class="custom-control custom-radio mt-0">
                                                        <input onclick="getBankList(this.value)" type="radio" id="payment_channel_bank" name="payment_channel" class="custom-control-input" value="2" />
                                                        <label class="custom-control-label" for="payment_channel_bank">โอนเงิน</label>
                                                    </div>
{{--                                                    <div class="custom-control custom-checkbox">--}}
{{--                                                        <input type="checkbox" class="custom-control-input" id="customCheck1" checked />--}}
{{--                                                        <label class="custom-control-label" for="customCheck1">เงินสด</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="custom-control custom-checkbox">--}}
{{--                                                        <input type="checkbox" class="custom-control-input" id="customCheck2" checked />--}}
{{--                                                        <label class="custom-control-label" for="customCheck2">เครดิต</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="custom-control custom-checkbox">--}}
{{--                                                        <input type="checkbox" class="custom-control-input" id="customCheck3" checked />--}}
{{--                                                        <label class="custom-control-label" for="customCheck3">โอนเงิน</label>--}}
{{--                                                    </div>--}}
                                                </div>
                                            </div>
                                        </div>
{{--                                        <div class="form-group row" id="bank_list">--}}
{{--                                        </div>--}}
                                        <div class="form-group row" id="bank_list">
                                        </div>
{{--                                        <div class="form-group row">--}}
{{--                                            <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">เงินสด</label>--}}
{{--                                            <div class="col-sm-7">--}}
{{--                                                <input--}}
{{--                                                    type="number"--}}
{{--                                                    class="form-control form-control-lg"--}}
{{--                                                    id="colFormLabelLg"--}}
{{--                                                    placeholder=""--}}
{{--                                                    value="30000"--}}
{{--                                                />--}}
{{--                                            </div>--}}
{{--                                            <div class="col-sm-2 col-form-label col-form-label-lg">บาท</div>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group row">--}}
{{--                                            <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">เครดิต</label>--}}
{{--                                            <div class="col-sm-7">--}}
{{--                                                <input--}}
{{--                                                    type="number"--}}
{{--                                                    class="form-control form-control-lg"--}}
{{--                                                    id="colFormLabelLg"--}}
{{--                                                    placeholder=""--}}
{{--                                                    value="2000"--}}
{{--                                                />--}}
{{--                                            </div>--}}
{{--                                            <div class="col-sm-2 col-form-label col-form-label-lg">บาท</div>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group row">--}}
{{--                                            <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">โอนเงิน</label>--}}
{{--                                            <div class="col-sm-7">--}}
{{--                                                <input--}}
{{--                                                    type="number"--}}
{{--                                                    class="form-control form-control-lg"--}}
{{--                                                    id="colFormLabelLg"--}}
{{--                                                    placeholder=""--}}
{{--                                                    value="500"--}}
{{--                                                />--}}
{{--                                            </div>--}}
{{--                                            <div class="col-sm-2 col-form-label col-form-label-lg">บาท</div>--}}
{{--                                        </div>--}}
                                        <hr />
                                        <button type="button" class="btn btn-primary btn-block btn-next place-order" id="confirmBuyGold">ยืนยันรายการ</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div
                class="modal fade text-left"
                id="modal_gold_add"
                tabindex="-1"
                role="dialog"
                aria-labelledby="myModalLabel1"
                aria-hidden="true"
            >
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel1">เพิ่มรายการทอง</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div id="preview_gold_add"></div>
                            <form id="form_gold_add">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="col-md-12 mb-2 mt-1">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <span>หมวดหมู่สินค้า</span>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="row">
                                                        <div class="custom-control custom-radio mr-1">
                                                            <input onchange="gold_category_form(this)" type="radio" id="gold_category_ninety_six" name="gold_category" value="1" class="custom-control-input" checked/>
                                                            <label class="custom-control-label" for="gold_category_ninety_six">ทอง 96.5 %</label>
                                                        </div>
                                                        <div class="custom-control custom-radio mr-1">
                                                            <input onchange="gold_category_form(this)" type="radio" id="gold_category_ninety_nine" name="gold_category" value="2" class="custom-control-input"/>
                                                            <label class="custom-control-label" for="gold_category_ninety_nine">ทอง 99.99 %</label>
                                                        </div>
                                                        <div class="custom-control custom-radio mr-1">
                                                            <input onchange="gold_category_form(this)" type="radio" id="gold_category_ninety" name="gold_category" value="3" class="custom-control-input" />
                                                            <label class="custom-control-label" for="gold_category_ninety">ทอง 90 %</label>
                                                        </div>
                                                        <div class="custom-control custom-radio">
                                                            <input onchange="gold_category_form(this)" type="radio" id="gold_category_other" name="gold_category" value="4" class="custom-control-input"/>
                                                            <label class="custom-control-label" for="gold_category_other">อื่น ๆ</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb-2" id="gold_type_input">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <span>ประเภททอง</span>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="custom-control custom-radio mr-1">
                                                            <input onchange="gold_type_form(this)" type="radio" class="custom-control-input" name="gold_type" value="1"  id="gold_type_ornament_add" checked />
                                                            <label class="custom-control-label" for="gold_type_ornament_add">ทองรูปพรรณ</label>
                                                        </div>
                                                        <div class="custom-control custom-radio mr-1">
                                                            <input onchange="gold_type_form(this)" type="radio" class="custom-control-input" name="gold_type" value="2" id="gold_type_bar_add" />
                                                            <label class="custom-control-label" for="gold_type_bar_add">ทองแท่ง</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb-2" id="gold_sub_category_input">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <span>ประเภทสินค้า</span>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <select class="form-control" id="gold_sub_category" name="gold_sub_category">
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb-2">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <span>ลายทอง</span>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" id="gold_pattern_add" name="gold_pattern"/>
                                                        <div id="gold_pattern_error_add" class="invalid-feedback"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
{{--                                        <div class="col-md-12 mb-2">--}}
{{--                                            <div class="row">--}}
{{--                                                <div class="col-md-3">--}}
{{--                                                    <span>รหัสสินค้า</span>--}}
{{--                                                </div>--}}
{{--                                                <div class="col-md-6">--}}
{{--                                                    <div class="form-group">--}}
{{--                                                        <strong id="gold_sku_add_display"></strong>--}}
{{--                                                        <input type="hidden" class="form-control" id="gold_sku_add" name="gold_sku" value=""/>--}}
{{--                                                        <div id="gold_sku_error_add" class="invalid-feedback"></div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="col-md-12 mb-2" id="gold_weight_input">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <span>น้ำหนัก</span>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="number" class="form-control" id="gold_weight_add" name="gold_weight"/>
                                                        <div id="gold_weight_error_add" class="invalid-feedback"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <select class="form-control" id="gold_unit" name="gold_unit">
                                                            @foreach(\App\Models\Gold\GoldUnits::query()->get() as $row)
                                                                <option value="{{ $row->id }}">{{ $row->title }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb-2">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <span>น้ำหนักจริง</span>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="number" class="form-control" id="gold_weight_actual_add" name="gold_weight_actual"/>
                                                        <div id="gold_weight_actual_error_add" class="invalid-feedback"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <span>กรัม (g)</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb-2">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <span>ราคารับซื้อ</span>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="number" class="form-control" id="gold_cost_add" name="gold_cost"/>
                                                        <div id="gold_cost_error_add" class="invalid-feedback"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <span>บาท</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb-2" id="gold_size_input">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <span>ขนาด/ความยาว</span>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="number" class="form-control" id="gold_size_add" name="gold_size"/>
                                                        <div id="gold_size_error_add" class="invalid-feedback"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
{{--                                                    <span>บาท</span>--}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb-2">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <span>รูปภาพสินค้า</span>
                                                </div>
                                                <div class="col-md-9" id="gold_image_input">
                                                </div>
                                                <input type="hidden" id="gold_image_base64_add" name="gold_image_base64" value="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn_gold_add">บันทึก</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                        </div>
                    </div>
                </div>
            </div>

            <div
                class="modal fade text-left"
                id="large"
                tabindex="-1"
                role="dialog"
                aria-labelledby="myModalLabel17"
                aria-hidden="true"
            >
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">รายละเอียดสินค้า</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xl-2 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">หมวดหมู่สินค้า</label>
                                </div>
                                <div class="col-xl-3 col-md-6 col-12 mb-1">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input" checked />
                                        <label class="custom-control-label" for="customRadio1">ทอง 96.5 %</label>
                                    </div>
                                    <div class="custom-control custom-radio mt-1">
                                        <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input" />
                                        <label class="custom-control-label" for="customRadio2">ทอง 99.99 %</label>
                                    </div>
                                </div>
                                <div class="col-xl-7 col-md-6 col-12 mb-1">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input" />
                                        <label class="custom-control-label" for="customRadio3">ทอง 90 %</label>
                                    </div>
                                    <div class="custom-control custom-radio mt-1">
                                        <input type="radio" id="customRadio4" name="customRadio" class="custom-control-input" />
                                        <label class="custom-control-label" for="customRadio4">อื่น ๆ</label>
                                    </div>
                                </div>

                                <div class="col-xl-2 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">ประเภททอง</label>
                                </div>
                                <div class="col-xl-3 col-md-6 col-12 mb-1">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="Radio1" name="customRadio" class="custom-control-input" checked />
                                        <label class="custom-control-label" for="Radio1">ทองรูปพรรณ</label>
                                    </div>
                                </div>
                                <div class="col-xl-7 col-md-6 col-12 mb-1">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="Radio2" name="customRadio" class="custom-control-input" />
                                        <label class="custom-control-label" for="Radio2">ทองแท่ง</label>
                                    </div>
                                </div>

                                <div class="col-xl-2 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">ประเภทสินค้า</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <select class="form-control" id="basicSelect">
                                            <option>สร้อยคอ</option>
                                            <option>แหวน</option>
                                            <option>ทองแท่ง</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xl-2 col-md-6 col-12 mb-1">
                                    <label for="basicInput"></label>
                                </div>

                                <div class="col-xl-2 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">ชื่อสินค้า</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="basicInput"/>
                                    </div>
                                </div>
                                <div class="col-xl-2 col-md-6 col-12 mb-1">
                                    <label for="basicInput"></label>
                                </div>

                                <div class="col-xl-2 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">รหัสสินค้า</label>
                                </div>
                                <div class="col-xl-10 col-md-6 col-12 mb-1">
                                    <span>G121102111243840</span>
                                </div>

                                <div class="col-xl-2 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">น้ำหนัก</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="basicInput"/>
                                    </div>
                                </div>
                                <div class="col-xl-2 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <select class="form-control" id="basicSelect">
                                            <option>สลึง</option>
                                            <option>บาท</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xl-2 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">น้ำหนักจริง</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="basicInput"/>
                                    </div>
                                </div>
                                <div class="col-xl-2 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">กรัม (g)</label>
                                </div>

                                <div class="col-xl-2 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">ราคารับซื้อ</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="basicInput"/>
                                    </div>
                                </div>
                                <div class="col-xl-2 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">บาท</label>
                                </div>

                                <div class="col-xl-2 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">ขนาด/ความยาว</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="basicInput"/>
                                    </div>
                                </div>
                                <div class="col-xl-2 col-md-6 col-12 mb-1">
                                </div>
                                <div class="col-xl-2 col-md-6 col-12 mb-1">
                                    <label for="basicInput" class="subText">รูปสินค้า</label>
                                </div>
                                <div class="col-xl-8 col-md-6 col-12 mb-1">
                                    <form action="#" class="dropzone dropzone-area" id="dpz-single-file">
                                        <div class="dz-message">Drop files here or click to upload.</div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary"  data-dismiss="modal">บันทึก</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">ปิด</button>
                        </div>
                    </div>
                </div>
            </div>
            <div
                class="modal fade text-left"
                id="buyerModal"
                tabindex="-1"
                role="dialog"
                aria-labelledby="myModalLabel17"
                aria-hidden="true"
            >
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">ข้อมูลสมาชิก</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div id="preview_buyer_add"></div>
                            <form id="form_buyer_add">
                            @csrf
                                <div class="row">
                                    <div class="col-xl-12 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText text-danger">**สำหรับสมาชิกเก่ากรอกเพียงหมายเลขบัตรประชาชน หรือ เบอร์โทร</label>
                                    </div>

                                    <div class="col-xl-2 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">เลขบัตรประชาชน</label>
                                    </div>
                                    <div class="col-xl-8 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input onchange="getBuyerDataOld()" type="text" class="form-control" id="id_card_add" name="id_card" placeholder="เฉพาะตัวเลขเท่านั้น"/>
                                            <div id="id_card_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-2 col-md-6 col-12 mb-1">
                                        <button type="button" class="btn btn-danger" onclick="dummy()">DUMMY</button>
                                    </div>

                                    <div class="col-xl-2 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">เบอร์โทร<label class="text-danger">*</label></label>
                                    </div>
                                    <div class="col-xl-10 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input onchange="getBuyerDataOld()" type="text" class="form-control" id="phone_add" name="phone" placeholder="เฉพาะตัวเลขเท่านั้น"/>
                                            <div id="phone_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-2 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">คำนำหน้าชื่อ</label>
                                    </div>
                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <select id="title_name_add" name="title" class="form-control" >
                                                <option value="">โปรดระบุคำนำหน้าชื่อ</option>
                                                <option value="0">นาย</option>
                                                <option value="1">นาง</option>
                                                <option value="2">นางสาว</option>
                                            </select>
                                            <div id="title_name_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-xl-2 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">ไลน์ไอดี</label>
                                    </div>
                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="line_add" name="line" placeholder="@abc"/>
                                            <div id="line_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-2 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">ชื่อ<label class="text-danger">*</label></label>
                                    </div>
                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="name_add" name="name"/>
                                            <div id="name_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-xl-2 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">เฟสบุ๊ค URL</label>
                                    </div>
                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="facebook_add" name="facebook" placeholder="www.facebook.com/abc"/>
                                            <div id="facebook_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-2 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">นามสกุล</label>
                                    </div>
                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="surname_add" name="surname"/>
                                            <div id="surname_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-xl-2 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">คำนำหน้าชื่ออังกฤษ</label>
                                    </div>
                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <select id="title_name_en_add" name="title_en" class="form-control" >
                                                <option value=""></option>
                                                <option value="0">Mr.</option>
                                                <option value="1">Mrs.</option>
                                                <option value="2">Miss</option>
                                            </select>
                                            <div id="title_name_en_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-2 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">บ้านเลขที่</label>
                                    </div>
                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="address_add" name="address"/>
                                            <div id="address_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-xl-2 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">ชื่ออังกฤษ</label>
                                    </div>
                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="name_en_add" name="name_en"/>
                                            <div id="name_en_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-2 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">จังหวัด</label>
                                    </div>
                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <select id="province_id_add" name="province_id" class="form-control">
                                                <option value="">กรุณาเลือกจังหวัด</option>
                                                @foreach(\App\Models\Province::query()->get() as $item)
                                                    <option value="{{ $item->id }}">{{ $item->title_th }}</option>
                                                @endforeach
                                            </select>
                                            <div id="province_id_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-xl-2 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">นามสกุลอังกฤษ</label>
                                    </div>
                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="surname_en_add" name="surname_en"/>
                                            <div id="surname_en_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-2 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">อำเภอ</label>
                                    </div>
                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <select id="district_id_add" name="district_id" class="form-control">
                                            </select>
                                            <div id="district_id_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-xl-2 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">วันเกิด</label>
                                    </div>
                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" id="birthday_add" name="birthday" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                            <div id="birthday_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-2 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">ตำบล</label>
                                    </div>
                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <select id="subdistrict_id_add" name="subdistrict_id" class="form-control">
                                            </select>
                                            <div id="subdistrict_id_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-xl-2 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">เพศ</label>
                                    </div>
                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <select id="gender_add" name="gender" class="form-control" >
                                                <option value=""></option>
                                                <option value="0">ชาย</option>
                                                <option value="1">หญิง</option>
                                            </select>
                                            <div id="gender_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-2 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">รหัสไปรษณีย์</label>
                                    </div>
                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="zipcode_add" name="zipcode" readonly/>
                                            <div id="zipcode_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-xl-2 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">วันที่ออกบัตร</label>
                                    </div>
                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" id="issue_add" name="issue" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                            <div id="issue_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-2 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">อีเมล</label>
                                    </div>
                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="email_add" name="email" placeholder="abc@gmail.com"/>
                                            <div id="email_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-xl-2 col-md-6 col-12 mb-1">
                                        <label for="basicInput" class="subText">วันที่บัตรหมดอายุ</label>
                                    </div>
                                    <div class="col-xl-4 col-md-6 col-12 mb-1">
                                        <div class="form-group">
                                            <input type="text" id="expire_add" name="expire" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                            <div id="expire_error_add" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn_buyer_add">บันทึก</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">ปิด</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('vendor-script')
    <!-- Vendor js files -->
    <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/spinner/jquery.bootstrap-touchspin.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/repeater/jquery.repeater.min.js')) }}"></script>
@endsection

@section('page-script')
    <!-- Page js files -->

    <script src="{{ asset('js/dropify/dist/js/dropify.js') }}"></script>
    <script src="{{ asset(mix('js/scripts/pages/app-ecommerce-checkout.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-repeater.js')) }}"></script>
    <script src="{{ asset('js/numeral/src/numeral.js') }}"></script>

    <script>
        {{--$(document).ready(function() {--}}
        {{--    $("#confirmPayment").click(function() {--}}
        {{--        window.location.href = "{{ url('/buy-invoice') }}";--}}
        {{--    });--}}
        {{--});--}}
        $(document).ready(function () {
            $('#modal_gold_add').on('show.bs.modal', function() {
                // gold_category_form({ value: '1' });
                gold_type_form({ value: '1' });
                gold_sub_category(1);

                $("#gold_image_input").html("<input type=\"file\" class=\"dropify\" name=\"gold_image\" id=\"gold_image_add\" data-width=\"200\" data-height=\"150\">\n" +
                    "<div id=\"gold_image_error_add\" class=\"invalid-feedback\" style=\"display:block;\"></div>");

                $('.dropify').dropify({
                    messages: {
                        'default': 'Drag and drop a file here or click',
                        'replace': 'Drag and drop or click to replace',
                        'remove': 'Remove',
                        'error': 'Ooops, something wrong happended.'
                    },
                    tpl: {
                        wrap:            '<div class="dropify-wrapper"></div>',
                        loader:          '<div class="dropify-loader"></div>',
                        message:         '<div class="dropify-message"><span class="file-icon" /> <p></p></div>',
                        preview:         '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message"></p></div></div></div>',
                        filename:        '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
                        clearButton:     '<button type="button" class="dropify-clear">ลบ</button>',
                        errorLine:       '<p class="dropify-error"></p>',
                        errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
                    }
                });

                $("#gold_image_add").change(function() {
                    readURL(this, "gold_image_base64_add");
                });

                {{--$.ajax({--}}
                {{--    headers: {--}}
                {{--        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
                {{--    },--}}
                {{--    url: '{{ url('receivebuy/barcode') }}',--}}
                {{--    type: "GET",--}}
                {{--    enctype: 'multipart/form-data',--}}
                {{--    dataType: 'JSON',--}}
                {{--    processData: false,--}}
                {{--    contentType: false,--}}
                {{--    cache: false,--}}
                {{--    success: function (res) {--}}
                {{--        $('#gold_sku_add_display').html(res.barcode_number);--}}
                {{--        $('#gold_sku_add').val(res.barcode_number);--}}
                {{--    }--}}
                {{--});--}}
            });

            $('#modal_gold_add').on('hide.bs.modal', function() {
                $('#form_gold_add')[0].reset();
                $('#gold_image_base64_add').val("");
                $('#gold_sku_add').val("");

                removeValidation('add');
            });

            $('#btn_gold_add').click(function (e) {
                $('#btn_gold_add').empty();
                $('#btn_gold_add').attr("disabled", true);
                $('#btn_gold_add').append("<div class=\"spinner-border spinner-grow-sm text-light\" role=\"status\"><span class=\"sr-only\">Loading...</span></div> กำลังบันทึกข้อมูล");

                removeValidation('add');

                e.preventDefault();
                var form = $('#form_gold_add')[0];
                var data = new FormData(form);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{url('/receivebuy/store')}}',
                    type: "POST",
                    enctype: 'multipart/form-data',
                    dataType: 'JSON',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (res) {

                        if(res.error == 0){
                            $('#preview_gold_add').append("<div class=\"alert alert-success\" role=\"alert\">\n" +
                                "<h4 class=\"alert-heading\">เพิ่มข้อมูลเรียบร้อย</h4>\n" +
                                "</div>");
                            setTimeout(function(){
                                $('#modal_gold_add').modal('hide');

                                if (res.data.gold_category == 1){
                                    if(res.data.gold_type == 1){
                                        $('#gold_buy_list').append('<div class="card ecommerce-card" id="gold_buy_list_' + res.gold_sku + '">\n' +
                                            '                                        <div class="item-img ml-2">\n' +
                                            '                                            <img src="' + ((res.data.gold_image_base64) ? res.data.gold_image_base64 : 'https://via.placeholder.com/400x300') + '" alt="img-placeholder" />\n' +
                                            '                                        </div>\n' +
                                            '                                        <div class="card-body">\n' +
                                            '                                            <div class="item-name">\n' +
                                            '                                                <h4 class="mb-0 text-body bold">' + res.data.gold_pattern + '</h4>\n' +
                                            '                                                <span class="item-company text-primary">' + res.gold_type.title + '</span>\n' +
                                            '                                                <div class="badge badge-pill badge-warning">' + res.gold_percent.title + '</div>\n' +
                                            '                                            </div>\n' +
                                            '                                            <span class="item-company">หนัก: ' + res.data.gold_weight + ' ' + res.gold_units.title + '</span>\n' +
                                            '                                            <span class="item-company">น้ำหนักจริง: ' + res.data.gold_weight_actual + ' กรัม</span>\n' +
                                            '                                            <span class="item-company">ขนาด/ความยาว: ' + res.data.gold_size + '</span>\n' +
                                            '                                        </div>\n' +
                                            '                                        <div class="item-options text-center">\n' +
                                            '                                            <div class="item-wrapper">\n' +
                                            '                                                <div class="item-cost">\n' +
                                            '                                                    <h4 class="item-price">฿ ' + numeral(res.data.gold_cost).format('0,0.00') + '</h4>\n' +
                                            '                                                    <p class="card-text shipping">\n' +
                                            '                                                        <span class="badge badge-pill badge-light-success">รับซื้อ</span>\n' +
                                            '                                                    </p>\n' +
                                            '                                                </div>\n' +
                                            '                                            </div>\n' +
                                            '                                            <button type="button" class="btn btn-dark mt-1 remove-wishlist" onclick="removeProductList(\'' + res.gold_sku + '\')">\n' +
                                            '                                                <i data-feather="x" class="align-middle mr-25"></i>\n' +
                                            '                                                <span>ยกเลิก</span>\n' +
                                            '                                            </button>\n' +
                                            '                                        </div>\n' +
                                            '                                        <input type="hidden" name="gold_category[]" value="' + res.data.gold_category  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_type[]" value="' + res.data.gold_type + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_sub_category[]" value="' + res.data.gold_sub_category  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_pattern[]" value="' + res.data.gold_pattern  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_sku[]" value="' + res.gold_sku  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_weight[]" value="' + res.data.gold_weight + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_unit[]" value="' + res.data.gold_unit + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_weight_actual[]" value="' + res.data.gold_weight_actual  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_cost[]" class="price-list-hidden" value="' + res.data.gold_cost  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_size[]" value="' + res.data.gold_size  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_image[]" value="' +  res.data.gold_image_base64 + '"/>\n' +
                                            '                                    </div>');
                                        feather.replace();
                                        calculateTotal();
                                    }
                                    else if(res.data.gold_type == 2) {
                                        $('#gold_buy_list').append('<div class="card ecommerce-card" id="gold_buy_list_' + res.gold_sku + '">\n' +
                                            '                                        <div class="item-img ml-2">\n' +
                                            '                                            <img src="' + ((res.data.gold_image_base64) ? res.data.gold_image_base64 : 'https://via.placeholder.com/400x300') + '" alt="img-placeholder" />\n' +
                                            '                                        </div>\n' +
                                            '                                        <div class="card-body">\n' +
                                            '                                            <div class="item-name">\n' +
                                            '                                                <h4 class="mb-0 text-body bold">' + res.data.gold_pattern + '</h4>\n' +
                                            '                                                <span class="item-company text-primary">' + res.gold_type.title + '</span>\n' +
                                            '                                                <div class="badge badge-pill badge-warning">' + res.gold_percent.title + '</div>\n' +
                                            '                                            </div>\n' +
                                            '                                            <span class="item-company">หนัก: ' + res.data.gold_weight + ' ' + res.gold_units.title + '</span>\n' +
                                            '                                            <span class="item-company">น้ำหนักจริง: ' + res.data.gold_weight_actual + ' กรัม</span>\n' +
                                            // '                                            <span class="item-company">ขนาด/ความยาว: ' + res.data.gold_size + '</span>\n' +
                                            '                                        </div>\n' +
                                            '                                        <div class="item-options text-center">\n' +
                                            '                                            <div class="item-wrapper">\n' +
                                            '                                                <div class="item-cost">\n' +
                                            '                                                    <h4 class="item-price">฿ ' + numeral(res.data.gold_cost).format('0,0.00') + '</h4>\n' +
                                            '                                                    <p class="card-text shipping">\n' +
                                            '                                                        <span class="badge badge-pill badge-light-success">รับซื้อ</span>\n' +
                                            '                                                    </p>\n' +
                                            '                                                </div>\n' +
                                            '                                            </div>\n' +
                                            '                                            <button type="button" class="btn btn-dark mt-1 remove-wishlist" onclick="removeProductList(\'' + res.gold_sku + '\')">\n' +
                                            '                                                <i data-feather="x" class="align-middle mr-25"></i>\n' +
                                            '                                                <span>ยกเลิก</span>\n' +
                                            '                                            </button>\n' +
                                            '                                        </div>\n' +
                                            '                                        <input type="hidden" name="gold_category[]" value="' + res.data.gold_category  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_type[]" value="' + res.data.gold_type + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_sub_category[]" value="' + res.data.gold_sub_category  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_pattern[]" value="' + res.data.gold_pattern  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_sku[]" value="' + res.gold_sku  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_weight[]" value="' + res.data.gold_weight + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_unit[]" value="' + res.data.gold_unit + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_weight_actual[]" value="' + res.data.gold_weight_actual  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_cost[]" class="price-list-hidden" value="' + res.data.gold_cost  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_size[]" value="' + res.data.gold_size  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_image[]" value="' +  res.data.gold_image_base64 + '"/>\n' +
                                            '                                    </div>');
                                        feather.replace();
                                        calculateTotal();
                                    }
                                }
                                else if (res.data.gold_category == 2){
                                    if(res.data.gold_type == 1){
                                        $('#gold_buy_list').append('<div class="card ecommerce-card" id="gold_buy_list_' + res.gold_sku + '">\n' +
                                            '                                        <div class="item-img ml-2">\n' +
                                            '                                            <img src="' + ((res.data.gold_image_base64) ? res.data.gold_image_base64 : 'https://via.placeholder.com/400x300') + '" alt="img-placeholder" />\n' +
                                            '                                        </div>\n' +
                                            '                                        <div class="card-body">\n' +
                                            '                                            <div class="item-name">\n' +
                                            '                                                <h4 class="mb-0 text-body bold">' + res.data.gold_pattern + '</h4>\n' +
                                            '                                                <span class="item-company text-primary">' + res.gold_type.title + '</span>\n' +
                                            '                                                <div class="badge badge-pill badge-warning">' + res.gold_percent.title + '</div>\n' +
                                            '                                            </div>\n' +
                                            // '                                            <span class="item-company">หนัก: ' + res.data.gold_weight + ' ' + res.gold_units.title + '</span>\n' +
                                            '                                            <span class="item-company">น้ำหนักจริง: ' + res.data.gold_weight_actual + ' กรัม</span>\n' +
                                            '                                            <span class="item-company">ขนาด/ความยาว: ' + res.data.gold_size + '</span>\n' +
                                            '                                        </div>\n' +
                                            '                                        <div class="item-options text-center">\n' +
                                            '                                            <div class="item-wrapper">\n' +
                                            '                                                <div class="item-cost">\n' +
                                            '                                                    <h4 class="item-price">฿ ' + numeral(res.data.gold_cost).format('0,0.00') + '</h4>\n' +
                                            '                                                    <p class="card-text shipping">\n' +
                                            '                                                        <span class="badge badge-pill badge-light-success">รับซื้อ</span>\n' +
                                            '                                                    </p>\n' +
                                            '                                                </div>\n' +
                                            '                                            </div>\n' +
                                            '                                            <button type="button" class="btn btn-dark mt-1 remove-wishlist" onclick="removeProductList(\'' + res.gold_sku + '\')">\n' +
                                            '                                                <i data-feather="x" class="align-middle mr-25"></i>\n' +
                                            '                                                <span>ยกเลิก</span>\n' +
                                            '                                            </button>\n' +
                                            '                                        </div>\n' +
                                            '                                        <input type="hidden" name="gold_category[]" value="' + res.data.gold_category  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_type[]" value="' + res.data.gold_type + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_sub_category[]" value="' + res.data.gold_sub_category  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_pattern[]" value="' + res.data.gold_pattern  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_sku[]" value="' + res.gold_sku  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_weight[]" value="' + res.data.gold_weight + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_unit[]" value="' + res.data.gold_unit + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_weight_actual[]" value="' + res.data.gold_weight_actual  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_cost[]" class="price-list-hidden" value="' + res.data.gold_cost  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_size[]" value="' + res.data.gold_size  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_image[]" value="' +  res.data.gold_image_base64 + '"/>\n' +
                                            '                                    </div>');
                                        feather.replace();
                                        calculateTotal();
                                    }
                                    else if(res.data.gold_type == 2) {
                                        $('#gold_buy_list').append('<div class="card ecommerce-card" id="gold_buy_list_' + res.gold_sku + '">\n' +
                                            '                                        <div class="item-img ml-2">\n' +
                                            '                                            <img src="' + ((res.data.gold_image_base64) ? res.data.gold_image_base64 : 'https://via.placeholder.com/400x300') + '" alt="img-placeholder" />\n' +
                                            '                                        </div>\n' +
                                            '                                        <div class="card-body">\n' +
                                            '                                            <div class="item-name">\n' +
                                            '                                                <h4 class="mb-0 text-body bold">' + res.data.gold_pattern + '</h4>\n' +
                                            '                                                <span class="item-company text-primary">' + res.gold_type.title + '</span>\n' +
                                            '                                                <div class="badge badge-pill badge-warning">' + res.gold_percent.title + '</div>\n' +
                                            '                                            </div>\n' +
                                            // '                                            <span class="item-company">หนัก: ' + res.data.gold_weight + ' ' + res.gold_units.title + '</span>\n' +
                                            '                                            <span class="item-company">น้ำหนักจริง: ' + res.data.gold_weight_actual + ' กรัม</span>\n' +
                                            // '                                            <span class="item-company">ขนาด/ความยาว: ' + res.data.gold_size + '</span>\n' +
                                            '                                        </div>\n' +
                                            '                                        <div class="item-options text-center">\n' +
                                            '                                            <div class="item-wrapper">\n' +
                                            '                                                <div class="item-cost">\n' +
                                            '                                                    <h4 class="item-price">฿ ' + numeral(res.data.gold_cost).format('0,0.00') + '</h4>\n' +
                                            '                                                    <p class="card-text shipping">\n' +
                                            '                                                        <span class="badge badge-pill badge-light-success">รับซื้อ</span>\n' +
                                            '                                                    </p>\n' +
                                            '                                                </div>\n' +
                                            '                                            </div>\n' +
                                            '                                            <button type="button" class="btn btn-dark mt-1 remove-wishlist" onclick="removeProductList(\'' + res.gold_sku + '\')">\n' +
                                            '                                                <i data-feather="x" class="align-middle mr-25"></i>\n' +
                                            '                                                <span>ยกเลิก</span>\n' +
                                            '                                            </button>\n' +
                                            '                                        </div>\n' +
                                            '                                        <input type="hidden" name="gold_category[]" value="' + res.data.gold_category  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_type[]" value="' + res.data.gold_type + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_sub_category[]" value="' + res.data.gold_sub_category  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_pattern[]" value="' + res.data.gold_pattern  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_sku[]" value="' + res.gold_sku  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_weight[]" value="' + res.data.gold_weight + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_unit[]" value="' + res.data.gold_unit + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_weight_actual[]" value="' + res.data.gold_weight_actual  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_cost[]" class="price-list-hidden" value="' + res.data.gold_cost  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_size[]" value="' + res.data.gold_size  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_image[]" value="' +  res.data.gold_image_base64 + '"/>\n' +
                                            '                                    </div>');
                                        feather.replace();
                                        calculateTotal();
                                    }
                                }
                                else if (res.data.gold_category == 3){
                                    if(res.data.gold_type == 1){
                                        $('#gold_buy_list').append('<div class="card ecommerce-card" id="gold_buy_list_' + res.gold_sku + '">\n' +
                                            '                                        <div class="item-img ml-2">\n' +
                                            '                                            <img src="' + ((res.data.gold_image_base64) ? res.data.gold_image_base64 : 'https://via.placeholder.com/400x300') + '" alt="img-placeholder" />\n' +
                                            '                                        </div>\n' +
                                            '                                        <div class="card-body">\n' +
                                            '                                            <div class="item-name">\n' +
                                            '                                                <h4 class="mb-0 text-body bold">' + res.data.gold_pattern + '</h4>\n' +
                                            '                                                <span class="item-company text-primary">' + res.gold_type.title + '</span>\n' +
                                            '                                                <div class="badge badge-pill badge-warning">' + res.gold_percent.title + '</div>\n' +
                                            '                                            </div>\n' +
                                            // '                                            <span class="item-company">หนัก: ' + res.data.gold_weight + ' ' + res.gold_units.title + '</span>\n' +
                                            '                                            <span class="item-company">น้ำหนักจริง: ' + res.data.gold_weight_actual + ' กรัม</span>\n' +
                                            '                                            <span class="item-company">ขนาด/ความยาว: ' + res.data.gold_size + '</span>\n' +
                                            '                                        </div>\n' +
                                            '                                        <div class="item-options text-center">\n' +
                                            '                                            <div class="item-wrapper">\n' +
                                            '                                                <div class="item-cost">\n' +
                                            '                                                    <h4 class="item-price">฿ ' + numeral(res.data.gold_cost).format('0,0.00') + '</h4>\n' +
                                            '                                                    <p class="card-text shipping">\n' +
                                            '                                                        <span class="badge badge-pill badge-light-success">รับซื้อ</span>\n' +
                                            '                                                    </p>\n' +
                                            '                                                </div>\n' +
                                            '                                            </div>\n' +
                                            '                                            <button type="button" class="btn btn-dark mt-1 remove-wishlist" onclick="removeProductList(\'' + res.gold_sku + '\')">\n' +
                                            '                                                <i data-feather="x" class="align-middle mr-25"></i>\n' +
                                            '                                                <span>ยกเลิก</span>\n' +
                                            '                                            </button>\n' +
                                            '                                        </div>\n' +
                                            '                                        <input type="hidden" name="gold_category[]" value="' + res.data.gold_category  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_type[]" value="' + res.data.gold_type + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_sub_category[]" value="' + res.data.gold_sub_category  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_pattern[]" value="' + res.data.gold_pattern  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_sku[]" value="' + res.gold_sku  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_weight[]" value="' + res.data.gold_weight + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_unit[]" value="' + res.data.gold_unit + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_weight_actual[]" value="' + res.data.gold_weight_actual  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_cost[]" class="price-list-hidden" value="' + res.data.gold_cost  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_size[]" value="' + res.data.gold_size  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_image[]" value="' +  res.data.gold_image_base64 + '"/>\n' +
                                            '                                    </div>');
                                        feather.replace();
                                        calculateTotal();
                                    }
                                    else if(res.data.gold_type == 2) {
                                        $('#gold_buy_list').append('<div class="card ecommerce-card" id="gold_buy_list_' + res.gold_sku + '">\n' +
                                            '                                        <div class="item-img ml-2">\n' +
                                            '                                            <img src="' + ((res.data.gold_image_base64) ? res.data.gold_image_base64 : 'https://via.placeholder.com/400x300') + '" alt="img-placeholder" />\n' +
                                            '                                        </div>\n' +
                                            '                                        <div class="card-body">\n' +
                                            '                                            <div class="item-name">\n' +
                                            '                                                <h4 class="mb-0 text-body bold">' + res.data.gold_pattern + '</h4>\n' +
                                            '                                                <span class="item-company text-primary">' + res.gold_type.title + '</span>\n' +
                                            '                                                <div class="badge badge-pill badge-warning">' + res.gold_percent.title + '</div>\n' +
                                            '                                            </div>\n' +
                                            // '                                            <span class="item-company">หนัก: ' + res.data.gold_weight + ' ' + res.gold_units.title + '</span>\n' +
                                            '                                            <span class="item-company">น้ำหนักจริง: ' + res.data.gold_weight_actual + ' กรัม</span>\n' +
                                            // '                                            <span class="item-company">ขนาด/ความยาว: ' + res.data.gold_size + '</span>\n' +
                                            '                                        </div>\n' +
                                            '                                        <div class="item-options text-center">\n' +
                                            '                                            <div class="item-wrapper">\n' +
                                            '                                                <div class="item-cost">\n' +
                                            '                                                    <h4 class="item-price">฿ ' + numeral(res.data.gold_cost).format('0,0.00') + '</h4>\n' +
                                            '                                                    <p class="card-text shipping">\n' +
                                            '                                                        <span class="badge badge-pill badge-light-success">รับซื้อ</span>\n' +
                                            '                                                    </p>\n' +
                                            '                                                </div>\n' +
                                            '                                            </div>\n' +
                                            '                                            <button type="button" class="btn btn-dark mt-1 remove-wishlist" onclick="removeProductList(\'' + res.gold_sku + '\')">\n' +
                                            '                                                <i data-feather="x" class="align-middle mr-25"></i>\n' +
                                            '                                                <span>ยกเลิก</span>\n' +
                                            '                                            </button>\n' +
                                            '                                        </div>\n' +
                                            '                                        <input type="hidden" name="gold_category[]" value="' + res.data.gold_category  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_type[]" value="' + res.data.gold_type + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_sub_category[]" value="' + res.data.gold_sub_category  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_pattern[]" value="' + res.data.gold_pattern  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_sku[]" value="' + res.gold_sku  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_weight[]" value="' + res.data.gold_weight + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_unit[]" value="' + res.data.gold_unit + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_weight_actual[]" value="' + res.data.gold_weight_actual  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_cost[]" class="price-list-hidden" value="' + res.data.gold_cost  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_size[]" value="' + res.data.gold_size  + '"/>\n' +
                                            '                                        <input type="hidden" name="gold_image[]" value="' +  res.data.gold_image_base64 + '"/>\n' +
                                            '                                    </div>');
                                        feather.replace();
                                        calculateTotal();
                                    }
                                }
                                else if (res.data.gold_category == 4){
                                    $('#gold_buy_list').append('<div class="card ecommerce-card" id="gold_buy_list_' + res.gold_sku + '">\n' +
                                        '                                        <div class="item-img ml-2">\n' +
                                        '                                            <img src="' + ((res.data.gold_image_base64) ? res.data.gold_image_base64 : 'https://via.placeholder.com/400x300') + '" alt="img-placeholder" />\n' +
                                        '                                        </div>\n' +
                                        '                                        <div class="card-body">\n' +
                                        '                                            <div class="item-name">\n' +
                                        '                                                <h4 class="mb-0 text-body bold">' + res.data.gold_pattern + '</h4>\n' +
                                        // '                                                <span class="item-company text-primary">' + res.gold_type.title + '</span>\n' +
                                        '                                                <div class="badge badge-pill badge-warning">' + res.gold_percent.title + '</div>\n' +
                                        '                                            </div>\n' +
                                        // '                                            <span class="item-company">หนัก: ' + res.data.gold_weight + ' ' + res.gold_units.title + '</span>\n' +
                                        '                                            <span class="item-company">น้ำหนักจริง: ' + res.data.gold_weight_actual + ' กรัม</span>\n' +
                                        '                                            <span class="item-company">ขนาด/ความยาว: ' + res.data.gold_size + '</span>\n' +
                                        '                                        </div>\n' +
                                        '                                        <div class="item-options text-center">\n' +
                                        '                                            <div class="item-wrapper">\n' +
                                        '                                                <div class="item-cost">\n' +
                                        '                                                    <h4 class="item-price">฿ ' + numeral(res.data.gold_cost).format('0,0.00') + '</h4>\n' +
                                        '                                                    <p class="card-text shipping">\n' +
                                        '                                                        <span class="badge badge-pill badge-light-success">รับซื้อ</span>\n' +
                                        '                                                    </p>\n' +
                                        '                                                </div>\n' +
                                        '                                            </div>\n' +
                                        '                                            <button type="button" class="btn btn-dark mt-1 remove-wishlist" onclick="removeProductList(\'' + res.gold_sku + '\')">\n' +
                                        '                                                <i data-feather="x" class="align-middle mr-25"></i>\n' +
                                        '                                                <span>ยกเลิก</span>\n' +
                                        '                                            </button>\n' +
                                        '                                        </div>\n' +
                                        '                                        <input type="hidden" name="gold_category[]" value="' + res.data.gold_category  + '"/>\n' +
                                        '                                        <input type="hidden" name="gold_type[]" value="' + res.data.gold_type + '"/>\n' +
                                        '                                        <input type="hidden" name="gold_sub_category[]" value="' + res.data.gold_sub_category  + '"/>\n' +
                                        '                                        <input type="hidden" name="gold_pattern[]" value="' + res.data.gold_pattern  + '"/>\n' +
                                        '                                        <input type="hidden" name="gold_sku[]" value="' + res.gold_sku  + '"/>\n' +
                                        '                                        <input type="hidden" name="gold_weight[]" value="' + res.data.gold_weight + '"/>\n' +
                                        '                                        <input type="hidden" name="gold_unit[]" value="' + res.data.gold_unit + '"/>\n' +
                                        '                                        <input type="hidden" name="gold_weight_actual[]" value="' + res.data.gold_weight_actual  + '"/>\n' +
                                        '                                        <input type="hidden" name="gold_cost[]" class="price-list-hidden" value="' + res.data.gold_cost  + '"/>\n' +
                                        '                                        <input type="hidden" name="gold_size[]" value="' + res.data.gold_size  + '"/>\n' +
                                        '                                        <input type="hidden" name="gold_image[]" value="' +  res.data.gold_image_base64 + '"/>\n' +
                                        '                                    </div>');
                                    feather.replace();
                                    calculateTotal();
                                }
                                $('#product_error_add').html("");

                                // $('#table').dataTable().fnClearTable();
                                // $('#table').dataTable().fnDestroy();
                                // listUser();
                            }, 1000);
                        }else{
                            $('#preview_gold_add').append("<div class=\"alert alert-danger\" role=\"alert\">\n" +
                                "<h4 class=\"alert-heading\">พบข้อผิดพลาด</h4>\n" +
                                "</div>");
                            $.each(res.messages, function (k,v) {
                                $('#'+ k + '_add').addClass('is-invalid');
                                $('#'+ k + '_error_add').html(v);
                            });
                        }

                        $('#btn_gold_add').empty();
                        $('#btn_gold_add').append("บันทึก");
                        $('#btn_gold_add').attr("disabled",false);
                    }
                });
            });

            $('#buyerModal').on('show.bs.modal', function() {
                if($('#buyer_id').val()) {
                    getBuyerData($('#buyer_id').val());
                }
            });

            $('#buyerModal').on('hide.bs.modal', function() {
                $('#form_buyer_add')[0].reset();
                $('#btn_buyer_add').val('');
                // $('#gold_image_base64_add').val("");
                // $('#gold_sku_add').val("");

                removeValidationBuyer('add');
            });

            $('#btn_buyer_add').click(function (e) {
                $('#btn_buyer_add').empty();
                $('#btn_buyer_add').attr("disabled", true);
                $('#btn_buyer_add').append("<div class=\"spinner-border spinner-grow-sm text-light\" role=\"status\"><span class=\"sr-only\">Loading...</span></div> กำลังบันทึกข้อมูล");

                removeValidationBuyer('add');

                e.preventDefault();
                var form = $('#form_buyer_add')[0];
                var data = new FormData(form);

                var url = '';
                $('#btn_buyer_add').val() ? url = '{{url('/member-update')}}' + '/' + $('#btn_buyer_add').val() : url = '{{url('/member-store')}}' ;

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    type: "POST",
                    enctype: 'multipart/form-data',
                    dataType: 'JSON',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (res) {

                        if(res.error == 0){
                            $('#preview_buyer_add').append("<div class=\"alert alert-success\" role=\"alert\">\n" +
                                "<h4 class=\"alert-heading\">เพิ่มข้อมูลเรียบร้อย</h4>\n" +
                                "</div>");
                            setTimeout(function(){
                                $('#buyerModal').modal('hide');

                                $('#buyer_add_input').css('display', 'none');
                                $('#buyer_profile').css('display', 'block');
                                $('#buyer_name').html((res.data.name ? res.data.name : '') + ' '
                                    + (res.data.surname ? res.data.surname : ''));
                                $('#buyer_address').html((res.data.address ? res.data.address : '-') + ' '
                                    + (res.data.subdistrict ? res.data.subdistrict.title_th : '') + ' '
                                    + (res.data.district ? res.data.district.title_th : '') + ' '
                                    + (res.data.province ? res.data.province.title_th : '') + ' '
                                    + (res.data.zipcode ? res.data.zipcode : ''));
                                $('#buyer_phone').html(res.data.phone ? res.data.phone : '-');
                                $('#buyer_id').val(res.data.id);
                                $('#buyer_error_add' + status).html("");

                                // $('#table').dataTable().fnClearTable();
                                // $('#table').dataTable().fnDestroy();
                                // listUser();
                            }, 1000);
                        }else{
                            $('#preview_buyer_add').append("<div class=\"alert alert-danger\" role=\"alert\">\n" +
                                "<h4 class=\"alert-heading\">พบข้อผิดพลาด</h4>\n" +
                                "</div>");
                            $.each(res.messages, function (k,v) {
                                $('#'+ k + '_add').addClass('is-invalid');
                                $('#'+ k + '_error_add').html(v);
                            });
                        }

                        $('#btn_buyer_add').empty();
                        $('#btn_buyer_add').append("บันทึก");
                        $('#btn_buyer_add').attr("disabled",false);
                    }
                });
            });

            $('#btn_buyer_remove').click(function (e) {
                $('#buyer_add_input').css('display', 'block');
                $('#buyer_profile').css('display', 'none');
                $('#buyer_name').html('');
                $('#buyer_address').html('');
                $('#buyer_phone').html('');
                $('#buyer_id').val('');
            });

            $('#modal_view').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget);
                $.get('{{ url('/get-gold?id=') }}' + button.data('id'), function(res) {
                    // alert(' Id: '+res.data.id + ' Title: '+ res.data.title + ' Size: '+ res.data.size + ' price: '+ res.price);
                    //alert(' Id: '+res.data.id + ' Title: '+ res.data.title + ' Size: '+ res.data.size + ' price: '+ res.price);
                    // console.log('ราคา : ' + res.price);
                    // alert(res.price);

                    $('#title_view').html(res.data.title);
                    $('#weight_real_view').html(res.data.weight_real + ' กรัม (g)');
                    $('#weight_view').html(res.data.weight+' บาท');

                    $('#price_view').html(res.price + ' บาท');
                    $('#amount_view').html(res.amount + ' ชิ้น');
                    $('#smith_view').html(res.smith + ' บาท');

                    // $('#size_view').html(res.data.size);

                    // $('#category_id_view').html(res.data.category_id);

                    if(res.data.category_id == 1){
                        $('#category_id_view').html("ทอง 96.5 %");
                    }else if(res.data.category_id == 2){
                        $('#category_id_view').html("ทอง 99.99 %");
                    }else if(res.data.category_id == 3){
                        $('#category_id_view').html("ทอง 90 %");
                    }else {
                        $('#category_id_view').html("อื่น ๆ");
                    }

                    if (res.data.gold_type == 1){
                        $('#subcategory_id_view').html(res.subcategory.title + ' / ทองรูปพรรณ');
                    }else if (res.data.gold_type == 2){
                        $('#subcategory_id_view').html('ทองคำแท่ง');
                    }else {
                        $('#subcategory_id_view').html(res.subcategory.title);
                    }

                    if(res.data.pic != null){
                        $('#pic_view').html('<img src="{{ asset("_uploads/_products") }}/' + res.data.pic + '" style="width: 300px;" />');
                    }else{
                        $('#pic_view').html('<img src="{{ asset("assets/images/default.jpg") }}" style="width: 300px;" />');
                    }

                    // if(res.data.size != null){
                    //     $('#size_view').html(res.data.size);
                    // }else{
                    //     $('#size_view').html("-");
                    // }

                    res.data.size != null ? $('#size_view').html(res.data.size) : $('#size_view').html("-");

                    // alert(res.data.weight);

                    if(res.data.weight != null){
                        $('#hideweight').attr("hidden",false);
                        $('#weight_view').html(res.data.weight + ' ' + res.unit.title);
                    }else{
                        $('#hideweight').attr("hidden",true);
                    }


                    // $('#price_income_view').html(res.price + ' บาท');







                    //     if(res.error == false){
                    //         if(res.data.category_id == 1){
                    //             $('#category_id_view').html("ทอง 96.5 %");
                    //         }else if(res.data.category_id == 2){
                    //             $('#category_id_view').html("ทอง 99.99 %");
                    //         }else if(res.data.category_id == 3){
                    //             $('#category_id_view').html("ทอง 90 %");
                    //         }else {
                    //             $('#category_id_view').html("อื่น ๆ");
                    //         }

                    //         if (res.data.gold_type == 1){
                    //             $('#subcategory_id_view').html(res.subcategory.title + ' / ทองรูปพรรณ');
                    //         }else if (res.data.gold_type == 2){
                    //             $('#subcategory_id_view').html('ทองคำแท่ง');
                    //         }else {
                    //             $('#subcategory_id_view').html(res.subcategory.title);
                    //         }

                    //         // $('#subcategory_id_view').html(res.subcategory + ' / ' + res.category);
                    //         $('#title_view').html(res.data.title);

                    //         if(res.data.weight != null){
                    //             $('#hideweight').attr("hidden",false);
                    //             $('#weight_view').html(res.data.weight + ' ' + res.unit.title);
                    //         }else{
                    //             $('#hideweight').attr("hidden",true);
                    //         }

                    //         $('#weight_real_view').html(res.data.weight_real + ' กรัม (g)');
                    //         $('#price_income_view').html(res.price + ' บาท');
                    //         $('#price_goldsmith_view').html(res.smith + ' บาท');
                    //         $('#size_view').html(res.data.size);
                    //         $('#amount_view').html(res.amount + ' ชิ้น');

                    //         if(res.data.pic != null){
                    //             $('#pic_view').html('<img src="{{ asset("__Uploads/__Products") }}/' + res.data.pic + '" style="max-width:100%;" />');
                    //         }else{
                    //             $('#pic_view').html('<img src="{{ asset("assets/images/default.jpg") }}" style="max-width:100%;" />');
                    //         }

                    //     }
                });
            });

            gold_stock(1);

            $('#province_id_add').change(function (e) {
                e.preventDefault();
                district();

                $('#subdistrict_id_add').empty();
                $('#zipcode_add').val('');
            });

            $('#district_id_add').change(function (e) {
                e.preventDefault();
                subdistrict();

                $('#zipcode_add').val('');
            });

            $('#subdistrict_id_add').change(function (e) {
                e.preventDefault();
                zipcode();
            });

            $('#confirmBuyGold').click(function (e) {
                // $('#confirmBuyGold').empty();
                // $('#confirmBuyGold').attr("disabled", true);
                // $('#confirmBuyGold').append("<div class=\"spinner-border spinner-grow-sm text-light\" role=\"status\"><span class=\"sr-only\">Loading...</span></div> กำลังบันทึกข้อมูล");

                removeValidationBuy('add');

                e.preventDefault();
                var form = $('#form_gold_buy')[0];
                var data = new FormData(form);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{url('/receivebuy/buy-gold-store')}}',
                    type: "POST",
                    enctype: 'multipart/form-data',
                    dataType: 'JSON',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (res) {

                        if(res.error == 0){
                            $('#preview_gold_buy_add').append("<div class=\"alert alert-success\" role=\"alert\">\n" +
                                "<h4 class=\"alert-heading\">เพิ่มข้อมูลเรียบร้อย</h4>\n" +
                                "</div>");
                            setTimeout(function(){
                                window.location.replace('{{ url('/receivebuy/buy-summary') }}' + '/' + res.data.id);
                                // $('#modal_gold_add').modal('hide');
                                // $('#table').dataTable().fnClearTable();
                                // $('#table').dataTable().fnDestroy();
                                // listUser();
                            }, 1000);
                        }else{
                            $('#preview_gold_buy_add').append("<div class=\"alert alert-danger\" role=\"alert\">\n" +
                                "<h4 class=\"alert-heading\">พบข้อผิดพลาด</h4>\n" +
                                "</div>");
                            $.each(res.messages, function (k,v) {
                                $('#'+ k + '_error_add').html(v);
                            });
                        }

                        $('#confirmBuyGold').empty();
                        $('#confirmBuyGold').append("บันทึก");
                        $('#confirmBuyGold').attr("disabled",false);
                    }
                });
            });
        });

        function readURL(input, hidden) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    console.log(e.target.result);
                    $('#' + hidden).val(e.target.result);
                    // $('#blah').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        function getBuyerData(id) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('member-show') }}/' + id,
                type: "GET",
                enctype: 'multipart/form-data',
                dataType: 'JSON',
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    $('#btn_buyer_add').val(res.data.id);
                    $('#id_card_add').val(res.data.id_card);
                    $('#phone_add').val(res.data.phone);
                    res.data.title ? $('#title_name_add').val(res.data.title.id) : '';
                    $('#name_add').val(res.data.name);
                    $('#surname_add').val(res.data.surname);
                    $('#address_add').val(res.data.address);
                    res.data.province ? $('#province_id_add').val(res.data.province.id) : '';
                    district('add');
                    res.data.district ? $('#district_id_add').val(res.data.district.id) : '';
                    subdistrict('add');
                    res.data.subdistrict ? $('#subdistrict_id_add').val(res.data.subdistrict.id) : '';
                    zipcode('add');
                    // $('#zipcode_add').val(res.data.zipcode);
                    $('#email_add').val(res.data.email);
                    $('#line_add').val(res.data.line);
                    $('#facebook_add').val(res.data.facebook);
                    res.data.title_en ? $('#title_name_en_add').val(res.data.title_en.id) : '';
                    $('#name_en_add').val(res.data.name_en);
                    $('#surname_en_add').val(res.data.surname_en);

                    if(res.data.birthday) {
                        let pos = res.data.birthday.indexOf(" ");
                        let str = res.data.birthday.substr(0, pos);
                        let [year, month, day] = str.split("-")
                        $("#birthday_add").flatpickr({
                            defaultDate: new Date(year + '-' + month + '-' + day),
                        });
                    }

                    res.data.gender ? $('#gender_add').val(res.data.gender.id) : '';

                    if(res.data.issue) {
                        pos = res.data.issue.indexOf(" ");
                        str = res.data.issue.substr(0, pos);
                        [year, month, day] = str.split("-")
                        $("#issue_add").flatpickr({
                            defaultDate: new Date(year + '-' + month + '-' + day),
                        });
                    }

                    if(res.data.expire) {
                        pos = res.data.expire.indexOf(" ");
                        str = res.data.expire.substr(0, pos);
                        [year, month, day] = str.split("-")
                        $("#expire_add").flatpickr({
                            defaultDate: new Date(year + '-' + month + '-' + day),
                        });
                    }
                }
            });
        }

        function getBuyerDataOld() {
            const id = $('#id_card_add').val();
            const phone = $('#phone_add').val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('member-show-old?id=') }}' + id + '&phone=' + phone,
                type: "GET",
                enctype: 'multipart/form-data',
                dataType: 'JSON',
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    if(res.data) {
                        $('#btn_buyer_add').val(res.data.id);
                        $('#id_card_add').val(res.data.id_card);
                        $('#phone_add').val(res.data.phone);
                        res.data.title ? $('#title_name_add').val(res.data.title.id) : '';
                        $('#name_add').val(res.data.name);
                        $('#surname_add').val(res.data.surname);
                        $('#address_add').val(res.data.address);
                        res.data.province ? $('#province_id_add').val(res.data.province.id) : '';
                        district('add');
                        res.data.district ? $('#district_id_add').val(res.data.district.id) : '';
                        subdistrict('add');
                        res.data.subdistrict ? $('#subdistrict_id_add').val(res.data.subdistrict.id) : '';
                        zipcode('add');
                        // $('#zipcode_add').val(res.data.zipcode);
                        $('#email_add').val(res.data.email);
                        $('#line_add').val(res.data.line);
                        $('#facebook_add').val(res.data.facebook);
                        res.data.title_en ? $('#title_name_en_add').val(res.data.title_en.id) : '';
                        $('#name_en_add').val(res.data.name_en);
                        $('#surname_en_add').val(res.data.surname_en);

                        if (res.data.birthday) {
                            let pos = res.data.birthday.indexOf(" ");
                            let str = res.data.birthday.substr(0, pos);
                            let [year, month, day] = str.split("-")
                            $("#birthday_add").flatpickr({
                                defaultDate: new Date(year + '-' + month + '-' + day),
                            });
                        }

                        res.data.gender ? $('#gender_add').val(res.data.gender.id) : '';

                        if (res.data.issue) {
                            pos = res.data.issue.indexOf(" ");
                            str = res.data.issue.substr(0, pos);
                            [year, month, day] = str.split("-")
                            $("#issue_add").flatpickr({
                                defaultDate: new Date(year + '-' + month + '-' + day),
                            });
                        }

                        if (res.data.expire) {
                            pos = res.data.expire.indexOf(" ");
                            str = res.data.expire.substr(0, pos);
                            [year, month, day] = str.split("-")
                            $("#expire_add").flatpickr({
                                defaultDate: new Date(year + '-' + month + '-' + day),
                            });
                        }
                    }
                }
            });
        }

        function dummy(){
            @if($dummy)
{{--                $('#buyerModal').modal('hide');--}}

{{--                $('#buyer_add_input').css('display', 'none');--}}
{{--                $('#buyer_profile').css('display', 'block');--}}

{{--                $('#buyer_name').html('{{ $dummy->name. ' '. $dummy->surname }}');--}}
{{--                $('#buyer_address').html('{{ $dummy->address. ' '. $dummy->subdistrict->title_th. ' '. $dummy->district->title_th. ' '. $dummy->province->title_th. ' '. $dummy->subdistrict->districtCode->zipcode }}');--}}
{{--                $('#buyer_phone').html('{{ $dummy->phone }}');--}}
{{--                $('#buyer_id').val({{ $dummy->id }});--}}

                getBuyerData({{ $dummy->id }});
            @endif
        }

        function removeProductList(i){
            $('#gold_buy_list_' + i).remove();
            calculateTotal();
        }

        function gold_category_form(el) {
            removeValidation('add');
            resetForm('add');

            switch (el.value) {
                case '1':
                    $('#gold_type_input').css('display', 'block');
                    $('#gold_weight_input').css('display', 'block');
                    gold_sub_category(1);
                    break;
                case '2':
                    $('#gold_type_input').css('display', 'block');
                    $('#gold_weight_input').css('display', 'none');
                    gold_sub_category(1);
                    break;
                case '3':
                    $('#gold_type_input').css('display', 'block');
                    $('#gold_weight_input').css('display', 'none');
                    gold_sub_category(1);
                    break;
                case '4':
                    $('#gold_type_input').css('display', 'none');
                    $('#gold_weight_input').css('display', 'none');
                    gold_sub_category(4);
                    break;
                default:
                    break;
            }
        }

        function gold_type_form(el) {
            removeValidation('add');

            switch (el.value) {
                case '1':
                    $('#gold_sub_category_input').css('display', 'block');
                    $('#gold_size_input').css('display', 'block');
                    $('#gold_list_input').css('display', 'block');
                    $('#gold_amount_input').css('display', 'none');
                    break;
                case '2':
                    $('#gold_sub_category_input').css('display', 'none');
                    $('#gold_size_input').css('display', 'none');
                    $('#gold_list_input').css('display', 'none');
                    $('#gold_amount_input').css('display', 'block');
                    break;
                default:
                    break;
            }
        }

        function gold_sub_category(type) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('gold-sub-category?type=') }}' + type,
                type: "GET",
                enctype: 'multipart/form-data',
                dataType: 'JSON',
                async: false,
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    $('#gold_sub_category').empty();
                    $.each(res.data, function (index, sub_category) {
                        $('#gold_sub_category').append('<option value="' + sub_category.id + '">' + sub_category.title + '</option>');
                    });
                }
            });
        }

        function removeValidation(status) {
            $('#preview_gold_' + status).empty();
            $('#gold_pattern_' + status).removeClass('is-invalid');
            $('#gold_sku_' + status).removeClass('is-invalid');
            $('#gold_weight_' + status).removeClass('is-invalid');
            $('#gold_weight_actual_' + status).removeClass('is-invalid');
            $('#gold_cost_' + status).removeClass('is-invalid');
            $('#gold_smith_charge_' + status).removeClass('is-invalid');
            $('#gold_image_' + status).removeClass('is-invalid');
            $('#gold_list_' + status).removeClass('is-invalid');
            $('#gold_amount_' + status).removeClass('is-invalid');
            $('#gold_size_' + status).removeClass('is-invalid');

            $('#gold_pattern_error_' + status).html("");
            $('#gold_sku_error_' + status).html("");
            $('#gold_weight_error_' + status).html("");
            $('#gold_weight_actual_error_' + status).html("");
            $('#gold_cost_error_' + status).html("");
            $('#gold_smith_charge_error_' + status).html("");
            $('#gold_image_error_' + status).html("");
            $('#gold_list_error_' + status).html("");
            $('#gold_amount_error_' + status).html("");
            $('#gold_size_error_' + status).html("");
        }

        function removeValidationBuyer(status) {
            $('#preview_buyer_' + status).empty();
            $('#phone_' + status).removeClass('is-invalid');
            $('#name_' + status).removeClass('is-invalid');

            $('#phone_error_' + status).html("");
            $('#name_error_' + status).html("");
        }

        function removeValidationBuy(status) {
            $('#preview_gold_buy_' + status).empty();
            $('#product_error_' + status).html("");
            $('#buyer_error_' + status).html("");
        }

        function resetForm(status) {
            $('#gold_type_ornament_' + status).prop("checked", true);
            gold_type_form({ value: '1' });
        }

        function gold_stock(page) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('gold-stock?page=') }}' + page,
                type: "GET",
                enctype: 'multipart/form-data',
                dataType: 'JSON',
                async: false,
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    $('#gold_list_left').empty();
                    $('#gold_list_right').empty();

                    $.each(res.data.data, function (index, item) {
                        let column = ((index % 2) === 0 ? '#gold_list_left' : '#gold_list_right');

                        if(item.category_id === 1 && item.gold_type === 1) {
                            $(column).append('<div class="card ecommerce-card" style="grid-template-columns: 1fr 3fr 2fr;">\n' +
                                '                            <div class="item-img ml-2">\n' +
                                '                                <img src="{{ asset('_uploads/_products') }}/' + item.pic + '" alt="img-placeholder" style="width: 160px;" />\n' +
                                '                            </div>\n' +
                                '                            <div class="card-body">\n' +
                                '                                <div class="item-name">\n' +
                                '                                    <h4 class="mb-0 text-body bold">' + item.title + '</h4>\n' +
                                '                                    <span class="item-company text-primary">' + item.subcategory.title + '</span>\n' +
                                '                                    <div class="badge badge-pill badge-warning">' + item.category.title + '</div>\n' +
                                '                                </div>\n' +
                                '                                <span class="item-company">หนัก: ' + item.weight + ' ' + item.units.title + '</span>\n' +
                                '                                <span class="item-company">ขนาด/ความยาว: ' + item.size + '</span>\n' +
                                '                            </div>\n' +
                                '                            <div class="item-options text-center">\n' +
                                '                                <div class="item-wrapper">\n' +
                                '                                    <div class="item-cost">\n' +
                                '                                        <h4 class="item-price">฿ ' + item.price.income + '</h4>\n' +
                                '                                        <p class="card-text shipping">\n' +
                                '                                            <span class="badge badge-pill badge-light-success">คงเหลือ ' + item.stock.count + ' ชิ้น</span>\n' +
                                '                                        </p>\n' +
                                '                                    </div>\n' +
                                '                                </div>\n' +
                                '                                <div class="demo-inline-spacing">\n' +
                                '                                    <button type="button" class="btn btn-icon btn-warning btn-sm" data-toggle="modal" data-target="#edit_product" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="edit-2"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-info btn-sm" data-toggle="modal" data-id="' + item.id + '" data-target="#modal_view" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="eye"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-danger btn-sm" data-toggle="modal" data-target="#delete_product" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="trash-2"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-secondary btn-sm" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="printer"></i>\n' +
                                '                                    </button>\n' +
                                '                                </div>\n' +
                                '                            </div>\n' +
                                '                        </div>');
                        }
                        else if(item.category_id === 1 && item.gold_type === 2) {
                            $(column).append('<div class="card ecommerce-card" style="grid-template-columns: 1fr 3fr 2fr;">\n' +
                                '                            <div class="item-img ml-2">\n' +
                                '                                <img src="{{ asset('_uploads/_products') }}/' + item.pic + '" alt="img-placeholder" style="width: 160px;" />\n' +
                                '                            </div>\n' +
                                '                            <div class="card-body">\n' +
                                '                                <div class="item-name">\n' +
                                '                                    <h4 class="mb-0 text-body bold">' + item.title + '</h4>\n' +
                                '                                    <span class="item-company text-primary">ทองแท่ง</span>\n' +
                                '                                    <div class="badge badge-pill badge-warning">' + item.category.title + '</div>\n' +
                                '                                </div>\n' +
                                '                                <span class="item-company">หนัก: ' + item.weight_real + ' กรัม(g)</span>\n' +
                                '                                <span class="item-company">ขนาด/ความยาว: -</span>\n' +
                                '                            </div>\n' +
                                '                            <div class="item-options text-center">\n' +
                                '                                <div class="item-wrapper">\n' +
                                '                                    <div class="item-cost">\n' +
                                '                                        <h4 class="item-price">฿ ' + item.price.income + '</h4>\n' +
                                '                                        <p class="card-text shipping">\n' +
                                '                                            <span class="badge badge-pill badge-light-success">คงเหลือ ' + item.stock.count + ' ชิ้น</span>\n' +
                                '                                        </p>\n' +
                                '                                    </div>\n' +
                                '                                </div>\n' +
                                '                                <div class="demo-inline-spacing">\n' +
                                '                                    <button type="button" class="btn btn-icon btn-warning btn-sm" data-toggle="modal" data-target="#edit_product" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="edit-2"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-info btn-sm" data-toggle="modal" data-id="' + item.id + '" data-target="#modal_view" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="eye"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-danger btn-sm" data-toggle="modal" data-target="#delete_product" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="trash-2"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-secondary btn-sm" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="printer"></i>\n' +
                                '                                    </button>\n' +
                                '                                </div>\n' +
                                '                            </div>\n' +
                                '                        </div>');
                        }
                        else if((item.category_id === 2 || item.category_id === 3) && item.gold_type === 1) {
                            $(column).append('<div class="card ecommerce-card" style="grid-template-columns: 1fr 3fr 2fr;">\n' +
                                '                            <div class="item-img ml-2">\n' +
                                '                                <img src="{{ asset('_uploads/_products') }}/' + item.pic + '" alt="img-placeholder" style="width: 160px;" />\n' +
                                '                            </div>\n' +
                                '                            <div class="card-body">\n' +
                                '                                <div class="item-name">\n' +
                                '                                    <h4 class="mb-0 text-body bold">' + item.title + '</h4>\n' +
                                '                                    <span class="item-company text-primary">' + item.subcategory.title + '</span>\n' +
                                '                                    <div class="badge badge-pill badge-warning">' + item.category.title + '</div>\n' +
                                '                                </div>\n' +
                                '                                <span class="item-company">หนัก: ' + item.weight_real + ' กรัม(g)</span>\n' +
                                '                                <span class="item-company">ขนาด/ความยาว: ' + item.size + '</span>\n' +
                                '                            </div>\n' +
                                '                            <div class="item-options text-center">\n' +
                                '                                <div class="item-wrapper">\n' +
                                '                                    <div class="item-cost">\n' +
                                '                                        <h4 class="item-price">฿ ' + item.price.income + '</h4>\n' +
                                '                                        <p class="card-text shipping">\n' +
                                '                                            <span class="badge badge-pill badge-light-success">คงเหลือ ' + item.stock.count + ' ชิ้น</span>\n' +
                                '                                        </p>\n' +
                                '                                    </div>\n' +
                                '                                </div>\n' +
                                '                                <div class="demo-inline-spacing">\n' +
                                '                                    <button type="button" class="btn btn-icon btn-warning btn-sm" data-toggle="modal" data-target="#edit_product" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="edit-2"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-info btn-sm" data-toggle="modal" data-id="' + item.id + '" data-target="#modal_view" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="eye"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-danger btn-sm" data-toggle="modal" data-target="#delete_product" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="trash-2"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-secondary btn-sm" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="printer"></i>\n' +
                                '                                    </button>\n' +
                                '                                </div>\n' +
                                '                            </div>\n' +
                                '                        </div>');
                        }
                        else if((item.category_id === 2 || item.category_id === 3) && item.gold_type === 2) {
                            $(column).append('<div class="card ecommerce-card" style="grid-template-columns: 1fr 3fr 2fr;">\n' +
                                '                            <div class="item-img ml-2">\n' +
                                '                                <img src="{{ asset('_uploads/_products') }}/' + item.pic + '" alt="img-placeholder" style="width: 160px;" />\n' +
                                '                            </div>\n' +
                                '                            <div class="card-body">\n' +
                                '                                <div class="item-name">\n' +
                                '                                    <h4 class="mb-0 text-body bold">' + item.title + '</h4>\n' +
                                '                                    <span class="item-company text-primary">ทองแท่ง</span>\n' +
                                '                                    <div class="badge badge-pill badge-warning">' + item.category.title + '</div>\n' +
                                '                                </div>\n' +
                                '                                <span class="item-company">หนัก: ' + item.weight_real + ' กรัม(g)</span>\n' +
                                '                                <span class="item-company">ขนาด/ความยาว: -</span>\n' +
                                '                            </div>\n' +
                                '                            <div class="item-options text-center">\n' +
                                '                                <div class="item-wrapper">\n' +
                                '                                    <div class="item-cost">\n' +
                                '                                        <h4 class="item-price">฿ ' + item.price.income + '</h4>\n' +
                                '                                        <p class="card-text shipping">\n' +
                                '                                            <span class="badge badge-pill badge-light-success">คงเหลือ ' + item.stock.count + ' ชิ้น</span>\n' +
                                '                                        </p>\n' +
                                '                                    </div>\n' +
                                '                                </div>\n' +
                                '                                <div class="demo-inline-spacing">\n' +
                                '                                    <button type="button" class="btn btn-icon btn-warning btn-sm" data-toggle="modal" data-target="#edit_product" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="edit-2"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-info btn-sm" data-toggle="modal" data-id="' + item.id + '" data-target="#modal_view" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="eye"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-danger btn-sm" data-toggle="modal" data-target="#delete_product" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="trash-2"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-secondary btn-sm" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="printer"></i>\n' +
                                '                                    </button>\n' +
                                '                                </div>\n' +
                                '                            </div>\n' +
                                '                        </div>');
                        }
                        else if(item.category_id === 4) {
                            $(column).append('<div class="card ecommerce-card" style="grid-template-columns: 1fr 3fr 2fr;">\n' +
                                '                            <div class="item-img ml-2">\n' +
                                '                                <img src="{{ asset('_uploads/_products') }}/' + item.pic + '" alt="img-placeholder" style="width: 160px;" />\n' +
                                '                            </div>\n' +
                                '                            <div class="card-body">\n' +
                                '                                <div class="item-name">\n' +
                                '                                    <h4 class="mb-0 text-body bold">' + item.title + '</h4>\n' +
                                '                                    <span class="item-company text-primary">' + item.subcategory.title + '</span>\n' +
                                '                                    <div class="badge badge-pill badge-warning">อื่นๆ</div>\n' +
                                '                                </div>\n' +
                                '                                <span class="item-company">หนัก: ' + item.weight_real + ' กรัม(g)</span>\n' +
                                '                                <span class="item-company">ขนาด/ความยาว: ' + item.size + '</span>\n' +
                                '                            </div>\n' +
                                '                            <div class="item-options text-center">\n' +
                                '                                <div class="item-wrapper">\n' +
                                '                                    <div class="item-cost">\n' +
                                '                                        <h4 class="item-price">฿ ' + item.price.income + '</h4>\n' +
                                '                                        <p class="card-text shipping">\n' +
                                '                                            <span class="badge badge-pill badge-light-success">คงเหลือ ' + item.stock.count + ' ชิ้น</span>\n' +
                                '                                        </p>\n' +
                                '                                    </div>\n' +
                                '                                </div>\n' +
                                '                                <div class="demo-inline-spacing">\n' +
                                '                                    <button type="button" class="btn btn-icon btn-warning btn-sm" data-toggle="modal" data-target="#edit_product" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="edit-2"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-info btn-sm" data-toggle="modal" data-id="' + item.id + '" data-target="#modal_view" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="eye"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-danger btn-sm" data-toggle="modal" data-target="#delete_product" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="trash-2"></i>\n' +
                                '                                    </button>\n' +
                                '                                    <button type="button" class="btn btn-icon btn-secondary btn-sm" style="margin-right: 2px;">\n' +
                                '                                        <i data-feather="printer"></i>\n' +
                                '                                    </button>\n' +
                                '                                </div>\n' +
                                '                            </div>\n' +
                                '                        </div>');
                        }
                    });

                    gold_stock_pagination(res.data);
                    feather.replace();
                }
            });
        }

        function gold_stock_pagination(p) {
            let first_page = p.first_page_url.replace(/.+page=/, '');
            let last_page = p.last_page_url.replace(/.+page=/, '');

            $('#pagination').empty();

            if(p.last_page > 1) {
                $('#pagination').append('<li class="page-item first">' +
                    '<a href="javascript:gold_stock(' + first_page + ');" class="page-link">First</a>' +
                    '</li>');
            }

            if(p.prev_page_url !== null) {
                let prev_page = p.prev_page_url.replace(/.+page=/, '');
                $('#pagination').append('<li class="page-item prev">' +
                    '<a href="javascript:gold_stock(' + prev_page + ');" class="page-link">Prev</a>' +
                    '</li>');
            }

            $.each(p.links, function (index, link) {
                if(link.label.indexOf('Previous') === -1 && link.label.indexOf('Next ')) {
                    let page_link = link.url.replace(/.+page=/, '');
                    $('#pagination').append('<li class="page-item' + (link.active ? ' active' : '') + '">' +
                        '<a href="javascript:gold_stock(' + page_link + ');" class="page-link">' + page_link + '</a>' +
                        '</li>');
                }
            });

            if(p.next_page_url !== null) {
                let next_page = p.next_page_url.replace(/.+page=/, '');
                $('#pagination').append('<li class="page-item next">' +
                    '<a href="javascript:gold_stock(' + next_page + ');" class="page-link">Next</a>' +
                    '</li>');
            }

            if(p.last_page > 1) {
                $('#pagination').append('<li class="page-item first">' +
                    '<a href="javascript:gold_stock(' + last_page + ');" class="page-link">Last</a>' +
                    '</li>');
            }
        }

        function calculateTotal() {
            $('#total_price_text').html("");
            $('#total_price').val("");
            $('#pay_text').html("");
            $('#pay').val("");
            var sum = 0;
            $('.price-list-hidden').each(function() {
                var num = $(this).val();
                if(num !== 0) {
                    sum += parseFloat(num);
                }

                // if(sum >= 100000){
                //     $('#fordis').addClass('disabled');
                // }else {
                //     $('#fordis').removeClass('disabled');
                // }

            });
            // alert(sum);
            if(sum > 0) {
                $('#total_price_text').html(numeral(sum).format('0,0.00') + ' บาท');
                $('#total_price').val(sum);
                $('#pay_text').html(numeral(sum).format('0,0.00') + ' บาท');
                $('#pay').val(sum);

                // if(sum >= 100000){
                //     $('#fordis').addClass('disabled');
                // }else {
                //     $('#fordis').removeClass('disabled');
                // }
            }
        }

        function district() {
            var province_id = $('#province_id_add').val();
            /*$.get('{{ url('location/district?province_id=') }}' + province_id, function (res) {
                $('#district_id').empty();
                $('#district_id').append('<option value="">กรุณาเลือกอำเภอ/เขต</option>');
                $.each(res.data, function (index, district) {
                    $('#district_id').append('<option value="' + district.id + '">' + district.title_th + '</option>');
                });
            });*/

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('location/district?province_id=') }}' + province_id,
                type: "GET",
                enctype: 'multipart/form-data',
                dataType: 'JSON',
                async: false,
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    $('#district_id_add').empty();
                    $('#district_id_add').append('<option value="">กรุณาเลือกอำเภอ/เขต</option>');
                    $.each(res.data, function (index, district) {
                        $('#district_id_add').append('<option value="' + district.id + '">' + district.title_th + '</option>');
                    });
                }
            });
        }

        function subdistrict() {
            var district_id = $('#district_id_add').val();
            /*$.get('{{ url('location/subdistrict?district_id=') }}' + district_id, function (res) {
                $('#subdistrict_id').empty();
                $('#subdistrict_id').append('<option value="">กรุณาเลือกตำบล/แขวง</option>');
                $.each(res.data, function (index, subdistrict) {
                    $('#subdistrict_id').append('<option value="' + subdistrict.id + '">' + subdistrict.title_th + '</option>');
                });
            });*/

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('location/subdistrict?district_id=') }}' + district_id,
                type: "GET",
                enctype: 'multipart/form-data',
                dataType: 'JSON',
                async: false,
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    $('#subdistrict_id_add').empty();
                    $('#subdistrict_id_add').append('<option value="">กรุณาเลือกตำบล/แขวง</option>');
                    $.each(res.data, function (index, subdistrict) {
                        $('#subdistrict_id_add').append('<option value="' + subdistrict.id + '">' + subdistrict.title_th + '</option>');
                    });
                }
            });
        }

        function zipcode() {
            var subdistrict_id = $('#subdistrict_id_add').val();
            /*$.get('{{ url('location/zipcode?subdistrict_id=') }}' + subdistrict_id, function (res) {
                $('#zipcode').val(res.data.zipcode);
            });*/

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('location/zipcode?subdistrict_id=') }}' + subdistrict_id,
                type: "GET",
                enctype: 'multipart/form-data',
                dataType: 'JSON',
                // async: false,
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    $('#zipcode_add').val(res.data.zipcode);
                }
            });
        }

        function getBankList(value) {
            if(value == 2) {
                $.get('{{ url('/bank-list') }}', function (res) {
                    var line ="<label for=\"bank\" class=\"col-sm-5 col-form-label col-form-label-lg\">ธนาคาร</label>";
                    line += "<div class=\"col-sm-7\"><select id=\"bank\" name=\"bank\" class=\"form-control\">";
                    $.each(res.data, function (index, bank) {
                        line += "<option value=\"" + bank.id + "\">" + bank.title + "</option>";
                    });
                    line +="</select></div>";
                    $('#bank_list').append(line);
                });
            }
            else {
                $('#bank_list').empty();
            }
        }
    </script>
@endsection
