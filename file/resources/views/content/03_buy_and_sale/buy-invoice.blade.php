@extends('layouts/contentLayoutMasterMain')

@section('title', 'สรุปรายการรับซื้อทอง')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('vendors/css/pickers/flatpickr/flatpickr.min.css')}}">
@endsection
@section('page-style')
<link rel="stylesheet" href="{{asset('css/base/plugins/forms/pickers/form-flat-pickr.css')}}">
<link rel="stylesheet" href="{{asset('css/base/pages/app-invoice.css')}}">
@endsection

@section('content')
<section class="invoice-preview-wrapper">
  <div class="row invoice-preview">
    <!-- Invoice -->
    <div class="col-xl-9 col-md-8 col-12">
      <div class="card invoice-preview-card">
        <div class="card-body invoice-padding pb-0">
          <!-- Header starts -->
          <div class="d-flex justify-content-between flex-md-row flex-column invoice-spacing mt-0">
            <div>
              <div class="logo-wrapper">
                  <img class="img-fluid" src="{{asset('images/logo_sesame.svg')}}" />
              </div>
              <p class="card-text mb-25">{{ Auth::user()->company->address }}</p>
              <p class="card-text mb-25">ตำบล{{ Auth::user()->company->subdistrict->title_th }} อำเภอ{{ Auth::user()->company->district->title_th }} จังหวัด{{ Auth::user()->company->province->title_th }} {{ Auth::user()->company->subdistrict->districtCode->zipcode }}</p>
              <p class="card-text mb-0">โทร. {{ Auth::user()->company->telephone }}</p>
            </div>
            <div class="mt-md-0 mt-2">
              <h4 class="invoice-title">
                เลขที่
                <span class="invoice-number">#{{ $transection->transection_id }}</span>
              </h4>
              <div class="invoice-date-wrapper">
                <p class="invoice-date-title">วันที่ออก:</p>
                <p class="invoice-date">{{ $transectionDate }}</p>
              </div>
              <div class="invoice-date-wrapper">
                <p class="invoice-date-title">วันที่ชำระเงิน:</p>
                <p class="invoice-date">{{ $transectionDate }}</p>
              </div>
            </div>
          </div>
          <!-- Header ends -->
        </div>

        <hr class="invoice-spacing" />

        <!-- Address and Contact starts -->
        <div class="card-body invoice-padding pt-0">
          <div class="row invoice-spacing">
            <div class="col-xl-8 p-0">
              <h4 class="mb-2"><strong>ข้อมูลลูกค้า</strong></h4>
                <p class="card-text mb-25"><strong>คุณ {{ $transection->customer->name }} {{ $transection->customer->surname }}</strong></p>
              <p class="card-text mb-25">{{ $transection->customer->address }}</p>
              <p class="card-text mb-25">ต.{{ $transection->customer->subdistrict->title_th }} อ.{{ $transection->customer->district->title_th }}</p>
              <p class="card-text mb-25">จ.{{ $transection->customer->province->title_th }} {{ $transection->customer->subdistrict->districtCode->zipcode }}</p>
              <p class="card-text mb-0">โทร. {{ $transection->customer->phone }}</p>
            </div>
            <div class="col-xl-4 p-0 mt-xl-0 mt-2">
              <h4 class="mb-2"><strong>ข้อมูลการชำระเงิน</strong></h4>
              <table>
                <tbody>
                  <tr>
                    <td class="pr-1">ยอดรวม:</td>
                    <td><span class="font-weight-bold">{{ number_format($transection->total, 2) }}</span></td>
                  </tr>
                  <tr>
                    <td class="pr-1">ส่วนลด:</td>
                    <td>-</td>
                  </tr>
                  <tr>
                    <td class="pr-1">ยอดสุทธิ:</td>
                    <td>{{ number_format($transection->total, 2) }}</td>
                  </tr>
                  <tr>
                    <td class="pr-1">จ่ายเงิน:</td>
                    <td>{{ number_format($transection->total, 2) }}</td>
                  </tr>
                  <tr>
                    <td class="pr-1">เงินทอน:</td>
                    <td>-</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- Address and Contact ends -->

        <!-- Invoice Description starts -->
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th class="py-1" style="font-size: 16px;">รายการทอง</th>
                <th class="py-1" style="font-size: 16px;">จำนวน</th>
                <th class="py-1" style="font-size: 16px;">น้ำหนัก</th>
                <th class="py-1" style="font-size: 16px;">รวม</th>
              </tr>
            </thead>
            <tbody>
                @foreach($transection->goldbuy as $item)
                  <tr class="border-bottom">
                    <td class="py-1">
                      <p class="card-text font-weight-bold mb-25">{{ $item->title }}</p>
                      <p class="card-text text-nowrap d-inline">
                        @if($item->goldType->id == 1)
                            {{ $item->goldSubCategory->title }}
                        @else
                            {{ $item->goldType->title }}
                        @endif
                        <div class="badge badge-pill badge-warning">{{ $item->goldPercent->title }}</div>
                      </p>
                    </td>
                    <td class="py-1">
                      <span class="font-weight-bold">1 ชิ้น</span>
                    </td>
                    <td class="py-1">
                      <span class="font-weight-bold">{{ $item->weight_real }} กรัม (g)</span>
                    </td>
                    <td class="py-1">
                      <span class="font-weight-bold">{{ number_format($item->price, 2) }}</span>
                    </td>
                  </tr>
                @endforeach
{{--              <tr class="border-bottom">--}}
{{--                <td class="py-1">--}}
{{--                  <p class="card-text font-weight-bold mb-25">สปริงหลุยส์</p>--}}
{{--                  <p class="card-text text-nowrap">สร้อยคอ 96.5%</p>--}}
{{--                </td>--}}
{{--                <td class="py-1">--}}
{{--                  <span class="font-weight-bold">1 ชิ้น</span>--}}
{{--                </td>--}}
{{--                <td class="py-1">--}}
{{--                  <span class="font-weight-bold">2 บาท</span>--}}
{{--                </td>--}}
{{--                <td class="py-1">--}}
{{--                  <span class="font-weight-bold">58,000</span>--}}
{{--                </td>--}}
{{--              </tr>--}}
{{--              <tr class="border-bottom">--}}
{{--                  <td class="py-1">--}}
{{--                      <p class="card-text font-weight-bold mb-25">ดอกไม้ฝังพลอย์</p>--}}
{{--                      <p class="card-text text-nowrap">แหวน 96.5%</p>--}}
{{--                  </td>--}}
{{--                  <td class="py-1">--}}
{{--                      <span class="font-weight-bold">1 ชิ้น</span>--}}
{{--                  </td>--}}
{{--                  <td class="py-1">--}}
{{--                      <span class="font-weight-bold">1 บาท</span>--}}
{{--                  </td>--}}
{{--                  <td class="py-1">--}}
{{--                      <span class="font-weight-bold">25,000</span>--}}
{{--                  </td>--}}
{{--              </tr>--}}
            </tbody>
          </table>
        </div>

        <div class="card-body invoice-padding pb-0">
          <div class="row invoice-sales-total-wrapper">
            <div class="col-md-6 order-md-1 order-2 mt-md-0 mt-3">
              <p class="card-text mb-0">
                <span class="font-weight-bold">พนักงานรับซื้อ:</span> <span class="ml-75">{{ $transection->userAdd->name }} {{ $transection->userAdd->surname }}</span>
              </p>
            </div>
            <div class="col-md-6 d-flex justify-content-end order-md-2 order-1">
              <div class="invoice-total-wrapper" style="max-width: 300px;">
                <div class="invoice-total-item">
                  <p class="invoice-total-title">ยอดรวม:</p>
                  <p class="invoice-total-amount">{{ number_format($transection->total, 2) }}</p>
                </div>
                <div class="invoice-total-item">
                  <p class="invoice-total-title">ส่วนลด:</p>
                  <p class="invoice-total-amount">-</p>
                </div>
                <div class="invoice-total-item">
                  <p class="invoice-total-title">ภาษี(7%):</p>
                  <p class="invoice-total-amount">-</p>
                </div>
                <hr class="my-50" />
                <div class="invoice-total-item">
                  <p class="invoice-total-title">ยอดรวมสุทธิ:</p>
                  <p class="invoice-total-amount">{{ number_format($transection->total, 2) }}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Invoice Description ends -->

        <hr class="invoice-spacing" />

        <!-- Invoice Note starts -->
        <div class="card-body invoice-padding pt-0">
          <div class="row">
            <div class="col-12">
              <span class="font-weight-bold">Note:</span>
              <span>โปรดรักษาเอกสารฉบับนี้สำหรับเป็นหลักฐานการชำระเงิน</span>
            </div>
          </div>
        </div>
        <!-- Invoice Note ends -->
      </div>
    </div>
    <!-- /Invoice -->

    <!-- Invoice Actions -->
    <div class="col-xl-3 col-md-4 col-12 invoice-actions mt-md-0 mt-2">
      <div class="card">
        <div class="card-body">
          <button class="btn btn-primary btn-block mb-75" data-toggle="modal" data-target="#print-preview">
            พิมพ์
          </button>
          <button class="btn btn-outline-secondary btn-block btn-download-invoice mb-75">บันทึก</button>
          <a class="btn btn-success btn-block" href="{{ url('/') }}">
            ปิด
          </a>
        </div>
      </div>
    </div>
    <!-- /Invoice Actions -->
  </div>
</section>

<!-- Modal -->
<div
    class="modal fade text-left"
    id="print-preview"
    tabindex="-1"
    role="dialog"
    aria-labelledby="myModalLabel17"
    aria-hidden="true"
>
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
{{--                <h4 class="modal-title" id="myModalLabel17">ตัวอย่างก่อนพิมพ์ใบเสร็จรับซื้อทอง</h4>--}}
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <iframe id="print_slip" name="print_slip" src="{{ url('/receivebuy/buy-bill/'. $transection->id) }}" frameborder="0" style="width: 100%; height: 80vh" frameborder="yes" scrolling="yes"></iframe>
            </div>
            <div class="modal-footer">
{{--                <button type="button" class="btn btn-primary" data-dismiss="modal">Accept</button>--}}
                <button type="button" class="btn btn-primary" onclick="printSlip()">พิมพ์</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
            </div>
        </div>
    </div>
</div>
</div>

<!-- Send Invoice Sidebar -->
{{--<div class="modal modal-slide-in fade" id="send-invoice-sidebar" aria-hidden="true">--}}
{{--  <div class="modal-dialog sidebar-lg">--}}
{{--    <div class="modal-content p-0">--}}
{{--      <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>--}}
{{--      <div class="modal-header mb-1">--}}
{{--        <h5 class="modal-title">--}}
{{--          <span class="align-middle">Send Invoice</span>--}}
{{--        </h5>--}}
{{--      </div>--}}
{{--      <div class="modal-body flex-grow-1">--}}
{{--        <form>--}}
{{--          <div class="form-group">--}}
{{--            <label for="invoice-from" class="form-label">From</label>--}}
{{--            <input--}}
{{--              type="text"--}}
{{--              class="form-control"--}}
{{--              id="invoice-from"--}}
{{--              value="shelbyComapny@email.com"--}}
{{--              placeholder="company@email.com"--}}
{{--            />--}}
{{--          </div>--}}
{{--          <div class="form-group">--}}
{{--            <label for="invoice-to" class="form-label">To</label>--}}
{{--            <input--}}
{{--              type="text"--}}
{{--              class="form-control"--}}
{{--              id="invoice-to"--}}
{{--              value="qConsolidated@email.com"--}}
{{--              placeholder="company@email.com"--}}
{{--            />--}}
{{--          </div>--}}
{{--          <div class="form-group">--}}
{{--            <label for="invoice-subject" class="form-label">Subject</label>--}}
{{--            <input--}}
{{--              type="text"--}}
{{--              class="form-control"--}}
{{--              id="invoice-subject"--}}
{{--              value="Invoice of purchased Admin Templates"--}}
{{--              placeholder="Invoice regarding goods"--}}
{{--            />--}}
{{--          </div>--}}
{{--          <div class="form-group">--}}
{{--            <label for="invoice-message" class="form-label">Message</label>--}}
{{--            <textarea--}}
{{--              class="form-control"--}}
{{--              name="invoice-message"--}}
{{--              id="invoice-message"--}}
{{--              cols="3"--}}
{{--              rows="11"--}}
{{--              placeholder="Message..."--}}
{{--            >--}}
{{--Dear Queen Consolidated,--}}

{{--Thank you for your business, always a pleasure to work with you!--}}

{{--We have generated a new invoice in the amount of $95.59--}}

{{--We would appreciate payment of this invoice by 05/11/2019</textarea--}}
{{--            >--}}
{{--          </div>--}}
{{--          <div class="form-group">--}}
{{--            <span class="badge badge-light-primary">--}}
{{--              <i data-feather="link" class="mr-25"></i>--}}
{{--              <span class="align-middle">Invoice Attached</span>--}}
{{--            </span>--}}
{{--          </div>--}}
{{--          <div class="form-group d-flex flex-wrap mt-2">--}}
{{--            <button type="button" class="btn btn-primary mr-1" data-dismiss="modal">Send</button>--}}
{{--            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>--}}
{{--          </div>--}}
{{--        </form>--}}
{{--      </div>--}}
{{--    </div>--}}
{{--  </div>--}}
{{--</div>--}}
<!-- /Send Invoice Sidebar -->

<!-- Add Payment Sidebar -->
{{--<div class="modal modal-slide-in fade" id="add-payment-sidebar" aria-hidden="true">--}}
{{--  <div class="modal-dialog sidebar-lg">--}}
{{--    <div class="modal-content p-0">--}}
{{--      <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>--}}
{{--      <div class="modal-header mb-1">--}}
{{--        <h5 class="modal-title">--}}
{{--          <span class="align-middle">Add Payment</span>--}}
{{--        </h5>--}}
{{--      </div>--}}
{{--      <div class="modal-body flex-grow-1">--}}
{{--        <form>--}}
{{--          <div class="form-group">--}}
{{--            <input id="balance" class="form-control" type="text" value="Invoice Balance: 5000.00" disabled />--}}
{{--          </div>--}}
{{--          <div class="form-group">--}}
{{--            <label class="form-label" for="amount">Payment Amount</label>--}}
{{--            <input id="amount" class="form-control" type="number" placeholder="$1000" />--}}
{{--          </div>--}}
{{--          <div class="form-group">--}}
{{--            <label class="form-label" for="payment-date">Payment Date</label>--}}
{{--            <input id="payment-date" class="form-control date-picker" type="text" />--}}
{{--          </div>--}}
{{--          <div class="form-group">--}}
{{--            <label class="form-label" for="payment-method">Payment Method</label>--}}
{{--            <select class="form-control" id="payment-method">--}}
{{--              <option value="" selected disabled>Select payment method</option>--}}
{{--              <option value="Cash">Cash</option>--}}
{{--              <option value="Bank Transfer">Bank Transfer</option>--}}
{{--              <option value="Debit">Debit</option>--}}
{{--              <option value="Credit">Credit</option>--}}
{{--              <option value="Paypal">Paypal</option>--}}
{{--            </select>--}}
{{--          </div>--}}
{{--          <div class="form-group">--}}
{{--            <label class="form-label" for="payment-note">Internal Payment Note</label>--}}
{{--            <textarea class="form-control" id="payment-note" rows="5" placeholder="Internal Payment Note"></textarea>--}}
{{--          </div>--}}
{{--          <div class="form-group d-flex flex-wrap mb-0">--}}
{{--            <button type="button" class="btn btn-primary mr-1" data-dismiss="modal">Send</button>--}}
{{--            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>--}}
{{--          </div>--}}
{{--        </form>--}}
{{--      </div>--}}
{{--    </div>--}}
{{--  </div>--}}
{{--</div>--}}
<!-- /Add Payment Sidebar -->
@endsection

@section('vendor-script')
<script src="{{asset('vendors/js/forms/repeater/jquery.repeater.min.js')}}"></script>
<script src="{{asset('vendors/js/pickers/flatpickr/flatpickr.min.js')}}"></script>
@endsection

@section('page-script')
<script src="{{asset('js/scripts/pages/app-invoice.js')}}"></script>
<script type="text/javascript">
    function printSlip() {
        window.frames["print_slip"].focus();
        window.frames["print_slip"].print();
    }
</script>
@endsection
