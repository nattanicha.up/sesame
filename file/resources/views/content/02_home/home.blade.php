@extends('layouts/contentLayoutMasterMain')

{{--@section('title', 'Statistics Cards')--}}


@section('vendor-style')

    {{-- Vendor Css files --}}
{{--    <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{asset('vendors/animate-css/animate.css')}}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{asset('vendors/chartist-js/chartist.min.css')}}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{asset('vendors/chartist-js/chartist-plugin-tooltip.css')}}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{asset('js/jquery-ui/jquery-ui.css')}}">--}}
@endsection
@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/charts/chart-apex.css')) }}">
@endsection

@section('content')
    <style>
        #typeGlod {
            width: 100%;
            height: 300px;
            font-size: 14px;
        }
        #ChartSaleByTime {
            width: 100%;
            height: 500px;
            font-size: 14px;
        }

    </style>
    <!-- Statistics card section -->
    <section id="statistics-card">
        <!-- Miscellaneous Charts -->
        <div class="row">
            <!-- Bar Chart -Orders -->
{{--            <div class="col-lg-2 col-6">--}}
{{--                <div class="card">--}}
{{--                    <div class="card-body pb-50">--}}
{{--                        <h6>Orders</h6>--}}
{{--                        <h2 class="font-weight-bolder mb-1">2,76k</h2>--}}
{{--                        <div id="statistics-bar-chart"></div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
            <!--/ Bar Chart -->

            <!-- Line Chart - Profit -->
{{--            <div class="col-lg-2 col-6">--}}
{{--                <div class="card card-tiny-line-stats">--}}
{{--                    <div class="card-body pb-50">--}}
{{--                        <h6>Profit</h6>--}}
{{--                        <h2 class="font-weight-bolder mb-1">6,24k</h2>--}}
{{--                        <div id="statistics-line-chart"></div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
            <!--/ Line Chart -->
            <div class="col-lg-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"><i data-feather='clock'></i> ช่วงเวลา</h4>
{{--                        <div class="d-flex align-items-center">--}}
{{--                            <p class="card-text mr-25 mb-0">Updated 1 month ago</p>--}}
{{--                        </div>--}}
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-12 mb-2 mb-md-0">
                                <input type="text" id="fp-default" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-md-4 col-sm-6">
                <div class="card text-center">
                    <div class="card-body">
                        <div class="avatar bg-light-warning p-50 mb-1">
                            <div class="avatar-content">
                                <i data-feather="users" class="font-medium-5"></i>
                            </div>
                        </div>
                        <h2 class="font-weight-bolder">125 คน</h2>
                        <p class="card-text">จำนวนลูกค้า</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-4 col-sm-6">
                <div class="card text-center">
                    <div class="card-body">
                        <div class="avatar bg-light-success p-50 mb-1">
                            <div class="avatar-content">
                                <i data-feather="arrow-up" class="font-medium-5"></i>
                            </div>
                        </div>
                        <h2 class="font-weight-bolder">0.00 บาท</h2>
                        <p class="card-text">ยอดขายออก</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-4 col-sm-6">
                <div class="card text-center">
                    <div class="card-body">
                        <div class="avatar bg-light-danger p-50 mb-1">
                            <div class="avatar-content">
                                <i data-feather="arrow-down" class="font-medium-5"></i>
                            </div>
                        </div>
                        <h2 class="font-weight-bolder">0.00 บาท</h2>
                        <p class="card-text">ยอดรับซื้อ</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">ยอดขายแยกตามช่วงเวลา</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div id="ChartSaleByTime"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-12">
                <div class="card" style="height: 400px;">
                    <div class="card-header">
                        <h4 class="card-title">ประเภททองขายดี</h4>
                    </div>
                    <div class="card-body" >
                        <div class="row">
{{--                            <div class="col-md-3 col-sm-6 col-12 mb-2 mb-md-0">--}}
                                <div id="typeGlod"></div>
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-12">
                <div class="card" style="height: 400px;">
                    <div class="card-header">
                        <h4 class="card-title">ชนิดทองขายดี</h4>
                    </div>
                    <div class="card-body">
                        <div class="demo-vertical-spacing">
                            <div class="progress-wrapper">
                                <div id="example-caption-1">ทองคำแท่ง</div>
                                <div class="progress progress-bar-primary">
                                    <div
                                        class="progress-bar"
                                        role="progressbar"
                                        aria-valuenow="25"
                                        aria-valuemin="25"
                                        aria-valuemax="100"
                                        style="width: 25%"
                                    >
                                        25%
                                    </div>
                                </div>
                            </div>
                            <div class="progress-wrapper">
                                <div id="example-caption-2">สร้อยคอ</div>
                                <div class="progress progress-bar-secondary">
                                    <div
                                        class="progress-bar"
                                        role="progressbar"
                                        aria-valuenow="35"
                                        aria-valuemin="35"
                                        aria-valuemax="100"
                                        style="width: 35%"
                                    >
                                        35%
                                    </div>
                                </div>
                            </div>
                            <div class="progress-wrapper">
                                <div id="example-caption-3">แหวน</div>
                                <div class="progress progress-bar-success">
                                    <div
                                        class="progress-bar"
                                        role="progressbar"
                                        aria-valuenow="45"
                                        aria-valuemin="45"
                                        aria-valuemax="100"
                                        style="width: 45%"
                                    >
                                        45%
                                    </div>
                                </div>
                            </div>
                            <div class="progress-wrapper">
                                <div id="example-caption-4">ข้อมือ</div>
                                <div class="progress progress-bar-danger">
                                    <div
                                        class="progress-bar"
                                        role="progressbar"
                                        aria-valuenow="55"
                                        aria-valuemin="55"
                                        aria-valuemax="100"
                                        style="width: 55%"
                                    >
                                        55%
                                    </div>
                                </div>
                            </div>
                            <div class="progress-wrapper">
                                <div id="example-caption-5">ข้อเท้า</div>
                                <div class="progress progress-bar-warning">
                                    <div
                                        class="progress-bar"
                                        role="progressbar"
                                        aria-valuenow="65"
                                        aria-valuemin="65"
                                        aria-valuemax="100"
                                        style="width: 65%"
                                    >
                                        65%
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--/ Miscellaneous Charts -->

        <!-- Stats Vertical Card -->

        <!--/ Line Chart Card -->
    </section>


    <!--/ Statistics Card section-->
@endsection

@section('vendor-script')
    {{-- Vendor js files --}}
{{--    <script src="{{ asset(mix('vendors/js/jquery/jquery.min.js')) }}"></script>--}}
{{--        <script src="{{ asset('vendors/js/jquery/jquery.min.js') }}"></script>--}}
@endsection
@section('page-script')
    {{-- Page js files --}}
{{--    <script src="{{ asset(mix('js/scripts/cards/card-statistics.js')) }}"></script>--}}
@endsection
<script src="{{ asset('vendors/js/jquery/jquery.min.js') }}"></script>
<script type='text/javascript' src='{{ asset('js/amcharts/core.js?ver=20210901-5e9') }}' id='-lib-4-core-js-js'></script>
<script type='text/javascript' src='{{ asset('js/amcharts/charts.js?ver=20210901-5e9') }}' id='-lib-4-charts-js-js'></script>
<script type='text/javascript' src='{{ asset('js/amcharts/dataviz.js?ver=20210901-5e9') }}' id='-lib-4-themes-dataviz-js-js'></script>
<script type='text/javascript' src='{{ asset('js/amcharts/material.js?ver=20210901-5e9') }}' id='-lib-4-themes-material-js-js'></script>
<script type='text/javascript' src='{{ asset('js/amcharts/kelly.js?ver=20210901-5e9') }}' id='-lib-4-themes-kelly-js-js'></script>
<script type='text/javascript' src='{{ asset('js/amcharts/dark.js?ver=20210901-5e9') }}' id='-lib-4-themes-dark-js-js'></script>
<script type='text/javascript' src='{{ asset('js/amcharts/frozen.js?ver=20210901-5e9') }}' id='-lib-4-themes-frozen-js-js'></script>
<script type='text/javascript' src='{{ asset('js/amcharts/moonrisekingdom.js?ver=20210901-5e9') }}' id='-lib-4-themes-moonrisekingdom-js-js'></script>
<script type='text/javascript' src='{{ asset('js/amcharts/spiritedaway.js?ver=20210901-5e9') }}' id='-lib-4-themes-spiritedaway-js-js'></script>
<script type='text/javascript' src='{{ asset('js/amcharts/animated.js?ver=20210901-5e9') }}' id='-lib-4-themes-animated-js-js'></script>


<script>
    $(document).ready(function(){
        //alert('test');
        TypeGlod();
        SaleByTime();
    });

    function TypeGlod() {

        try {
            // Themes begin
            am4core.useTheme(am4themes_kelly);
            am4core.useTheme(am4themes_animated);
            // Themes end

            /**
             * Define data for each year
             */


                // Create chart instance
            var chart = am4core.create("typeGlod", am4charts.PieChart);
            chart.logo.disabled = true;
            // Add data
            chart.data = [
                { "sector": "ทองรูปพรรณ", "size": 50.0 },
                { "sector": "ทองแท่ง", "size": 50.0 }

            ];


            // Add label
            chart.innerRadius = 50;

            // Add and configure Series
            var pieSeries = chart.series.push(new am4charts.PieSeries());
            pieSeries.dataFields.value = "size";
            pieSeries.dataFields.category = "sector";

            // Animate chart data
        }
        catch( e ) {
            console.log( e );
        }
    }

    //Sales by time
    function SaleByTime() {

        try {
            // Themes begin
            am4core.useTheme(am4themes_material);
            am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
            var chart = am4core.create("ChartSaleByTime", am4charts.XYChart);
            chart.scrollbarX = new am4core.Scrollbar();
            chart.logo.disabled = true;

// Add data
            chart.data = [{
                "country": "08:00",
                "visits": 3025
            }, {
                "country": "09:00",
                "visits": 1882
            }, {
                "country": "10:00",
                "visits": 1809
            }, {
                "country": "11:00",
                "visits": 1322
            }, {
                "country": "12:00",
                "visits": 1122
            }, {
                "country": "13:00",
                "visits": 1114
            }, {
                "country": "14:00",
                "visits": 984
            }, {
                "country": "15:00",
                "visits": 711
            }];

// Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "country";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 30;
            categoryAxis.renderer.labels.template.horizontalCenter = "right";
            categoryAxis.renderer.labels.template.verticalCenter = "middle";
            categoryAxis.renderer.labels.template.rotation = 270;
            categoryAxis.tooltip.disabled = true;
            categoryAxis.renderer.minHeight = 110;

            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.renderer.minWidth = 50;

// Create series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.sequencedInterpolation = true;
            series.dataFields.valueY = "visits";
            series.dataFields.categoryX = "country";
            series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
            series.columns.template.strokeWidth = 0;

            series.tooltip.pointerOrientation = "vertical";

            series.columns.template.column.cornerRadiusTopLeft = 10;
            series.columns.template.column.cornerRadiusTopRight = 10;
            series.columns.template.column.fillOpacity = 0.8;

// on hover, make corner radiuses bigger
            var hoverState = series.columns.template.column.states.create("hover");
            hoverState.properties.cornerRadiusTopLeft = 0;
            hoverState.properties.cornerRadiusTopRight = 0;
            hoverState.properties.fillOpacity = 1;

            series.columns.template.adapter.add("fill", function(fill, target) {
                return chart.colors.getIndex(target.dataItem.index);
            });


            chart.cursor = new am4charts.XYCursor();            }
        catch( e ) {
            console.log( e );
        }
    }
</script>
@section('vendor-script')


@endsection
