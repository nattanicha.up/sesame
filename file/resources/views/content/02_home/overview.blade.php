@extends('layouts/contentLayoutMasterMain')

{{--@section('title', 'Statistics Cards')--}}


@section('vendor-style')

    <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
@endsection
@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/dashboard-ecommerce.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/charts/chart-apex.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
@endsection

@section('content')
    <div class="col-lg-12 col-md-12 col-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h4 class="card-title">ภาพรวมการออมทอง</h4>
            </div>
            <div class="card-body p-0">
                <div id="goal-overview-radial-bar-chart" class="my-2"></div>
                <div class="row border-top text-center mx-0">
                    <div class="col-6 border-right py-1">
                        <p class="card-text text-muted mb-0">ยอดออมทั้งหมด</p>
                        <h3 class="font-weight-bolder mb-0">฿100,000</h3>
                    </div>
                    <div class="col-6 py-1">
                        <p class="card-text text-muted mb-0">ยอดออมปัจจุบัน</p>
                        <h3 class="font-weight-bolder mb-0">฿83,000</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h4 class="card-title">รายการออมทอง</h4>
            </div>
            <div class="card-body p-0">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-3 ml-2">
                        <div class="avatar bg-light-success mr-2">
                            <div class="avatar-content">
                                <i data-feather="trending-up" class="avatar-icon"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-5">
                        <span>C2021-0921105342</span><br>
                        <span>สร้อยคอ <div class="badge badge-warning" style="font-size: 9px;">50,000</div></span>
                    </div>
                    <div class="col-lg-3 col-md-3 col-3">
                        <span>45,000.00</span>
                        <div class="badge badge-success" style="font-size: 9px;">+1,000.0</div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-3 ml-2 mt-2">
                        <div class="avatar bg-light-success mr-2">
                            <div class="avatar-content">
                                <i data-feather="trending-up" class="avatar-icon"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-5 mt-2">
                        <span>C2021-0918031224</span><br>
                        <span>ทองคำแท่ง <div class="badge badge-warning" style="font-size: 9px;">30,000</div></span>
                    </div>
                    <div class="col-lg-3 col-md-3 col-3 mt-2">
                        <span>25,000.00</span>
                        <div class="badge badge-success" style="font-size: 9px;">+500.0</div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-3 ml-2 mt-2 mb-2">
                        <div class="avatar bg-light-danger mr-2">
                            <div class="avatar-content">
                                <i data-feather="trending-down" class="avatar-icon"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-5 mt-2 mb-2">
                        <span>C2020-072310350</span><br>
                        <span>แหวน <div class="badge badge-warning" style="font-size: 9px;">20,000</div></span>
                    </div>
                    <div class="col-lg-3 col-md-3 col-3 mt-2 mb-2">
                        <span>13,000.00</span>
                        <div class="badge badge-danger" style="font-size: 9px;">-100.0</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h4 class="card-title">ยอดรวม</h4>
            </div>
            <div class="card-body p-0">
                <div class="row">
                    <div class="col-md-7 col-7 ml-2 mt-1 mb-2">
                        <span style="font-size: 28px;">84,400.00</span><span class="text-success mb-1"> (+1,400.0)</span>
                    </div>
                    <div class="col-md-4 col-4 mb-2">
                        <span>อัตราเติบโต</span><br>
                        <span class="text-success"><i data-feather='chevron-up'></i>1.66%</span>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--/ Statistics Card section-->
@endsection

@section('vendor-script')
    {{-- Vendor js files --}}
    <script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
@endsection
@section('page-script')
    {{-- Page js files --}}
    <script src="{{ asset(mix('js/scripts/pages/dashboard-ecommerce.js')) }}"></script>

@endsection

@section('vendor-script')

@endsection
