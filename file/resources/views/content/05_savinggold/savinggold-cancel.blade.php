@extends('layouts/contentLayoutMasterMain')



@section('title', 'ชำระค่างวดออมทอง')



@section('vendor-style')

    <!-- Vendor css files -->

    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">

    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')) }}">

    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">

    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">

    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">

    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">

@endsection



@section('page-style')

    <!-- Page css files -->

    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-ecommerce.css')) }}">

    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">

    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">

    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">

    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-number-input.css')) }}">

    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">

    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">

    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">

@endsection



@section('content')



    <style>

        .subText{

            font-size: 14px;

        }

    </style>



        <div class="row">

            <div class="col-lg-3 col-sm-6 col-12">

                <div class="card">

                    <div class="card-header">

                        <div>

                            <h2 class="font-weight-bolder mb-0">27,800</h2>

                            <p class="card-text">ซื้อทองคำแท่ง</p>

                        </div>

                        <div class="avatar bg-light-danger p-50 m-0">

                            <div class="avatar-content">

                                <i data-feather='shopping-bag' class="font-medium-5"></i>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="col-lg-3 col-sm-6 col-12">

                <div class="card">

                    <div class="card-header">

                        <div>

                            <h2 class="font-weight-bolder mb-0">27,900</h2>

                            <p class="card-text">ขายทองคำแท่ง</p>

                        </div>

                        <div class="avatar bg-light-success p-50 m-0">

                            <div class="avatar-content">

                                <i data-feather="shopping-cart" class="font-medium-5"></i>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="col-lg-3 col-sm-6 col-12">

                <div class="card">

                    <div class="card-header">

                        <div>

                            <h2 class="font-weight-bolder mb-0">27,300</h2>

                            <p class="card-text">ซื้อทองรูปพรรณ</p>

                        </div>

                        <div class="avatar bg-light-danger p-50 m-0">

                            <div class="avatar-content">

                                <i data-feather="shopping-bag" class="font-medium-5"></i>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="col-lg-3 col-sm-6 col-12">

                <div class="card">

                    <div class="card-header">

                        <div>

                            <h2 class="font-weight-bolder mb-0">28,400</h2>

                            <p class="card-text">ขายทองรูปพรรณ</p>

                        </div>

                        <div class="avatar bg-light-success p-50 m-0">

                            <div class="avatar-content">

                                <i data-feather="shopping-cart" class="font-medium-5"></i>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="col-lg-12 col-sm-6 col-12">

                    <div class="list-view product-checkout">

                        <div class="checkout-items">

                            <div class="card user-card">

                                <div class="card-body">

                                    <label class="section-label" style="font-size: 16px;">รายละเอียดสัญญา</label>

                                    <hr />

                                    <div class="row">

                                        <div class="col-xl-4 col-md-4 col-12 mb-1">

                                            <label for="basicInput" class="subText">หมวดหมู่ทอง</label>

                                        </div>

                                        <div class="col-xl-6 col-md-6 col-12 mb-1">

                                            <div class="form-group">

                                                <strong>ทอง 96.5%</strong>

                                            </div>

                                        </div>

                                        <div class="col-xl-2 col-md-2 col-12 mb-1">

                                        </div>



                                        <div class="col-xl-4 col-md-4 col-12 mb-1">

                                            <label for="basicInput" class="subText">ประเภททอง</label>

                                        </div>

                                        <div class="col-xl-6 col-md-6 col-12 mb-1">

                                            <div class="form-group">

                                                <strong>ไม่ระบุ</strong>

                                            </div>

                                        </div>

                                        <div class="col-xl-2 col-md-2 col-12 mb-1">

                                        </div>



                                        <div class="col-xl-4 col-md-4 col-12 mb-1">

                                            <label for="basicInput" class="subText">น้ำหนัก</label>

                                        </div>

                                        <div class="col-xl-6 col-md-6 col-12 mb-1">

                                            <div class="form-group">

                                                <strong>1 บาท</strong>

                                            </div>

                                        </div>

                                        <div class="col-xl-2 col-md-2 col-12 mb-1">

                                        </div>



                                        <div class="col-xl-4 col-md-4 col-12 mb-1">

                                            <label for="basicInput" class="subText">ยอดรวม</label>

                                        </div>

                                        <div class="col-xl-6 col-md-6 col-12 mb-1">

                                            <div class="form-group">

                                                <strong>25,000 บาท</strong>

                                            </div>

                                        </div>

                                        <div class="col-xl-2 col-md-2 col-12 mb-1">

                                        </div>



                                        <div class="col-xl-4 col-md-4 col-12 mb-1">

                                            <label for="basicInput" class="subText">จำนวนงวด</label>

                                        </div>

                                        <div class="col-xl-6 col-md-6 col-12 mb-1">

                                            <div class="form-group">

                                                <strong>10 งวด</strong>

                                            </div>

                                        </div>

                                        <div class="col-xl-2 col-md-2 col-12 mb-1">

                                        </div>



                                        <div class="col-xl-4 col-md-4 col-12 mb-1">

                                            <label for="basicInput" class="subText">ยอดออมต่องวด</label>

                                        </div>

                                        <div class="col-xl-6 col-md-6 col-12 mb-1">

                                            <div class="form-group">

                                                <strong>2,500 บาท</strong>

                                            </div>

                                        </div>

                                        <div class="col-xl-2 col-md-2 col-12 mb-1">

                                        </div>



                                        <div class="col-xl-4 col-md-4 col-12 mb-1">

                                            <label for="basicInput" class="subText">วันที่ทำสัญญา</label>

                                        </div>

                                        <div class="col-xl-6 col-md-6 col-12 mb-1">

                                            <div class="form-group">

                                                <strong>04/10/2021</strong>

                                            </div>

                                        </div>

                                        <div class="col-xl-2 col-md-2 col-12 mb-1">

                                        </div>



                                        <div class="col-xl-4 col-md-4 col-12 mb-1">

                                            <label for="basicInput" class="subText">วันที่ครบสัญญา</label>

                                        </div>

                                        <div class="col-xl-6 col-md-6 col-12 mb-1">

                                            <div class="form-group">

                                                <strong>04/08/2022</strong>

                                            </div>

                                        </div>

                                        <div class="col-xl-2 col-md-2 col-12 mb-1">

                                        </div>



                                    </div>

                                </div>

                            </div>

                        </div>



                        <div class="checkout-options">

                            <div class="card user-card">

                                <div class="card-body">

                                    <label class="section-label" style="font-size: 16px;">ผู้ทำสัญญา</label>

                                    <hr />

                                    <div class="row">

                                        <div class="col-xl-12 col-lg-12 d-flex flex-column justify-content-between border-container-lg">

                                            <div class="user-avatar-section">

                                                <div class="d-flex justify-content-start">

                                                    <img

                                                        class="avatar"

                                                        src="{{asset('images/avatars/7.png')}}"

                                                        height="104"

                                                        width="104"

                                                        alt="User avatar"

                                                    />

                                                    <div class="ml-1">

                                                        <div class="user-info mb-1">

                                                            <h4 class="mb-0">ธนารีย์ ภคพร</h4>

                                                            <span class="card-text" style="font-size: 10px;"><i data-feather='map-pin'></i> 112 หมู่ 3 ต.บ้านปง อ.หางดง จ.เชียงใหม่ 50230</span>

                                                            <div class="d-flex flex-wrap mt-0">

                                                                <span class="card-text" style="font-size: 10px;"><i data-feather='phone'></i> 095-451-3731</span>

                                                            </div>

                                                        </div>



                                                        <div class="d-flex flex-wrap">

{{--                                                            <a href="{{url('app/user/edit')}}" class="btn btn-primary">แก้ไข</a>--}}

                                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editModal">

                                                                แก้ไข

                                                            </button>

                                                            <button class="btn btn-outline-danger ml-1">ลบ</button>

                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                            <div class="card">

                                <div class="card-body">

                                    <label class="section-label" style="font-size: 16px;">ชำระเงิน</label>

                                    <hr />

                                    <div class="price-details">

{{--                                        <h6 class="price-title">Price Details</h6>--}}

                                        <div class="form-group row">

                                            <label for="colFormLabelLg" class="col-sm-5 col-form-label col-form-label-lg">ยอดออมปัจจุบัน</label>

                                            <div class="col-sm-7 col-form-label col-form-label-lg"><strong>2,500.00 บาท (1 งวด)</strong></div>

                                        </div>

                                        <div class="form-group row">

                                            <label for="colFormLabelLg" class="col-sm-5 col-form-label col-form-label-lg">อัตราค่าธรรมเนียม</label>

                                            <div class="col-sm-7 col-form-label col-form-label-lg"><strong class="text-danger">1,000.00 บาท</strong></div>

                                        </div>

                                        <div class="form-group row">

                                            <label for="colFormLabelLg" class="col-sm-5 col-form-label col-form-label-lg">ยอดเงินคืน</label>

                                            <div class="col-sm-7 col-form-label col-form-label-lg"><strong class="text-success">1,500.00 บาท (1 งวด)</strong></div>

                                        </div>

                                        <hr class="mt-2" />

                                        <button type="button" class="btn btn-primary btn-block btn-next place-order" id="confirmPayment">ยืนยันการชำระเงิน</button>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

            </div>



{{--            <div--}}

{{--                class="modal fade text-left"--}}

{{--                id="large"--}}

{{--                tabindex="-1"--}}

{{--                role="dialog"--}}

{{--                aria-labelledby="myModalLabel17"--}}

{{--                aria-hidden="true"--}}

{{--            >--}}

{{--                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">--}}

{{--                    <div class="modal-content">--}}

{{--                        <div class="modal-header">--}}

{{--                            <h4 class="modal-title" id="myModalLabel17">รายละเอียดสินค้า</h4>--}}

{{--                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}

{{--                                <span aria-hidden="true">&times;</span>--}}

{{--                            </button>--}}

{{--                        </div>--}}

{{--                        <div class="modal-body">--}}

{{--                            <div class="row">--}}

{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <label for="basicInput" class="subText">หมวดหมู่สินค้า</label>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-3 col-md-6 col-12 mb-1">--}}

{{--                                    <div class="custom-control custom-radio">--}}

{{--                                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input" checked />--}}

{{--                                        <label class="custom-control-label" for="customRadio1">ทอง 96.5 %</label>--}}

{{--                                    </div>--}}

{{--                                    <div class="custom-control custom-radio mt-1">--}}

{{--                                        <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input" />--}}

{{--                                        <label class="custom-control-label" for="customRadio2">ทอง 99.99 %</label>--}}

{{--                                    </div>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-7 col-md-6 col-12 mb-1">--}}

{{--                                    <div class="custom-control custom-radio">--}}

{{--                                        <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input" />--}}

{{--                                        <label class="custom-control-label" for="customRadio3">ทอง 90 %</label>--}}

{{--                                    </div>--}}

{{--                                    <div class="custom-control custom-radio mt-1">--}}

{{--                                        <input type="radio" id="customRadio4" name="customRadio" class="custom-control-input" />--}}

{{--                                        <label class="custom-control-label" for="customRadio4">อื่น ๆ</label>--}}

{{--                                    </div>--}}

{{--                                </div>--}}



{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <label for="basicInput" class="subText">ประเภททอง</label>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-3 col-md-6 col-12 mb-1">--}}

{{--                                    <div class="custom-control custom-radio">--}}

{{--                                        <input type="radio" id="Radio1" name="customRadio" class="custom-control-input" checked />--}}

{{--                                        <label class="custom-control-label" for="Radio1">ทองรูปพรรณ</label>--}}

{{--                                    </div>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-7 col-md-6 col-12 mb-1">--}}

{{--                                    <div class="custom-control custom-radio">--}}

{{--                                        <input type="radio" id="Radio2" name="customRadio" class="custom-control-input" />--}}

{{--                                        <label class="custom-control-label" for="Radio2">ทองแท่ง</label>--}}

{{--                                    </div>--}}

{{--                                </div>--}}



{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <label for="basicInput" class="subText">ประเภทสินค้า</label>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}

{{--                                    <div class="form-group">--}}

{{--                                        <select class="form-control" id="basicSelect">--}}

{{--                                            <option>สร้อยคอ</option>--}}

{{--                                            <option>แหวน</option>--}}

{{--                                            <option>ทองแท่ง</option>--}}

{{--                                        </select>--}}

{{--                                    </div>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <label for="basicInput"></label>--}}

{{--                                </div>--}}



{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <label for="basicInput" class="subText">ชื่อสินค้า</label>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}

{{--                                    <div class="form-group">--}}

{{--                                        <input type="text" class="form-control" id="basicInput"/>--}}

{{--                                    </div>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <label for="basicInput"></label>--}}

{{--                                </div>--}}



{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <label for="basicInput" class="subText">รหัสสินค้า</label>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-10 col-md-6 col-12 mb-1">--}}

{{--                                    <span>G121102111243840</span>--}}

{{--                                </div>--}}



{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <label for="basicInput" class="subText">น้ำหนัก</label>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}

{{--                                    <div class="form-group">--}}

{{--                                        <input type="text" class="form-control" id="basicInput"/>--}}

{{--                                    </div>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <div class="form-group">--}}

{{--                                        <select class="form-control" id="basicSelect">--}}

{{--                                            <option>สลึง</option>--}}

{{--                                            <option>บาท</option>--}}

{{--                                        </select>--}}

{{--                                    </div>--}}

{{--                                </div>--}}



{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <label for="basicInput" class="subText">น้ำหนักจริง</label>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}

{{--                                    <div class="form-group">--}}

{{--                                        <input type="text" class="form-control" id="basicInput"/>--}}

{{--                                    </div>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <label for="basicInput" class="subText">กรัม (g)</label>--}}

{{--                                </div>--}}



{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <label for="basicInput" class="subText">ราคารับซื้อ</label>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}

{{--                                    <div class="form-group">--}}

{{--                                        <input type="text" class="form-control" id="basicInput"/>--}}

{{--                                    </div>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <label for="basicInput" class="subText">บาท</label>--}}

{{--                                </div>--}}



{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <label for="basicInput" class="subText">ขนาด/ความยาว</label>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}

{{--                                    <div class="form-group">--}}

{{--                                        <input type="text" class="form-control" id="basicInput"/>--}}

{{--                                    </div>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <label for="basicInput" class="subText">รูปสินค้า</label>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}

{{--                                    <form action="#" class="dropzone dropzone-area" id="dpz-single-file">--}}

{{--                                        <div class="dz-message">Drop files here or click to upload.</div>--}}

{{--                                    </form>--}}

{{--                                </div>--}}

{{--                            </div>--}}

{{--                        </div>--}}

{{--                        <div class="modal-footer">--}}

{{--                            <button type="button" class="btn btn-primary"  data-dismiss="modal">บันทึก</button>--}}

{{--                            <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">ปิด</button>--}}

{{--                        </div>--}}

{{--                    </div>--}}

{{--                </div>--}}

{{--            </div>--}}

            <div

                class="modal fade text-left"

                id="editModal"

                tabindex="-1"

                role="dialog"

                aria-labelledby="myModalLabel17"

                aria-hidden="true"

            >

                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">

                    <div class="modal-content">

                        <div class="modal-header">

                            <h4 class="modal-title" id="myModalLabel17">ข้อมูลสมาชิก</h4>

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                                <span aria-hidden="true">&times;</span>

                            </button>

                        </div>

                        <div class="modal-body">

                            <div class="row">

                                <div class="col-xl-12 col-md-6 col-12 mb-1">

                                    <label for="basicInput" class="subText text-danger">**สำหรับสมาชิกเก่ากรอกเพียงหมายเลขบัตรประชาชน หรือ เบอร์โทร</label>

                                </div>

{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <label for="basicInput" class="subText">หมวดหมู่สินค้า</label>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-3 col-md-6 col-12 mb-1">--}}

{{--                                    <div class="custom-control custom-radio">--}}

{{--                                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input" checked />--}}

{{--                                        <label class="custom-control-label" for="customRadio1">ทอง 96.5 %</label>--}}

{{--                                    </div>--}}

{{--                                    <div class="custom-control custom-radio mt-1">--}}

{{--                                        <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input" />--}}

{{--                                        <label class="custom-control-label" for="customRadio2">ทอง 99.99 %</label>--}}

{{--                                    </div>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-7 col-md-6 col-12 mb-1">--}}

{{--                                    <div class="custom-control custom-radio">--}}

{{--                                        <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input" />--}}

{{--                                        <label class="custom-control-label" for="customRadio3">ทอง 90 %</label>--}}

{{--                                    </div>--}}

{{--                                    <div class="custom-control custom-radio mt-1">--}}

{{--                                        <input type="radio" id="customRadio4" name="customRadio" class="custom-control-input" />--}}

{{--                                        <label class="custom-control-label" for="customRadio4">อื่น ๆ</label>--}}

{{--                                    </div>--}}

{{--                                </div>--}}



{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <label for="basicInput" class="subText">ประเภททอง</label>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-3 col-md-6 col-12 mb-1">--}}

{{--                                    <div class="custom-control custom-radio">--}}

{{--                                        <input type="radio" id="Radio1" name="customRadio" class="custom-control-input" checked />--}}

{{--                                        <label class="custom-control-label" for="Radio1">ทองรูปพรรณ</label>--}}

{{--                                    </div>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-7 col-md-6 col-12 mb-1">--}}

{{--                                    <div class="custom-control custom-radio">--}}

{{--                                        <input type="radio" id="Radio2" name="customRadio" class="custom-control-input" />--}}

{{--                                        <label class="custom-control-label" for="Radio2">ทองแท่ง</label>--}}

{{--                                    </div>--}}

{{--                                </div>--}}







                                <div class="col-xl-2 col-md-6 col-12 mb-1">

                                    <label for="basicInput" class="subText">เลขบัตรประชาชน</label>

                                </div>

                                <div class="col-xl-8 col-md-6 col-12 mb-1">

                                    <div class="form-group">

                                        <input type="text" class="form-control" id="id_card" placeholder="เฉพาะตัวเลขเท่านั้น"/>

                                    </div>

                                </div>

                                <div class="col-xl-2 col-md-6 col-12 mb-1">

                                    <button type="button" class="btn btn-danger">DUMMY</button>

                                </div>



                                <div class="col-xl-2 col-md-6 col-12 mb-1">

                                    <label for="basicInput" class="subText">เบอร์โทร<label class="text-danger">*</label></label>

                                </div>

                                <div class="col-xl-10 col-md-6 col-12 mb-1">

                                    <div class="form-group">

                                        <input type="text" class="form-control" id="telephone" placeholder="เฉพาะตัวเลขเท่านั้น"/>

                                    </div>

                                </div>



                                <div class="col-xl-2 col-md-6 col-12 mb-1">

                                    <label for="basicInput" class="subText">คำนำหน้าชื่อ</label>

                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">

                                    <div class="form-group">

                                        <select class="form-control" id="prefix">

                                            <option>นาย</option>

                                            <option>นาง</option>

                                            <option>นางสาว</option>

                                        </select>

                                    </div>

                                </div>

                                <div class="col-xl-2 col-md-6 col-12 mb-1">

                                    <label for="basicInput" class="subText">ไลน์ไอดี</label>

                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">

                                    <div class="form-group">

                                        <input type="text" class="form-control" id="lineID" placeholder="@abc"/>

                                    </div>

                                </div>



                                <div class="col-xl-2 col-md-6 col-12 mb-1">

                                    <label for="basicInput" class="subText">ชื่อ<label class="text-danger">*</label></label>

                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">

                                    <div class="form-group">

                                        <input type="text" class="form-control" id="name_th"/>

                                    </div>

                                </div>

                                <div class="col-xl-2 col-md-6 col-12 mb-1">

                                    <label for="basicInput" class="subText">เฟสบุ๊ค URL</label>

                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">

                                    <div class="form-group">

                                        <input type="text" class="form-control" id="facebook" placeholder="www.facebook.com/abc"/>

                                    </div>

                                </div>



                                <div class="col-xl-2 col-md-6 col-12 mb-1">

                                    <label for="basicInput" class="subText">นามสกุล</label>

                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">

                                    <div class="form-group">

                                        <input type="text" class="form-control" id="last_name"/>

                                    </div>

                                </div>

                                <div class="col-xl-2 col-md-6 col-12 mb-1">

                                    <label for="basicInput" class="subText">คำนำหน้าชื่ออังกฤษ</label>

                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">

                                    <div class="form-group">

                                        <select class="form-control" id="prefix_en">

                                            <option></option>

                                        </select>

                                    </div>

                                </div>



                                <div class="col-xl-2 col-md-6 col-12 mb-1">

                                    <label for="basicInput" class="subText">บ้านเลขที่</label>

                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">

                                    <div class="form-group">

                                        <input type="text" class="form-control" id="number"/>

                                    </div>

                                </div>

                                <div class="col-xl-2 col-md-6 col-12 mb-1">

                                    <label for="basicInput" class="subText">ชื่ออังกฤษ</label>

                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">

                                    <div class="form-group">

                                        <input type="text" class="form-control" id="name_en"/>

                                    </div>

                                </div>



                                <div class="col-xl-2 col-md-6 col-12 mb-1">

                                    <label for="basicInput" class="subText">จังหวัด</label>

                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">

                                    <div class="form-group">

                                        <select class="form-control" id="province">

                                            <option>กรุณาเลือกจังหวัด</option>

                                        </select>

                                    </div>

                                </div>

                                <div class="col-xl-2 col-md-6 col-12 mb-1">

                                    <label for="basicInput" class="subText">นามสกุลอังกฤษ</label>

                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">

                                    <div class="form-group">

                                        <input type="text" class="form-control" id="lastname_en"/>

                                    </div>

                                </div>



                                <div class="col-xl-2 col-md-6 col-12 mb-1">

                                    <label for="basicInput" class="subText">อำเภอ</label>

                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">

                                    <div class="form-group">

                                        <select class="form-control" id="district">

                                            <option></option>

                                        </select>

                                    </div>

                                </div>

                                <div class="col-xl-2 col-md-6 col-12 mb-1">

                                    <label for="basicInput" class="subText">วันเกิด</label>

                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">

                                    <div class="form-group">

                                        <input type="text" id="birthday" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />

                                    </div>

                                </div>



                                <div class="col-xl-2 col-md-6 col-12 mb-1">

                                    <label for="basicInput" class="subText">ตำบล</label>

                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">

                                    <div class="form-group">

                                        <select class="form-control" id="sub_district">

                                            <option></option>

                                        </select>

                                    </div>

                                </div>

                                <div class="col-xl-2 col-md-6 col-12 mb-1">

                                    <label for="basicInput" class="subText">เพศ</label>

                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">

                                    <div class="form-group">

                                        <select class="form-control" id="sex">

                                            <option>ชาย</option>

                                            <option>หญิง</option>

                                        </select>

                                    </div>

                                </div>



                                <div class="col-xl-2 col-md-6 col-12 mb-1">

                                    <label for="basicInput" class="subText">รหัสไปรษณีย์</label>

                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">

                                    <div class="form-group">

                                        <input type="text" class="form-control" id="zipcode" disabled/>

                                    </div>

                                </div>

                                <div class="col-xl-2 col-md-6 col-12 mb-1">

                                    <label for="basicInput" class="subText">วันที่ออกบัตร</label>

                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">

                                    <div class="form-group">

                                        <input type="text" id="date_start" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />

                                    </div>

                                </div>



                                <div class="col-xl-2 col-md-6 col-12 mb-1">

                                    <label for="basicInput" class="subText">อีเมล</label>

                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">

                                    <div class="form-group">

                                        <input type="text" class="form-control" id="email" placeholder="abc@gmail.com"/>

                                    </div>

                                </div>

                                <div class="col-xl-2 col-md-6 col-12 mb-1">

                                    <label for="basicInput" class="subText">วันที่บัตรหมดอายุ</label>

                                </div>

                                <div class="col-xl-4 col-md-6 col-12 mb-1">

                                    <div class="form-group">

                                        <input type="text" id="date_exp" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />

                                    </div>

                                </div>



{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <label for="basicInput" class="subText">รหัสสินค้า</label>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-10 col-md-6 col-12 mb-1">--}}

{{--                                    <span>G121102111243840</span>--}}

{{--                                </div>--}}



{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <label for="basicInput" class="subText">น้ำหนัก</label>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}

{{--                                    <div class="form-group">--}}

{{--                                        <input type="text" class="form-control" id="basicInput"/>--}}

{{--                                    </div>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <div class="form-group">--}}

{{--                                        <select class="form-control" id="basicSelect">--}}

{{--                                            <option>สลึง</option>--}}

{{--                                            <option>บาท</option>--}}

{{--                                        </select>--}}

{{--                                    </div>--}}

{{--                                </div>--}}



{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <label for="basicInput" class="subText">น้ำหนักจริง</label>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}

{{--                                    <div class="form-group">--}}

{{--                                        <input type="text" class="form-control" id="basicInput"/>--}}

{{--                                    </div>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <label for="basicInput" class="subText">กรัม (g)</label>--}}

{{--                                </div>--}}



{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <label for="basicInput" class="subText">ราคารับซื้อ</label>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}

{{--                                    <div class="form-group">--}}

{{--                                        <input type="text" class="form-control" id="basicInput"/>--}}

{{--                                    </div>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <label for="basicInput" class="subText">บาท</label>--}}

{{--                                </div>--}}



{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <label for="basicInput" class="subText">ขนาด/ความยาว</label>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}

{{--                                    <div class="form-group">--}}

{{--                                        <input type="text" class="form-control" id="basicInput"/>--}}

{{--                                    </div>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}

{{--                                    <label for="basicInput" class="subText">รูปสินค้า</label>--}}

{{--                                </div>--}}

{{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}

{{--                                    <form action="#" class="dropzone dropzone-area" id="dpz-single-file">--}}

{{--                                        <div class="dz-message">Drop files here or click to upload.</div>--}}

{{--                                    </form>--}}

{{--                                </div>--}}

                            </div>

                        </div>

                        <div class="modal-footer">

                            <button type="button" class="btn btn-primary"  data-dismiss="modal">บันทึก</button>

                            <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">ปิด</button>

                        </div>

                    </div>

                </div>

            </div>

        </div>

@endsection



@section('vendor-script')

    <!-- Vendor js files -->

    <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>

    <script src="{{ asset(mix('vendors/js/forms/spinner/jquery.bootstrap-touchspin.js')) }}"></script>

    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>

    <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>

    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>

    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>

    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>

    <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>

    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>

@endsection



@section('page-script')

    <!-- Page js files -->

    <script src="{{ asset(mix('js/scripts/pages/app-ecommerce-checkout.js')) }}"></script>

    <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>

    <script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>

    <script>
        $(document).ready(function() {
            $("#confirmPayment").click(function() {
                window.location.href = "{{ url('/saving-gold-cancel-invoice') }}";
            });
        });
    </script>

@endsection

