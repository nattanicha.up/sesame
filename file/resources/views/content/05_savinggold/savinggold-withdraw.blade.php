@extends('layouts/contentLayoutSellGold')

@section('title', 'ร้านขายออก')

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')) }}">
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection

@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset('css/base/pages/app-ecommerce-sellgold.css') }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
    <link rel="stylesheet" href="{{asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css'))}}">
@endsection

@section('content')
    <section id="input-group-basic">
        <div class="row">
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h2 class="font-weight-bolder mb-0">27,800</h2>
                            <p class="card-text">ซื้อทองคำแท่ง</p>
                        </div>
                        <div class="avatar bg-light-danger p-50 m-0">
                            <div class="avatar-content">
                                <i data-feather='shopping-bag' class="font-medium-5"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h2 class="font-weight-bolder mb-0">27,900</h2>
                            <p class="card-text">ขายทองคำแท่ง</p>
                        </div>
                        <div class="avatar bg-light-success p-50 m-0">
                            <div class="avatar-content">
                                <i data-feather="shopping-cart" class="font-medium-5"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h2 class="font-weight-bolder mb-0">27,300</h2>
                            <p class="card-text">ซื้อทองรูปพรรณ</p>
                        </div>
                        <div class="avatar bg-light-danger p-50 m-0">
                            <div class="avatar-content">
                                <i data-feather="shopping-bag" class="font-medium-5"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h2 class="font-weight-bolder mb-0">28,400</h2>
                            <p class="card-text">ขายทองรูปพรรณ</p>
                        </div>
                        <div class="avatar bg-light-success p-50 m-0">
                            <div class="avatar-content">
                                <i data-feather="shopping-cart" class="font-medium-5"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Basic -->
            <div class="col-md-8">
                <div class="input-group mb-2">
                    <input
                        type="text"
                        class="form-control"
                        placeholder="ค้นหาทอง"
                        aria-label="Search..."
                        aria-describedby="basic-addon-search1"
                    />
                    <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon-search1"><i data-feather="search"></i></span>
                    </div>
                </div>
                <div class="list-view product-checkout">
                    <div class="checkout-items">
                        <div class="text-center mb-2">
{{--                            <button type="button" class="btn btn-success mt-1 remove-wishlist">--}}
{{--                                <i data-feather="plus" class="align-middle mr-25"></i>--}}
{{--                                <span>เพิ่ม</span>--}}
{{--                            </button>--}}
                        </div>
                        <div class="card ecommerce-card">
                            <div class="item-img ml-2 mr-2">
                                <img src="{{asset('images/gold/gold1.jpg')}}" alt="img-placeholder" />
                            </div>
                            <div class="card-body">
                                <div class="item-name">
                                    <h4 class="mb-0 text-body bold">ออสสิริส</h4>
                                    <span class="text-primary">ทองคำแท่ง</span>
                                    <div class="badge badge-pill badge-warning">96.5 %</div>
                                </div>
                                <span class="item-company mt-1">หนัก: 1 บาท</span>
                                <span class="item-company">ขนาด/ความยาว: 1</span>
                                <div class="form-group row mb-0 mt-1">
                                    <label for="colFormLabelLg" class="col-sm-4 col-form-label"><strong>ค่ากำเหน็ด:</strong></label>
                                    <div class="col-sm-5">
                                        <input
                                            type="number"
                                            class="form-control item-company"
                                            id="colFormLabelLg"
                                            placeholder=""
                                            value="5000"
                                            readonly
                                        />
                                    </div>
                                </div>
                            </div>
                            <div class="item-options text-center">
                                <div class="item-wrapper">
                                    <div class="item-cost">
                                        <h4 class="item-price">฿ 30,000</h4>
                                        <span class="item-company">(฿ 5,000)</span>
                                        <p class="card-text shipping mt-1">
                                            <span class="badge badge-pill badge-light-success">คงเหลือ 10 ชิ้น</span>
                                        </p>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-dark mt-1 remove-wishlist mt-3">
                                    <i data-feather="x" class="align-middle mr-25"></i>
                                    <span>ยกเลิก</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Merged -->
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <label class="section-label" style="font-size: 16px;">ผู้รับทอง</label>
                        <hr />
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                <div class="user-avatar-section">
                                    <div class="d-flex justify-content-start">
                                        <img
                                            class="avatar"
                                            src="{{asset('images/avatars/7.png')}}"
                                            height="104"
                                            width="104"
                                            alt="User avatar"
                                        />
                                        <div class="ml-1">
                                            <div class="user-info mb-1">
                                                <h4 class="mb-0">ธนารีย์ ภคพร</h4>
                                                <span class="card-text" style="font-size: 10px;"><i data-feather='map-pin'></i> 112 หมู่ 3 ต.บ้านปง อ.หางดง จ.เชียงใหม่ 50230</span>
                                                <div class="d-flex flex-wrap mt-0">
                                                    <span class="card-text" style="font-size: 10px;"><i data-feather='phone'></i> 095-451-3731</span>
                                                </div>
                                            </div>

                                            {{--<div class="d-flex flex-wrap">
                                                <a href="{{url('app/user/edit')}}" class="btn btn-primary">แก้ไข</a>
                                                <button class="btn btn-outline-danger ml-1">ลบ</button>
                                            </div>--}}
                                            <button type="reset" class="btn btn-primary mr-1" data-toggle="modal" data-target="#editModal">แก้ไข</button>
                                            <button type="reset" class="btn btn-outline-danger" id="confirm-text">ลบ</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <label class="section-label" style="font-size: 16px;">ข้อมูลสัญญาการออมทอง</label>
                        <hr />
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group row">
                                    <label for="colFormLabelLg" class="col-sm-4 col-form-label col-form-label-lg">เลขที่สัญญา</label>
                                    <div class="col-sm-8 col-form-label col-form-label-lg"><strong>C2021-1012025459</strong></div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelLg" class="col-sm-4 col-form-label col-form-label-lg">ชนิดทอง</label>
                                    <div class="col-sm-8 col-form-label col-form-label-lg"><strong>ทองแท่ง</strong></div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelLg" class="col-sm-4 col-form-label col-form-label-lg">น้ำหนัก</label>
                                    <div class="col-sm-8 col-form-label col-form-label-lg"><strong>1 บาท</strong></div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelLg" class="col-sm-4 col-form-label col-form-label-lg">ยอดรวม</label>
                                    <div class="col-sm-8 col-form-label col-form-label-lg"><strong>35,000 บาท</strong></div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelLg" class="col-sm-4 col-form-label col-form-label-lg">จำนวนงวด</label>
                                    <div class="col-sm-8 col-form-label col-form-label-lg"><strong>2 งวด</strong></div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelLg" class="col-sm-4 col-form-label col-form-label-lg">ยอดออมต่องวด</label>
                                    <div class="col-sm-8 col-form-label col-form-label-lg"><strong>17,500 บาท</strong></div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelLg" class="col-sm-4 col-form-label col-form-label-lg">วันที่ทำสัญญา</label>
                                    <div class="col-sm-8 col-form-label col-form-label-lg"><strong>12/10/2021</strong></div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelLg" class="col-sm-4 col-form-label col-form-label-lg">วันที่ครบสัญญา</label>
                                    <div class="col-sm-8 col-form-label col-form-label-lg"><strong>12/12/2021</strong></div>
                                </div>
                                <hr />
                                <button type="button" class="btn btn-primary btn-block btn-next place-order" id="confirmPayment">ยืนยันการเบิกทอง</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div
        class="modal fade text-left"
        id="editModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="myModalLabel17"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel17">ข้อมูลสมาชิก</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText text-danger">**สำหรับสมาชิกเก่ากรอกเพียงหมายเลขบัตรประชาชน หรือ เบอร์โทร</label>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เลขบัตรประชาชน</label>
                        </div>
                        <div class="col-xl-8 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="id_card" placeholder="เฉพาะตัวเลขเท่านั้น"/>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <button type="button" class="btn btn-danger">DUMMY</button>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เบอร์โทร<label class="text-danger">*</label></label>
                        </div>
                        <div class="col-xl-10 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="telephone" placeholder="เฉพาะตัวเลขเท่านั้น"/>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">คำนำหน้าชื่อ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <select class="form-control" id="prefix">
                                    <option>นาย</option>
                                    <option>นาง</option>
                                    <option>นางสาว</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">ไลน์ไอดี</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="lineID" placeholder="@abc"/>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">ชื่อ<label class="text-danger">*</label></label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="name_th"/>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เฟสบุ๊ค URL</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="facebook" placeholder="www.facebook.com/abc"/>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">นามสกุล</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="last_name"/>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">คำนำหน้าชื่ออังกฤษ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <select class="form-control" id="prefix_en">
                                    <option></option>
                                </select>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">บ้านเลขที่</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="number"/>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">ชื่ออังกฤษ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="name_en"/>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">จังหวัด</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <select class="form-control" id="province">
                                    <option>กรุณาเลือกจังหวัด</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">นามสกุลอังกฤษ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="lastname_en"/>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">อำเภอ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <select class="form-control" id="district">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">วันเกิด</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" id="birthday" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">ตำบล</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <select class="form-control" id="sub_district">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เพศ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <select class="form-control" id="sex">
                                    <option>ชาย</option>
                                    <option>หญิง</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">รหัสไปรษณีย์</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="zipcode" disabled/>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">วันที่ออกบัตร</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" id="date_start" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">อีเมล</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="email" placeholder="abc@gmail.com"/>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">วันที่บัตรหมดอายุ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" id="date_exp" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary"  data-dismiss="modal">บันทึก</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">ปิด</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/spinner/jquery.bootstrap-touchspin.js')) }}"></script>
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/form-number-input.js')) }}"></script>
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-sweet-alerts.js')) }}"></script>
    <script>
        $(document).ready(function() {
            $("#confirmPayment").click(function() {
                window.location.href = "{{ url('/sell-invoice') }}";
            });
        });
    </script>
@endsection
