
@extends('layouts/contentLayoutMasterMain')

@section('title', 'รายการค่าใช้จ่าย')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('content')
    <style>
        .flsize{
            font-size: 16px;
        }
    </style>
    <!-- Basic table -->
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">ค้นหาค่าใช้จ่าย</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12 mb-2 mb-md-0">
                            <div class="form-group">
                                <input type="text" id="helperText" class="form-control" placeholder="เลขที่เอกสาร, ชื่อผู้จัดจำหน่าย, หมวดหมู่" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section id="basic-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <table class="datatables-basic table">
                        <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th>id</th>
                            <th>เลขที่เอกสาร</th>
                            <th>ชื่อผู้จัดจำหน่าย</th>
                            <th>หมวดหมู่</th>
                            <th>ยอดรวมสุทธิ</th>
                            <th>วันที่ออก</th>
                            <th>จัดการข้อมูล</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <div
        class="modal fade text-left"
        id="default"
        tabindex="-1"
        role="dialog"
        aria-labelledby="myModalLabel1"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">เพิ่มข้อมูล DUMMY</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เลขบัตรประชาชน <span class="text-danger">*</span></label>
                        </div>
                        <div class="col-xl-8 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="id_card" placeholder="เฉพาะตัวเลขเท่านั้น"/>
                            </div>
                        </div>

                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เบอร์โทร <span class="text-danger">*</span></label>
                        </div>
                        <div class="col-xl-8 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="telephone" placeholder="เฉพาะตัวเลขเท่านั้น"/>
                            </div>
                        </div>

                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">ชื่อ</label>
                        </div>
                        <div class="col-xl-8 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="ืfname" placeholder=""/>
                            </div>
                        </div>

                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">นามสกุล</label>
                        </div>
                        <div class="col-xl-8 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="lname" placeholder=""/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">บันทึก</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">ปิด</button>
                </div>
            </div>
        </div>
    </div>
    <div
            class="modal fade text-left"
            id="addModal"
            tabindex="-1"
            role="dialog"
            aria-labelledby="myModalLabel17"
            aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel17">ข้อมูลสมาชิก</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText text-danger">**สำหรับสมาชิกเก่ากรอกเพียงหมายเลขบัตรประชาชน หรือ เบอร์โทร</label>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เลขบัตรประชาชน</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="id_card" placeholder="เฉพาะตัวเลขเท่านั้น"/>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เบอร์โทร<label class="text-danger">*</label></label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="telephone" placeholder="เฉพาะตัวเลขเท่านั้น"/>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">คำนำหน้าชื่อ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <select class="form-control" id="prefix">
                                    <option>นาย</option>
                                    <option>นาง</option>
                                    <option>นางสาว</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">ไลน์ไอดี</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="lineID" placeholder="@abc"/>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">ชื่อ<label class="text-danger">*</label></label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="name_th"/>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เฟสบุ๊ค URL</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="facebook" placeholder="www.facebook.com/abc"/>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">นามสกุล</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="last_name"/>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">คำนำหน้าชื่ออังกฤษ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <select class="form-control" id="prefix_en">
                                    <option></option>
                                </select>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">บ้านเลขที่</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="number"/>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">ชื่ออังกฤษ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="name_en"/>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">จังหวัด</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <select class="form-control" id="province">
                                    <option>กรุณาเลือกจังหวัด</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">นามสกุลอังกฤษ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="lastname_en"/>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">อำเภอ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <select class="form-control" id="district">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">วันเกิด</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" id="birthday" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">ตำบล</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <select class="form-control" id="sub_district">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เพศ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <select class="form-control" id="sex">
                                    <option>ชาย</option>
                                    <option>หญิง</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">รหัสไปรษณีย์</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="zipcode" disabled/>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">วันที่ออกบัตร</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" id="date_start" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">อีเมล</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="email" placeholder="abc@gmail.com"/>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">วันที่บัตรหมดอายุ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" id="date_exp" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                            </div>
                        </div>

                        {{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}
                        {{--                                    <label for="basicInput" class="subText">รหัสสินค้า</label>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="col-xl-10 col-md-6 col-12 mb-1">--}}
                        {{--                                    <span>G121102111243840</span>--}}
                        {{--                                </div>--}}

                        {{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}
                        {{--                                    <label for="basicInput" class="subText">น้ำหนัก</label>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}
                        {{--                                    <div class="form-group">--}}
                        {{--                                        <input type="text" class="form-control" id="basicInput"/>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}
                        {{--                                    <div class="form-group">--}}
                        {{--                                        <select class="form-control" id="basicSelect">--}}
                        {{--                                            <option>สลึง</option>--}}
                        {{--                                            <option>บาท</option>--}}
                        {{--                                        </select>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}

                        {{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}
                        {{--                                    <label for="basicInput" class="subText">น้ำหนักจริง</label>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}
                        {{--                                    <div class="form-group">--}}
                        {{--                                        <input type="text" class="form-control" id="basicInput"/>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}
                        {{--                                    <label for="basicInput" class="subText">กรัม (g)</label>--}}
                        {{--                                </div>--}}

                        {{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}
                        {{--                                    <label for="basicInput" class="subText">ราคารับซื้อ</label>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}
                        {{--                                    <div class="form-group">--}}
                        {{--                                        <input type="text" class="form-control" id="basicInput"/>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}
                        {{--                                    <label for="basicInput" class="subText">บาท</label>--}}
                        {{--                                </div>--}}

                        {{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}
                        {{--                                    <label for="basicInput" class="subText">ขนาด/ความยาว</label>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}
                        {{--                                    <div class="form-group">--}}
                        {{--                                        <input type="text" class="form-control" id="basicInput"/>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}
                        {{--                                </div>--}}
                        {{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}
                        {{--                                    <label for="basicInput" class="subText">รูปสินค้า</label>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}
                        {{--                                    <form action="#" class="dropzone dropzone-area" id="dpz-single-file">--}}
                        {{--                                        <div class="dz-message">Drop files here or click to upload.</div>--}}
                        {{--                                    </form>--}}
                        {{--                                </div>--}}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary"  data-dismiss="modal">บันทึก</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">ปิด</button>
                </div>
            </div>
        </div>
    </div>
    <div
            class="modal fade text-left"
            id="editModal"
            tabindex="-1"
            role="dialog"
            aria-labelledby="myModalLabel17"
            aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel17">ข้อมูลสมาชิก</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText text-danger">**สำหรับสมาชิกเก่ากรอกเพียงหมายเลขบัตรประชาชน หรือ เบอร์โทร</label>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เลขบัตรประชาชน</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="id_card" placeholder="เฉพาะตัวเลขเท่านั้น"/>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เบอร์โทร<label class="text-danger">*</label></label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="telephone" placeholder="เฉพาะตัวเลขเท่านั้น"/>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">คำนำหน้าชื่อ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <select class="form-control" id="prefix">
                                    <option>นาย</option>
                                    <option>นาง</option>
                                    <option>นางสาว</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">ไลน์ไอดี</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="lineID" placeholder="@abc"/>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">ชื่อ<label class="text-danger">*</label></label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="name_th"/>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เฟสบุ๊ค URL</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="facebook" placeholder="www.facebook.com/abc"/>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">นามสกุล</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="last_name"/>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">คำนำหน้าชื่ออังกฤษ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <select class="form-control" id="prefix_en">
                                    <option></option>
                                </select>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">บ้านเลขที่</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="number"/>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">ชื่ออังกฤษ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="name_en"/>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">จังหวัด</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <select class="form-control" id="province">
                                    <option>กรุณาเลือกจังหวัด</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">นามสกุลอังกฤษ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="lastname_en"/>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">อำเภอ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <select class="form-control" id="district">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">วันเกิด</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" id="birthday" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">ตำบล</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <select class="form-control" id="sub_district">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เพศ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <select class="form-control" id="sex">
                                    <option>ชาย</option>
                                    <option>หญิง</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">รหัสไปรษณีย์</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="zipcode" disabled/>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">วันที่ออกบัตร</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" id="date_start" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">อีเมล</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" class="form-control" id="email" placeholder="abc@gmail.com"/>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">วันที่บัตรหมดอายุ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
                                <input type="text" id="date_exp" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                            </div>
                        </div>

                        {{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}
                        {{--                                    <label for="basicInput" class="subText">รหัสสินค้า</label>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="col-xl-10 col-md-6 col-12 mb-1">--}}
                        {{--                                    <span>G121102111243840</span>--}}
                        {{--                                </div>--}}

                        {{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}
                        {{--                                    <label for="basicInput" class="subText">น้ำหนัก</label>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}
                        {{--                                    <div class="form-group">--}}
                        {{--                                        <input type="text" class="form-control" id="basicInput"/>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}
                        {{--                                    <div class="form-group">--}}
                        {{--                                        <select class="form-control" id="basicSelect">--}}
                        {{--                                            <option>สลึง</option>--}}
                        {{--                                            <option>บาท</option>--}}
                        {{--                                        </select>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}

                        {{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}
                        {{--                                    <label for="basicInput" class="subText">น้ำหนักจริง</label>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}
                        {{--                                    <div class="form-group">--}}
                        {{--                                        <input type="text" class="form-control" id="basicInput"/>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}
                        {{--                                    <label for="basicInput" class="subText">กรัม (g)</label>--}}
                        {{--                                </div>--}}

                        {{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}
                        {{--                                    <label for="basicInput" class="subText">ราคารับซื้อ</label>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}
                        {{--                                    <div class="form-group">--}}
                        {{--                                        <input type="text" class="form-control" id="basicInput"/>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}
                        {{--                                    <label for="basicInput" class="subText">บาท</label>--}}
                        {{--                                </div>--}}

                        {{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}
                        {{--                                    <label for="basicInput" class="subText">ขนาด/ความยาว</label>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}
                        {{--                                    <div class="form-group">--}}
                        {{--                                        <input type="text" class="form-control" id="basicInput"/>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}
                        {{--                                </div>--}}
                        {{--                                <div class="col-xl-2 col-md-6 col-12 mb-1">--}}
                        {{--                                    <label for="basicInput" class="subText">รูปสินค้า</label>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="col-xl-8 col-md-6 col-12 mb-1">--}}
                        {{--                                    <form action="#" class="dropzone dropzone-area" id="dpz-single-file">--}}
                        {{--                                        <div class="dz-message">Drop files here or click to upload.</div>--}}
                        {{--                                    </form>--}}
                        {{--                                </div>--}}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary"  data-dismiss="modal">บันทึก</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">ปิด</button>
                </div>
            </div>
        </div>
    </div>
    <div
            class="modal fade text-left"
            id="detailModal"
            tabindex="-1"
            role="dialog"
            aria-labelledby="myModalLabel17"
            aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel17">ข้อมูลสมาชิก</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText text-danger">**สำหรับสมาชิกเก่ากรอกเพียงหมายเลขบัตรประชาชน หรือ เบอร์โทร</label>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เลขบัตรประชาชน</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
{{--                                <input type="text" class="form-control" id="id_card" placeholder="เฉพาะตัวเลขเท่านั้น"/>--}}
                                <span id="id_card">1 5044 00412 65 5<strong></strong></span>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เบอร์โทร</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
{{--                                <input type="text" class="form-control" id="telephone" placeholder="เฉพาะตัวเลขเท่านั้น"/>--}}
                                <span id="telephone">095 455 3031<strong></strong></span>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">คำนำหน้าชื่อ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
{{--                                <select class="form-control" id="prefix">--}}
{{--                                    <option>นาย</option>--}}
{{--                                    <option>นาง</option>--}}
{{--                                    <option>นางสาว</option>--}}
{{--                                </select>--}}
                                <span id="prefix">นาย<strong></strong></span>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">ไลน์ไอดี</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
{{--                                <input type="text" class="form-control" id="lineID" placeholder="@abc"/>--}}
                                <span id="prefix">pongston<strong></strong></span>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">ชื่อ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
{{--                                <input type="text" class="form-control" id="name_th"/>--}}
                                <span id="name_th">พงศธร<strong></strong></span>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เฟสบุ๊ค URL</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
{{--                                <input type="text" class="form-control" id="facebook" placeholder="www.facebook.com/abc"/>--}}
                                <span id="facebook">facebook.com/pongston<strong></strong></span>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">นามสกุล</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
{{--                                <input type="text" class="form-control" id="last_name"/>--}}
                                <span id="last_name">สนชาวไพร<strong></strong></span>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">คำนำหน้าชื่ออังกฤษ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
{{--                                <select class="form-control" id="prefix_en">--}}
{{--                                    <option></option>--}}
{{--                                </select>--}}
                                <span id="prefix_en">MR.<strong></strong></span>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">บ้านเลขที่</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
{{--                                <input type="text" class="form-control" id="number"/>--}}
                                <span id="address">149 หมู่ 5<strong></strong></span>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">ชื่ออังกฤษ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
{{--                                <input type="text" class="form-control" id="name_en"/>--}}
                                <span id="name_en">Pongston<strong></strong></span>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">จังหวัด</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
{{--                                <select class="form-control" id="province">--}}
{{--                                    <option>กรุณาเลือกจังหวัด</option>--}}
{{--                                </select>--}}
                                <span id="province">เชียงใหม่<strong></strong></span>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">นามสกุลอังกฤษ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
{{--                            <div class="form-group">--}}
{{--                                <input type="text" class="form-control" id="lastname_en"/>--}}
{{--                            </div>--}}
                            <span id="lastname_en">Sonchawpai<strong></strong></span>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">อำเภอ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
{{--                                <select class="form-control" id="district">--}}
{{--                                    <option></option>--}}
{{--                                </select>--}}
                                <span id="district">หางดง<strong></strong></span>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">วันเกิด</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
{{--                                <input type="text" id="birthday" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />--}}
                                <span id="birthday">1992-11-11<strong></strong></span>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">ตำบล</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
{{--                                <select class="form-control" id="sub_district">--}}
{{--                                    <option></option>--}}
{{--                                </select>--}}
                                <span id="sub_district">หนองควาย<strong></strong></span>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">เพศ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
{{--                                <select class="form-control" id="sex">--}}
{{--                                    <option>ชาย</option>--}}
{{--                                    <option>หญิง</option>--}}
{{--                                </select>--}}
                                <span id="sex">ชาย<strong></strong></span>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">รหัสไปรษณีย์</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
{{--                            <div class="form-group">--}}
{{--                                <input type="text" class="form-control" id="zipcode" disabled/>--}}
{{--                            </div>--}}
                            <span id="zipcode">50230<strong></strong></span>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">วันที่ออกบัตร</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
{{--                                <input type="text" id="date_start" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />--}}
                                <span id="date_start">2021-11-11<strong></strong></span>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">อีเมล</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
{{--                                <input type="text" class="form-control" id="email" placeholder="abc@gmail.com"/>--}}
                                <span id="email">pongston@gmail.com<strong></strong></span>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <label for="basicInput" class="subText">วันที่บัตรหมดอายุ</label>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12 mb-1">
                            <div class="form-group">
{{--                                <input type="text" id="date_exp" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />--}}
                                <span id="date_exp">2026-11-11<strong></strong></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary"  data-dismiss="modal">บันทึก</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">ปิด</button>
                </div>
            </div>
        </div>
    </div>
@endsection





@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/jszip.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.rowGroup.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
@endsection

@section('page-script')
    {{-- Page js files --}}
    {{--  <script src="{{ asset(mix('js/scripts/tables/table-datatables-basic.js')) }}"></script>--}}
    <script>
        /**
         * DataTables Basic
         */
        $(function () {
            'use strict';
            var dt_basic_table = $('.datatables-basic'),
                dt_date_table = $('.dt-date'),
                assetPath = '../../../app-assets/';
            if ($('body').attr('data-framework') === 'laravel') {
                assetPath = $('body').attr('data-asset-path');
            }

            // DataTable with buttons
            // --------------------------------------------------------------------
            if (dt_basic_table.length) {
                var dt_basic = dt_basic_table.DataTable({
                    ajax: assetPath + 'data/table-datatable-cost.json',
                    columns: [
                        { data: 'responsive_id' },
                        { data: 'id' },
                        { data: 'id' }, // used for sorting so will hide this column
                        { data: 'full_name' },
                        { data: 'email' },
                        { data: 'start_date' },
                        { data: 'post' },
                        { data: 'salary' },
                        { data: '' }
                    ],

                    columnDefs: [
                        {
                            // For Responsive
                            className: 'control',
                            orderable: false,
                            responsivePriority: 2,
                            targets: 0
                        },
                        {
                            // For Checkboxes
                            targets: 1,
                            orderable: false,
                            responsivePriority: 3,
                            render: function (data, type, full, meta) {
                                return (
                                    '<div class="custom-control custom-checkbox"> <input class="custom-control-input dt-checkboxes" type="checkbox" value="" id="checkbox' +
                                    data +
                                    '" /><label class="custom-control-label" for="checkbox' +
                                    data +
                                    '"></label></div>'
                                );
                            },
                            checkboxes: {
                                selectAllRender:
                                    '<div class="custom-control custom-checkbox"> <input class="custom-control-input" type="checkbox" value="" id="checkboxSelectAll" /><label class="custom-control-label" for="checkboxSelectAll"></label></div>'
                            }
                        },
                        {
                            targets: 2,
                            visible: false
                        },
                        {
                            responsivePriority: 1,
                            targets: 4
                        },
                        // {
                        //     // Label
                        //     targets: -2,
                        //     render: function (data, type, full, meta) {
                        //         var $status_number = full['status'];
                        //         var $status = {
                        //             1: { title: 'อยู่ในสัญญา', class: 'badge-light-success' },
                        //             2: { title: 'เลยกำหนดชำระ', class: 'badge-light-danger ' },
                        //             3: { title: 'ไถ่ถอนแล้ว', class: ' badge-light-warning' },
                        //             4: { title: 'Resigned', class: ' badge-light-primary' },
                        //             5: { title: 'Applied', class: ' badge-light-info' }
                        //         };
                        //         if (typeof $status[$status_number] === 'undefined') {
                        //             return data;
                        //         }
                        //         return (
                        //             '<span class="badge badge-pill ' +
                        //             $status[$status_number].class +
                        //             '">' +
                        //             $status[$status_number].title +
                        //             '</span>'
                        //         );
                        //     }
                        // },
                        {
                            // Actions
                            targets: -1,
                            title: 'จัดการข้อมูล',
                            orderable: false,
                            render: function (data, type, full, meta) {
                                /*return (
                                    '<div class="d-inline-flex">' +
                                    '<a class="pr-1 dropdown-toggle hide-arrow text-primary" data-toggle="dropdown">' +
                                    feather.icons['more-vertical'].toSvg({ class: 'font-small-4' }) +
                                    '</a>' +
                                    '<div class="dropdown-menu dropdown-menu-right">' +
                                    '<a href="javascript:;" class="dropdown-item">' +
                                    feather.icons['file-text'].toSvg({ class: 'font-small-4 mr-50' }) +
                                    'Details</a>' +
                                    '<a href="javascript:;" class="dropdown-item">' +
                                    feather.icons['archive'].toSvg({ class: 'font-small-4 mr-50' }) +
                                    'Archive</a>' +
                                    '<a href="javascript:;" class="dropdown-item delete-record">' +
                                    feather.icons['trash-2'].toSvg({ class: 'font-small-4 mr-50' }) +
                                    'Delete</a>' +
                                    '</div>' +
                                    '</div>' +
                                    '<a href="javascript:;" class="item-edit">' +
                                    feather.icons['edit'].toSvg({ class: 'font-small-4' }) +
                                    '</a>'
                                );*/
                                return (
                                    '<a href="{{ url('/cost/edit') }}" class="item-edit">' +
                                    feather.icons['edit'].toSvg({ class: 'font-medium-5' }) +
                                    '</a>' + ' ' +
                                    '<a href="{{ url('/cost/detail') }}" class="item-edit">' +
                                    feather.icons['eye'].toSvg({ class: 'font-medium-5' }) +
                                    '</a>' + ' ' +
                                    '<a href="javascript:;" class="item-edit" onclick="confirm_al()">' +
                                    feather.icons['trash-2'].toSvg({ class: 'font-medium-5' }) +
                                    '</a>'
                                );
                            }
                        }
                    ],
                    order: [[2, 'asc']],
                    dom:
                        '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-right"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                    displayLength: 7,
                    lengthMenu: [7, 10, 25, 50, 75, 100],
                    buttons: [
                        {
                            text: 'เพิ่มค่าใช้จ่าย',
                            className: 'create-new btn btn-primary mr-50',
                            attr: {
                                // 'data-toggle': 'modal',
                                // 'data-target': '#addModal'
                                'onclick': 'javascript:window.location.href = "{{ url('/cost/create') }}";'
                            },
                            init: function (api, node, config) {
                                $(node).removeClass('btn-secondary');
                            }
                        },
                        {
                            extend: 'collection',
                            className: 'btn btn-outline-secondary dropdown-toggle',
                            text: feather.icons['share'].toSvg({ class: 'font-small-4 mr-50' }) + 'Export',
                            buttons: [
                                {
                                    extend: 'print',
                                    text: feather.icons['printer'].toSvg({ class: 'font-small-4 mr-50' }) + 'Print',
                                    className: 'dropdown-item',
                                    exportOptions: { columns: [3, 4, 5, 6, 7] }
                                },
                                {
                                    extend: 'csv',
                                    text: feather.icons['file-text'].toSvg({ class: 'font-small-4 mr-50' }) + 'Csv',
                                    className: 'dropdown-item',
                                    exportOptions: { columns: [3, 4, 5, 6, 7] }
                                },
                                {
                                    extend: 'excel',
                                    text: feather.icons['file'].toSvg({ class: 'font-small-4 mr-50' }) + 'Excel',
                                    className: 'dropdown-item',
                                    exportOptions: { columns: [3, 4, 5, 6, 7] }
                                },
                                {
                                    extend: 'pdf',
                                    text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 mr-50' }) + 'Pdf',
                                    className: 'dropdown-item',
                                    exportOptions: { columns: [3, 4, 5, 6, 7] }
                                },
                                {
                                    extend: 'copy',
                                    text: feather.icons['copy'].toSvg({ class: 'font-small-4 mr-50' }) + 'Copy',
                                    className: 'dropdown-item',
                                    exportOptions: { columns: [3, 4, 5, 6, 7] }
                                }
                            ],
                            init: function (api, node, config) {
                                $(node).removeClass('btn-secondary');
                                $(node).parent().removeClass('btn-group');
                                setTimeout(function () {
                                    $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                                }, 50);
                            },
                        }
                    ],
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Details of ' + data['full_name'];
                                }
                            }),
                            type: 'column',
                            renderer: function (api, rowIdx, columns) {
                                var data = $.map(columns, function (col, i) {
                                    return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                                        ? '<tr data-dt-row="' +
                                        col.rowIndex +
                                        '" data-dt-column="' +
                                        col.columnIndex +
                                        '">' +
                                        '<td>' +
                                        col.title +
                                        ':' +
                                        '</td> ' +
                                        '<td>' +
                                        col.data +
                                        '</td>' +
                                        '</tr>'
                                        : '';
                                }).join('');
                                return data ? $('<table class="table"/>').append(data) : false;
                            }
                        }
                    },
                    language: {
                        paginate: {
                            // remove previous & next text from pagination
                            previous: '&nbsp;',
                            next: '&nbsp;'
                        }
                    }
                });
                $('div.head-label').html('<h6 class="card-title mb-0">ระบบจัดการรายการค่าใช้จ่าย</h6>');
            }

            // Flat Date picker
            if (dt_date_table.length) {
                dt_date_table.flatpickr({
                    monthSelectorType: 'static',
                    dateFormat: 'm/d/Y'
                });
            }
        });

        function confirm_al() {
            //--------------- Confirm Options ---------------
            Swal.fire({
                title: 'คุณแน่ใจหรือไม่ที่จะลบรายการนี้?',
                text: "เมื่อดำเนินการแล้วจะไม่สามารถกู้คืนกลับมาได้!",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'ยกเลิก',
                confirmButtonText: 'ยืนยัน',
                customClass: {
                    confirmButton: 'btn btn-primary',
                    cancelButton: 'btn btn-outline-danger ml-1'
                },
                buttonsStyling: false
            }).then(function (result) {
                if (result.value) {
                    Swal.fire({
                        icon: 'success',
                        title: 'ลบรายการเรียบร้อย!',
                        text: 'รายการดังกล่าวได้ถูกลบเรียบร้อยแล้ว.',
                        customClass: {
                            confirmButton: 'btn btn-success'
                        }
                    });
                }
            });
        }
    </script>
@endsection
