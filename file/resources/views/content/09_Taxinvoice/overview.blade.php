@extends('layouts/contentLayoutMasterMain')

{{--@section('title', 'Statistics Cards')--}}


@section('vendor-style')

    {{-- Vendor Css files --}}
{{--    <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{asset('vendors/animate-css/animate.css')}}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{asset('vendors/chartist-js/chartist.min.css')}}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{asset('vendors/chartist-js/chartist-plugin-tooltip.css')}}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{asset('js/jquery-ui/jquery-ui.css')}}">--}}
@endsection
@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/charts/chart-apex.css')) }}">
@endsection

@section('content')
    <style>
        #typeGlod {
            width: 100%;
            height: 300px;
            font-size: 14px;
        }
        #TincomeandTcost {
            width: 100%;
            height: 500px;
            font-size: 14px;
        }
        #totalincome {
            width: 100%;
            height: 300px;
            font-size: 14px;
        }
        #Categorycost {
            width: 100%;
            height: 300px;
            font-size: 14px;
        }
        #Totalcost {
            width: 100%;
            height: 300px;
            font-size: 14px;
        }

    </style>
    <!-- Statistics card section -->
    <section id="statistics-card">
        <!-- Miscellaneous Charts -->
        <div class="row">
            <div class="col-lg-6 col-12">
                <div class="card" style="height: 400px;">
                    <div class="card-header">
                        <h4 class="card-title">อัตราส่วนรายได้จากบริการ (%)</h4>
                        <div class="d-flex align-items-center">
                            <p class="card-text mb-0">
                                <select class="form-control input-group input-group-merge invoice-edit-input-group" id="basicSelect">
                                    <option>ปีนี้</option>
                                </select>
                            </p>
                        </div>
                    </div>
                    <div class="card-body" >
                        <div class="row">
                            {{--                            <div class="col-md-3 col-sm-6 col-12 mb-2 mb-md-0">--}}
                            <div id="typeGlod"></div>
                            {{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-12">
                <div class="card" style="height: 400px;">
                    <div class="card-header">
                        <h4 class="card-title">รายได้รวม (บาท)</h4>
                        <div class="d-flex align-items-center">
                            <p class="card-text mb-0">
                                <select class="form-control input-group input-group-merge invoice-edit-input-group" id="basicSelect">
                                    <option>ปีนี้</option>
                                </select>
                            </p>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div id="totalincome"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-12">
                <div class="card" style="height: 400px;">
                    <div class="card-header">
                        <h4 class="card-title">อัตราส่วนรายจ่ายจากหมวดหมู่ (%)</h4>
                        <div class="d-flex align-items-center">
                            <p class="card-text mb-0">
                                <select class="form-control input-group input-group-merge invoice-edit-input-group" id="basicSelect">
                                    <option>ปีนี้</option>
                                </select>
                            </p>
                        </div>
                    </div>
                    <div class="card-body" >
                        <div class="row">
                            {{--                            <div class="col-md-3 col-sm-6 col-12 mb-2 mb-md-0">--}}
                            <div id="Categorycost"></div>
                            {{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-12">
                <div class="card" style="height: 400px;">
                    <div class="card-header">
                        <h4 class="card-title">ค่าใช้จ่ายรวม (บาท)</h4>
                        <div class="d-flex align-items-center">
                            <p class="card-text mb-0">
                                <select class="form-control input-group input-group-merge invoice-edit-input-group" id="basicSelect">
                                    <option>ปีนี้</option>
                                </select>
                            </p>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div id="Totalcost"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">เปรียบเทียบรายได้รวมกับค่าใช้จ่ายรวม (บาท)</h4>
                        <div class="d-flex align-items-center">
                            <p class="card-text mb-0">
                                <select class="form-control input-group input-group-merge invoice-edit-input-group" id="basicSelect">
                                    <option>ปีนี้</option>
                                </select>
                            </p>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div id="TincomeandTcost"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!--/ Miscellaneous Charts -->

        <!-- Stats Vertical Card -->

        <!--/ Line Chart Card -->
    </section>


    <!--/ Statistics Card section-->
@endsection

@section('vendor-script')
    {{-- Vendor js files --}}
{{--    <script src="{{ asset(mix('vendors/js/jquery/jquery.min.js')) }}"></script>--}}
{{--        <script src="{{ asset('vendors/js/jquery/jquery.min.js') }}"></script>--}}
@endsection
@section('page-script')
    {{-- Page js files --}}
{{--    <script src="{{ asset(mix('js/scripts/cards/card-statistics.js')) }}"></script>--}}
@endsection
<script src="{{ asset('vendors/js/jquery/jquery.min.js') }}"></script>
<script type='text/javascript' src='https://cdn.amcharts.com/lib/4/core.js?ver=20210901-5e9' id='-lib-4-core-js-js'></script>
<script type='text/javascript' src='https://cdn.amcharts.com/lib/4/charts.js?ver=20210901-5e9' id='-lib-4-charts-js-js'></script>
<script type='text/javascript' src='https://cdn.amcharts.com/lib/4/themes/dataviz.js?ver=20210901-5e9' id='-lib-4-themes-dataviz-js-js'></script>
<script type='text/javascript' src='https://cdn.amcharts.com/lib/4/themes/material.js?ver=20210901-5e9' id='-lib-4-themes-material-js-js'></script>
<script type='text/javascript' src='https://cdn.amcharts.com/lib/4/themes/kelly.js?ver=20210901-5e9' id='-lib-4-themes-kelly-js-js'></script>
<script type='text/javascript' src='https://cdn.amcharts.com/lib/4/themes/dark.js?ver=20210901-5e9' id='-lib-4-themes-dark-js-js'></script>
<script type='text/javascript' src='https://cdn.amcharts.com/lib/4/themes/frozen.js?ver=20210901-5e9' id='-lib-4-themes-frozen-js-js'></script>
<script type='text/javascript' src='https://cdn.amcharts.com/lib/4/themes/moonrisekingdom.js?ver=20210901-5e9' id='-lib-4-themes-moonrisekingdom-js-js'></script>
<script type='text/javascript' src='https://cdn.amcharts.com/lib/4/themes/spiritedaway.js?ver=20210901-5e9' id='-lib-4-themes-spiritedaway-js-js'></script>
<script type='text/javascript' src='https://cdn.amcharts.com/lib/4/themes/animated.js?ver=20210901-5e9' id='-lib-4-themes-animated-js-js'></script>


<script>
    $(document).ready(function(){
        //alert('test');
        TypeGlod();
        TotalIncome();
        CategoryCost();
        TotalCost();
        TotalIncomeandTotalCost();
    });

    function TypeGlod() {

        try {
            // Themes begin
            // am4core.useTheme(am4themes_kelly);
            am4core.useTheme(am4themes_material);
            am4core.useTheme(am4themes_animated);
            // Themes end

            /**
             * Define data for each year
             */


                // Create chart instance
            var chart = am4core.create("typeGlod", am4charts.PieChart);
            chart.logo.disabled = true;
            // Add data
            chart.data = [
                { "sector": "จำนำ", "size": 21.7 },
                { "sector": "ออม", "size": 34.8 },
                { "sector": "ซื้อ", "size": 17.4 },
                { "sector": "ขาย", "size": 26.1 }

            ];


            // Add label
            chart.innerRadius = 50;

            // Add and configure Series
            var pieSeries = chart.series.push(new am4charts.PieSeries());
            pieSeries.dataFields.value = "size";
            pieSeries.dataFields.category = "sector";

            // Animate chart data
        }
        catch( e ) {
            console.log( e );
        }
    }

    function TotalIncome() {

        try {
            // Themes begin
            am4core.useTheme(am4themes_material);
            am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
            var chart = am4core.create("totalincome", am4charts.XYChart);
            chart.scrollbarX = new am4core.Scrollbar();
            chart.logo.disabled = true;

// Add data
            chart.data = [{
                "country": "Jan",
                "visits": 3025
            }, {
                "country": "Feb",
                "visits": 2500
            }, {
                "country": "Mar",
                "visits": 1500
            }, {
                "country": "Apr",
                "visits": 3500
            }, {
                "country": "May",
                "visits": 1800
            }, {
                "country": "Jun",
                "visits": 1200
            }, {
                "country": "Jul",
                "visits": 2200
            }, {
                "country": "Aug",
                "visits": 1900
            }];

// Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "country";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 30;
            categoryAxis.renderer.labels.template.horizontalCenter = "right";
            categoryAxis.renderer.labels.template.verticalCenter = "middle";
            categoryAxis.renderer.labels.template.rotation = 270;
            categoryAxis.tooltip.disabled = true;
            categoryAxis.renderer.minHeight = 110;

            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.renderer.minWidth = 50;

// Create series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.sequencedInterpolation = true;
            series.dataFields.valueY = "visits";
            series.dataFields.categoryX = "country";
            series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
            series.columns.template.strokeWidth = 0;

            series.tooltip.pointerOrientation = "vertical";

            series.columns.template.column.cornerRadiusTopLeft = 10;
            series.columns.template.column.cornerRadiusTopRight = 10;
            series.columns.template.column.fillOpacity = 0.8;

// on hover, make corner radiuses bigger
            var hoverState = series.columns.template.column.states.create("hover");
            hoverState.properties.cornerRadiusTopLeft = 0;
            hoverState.properties.cornerRadiusTopRight = 0;
            hoverState.properties.fillOpacity = 1;

            series.columns.template.adapter.add("fill", function(fill, target) {
                return chart.colors.getIndex(target.dataItem.index);
            });


            chart.cursor = new am4charts.XYCursor();            }
        catch( e ) {
            console.log( e );
        }
    }

    function CategoryCost() {

        try {
            // Themes begin
            am4core.useTheme(am4themes_kelly);
            am4core.useTheme(am4themes_animated);
            // Themes end

            /**
             * Define data for each year
             */


                // Create chart instance
            var chart = am4core.create("Categorycost", am4charts.PieChart);
            chart.logo.disabled = true;
            // Add data
            chart.data = [
                { "sector": "ออฟฟิศ", "size": 34.0 },
                { "sector": "สินค้า", "size": 18.9 },
                { "sector": "เงินเดือน", "size": 47.2 }

            ];


            // Add label
            chart.innerRadius = 50;

            // Add and configure Series
            var pieSeries = chart.series.push(new am4charts.PieSeries());
            pieSeries.dataFields.value = "size";
            pieSeries.dataFields.category = "sector";

            // Animate chart data
        }
        catch( e ) {
            console.log( e );
        }
    }

    function TotalCost() {

        try {
            // Themes begin
            am4core.useTheme(am4themes_kelly);
            am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
            var chart = am4core.create("Totalcost", am4charts.XYChart);
            chart.scrollbarX = new am4core.Scrollbar();
            chart.logo.disabled = true;

// Add data
            chart.data = [{
                "country": "Jan",
                "visits": 2800
            }, {
                "country": "Feb",
                "visits": 1200
            }, {
                "country": "Mar",
                "visits": 3000
            }, {
                "country": "Apr",
                "visits": 1500
            }, {
                "country": "May",
                "visits": 3200
            }, {
                "country": "Jun",
                "visits": 3800
            }, {
                "country": "Jul",
                "visits": 1500
            }, {
                "country": "Aug",
                "visits": 1000
            }];

// Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "country";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 30;
            categoryAxis.renderer.labels.template.horizontalCenter = "right";
            categoryAxis.renderer.labels.template.verticalCenter = "middle";
            categoryAxis.renderer.labels.template.rotation = 270;
            categoryAxis.tooltip.disabled = true;
            categoryAxis.renderer.minHeight = 110;

            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.renderer.minWidth = 50;

// Create series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.sequencedInterpolation = true;
            series.dataFields.valueY = "visits";
            series.dataFields.categoryX = "country";
            series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
            series.columns.template.strokeWidth = 0;

            series.tooltip.pointerOrientation = "vertical";

            series.columns.template.column.cornerRadiusTopLeft = 10;
            series.columns.template.column.cornerRadiusTopRight = 10;
            series.columns.template.column.fillOpacity = 0.8;

// on hover, make corner radiuses bigger
            var hoverState = series.columns.template.column.states.create("hover");
            hoverState.properties.cornerRadiusTopLeft = 0;
            hoverState.properties.cornerRadiusTopRight = 0;
            hoverState.properties.fillOpacity = 1;

            series.columns.template.adapter.add("fill", function(fill, target) {
                return chart.colors.getIndex(target.dataItem.index);
            });


            chart.cursor = new am4charts.XYCursor();            }
        catch( e ) {
            console.log( e );
        }
    }

    //Sales by time
    function TotalIncomeandTotalCost() {

        try {
            // Themes begin
            am4core.useTheme(am4themes_dataviz);
            am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
                    var chart = am4core.create("TincomeandTcost", am4charts.XYChart);

                    chart.logo.disabled = true;
                    // chart.logo.height = -15;

//

// Increase contrast by taking evey second color
                    chart.colors.step = 2;

// Add data
                    chart.data = generateChartData();

// Create axes
                    var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
                    dateAxis.renderer.minGridDistance = 50;

// Create series
                    function createAxisAndSeries(field, name, opposite, bullet) {
                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                        if(chart.yAxes.indexOf(valueAxis) != 0){
                            valueAxis.syncWithAxis = chart.yAxes.getIndex(0);
                        }

                        var series = chart.series.push(new am4charts.LineSeries());
                        series.dataFields.valueY = field;
                        series.dataFields.dateX = "date";
                        series.strokeWidth = 2;
                        series.yAxis = valueAxis;
                        series.name = name;
                        series.tooltipText = "{name}: [bold]{valueY}[/]";
                        series.tensionX = 0.8;
                        series.showOnInit = true;

                        var interfaceColors = new am4core.InterfaceColorSet();

                        switch(bullet) {
                            case "triangle":
                                var bullet = series.bullets.push(new am4charts.Bullet());
                                bullet.width = 12;
                                bullet.height = 12;
                                bullet.horizontalCenter = "middle";
                                bullet.verticalCenter = "middle";

                                var triangle = bullet.createChild(am4core.Triangle);
                                triangle.stroke = interfaceColors.getFor("background");
                                triangle.strokeWidth = 2;
                                triangle.direction = "top";
                                triangle.width = 12;
                                triangle.height = 12;
                                break;
                            case "rectangle":
                                var bullet = series.bullets.push(new am4charts.Bullet());
                                bullet.width = 10;
                                bullet.height = 10;
                                bullet.horizontalCenter = "middle";
                                bullet.verticalCenter = "middle";

                                var rectangle = bullet.createChild(am4core.Rectangle);
                                rectangle.stroke = interfaceColors.getFor("background");
                                rectangle.strokeWidth = 2;
                                rectangle.width = 10;
                                rectangle.height = 10;
                                break;
                            default:
                                var bullet = series.bullets.push(new am4charts.CircleBullet());
                                bullet.circle.stroke = interfaceColors.getFor("background");
                                bullet.circle.strokeWidth = 2;
                                break;
                        }

                        valueAxis.renderer.line.strokeOpacity = 1;
                        valueAxis.renderer.line.strokeWidth = 2;
                        valueAxis.renderer.line.stroke = series.stroke;
                        valueAxis.renderer.labels.template.fill = series.stroke;
                        valueAxis.renderer.opposite = opposite;
                    }

                    createAxisAndSeries("visits", "รายได้", false, "circle");
                    createAxisAndSeries("hits", "ค่าใช้จ่าย", true, "circle");

// Add legend
                    chart.legend = new am4charts.Legend();

// Add cursor
                    chart.cursor = new am4charts.XYCursor();

// generate some random data, quite different range
                    function generateChartData() {
                        var chartData = [];
                        var firstDate = new Date();
                        firstDate.setDate(firstDate.getDate() - 100);
                        firstDate.setHours(0, 0, 0, 0);
                        //
                        var visits = 1600;
                        var hits = 2900;
                        for (var i = 0; i < 50; i++) {
                            var newDate = new Date();
                            newDate.setHours(0,0,0,0);
                            newDate.setDate(newDate.getDate() + i);

                            visits += Math.round((Math.random()<0.5?1:-1)*Math.random()*10);
                            hits += Math.round((Math.random()<0.5?1:-1)*Math.random()*10);

                            //console.log(newDate);
                            chartData.push({
                                date: newDate,
                                visits: visits,
                                hits: hits
                            });
                        }
                        return chartData;
                        }
            //console.log(chartData);

                    } catch( e ) {
            console.log( e );
        }
    }
</script>
@section('vendor-script')


@endsection
