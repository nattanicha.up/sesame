@extends('layouts/contentLayoutMasterMain')

{{--@section('title', 'Statistics Cards')--}}


@section('vendor-style')

    {{-- Vendor Css files --}}
{{--    <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{asset('vendors/animate-css/animate.css')}}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{asset('vendors/chartist-js/chartist.min.css')}}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{asset('vendors/chartist-js/chartist-plugin-tooltip.css')}}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{asset('js/jquery-ui/jquery-ui.css')}}">--}}
@endsection
@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/charts/chart-apex.css')) }}">
@endsection

@section('content')

    <h1>Hello</h1>
    <!--/ Statistics Card section-->
@endsection

@section('vendor-script')
    {{-- Vendor js files --}}
{{--    <script src="{{ asset(mix('vendors/js/jquery/jquery.min.js')) }}"></script>--}}
{{--        <script src="{{ asset('vendors/js/jquery/jquery.min.js') }}"></script>--}}
@endsection
@section('page-script')
    {{-- Page js files --}}
{{--    <script src="{{ asset(mix('js/scripts/cards/card-statistics.js')) }}"></script>--}}
@endsection
<script src="{{ asset('vendors/js/jquery/jquery.min.js') }}"></script>
<script type='text/javascript' src='{{ asset('js/amcharts/core.js?ver=20210901-5e9') }}' id='-lib-4-core-js-js'></script>
<script type='text/javascript' src='{{ asset('js/amcharts/charts.js?ver=20210901-5e9') }}' id='-lib-4-charts-js-js'></script>
<script type='text/javascript' src='{{ asset('js/amcharts/dataviz.js?ver=20210901-5e9') }}' id='-lib-4-themes-dataviz-js-js'></script>
<script type='text/javascript' src='{{ asset('js/amcharts/material.js?ver=20210901-5e9') }}' id='-lib-4-themes-material-js-js'></script>
<script type='text/javascript' src='{{ asset('js/amcharts/kelly.js?ver=20210901-5e9') }}' id='-lib-4-themes-kelly-js-js'></script>
<script type='text/javascript' src='{{ asset('js/amcharts/dark.js?ver=20210901-5e9') }}' id='-lib-4-themes-dark-js-js'></script>
<script type='text/javascript' src='{{ asset('js/amcharts/frozen.js?ver=20210901-5e9') }}' id='-lib-4-themes-frozen-js-js'></script>
<script type='text/javascript' src='{{ asset('js/amcharts/moonrisekingdom.js?ver=20210901-5e9') }}' id='-lib-4-themes-moonrisekingdom-js-js'></script>
<script type='text/javascript' src='{{ asset('js/amcharts/spiritedaway.js?ver=20210901-5e9') }}' id='-lib-4-themes-spiritedaway-js-js'></script>
<script type='text/javascript' src='{{ asset('js/amcharts/animated.js?ver=20210901-5e9') }}' id='-lib-4-themes-animated-js-js'></script>


@section('vendor-script')


@endsection

