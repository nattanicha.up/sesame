<link rel="stylesheet" href="{{ asset(mix('vendors/css/vendors.min.css')) }}" />
<link rel="stylesheet" href="{{ asset(mix('vendors/css/ui/prism.min.css')) }}" />
{{-- Vendor Styles --}}
@yield('vendor-style')
{{-- Theme Styles --}}

<link rel="stylesheet" href="{{ asset(mix('css/core.css')) }}" />

{{-- {!! Helper::applClasses() !!} --}}
@php $configData = Helper::applClasses(); @endphp

{{-- Page Styles --}}
@if($configData['mainLayoutType'] === 'horizontal')
<link rel="stylesheet" href="{{ asset(mix('css/base/core/menu/menu-types/horizontal-menu.css')) }}" />
@endif
<link rel="stylesheet" href="{{ asset(mix('css/base/core/menu/menu-types/vertical-menu.css')) }}" />
<!-- <link rel="stylesheet" href="{{ asset(mix('css/base/core/colors/palette-gradient.css')) }}"> -->

{{-- Page Styles --}}
@yield('page-style')

{{-- Laravel Style --}}
<link rel="stylesheet" href="{{ asset(mix('css/overrides.css')) }}" />

{{-- Custom RTL Styles --}}

@if($configData['direction'] === 'rtl' && isset($configData['direction']))
<link rel="stylesheet" href="{{ asset(mix('css-rtl/custom-rtl.css')) }}" />
<link rel="stylesheet" href="{{ asset(mix('css-rtl/style-rtl.css')) }}" />
@endif

{{-- user custom styles --}}
<link rel="stylesheet" href="{{ asset(mix('css/style.css')) }}" />

<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Prompt&display=swap" rel="stylesheet">

<style>
    body{
        font-family: 'Prompt', sans-serif;
        font-size: 16px;
    }
    header{
        font-family: 'Prompt', sans-serif;
    }
    .btn{
        font-family: 'Prompt', sans-serif;
    }
    h1, h2, h3, h4, h5{
        font-family: 'Prompt', sans-serif;
    }
    .menu-title{
        font-family: 'Prompt', sans-serif;
    }
    .menu-item{
        font-family: 'Prompt', sans-serif;
    }
    .price-notify-row{
        padding: 0.9rem 1.28rem !important;
        border-bottom: 1px solid #ebe9f1;
    }
    .price-notify-title{
        margin-bottom: 0px;
        color: #6e6b7b;
    }
    .price-notify-date{
        color: #b9b9c3;
    }
</style>
