{{-- Vendor Scripts --}}
<script src="{{ asset(mix('vendors/js/vendors.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/ui/prism.min.js')) }}"></script>
@yield('vendor-script')
{{-- Theme Scripts --}}
<script src="{{ asset(mix('js/core/app-menu.js')) }}"></script>
<script src="{{ asset(mix('js/core/app.js')) }}"></script>
@if($configData['blankPage'] === false)
<script src="{{ asset(mix('js/scripts/customizer.js')) }}"></script>
@endif
{{-- page script --}}
@yield('page-script')
{{-- page script --}}
{{-- price notification script --}}
<script src="{{ asset('js/numeral/src/numeral.js') }}"></script>
<script>
    $(document).ready(function () {
        priceNotification();
    });

    function priceNotification() {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url('notification/price') }}',
            type: "GET",
            enctype: 'multipart/form-data',
            dataType: 'JSON',
            processData: false,
            contentType: false,
            cache: false,
            success: function (res) {
                res.notify ? $('#priceNotifyStatus').attr("hidden", false) : $('#priceNotifyStatus').attr("hidden", true);
                res.notify ? $('#priceNotifyModal').modal('show') : '';
                $('.priceNotifyDate').html(res.date);

                // if(res.data[0].goldbar_buy > res.data[1].goldbar_buy) {
                //     $('#goldbarBuyArrow').html('<div class="avatar bg-light-success">\n' +
                //         '<div class="avatar-content"><i class="avatar-icon" data-feather="arrow-up"></i></div>\n' +
                //         '</div>');
                //     $('#goldbarBuyPrice').html('<div class="font-weight-bolder text-success">' + numeral(res.data[0].goldbar_buy).format('0,0.00') + ' บาท</div>');
                // }
                // else if(res.data[0].goldbar_buy < res.data[1].goldbar_buy) {
                //     $('#goldbarBuyArrow').html('<div class="avatar bg-light-danger">\n' +
                //         '<div class="avatar-content"><i class="avatar-icon" data-feather="arrow-down"></i></div>\n' +
                //         '</div>');
                //     $('#goldbarBuyPrice').html('<div class="font-weight-bolder text-danger">' + numeral(res.data[0].goldbar_buy).format('0,0.00') + ' บาท</div>');
                // }
                // else {
                //     $('#goldbarBuyArrow').html('<div class="avatar bg-light-secondary">\n' +
                //         '<div class="avatar-content"><i class="avatar-icon" data-feather="arrow-right"></i></div>\n' +
                //         '</div>');
                //     $('#goldbarBuyPrice').html('<div class="font-weight-bolder text-secondary">' + numeral(res.data[0].goldbar_buy).format('0,0.00') + ' บาท</div>');
                // }
                // feather.replace();

                priceNotificationLabel('goldbarBuy', res.data[0].goldbar_buy, res.data[1].goldbar_buy);
                priceNotificationLabel('goldBuy', res.data[0].gold_buy, res.data[1].gold_buy);
                priceNotificationLabel('goldbarSell', res.data[0].goldbar_sell, res.data[1].goldbar_sell);
                priceNotificationLabel('goldSell', res.data[0].gold_sell, res.data[1].gold_sell);

                priceNotificationLabelModal('goldbarBuy', res.data[0].goldbar_buy, res.data[1].goldbar_buy);
                priceNotificationLabelModal('goldBuy', res.data[0].gold_buy, res.data[1].gold_buy);
                priceNotificationLabelModal('goldbarSell', res.data[0].goldbar_sell, res.data[1].goldbar_sell);
                priceNotificationLabelModal('goldSell', res.data[0].gold_sell, res.data[1].gold_sell);

                $('#goldbarBuyPriceBox').html(numeral(res.data[0].goldbar_buy, res.data[1].goldbar_buy).format('0,0'));
                $('#goldBuyPriceBox').html(numeral(res.data[0].gold_buy, res.data[1].gold_buy).format('0,0'));
                $('#goldbarSellPriceBox').html(numeral(res.data[0].goldbar_sell, res.data[1].goldbar_sell).format('0,0'));
                $('#goldSellPriceBox').html(numeral(res.data[0].gold_sell, res.data[1].gold_sell).format('0,0'));
            }
        });
    }

    function priceNotificationLabel(ele, curr, perv) {
        if(curr > perv) {
            $('#' + ele + 'Arrow').html('<div class="avatar bg-light-success">\n' +
                '<div class="avatar-content"><i class="avatar-icon" data-feather="arrow-up"></i></div>\n' +
                '</div>');
            $('#' + ele + 'Price').html('<div class="font-weight-bolder text-success">' + numeral(curr).format('0,0.00') + ' บาท</div>');
        }
        else if(curr < perv) {
            $('#' + ele + 'Arrow').html('<div class="avatar bg-light-danger">\n' +
                '<div class="avatar-content"><i class="avatar-icon" data-feather="arrow-down"></i></div>\n' +
                '</div>');
            $('#' + ele + 'Price').html('<div class="font-weight-bolder text-danger">' + numeral(curr).format('0,0.00') + ' บาท</div>');
        }
        else {
            $('#' + ele + 'Arrow').html('<div class="avatar bg-light-secondary">\n' +
                '<div class="avatar-content"><i class="avatar-icon" data-feather="arrow-right"></i></div>\n' +
                '</div>');
            $('#' + ele + 'Price').html('<div class="font-weight-bolder text-secondary">' + numeral(curr).format('0,0.00') + ' บาท</div>');
        }
        feather.replace();
    }

    function priceNotificationLabelModal(ele, curr, perv) {
        if(curr > perv) {
            $('#' + ele + 'ArrowModal').html('<div class="avatar bg-light-success">\n' +
                '<div class="avatar-content"><i class="avatar-icon" data-feather="arrow-up"></i></div>\n' +
                '</div>');
            $('#' + ele + 'PriceModal').html('<div class="font-weight-bolder text-success">' + numeral(curr).format('0,0.00') + ' บาท</div>');
        }
        else if(curr < perv) {
            $('#' + ele + 'ArrowModal').html('<div class="avatar bg-light-danger">\n' +
                '<div class="avatar-content"><i class="avatar-icon" data-feather="arrow-down"></i></div>\n' +
                '</div>');
            $('#' + ele + 'PriceModal').html('<div class="font-weight-bolder text-danger">' + numeral(curr).format('0,0.00') + ' บาท</div>');
        }
        else {
            $('#' + ele + 'ArrowModal').html('<div class="avatar bg-light-secondary">\n' +
                '<div class="avatar-content"><i class="avatar-icon" data-feather="arrow-right"></i></div>\n' +
                '</div>');
            $('#' + ele + 'PriceModal').html('<div class="font-weight-bolder text-secondary">' + numeral(curr).format('0,0.00') + ' บาท</div>');
        }
        feather.replace();
    }

    function updateNotify() {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url('notification/price') }}',
            type: "POST",
            enctype: 'multipart/form-data',
            dataType: 'JSON',
            processData: false,
            contentType: false,
            cache: false,
            success: function (res) {
                priceNotification();
            }
        });
    }
</script>
