<!-- BEGIN: Footer-->
<footer class="footer {{($configData['footerType']=== 'footer-sticky') ? 'd-none':''}} footer-light">
  <p class="clearfix mb-0">
    <span class="float-md-left d-block d-md-inline-block mt-25">นโยบายความเป็นส่วนตัว ข้อตกลงการใช้งานระบบ ติดต่อเพื่อขอใช้บริการ
    </span>
    <span class="float-md-right d-none d-md-block">Copyright 2021 Sesame POS All Rights Reserved.</span>
  </p>
</footer>
<button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>

<!-- Modal Price Notification -->
<div class="vertical-modal-ex">
    <div
        class="modal fade"
        id="priceNotifyModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">แจ้งเตือนเปลี่ยนแปลงราคาทอง</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="scrollable-container media-list menu-item">
                        <a class="d-flex" href="javascript:void(0)">
                            <div class="media d-flex align-items-start price-notify-row">
                                <div class="media-left" id="goldbarBuyArrowModal">
{{--                                                                  <div class="avatar bg-light-success">--}}
{{--                                                                      <div class="avatar-content"><i class="avatar-icon" data-feather="arrow-up"></i></div>--}}
{{--                                                                  </div>--}}
                                </div>
                                <div class="media-body">
                                    <p class="media-heading price-notify-title"><span class="font-weight-bolder">ทองคำแท่ง ซื้อ</span></p><small class="notification-text priceNotifyDate price-notify-date" id="priceNotifyDate"></small>
                                </div>
                                <div class="media-right" id="goldbarBuyPriceModal">
{{--                                                                  <div class="font-weight-bolder text-success">33,000.00 บาท</div>--}}
                                </div>
                            </div>
                        </a>
                        <a class="d-flex" href="javascript:void(0)">
                            <div class="media d-flex align-items-start price-notify-row">
                                <div class="media-left" id="goldBuyArrowModal">
{{--                                                                  <div class="avatar bg-light-success">--}}
{{--                                                                      <div class="avatar-content"><i class="avatar-icon" data-feather="arrow-up"></i></div>--}}
{{--                                                                  </div>--}}
                                </div>
                                <div class="media-body">
                                    <p class="media-heading price-notify-title"><span class="font-weight-bolder">ทองรูปพรรณ ซื้อ</span></p><small class="notification-text priceNotifyDate price-notify-date" id="priceNotifyDate"></small>
                                </div>
                                <div class="media-right" id="goldBuyPriceModal">
{{--                                                                  <div class="font-weight-bolder text-success">33,000.00 บาท</div>--}}
                                </div>
                            </div>
                        </a>
                        <a class="d-flex" href="javascript:void(0)">
                            <div class="media d-flex align-items-start price-notify-row">
                                <div class="media-left" id="goldbarSellArrowModal">
{{--                                                                  <div class="avatar bg-light-danger">--}}
{{--                                                                      <div class="avatar-content"><i class="avatar-icon" data-feather="arrow-down"></i></div>--}}
{{--                                                                  </div>--}}
                                </div>
                                <div class="media-body">
                                    <p class="media-heading price-notify-title"><span class="font-weight-bolder">ทองคำแท่ง ขาย</span></p><small class="notification-text priceNotifyDate price-notify-date" id="priceNotifyDate"></small>
                                </div>
                                <div class="media-right" id="goldbarSellPriceModal">
{{--                                                                  <div class="font-weight-bolder text-danger">33,000.00 บาท</div>--}}
                                </div>
                            </div>
                        </a>
                        <a class="d-flex" href="javascript:void(0)">
                            <div class="media d-flex align-items-start price-notify-row">
                                <div class="media-left" id="goldSellArrowModal">
{{--                                                                  <div class="avatar bg-light-danger">--}}
{{--                                                                      <div class="avatar-content"><i class="avatar-icon" data-feather="arrow-down"></i></div>--}}
{{--                                                                  </div>--}}
                                </div>
                                <div class="media-body">
                                    <p class="media-heading price-notify-title"><span class="font-weight-bolder">ทองรูปพรรณ ขาย</span></p><small class="notification-text priceNotifyDate price-notify-date" id="priceNotifyDate"></small>
                                </div>
                                <div class="media-right" id="goldSellPriceModal">
{{--                                                                  <div class="font-weight-bolder text-danger">33,000.00 บาท</div>--}}
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="updateNotify()">ยืนยัน</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">แจ้งเตือนภายหลัง</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Footer-->
