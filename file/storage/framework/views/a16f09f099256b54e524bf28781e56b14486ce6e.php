<?php
$configData = Helper::applClasses();
?>


<?php $__env->startSection('title', 'Login Page'); ?>

<?php $__env->startSection('page-style'); ?>
  
  <link rel="stylesheet" href="<?php echo e(asset(mix('css/base/plugins/forms/form-validation.css'))); ?>">
  <link rel="stylesheet" href="<?php echo e(asset(mix('css/base/pages/page-auth.css'))); ?>">

    <style>
        label{
            font-size: 16px;
        }

        .error{
            width: 100%;
            margin-top: 0.25rem;
            font-size: 0.857rem;
            color: #ea5455;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="auth-wrapper auth-v2">
  <div class="auth-inner row m-0">
    <!-- Brand logo-->
    <a class="brand-logo" href="javascript:void(0);">

        <defs>
          <lineargradient id="linearGradient-1" x1="100%" y1="10.5120544%" x2="50%" y2="89.4879456%">
            <stop stop-color="#000000" offset="0%"></stop>
            <stop stop-color="#FFFFFF" offset="100%"></stop>
          </lineargradient>
          <lineargradient id="linearGradient-2" x1="64.0437835%" y1="46.3276743%" x2="37.373316%" y2="100%">
            <stop stop-color="#EEEEEE" stop-opacity="0" offset="0%"></stop>
            <stop stop-color="#FFFFFF" offset="100%"></stop>
          </lineargradient>
        </defs>
        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g id="Artboard" transform="translate(-400.000000, -178.000000)">
            <g id="Group" transform="translate(400.000000, 178.000000)">
              <path class="text-primary" id="Path" d="M-5.68434189e-14,2.84217094e-14 L39.1816085,2.84217094e-14 L69.3453773,32.2519224 L101.428699,2.84217094e-14 L138.784583,2.84217094e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L6.71554594,44.4188507 C2.46876683,39.9813776 0.345377275,35.1089553 0.345377275,29.8015838 C0.345377275,24.4942122 0.230251516,14.560351 -5.68434189e-14,2.84217094e-14 Z" style="fill: currentColor"></path>
              <path id="Path1" d="M69.3453773,32.2519224 L101.428699,1.42108547e-14 L138.784583,1.42108547e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L32.8435758,70.5039241 L69.3453773,32.2519224 Z" fill="url(#linearGradient-1)" opacity="0.2"></path>
              <polygon id="Path-2" fill="#000000" opacity="0.049999997" points="69.3922914 32.4202615 32.8435758 70.5039241 54.0490008 16.1851325"></polygon>
              <polygon id="Path-21" fill="#000000" opacity="0.099999994" points="69.3922914 32.4202615 32.8435758 70.5039241 58.3683556 20.7402338"></polygon>
              <polygon id="Path-3" fill="url(#linearGradient-2)" opacity="0.099999994" points="101.428699 0 83.0667527 94.1480575 130.378721 47.0740288"></polygon>
            </g>
          </g>
        </g>
      </svg>
      <h2 class="brand-text text-primary ml-1"><img class="img-fluid" src="<?php echo e(asset('images/logo_sesame.svg')); ?>" /></h2>
    </a>
    <!-- /Brand logo-->

      <!-- Left Text-->
    <div class="d-none d-lg-flex col-lg-8 align-items-center p-5">
      <div class="w-100 d-lg-flex align-items-center justify-content-center px-5">
         <?php if($configData['theme'] === 'dark'): ?>
          <img class="img-fluid" src="<?php echo e(asset('images/pages/login-v2-dark.svg')); ?>" alt="Login V2" />
          <?php else: ?>
          <img class="img-fluid" src="<?php echo e(asset('images/pages/login-v2.svg')); ?>" alt="Login V2" />
          <?php endif; ?>
      </div>
    </div>
    <!-- /Left Text-->

    <!-- Login-->
    <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
      <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
        <h2 class="card-title font-weight-bold mb-1"><img class="img-fluid" src="<?php echo e(asset('images/logo_open.svg')); ?>" /></h2>

        <form id="form-auth" class="auth-login-form mt-2">
          <?php echo csrf_field(); ?>
          <div class="form-group">
              <label class="form-label" for="login-email">ชื่อผู้ใช้งาน</label>
              <input class="form-control" id="email" type="text" name="email" placeholder=""  aria-describedby="login-email" autofocus="" tabindex="1" />
              <span id="email-error" class="error"></span>
          </div>
          <div class="form-group mt-2">
            <div class="d-flex justify-content-between">
                <label for="login-password">รหัสผ่าน</label>
                <a href="<?php echo e(url("auth/forgot-password-v2")); ?>">
                    <label style="color: #847AF1;">Forgot Password?</label>
                </a>
            </div>
            <div class="input-group input-group-merge form-password-toggle">
                <input class="form-control form-control-merge" id="password" type="password" name="password" placeholder="" aria-describedby="login-password" tabindex="2" />
                <div class="input-group-append">
                  <span class="input-group-text cursor-pointer">
                      <i data-feather="eye"></i>
                  </span>
                </div>
            </div>
            <span id="password-error" class="error"></span>
          </div>






          <button id="saveButton" class="btn btn-primary btn-block mt-2" tabindex="4">เข้าสู่ระบบ</button>
        </form>























      </div>
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('vendor-script'); ?>
<script src="<?php echo e(asset(mix('vendors/js/forms/validation/jquery.validate.min.js'))); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-script'); ?>
<script src="<?php echo e(asset(mix('js/scripts/pages/page-auth-login.js'))); ?>"></script>
<script>
    $('#saveButton').click(function (e) {
        $('#saveButton').attr("disabled" ,true);

        // $('#email').removeClass('is-invalid');
        // $('#password').removeClass('is-invalid');
        $('#email-error').html("");
        $('#password-error').html("");

        e.preventDefault();
        var form = $('#form-auth')[0];
        var data = new FormData(form);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo e(url('/authenticate')); ?>',
            type: "POST",
            enctype: 'multipart/form-data',
            dataType: 'JSON',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            success: function (res) {

                if(res.error == 0){
                    window.location.replace("<?php echo e(url('/')); ?>");

                    /*$('#saveButton').empty();
                    $('#saveButton').append("<div class=\"spinner-border spinner-grow-sm text-light\" role=\"status\"></div> เข้าสู่ระบบ");

                    if (res.permission == 1){
                        window.location.replace("<?php echo e(url('/customer')); ?>");
                    }
                    else if (res.permission == 4){
                        window.location.replace("<?php echo e(url('/admin/users')); ?>");
                    }
                    else if (res.permission == 2){
                        window.location.replace("<?php echo e(url('/staff')); ?>");
                    }*/

                }else{
                    $.each(res.messages, function (k,v) {
                        // $('#'+ k).addClass('is-invalid');
                        $('#'+ k + '-error').html(v);
                    });
                }
                $('#saveButton').attr("disabled", false);

            }
        });
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/fullLayoutMaster', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\sesame_test_v2\file\resources\views//content/01_login/auth-login-v2.blade.php ENDPATH**/ ?>