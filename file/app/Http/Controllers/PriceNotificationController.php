<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PriceNotificationController extends Controller
{
    public function price(){
        try {
            $data = DB::connection('mysql2')->table("899_GOLD_PRICE")
                ->orderByDesc('id')
                ->take(2)
                ->get();

            $lastNotify = Carbon::createFromFormat('Y-m-d H:i:s', Auth::user()->notifi_price ? Auth::user()->notifi_price : '2022-01-01 00:00:00' );
            $lastUpdate = Carbon::createFromFormat('Y-m-d H:i:s', $data[0]->update_at);
            $notify = false;
            if($lastNotify->lte($lastUpdate) && (
                $data[0]->goldbar_buy != $data[1]->goldbar_buy ||
                $data[0]->gold_buy != $data[1]->gold_buy ||
                $data[0]->goldbar_sell != $data[1]->goldbar_sell ||
                $data[0]->gold_sell != $data[1]->gold_sell
            )) {
                $notify = true;
            }

            $date = $this->DateThai($data[0]->update_at);

            return response()->json([
                'error' => 0,
                'data' => $data,
                'notify' => $notify,
                'date' => $date,
            ], '200');

        } catch (\Throwable $e) {
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }
    public function update(){
        try {
            $user = User::query()->where('id', '=', Auth::user()->id)->first();
            $user->notifi_price = Carbon::now();
            $user->save();

            return response()->json([
                'error' => 0,
                'data' => 'อัพเดทข้อมูลเรียบร้อย',
            ], '200');

        } catch (\Throwable $e) {
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }

    public function DateThai($strDate)
    {
        $strYear = date("Y", strtotime($strDate)) + 543;
        $strMonth = date("n", strtotime($strDate));
        $strDay = date("j", strtotime($strDate));
        $strHour = date("H", strtotime($strDate));
        $strMinute = date("i", strtotime($strDate));
        $strSeconds = date("s", strtotime($strDate));
        $strMonthCut = array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
        $strMonthThai = $strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear, $strHour:$strMinute";
    }
}
