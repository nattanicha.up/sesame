<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SavingGoldController extends Controller
{
    public function index()
    {
        $pageConfigs = [
            'pageClass' => 'ecommerce-application',
        ];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['name' => "Checkout"]
        ];

        return view('/content/05_savinggold/savinggold', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function savingGoldInstallment()
    {
        $pageConfigs = [
            'pageClass' => 'ecommerce-application',
        ];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['name' => "Checkout"]
        ];

        return view('/content/05_savinggold/savinggold-installment', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function savingGoldInstallmentInvoice()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/content/05_savinggold/savinggold-installment-invoice', ['pageConfigs' => $pageConfigs]);
    }

    public function savingGoldCancel()
    {
        $pageConfigs = [
            'pageClass' => 'ecommerce-application',
        ];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['name' => "Checkout"]
        ];

        return view('/content/05_savinggold/savinggold-cancel', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function savingGoldCancelInvoice()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/content/05_savinggold/savinggold-cancel-invoice', ['pageConfigs' => $pageConfigs]);
    }

    public function savingGoldWithdraw()
    {
        $pageConfigs = [
            'pageClass' => 'ecommerce-application',
        ];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['name' => "Checkout"]
        ];

        return view('/content/05_savinggold/savinggold-withdraw', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function savingGoldWithdrawInvoice()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/content/05_savinggold/savinggold-withdraw-invoice', ['pageConfigs' => $pageConfigs]);
    }

    public function savingGoldContract()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Datatable"], ['name' => "Basic"]];
        return view('/content/05_savinggold/savinggold-contract', ['breadcrumbs' => $breadcrumbs]);
    }

    public function saving_report()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Datatable"], ['name' => "Basic"]];
        return view('/content/08_Report/savingreport', ['breadcrumbs' => $breadcrumbs]);
    }

    public function saving_report_detail()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Datatable"], ['name' => "Basic"]];
        return view('/content/08_Report/savingreportdetail', ['breadcrumbs' => $breadcrumbs]);
    }
}
