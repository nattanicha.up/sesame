<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReportSellController extends Controller
{
    public function sell_report()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Datatable"], ['name' => "Basic"]];
        return view('/content/08_Report/sellreport', ['breadcrumbs' => $breadcrumbs]);
    }

    public function sell_report_detail()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/content/08_Report/sellreportdetail', ['pageConfigs' => $pageConfigs]);
    }
}
