<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function authenticate(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ],[
            'email.required' => 'กรุณากรอกชื่อผู้ใช้งานหรืออีเมล',
            'password.required' => 'กรุณากรอกรหัสผ่าน',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => 1,
                'messages' => $validator->messages(),
            ], 200);
        }

        $email = User::where('email', $request->email)->first();
        $user = User::where('username', $request->email)->first();

        if($email == null && $user == null) {
            $fieldType = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
            if($fieldType == 'email') {
                return response()->json([
                    'error' => 1,
                    'messages' => ['email' => 'กรุณากรอกอีเมลให้ถูกต้อง'],
                ], 200);
            }
            else {
                return response()->json([
                    'error' => 1,
                    'messages' => ['email' => 'กรุณากรอกชื่อผู้ใช้งานให้ถูกต้อง'],
                ], 200);
            }
        }
        else if($email != null) {
            if(!Hash::check($request->password, $email->password)) {
                return response()->json([
                    'error' => 1,
                    'messages' => ['password' => 'กรุณากรอกรหัสผ่านให้ถูกต้อง'],
                ], 200);
            }
        }
        else if($user != null) {
            if(!Hash::check($request->password, $user->password)) {
                return response()->json([
                    'error' => 1,
                    'messages' => ['password' => 'กรุณากรอกรหัสผ่านให้ถูกต้อง'],
                ], 200);
            }
        }

        if($email != null) {
            if($email->status_id == 2) {
                return response()->json([
                    'error' => 1,
                    'messages' => ['email' => 'บัญชีนี้ถูกระงับแล้ว'],
                ], 200);
            }
        }
        else if($user != null) {
            if($user->status_id == 2) {
                return response()->json([
                    'error' => 1,
                    'messages' => ['email' => 'บัญชีนี้ถูกระงับแล้ว'],
                ], 200);
            }
        }

        if($email != null) {
            if($email->acc_expire != null && $email->acc_expire != '' && $email->acc_expire <= Carbon::now()->toDateTimeString()) {
                return response()->json([
                    'error' => 1,
                    'messages' => ['email' => 'ชื่อผู้ใช้งานนี้ครบอายุแล้ว'],
                ], 200);
            }
        }
        else if($user != null) {
            if($user->acc_expire != null && $user->acc_expire != '' && $user->acc_expire <= Carbon::now()->toDateTimeString()) {
                return response()->json([
                    'error' => 1,
                    'messages' => ['email' => 'ชื่อผู้ใช้งานนี้ครบอายุแล้ว'],
                ], 200);
            }
        }

        $fieldType = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        if(auth()->attempt(array($fieldType => $request->email, 'password' => $request->password)))
        {

            $permissin = Auth::user()->user_permission_id;

            return response()->json([
                'error' => 0,
                'permission' => $permissin,
                'messages' => 'เข้าสู่ระบบสำเร็จ',
            ], 200);
        }
    }

    public function logout() {
        Auth::logout();
        return redirect('login');
    }
}
