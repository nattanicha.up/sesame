<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function Home()
    {
        return view('/content/02_home/home');
    }

    public function overview()
    {
        return view('/content/02_home/overview');
    }

    public function editprofile()
    {
        return view('/content/02_home/editprofile');
    }
}
