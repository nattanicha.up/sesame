<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SellGoldController extends Controller
{
    public function index()
    {
        $pageConfigs = [
            'pageClass' => 'ecommerce-application',
        ];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['name' => "Checkout"]
        ];

        return view('/content/03_buy_and_sale/sellgold', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function sellInvoice()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/content/03_buy_and_sale/sell-invoice', ['pageConfigs' => $pageConfigs]);
    }
}
