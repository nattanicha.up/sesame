<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReportBuyController extends Controller
{
    public function buy_report()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Datatable"], ['name' => "Basic"]];
        return view('/content/08_Report/buyreport', ['breadcrumbs' => $breadcrumbs]);
    }

    public function buy_report_detail()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/content/08_Report/buyreportdetail', ['pageConfigs' => $pageConfigs]);
    }
}
