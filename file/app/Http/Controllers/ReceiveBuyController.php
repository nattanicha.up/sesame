<?php

namespace App\Http\Controllers;

use App\Models\Customers;
use App\Models\Gold\Gold;
use App\Models\Gold\GoldBuy;
use App\Models\Gold\GoldSubCategory;
use App\Models\Gold\GoldType;
use App\Models\Gold\GoldUnits;
use App\Models\Gold\TransectionStock;
use App\Models\Gold\Transectiontrade;
use App\Models\GoldPercent;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class ReceiveBuyController extends Controller
{
    public function index()
    {
        $pageConfigs = [
            'pageClass' => 'ecommerce-application',
        ];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['name' => "Checkout"]
        ];

        $dummy = Customers::query()->whereDummy(1)->first();

        return view('/content/03_buy_and_sale/buyreceive', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs,
            'dummy' => $dummy
        ]);
//        return view('/content/03_buy_and_sale/buyreceive');
    }

    public function buyinvoice()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/content/03_buy_and_sale/buy-invoice', ['pageConfigs' => $pageConfigs]);
    }

    public function goldStore(Request $request)
    {
        try{
            if($request->gold_category == 1) {
                if($request->gold_type == 1) {
                    $rules = [
                        'gold_pattern' => 'required',
//                        'gold_sku' => 'required',
                        'gold_weight' => 'required',
                        'gold_weight_actual' => 'required',
                        'gold_cost' => 'required',
//                        'gold_smith_charge' => 'required',
                        'gold_size' => 'required',
                    ];

                    $message = [
                        '*.required' => 'โปรดระบุ',
                    ];

                    if($request->file('gold_image') != null) {
                        $rules['gold_image'] = 'image|mimes:jpg,png,jpeg,gif,svg';
                        $message['gold_image.*'] = 'การอัพโหลดไฟล์รองรับนามสกุล jpg,png,jpeg,gif,svg เท่านั้น';
                    }

                    $validator = Validator::make($request->all(), $rules, $message);

                    if ($validator->fails()) {
                        return response()->json([
                            'error' => 1,
                            'messages' => $validator->messages(),
                        ], 200);
                    }

                    return response()->json([
                        'error' => 0,
                        'data' => $request->all(),
                        'gold_sku' => $this->generate(),
                        'gold_type' => GoldType::query()->select('title')->whereId($request->gold_type)->first(),
                        'gold_percent' => GoldPercent::query()->select('title')->whereId($request->gold_category)->first(),
                        'gold_category' => GoldSubCategory::query()->select('title')->whereId($request->gold_sub_category)->first(),
                        'gold_units' => GoldUnits::query()->whereId($request->gold_unit)->select('title')->first(),
                        'messages' => "เพิ่มข้อมูลเรียบร้อย",
                    ],200);
                }
                else if($request->gold_type == 2) {
                    $rules = [
                        'gold_pattern' => 'required',
//                        'gold_sku' => 'required',
                        'gold_weight' => 'required',
                        'gold_weight_actual' => 'required',
                        'gold_cost' => 'required',
                    ];

                    $message = [
                        '*.required' => 'โปรดระบุ',
                    ];

                    if($request->file('gold_image') != null) {
                        $rules['gold_image'] = 'image|mimes:jpg,png,jpeg,gif,svg';
                        $message['gold_image.*'] = 'การอัพโหลดไฟล์รองรับนามสกุล jpg,png,jpeg,gif,svg เท่านั้น';
                    }

                    $validator = Validator::make($request->all(), $rules, $message);

                    if ($validator->fails()) {
                        return response()->json([
                            'error' => 1,
                            'messages' => $validator->messages(),
                        ], 200);
                    }

                    return response()->json([
                        'error' => 0,
                        'data' => $request->all(),
                        'gold_sku' => $this->generate(),
                        'gold_type' => GoldType::query()->select('title')->whereId($request->gold_type)->first(),
                        'gold_percent' => GoldPercent::query()->select('title')->whereId($request->gold_category)->first(),
                        'gold_category' => null,
                        'gold_units' => GoldUnits::query()->whereId($request->gold_unit)->select('title')->first(),
                        'messages' => "เพิ่มข้อมูลเรียบร้อย",
                    ],200);
                }
            }
            else if($request->gold_category == 2) {
                if($request->gold_type == 1) {
                    $rules = [
                        'gold_pattern' => 'required',
//                        'gold_sku' => 'required',
//                        'gold_weight' => 'required',
                        'gold_weight_actual' => 'required',
                        'gold_cost' => 'required',
//                        'gold_smith_charge' => 'required',
                        'gold_size' => 'required',
                    ];

                    $message = [
                        '*.required' => 'โปรดระบุ',
                    ];

                    if($request->file('gold_image') != null) {
                        $rules['gold_image'] = 'image|mimes:jpg,png,jpeg,gif,svg';
                        $message['gold_image.*'] = 'การอัพโหลดไฟล์รองรับนามสกุล jpg,png,jpeg,gif,svg เท่านั้น';
                    }

                    $validator = Validator::make($request->all(), $rules, $message);

                    if ($validator->fails()) {
                        return response()->json([
                            'error' => 1,
                            'messages' => $validator->messages(),
                        ], 200);
                    }

                    return response()->json([
                        'error' => 0,
                        'data' => $request->all(),
                        'gold_sku' => $this->generate(),
                        'gold_type' => GoldType::query()->select('title')->whereId($request->gold_type)->first(),
                        'gold_percent' => GoldPercent::query()->select('title')->whereId($request->gold_category)->first(),
                        'gold_category' => GoldSubCategory::query()->select('title')->whereId($request->gold_sub_category)->first(),
                        'gold_units' => null,
                        'messages' => "เพิ่มข้อมูลเรียบร้อย",
                    ],200);
                }
                else if($request->gold_type == 2) {
                    $rules = [
                        'gold_pattern' => 'required',
//                        'gold_sku' => 'required',
//                        'gold_weight' => 'required',
                        'gold_weight_actual' => 'required',
                        'gold_cost' => 'required',
                    ];

                    $message = [
                        '*.required' => 'โปรดระบุ',
                    ];

                    if($request->file('gold_image') != null) {
                        $rules['gold_image'] = 'image|mimes:jpg,png,jpeg,gif,svg';
                        $message['gold_image.*'] = 'การอัพโหลดไฟล์รองรับนามสกุล jpg,png,jpeg,gif,svg เท่านั้น';
                    }

                    $validator = Validator::make($request->all(), $rules, $message);

                    if ($validator->fails()) {
                        return response()->json([
                            'error' => 1,
                            'messages' => $validator->messages(),
                        ], 200);
                    }

                    return response()->json([
                        'error' => 0,
                        'data' => $request->all(),
                        'gold_sku' => $this->generate(),
                        'gold_type' => GoldType::query()->select('title')->whereId($request->gold_type)->first(),
                        'gold_percent' => GoldPercent::query()->select('title')->whereId($request->gold_category)->first(),
                        'gold_category' => null,
                        'gold_units' => null,
                        'messages' => "เพิ่มข้อมูลเรียบร้อย",
                    ],200);
                }
            }
            else if($request->gold_category == 3) {
                if($request->gold_type == 1) {
                    $rules = [
                        'gold_pattern' => 'required',
//                        'gold_sku' => 'required',
//                        'gold_weight' => 'required',
                        'gold_weight_actual' => 'required',
                        'gold_cost' => 'required',
//                        'gold_smith_charge' => 'required',
                        'gold_size' => 'required',
                    ];

                    $message = [
                        '*.required' => 'โปรดระบุ',
                    ];

                    if($request->file('gold_image') != null) {
                        $rules['gold_image'] = 'image|mimes:jpg,png,jpeg,gif,svg';
                        $message['gold_image.*'] = 'การอัพโหลดไฟล์รองรับนามสกุล jpg,png,jpeg,gif,svg เท่านั้น';
                    }

                    $validator = Validator::make($request->all(), $rules, $message);

                    if ($validator->fails()) {
                        return response()->json([
                            'error' => 1,
                            'messages' => $validator->messages(),
                        ], 200);
                    }

                    return response()->json([
                        'error' => 0,
                        'data' => $request->all(),
                        'gold_sku' => $this->generate(),
                        'gold_type' => GoldType::query()->select('title')->whereId($request->gold_type)->first(),
                        'gold_percent' => GoldPercent::query()->select('title')->whereId($request->gold_category)->first(),
                        'gold_category' => GoldSubCategory::query()->select('title')->whereId($request->gold_sub_category)->first(),
                        'gold_units' => null,
                        'messages' => "เพิ่มข้อมูลเรียบร้อย",
                    ],200);
                }
                else if($request->gold_type == 2) {
                    $rules = [
                        'gold_pattern' => 'required',
//                        'gold_sku' => 'required',
//                        'gold_weight' => 'required',
                        'gold_weight_actual' => 'required',
                        'gold_cost' => 'required',
                    ];

                    $message = [
                        '*.required' => 'โปรดระบุ',
                    ];

                    if($request->file('gold_image') != null) {
                        $rules['gold_image'] = 'image|mimes:jpg,png,jpeg,gif,svg';
                        $message['gold_image.*'] = 'การอัพโหลดไฟล์รองรับนามสกุล jpg,png,jpeg,gif,svg เท่านั้น';
                    }

                    $validator = Validator::make($request->all(), $rules, $message);

                    if ($validator->fails()) {
                        return response()->json([
                            'error' => 1,
                            'messages' => $validator->messages(),
                        ], 200);
                    }

                    return response()->json([
                        'error' => 0,
                        'data' => $request->all(),
                        'gold_sku' => $this->generate(),
                        'gold_type' => GoldType::query()->select('title')->whereId($request->gold_type)->first(),
                        'gold_percent' => GoldPercent::query()->select('title')->whereId($request->gold_category)->first(),
                        'gold_category' => null,
                        'gold_units' => null,
                        'messages' => "เพิ่มข้อมูลเรียบร้อย",
                    ],200);
                }
            }
            else if($request->gold_category == 4) {
                $rules = [
                    'gold_pattern' => 'required',
//                        'gold_sku' => 'required',
//                        'gold_weight' => 'required',
                    'gold_weight_actual' => 'required',
                    'gold_cost' => 'required',
//                        'gold_smith_charge' => 'required',
                    'gold_size' => 'required',
                ];

                $message = [
                    '*.required' => 'โปรดระบุ',
                ];

                if($request->file('gold_image') != null) {
                    $rules['gold_image'] = 'image|mimes:jpg,png,jpeg,gif,svg';
                    $message['gold_image.*'] = 'การอัพโหลดไฟล์รองรับนามสกุล jpg,png,jpeg,gif,svg เท่านั้น';
                }

                $validator = Validator::make($request->all(), $rules, $message);

                if ($validator->fails()) {
                    return response()->json([
                        'error' => 1,
                        'messages' => $validator->messages(),
                    ], 200);
                }

                return response()->json([
                    'error' => 0,
                    'data' => $request->all(),
                    'gold_sku' => $this->generate(),
                    'gold_type' => null,
                    'gold_percent' => GoldPercent::query()->select('title')->whereId($request->gold_category)->first(),
                    'gold_category' => GoldSubCategory::query()->select('title')->whereId($request->gold_sub_category)->first(),
                    'gold_units' => null,
                    'messages' => "เพิ่มข้อมูลเรียบร้อย",
                ],200);
            }

            return response()->json([
                'error' => 0,
                'messages' => "เพิ่มข้อมูลเรียบร้อย",
            ],200);

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'messages' => $e,
            ], 200);
        }
    }

    public function generate(){
//        try{
            $generate = true;

            while($generate) {
                $barcode_number = 'G' . Auth::user()->company_id . date('ymdHis'). $this->randomBarcode(11);

                $barcode = TransectionStock::query()->where('sku',$barcode_number)->count();

                if ($barcode == 0){
                    $generate = false;
                }
            }

            return $barcode_number;

//            return response()->json([
//                'error' => 0,
//                'barcode_number' => $barcode_number
//            ], 200);
//
//        }catch (\Throwable $e) {
//            return response()->json([
//                'error' => 1,
//                'messages' => $e
//            ],200);
//        }
    }

    public function randomBarcode($num){
        srand((double)microtime()*10000000);
        $chars = "0123456789";
        $ret_str = "";
        //$num = strlen($chars);
        for($i = 0; $i < 2; $i++)
        {
            $ret_str.= $chars[rand()%$num];
            $ret_str.="";
        }
        return $ret_str;
    }

    public function buyGoldStore(Request $request)
    {
        try{
            $messages = [];
            $request->total_price ? '' : $messages['product'] = ['โปรดเพิ่มข้อมูลทอง'];
            $request->buyer_id ? '' : $messages['buyer'] = ['โปรดเพิ่มข้อมูลผู้ขาย'];
            if($messages) {
                return response()->json([
                    'error' => 1,
                    'messages' => $messages,
                ], 200);
            }

            // Add Transection
            $transection = Transectiontrade::create();
            $transection->transection_id = 'TB'.date('Y-mdhis');
            $transection->type = 1;
            $transection->total =  $request->total_price;
            $transection->net =  $request->total_price;
            $transection->channel = $request->purchase_channel;
            $transection->payment = $request->payment_channel;
            $transection->bank = $request->bank;
            $transection->user_id = Auth::user()->id;
            $transection->update_by = Auth::user()->id;
            $transection->company_id = Auth::user()->Company->id;
            $transection->customer_id = $request->buyer_id;
            $transection->save();

            for ($i = 0; $i < count($request->gold_sku); $i++) {
                $goldbuy = GoldBuy::create();
                $request->gold_pattern[$i] != 'null' ? $goldbuy->title = $request->gold_pattern[$i] : '';
                $request->gold_category[$i] != 'null' ? $goldbuy->category_id = $request->gold_category[$i] : '';
                $request->gold_type[$i] != 'null' ? $goldbuy->gold_type = $request->gold_type[$i] : '';
                $request->gold_sku[$i] != 'null' ? $goldbuy->sku = $request->gold_sku[$i] : '';
                $request->gold_sub_category[$i] != 'null' ? $goldbuy->subcategory_id = $request->gold_sub_category[$i] : '';
                $request->gold_weight[$i] != 'null' ? $goldbuy->weight = $request->gold_weight[$i] : '';
                $request->gold_unit[$i] != 'null' ? $goldbuy->unit_id = $request->gold_unit[$i] : '';
                $request->gold_weight_actual[$i] != 'null' ? $goldbuy->weight_real = $request->gold_weight_actual[$i] : '';
                $request->gold_size[$i] != 'null' ? $goldbuy->size = $request->gold_size[$i] : '';
                $request->gold_cost[$i] != 'null' ? $goldbuy->price = $request->gold_cost[$i] : '';
                if($request->gold_image[$i] != 'null') {
                    $extension = explode('/', mime_content_type($request->gold_image[$i]))[1];
                    if ($extension != '') {
                        $img = base64_decode(explode(',', $request->gold_image[$i])[1]);
                        $name = $goldbuy->sku;
                        $img = Image::make($img);
                        $img->fit(800, 800, function ($constraint) {
                            $constraint->upsize();
                        });
                        $img->save(public_path('_uploads/_products/') . $name . '.' . $extension);
                        $goldbuy->pic = $name . '.' . $extension;
                    }
                }
                $goldbuy->status = 1;
                $goldbuy->transection_id = $transection->id;
                $goldbuy->user_id = Auth::user()->id;
                $goldbuy->update_by = Auth::user()->id;
                $goldbuy->company_id = Auth::user()->company_id;
                $goldbuy->save();
            }

            return response()->json([
                'error' => 0,
                'data' => $transection,
                'messages' => "เพิ่มข้อมูลเรียบร้อย",
            ],200);

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'messages' => $e,
            ], 200);
        }
    }

    public function buySummary(Transectiontrade $transection)
    {
//        dd($transection->goldbuy);
        $pageConfigs = ['pageHeader' => false];

        return view('/content/03_buy_and_sale/buy-invoice', [
            'pageConfigs' => $pageConfigs,
            'transection' => $transection,
            'transectionDate' => $this->DateThai($transection->created_at),
        ]);
    }

    public function buyBill(Transectiontrade $transectiontrade)
    {
//        dd($transection);
        $title = "ใบเสร็จรับซื้อทอง";
        $transectionDate = $this->DateThai($transectiontrade->created_at);
        $customPaper = array(0,0,1500.00,150.00);
        $pdf = PDF::loadView('content.03_buy_and_sale.buy-bill',compact('transectiontrade', 'title', 'transectionDate'))->setPaper($customPaper, 'landscape');
        return $pdf->stream();
    }

    function DateThai($strDate)
    {
        $strYear = date("Y", strtotime($strDate)) + 543;
        $strMonth = date("n", strtotime($strDate));
        $strDay = date("j", strtotime($strDate));
        $strHour = date("H", strtotime($strDate));
        $strMinute = date("i", strtotime($strDate));
        $strSeconds = date("s", strtotime($strDate));
        $strMonthCut = array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
        $strMonthThai = $strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear, $strHour:$strMinute";
    }
}
