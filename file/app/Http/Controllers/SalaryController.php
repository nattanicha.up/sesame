<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SalaryController extends Controller
{
    public function index()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Datatable"], ['name' => "Basic"]];
        return view('/content/12_salary/index', ['breadcrumbs' => $breadcrumbs]);
    }

    public function create()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/12_salary/create', ['pageConfigs' => $pageConfigs]);
    }

    public function edit()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/12_salary/edit', ['pageConfigs' => $pageConfigs]);
    }

    public function detail()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/12_salary/detail', ['pageConfigs' => $pageConfigs]);
    }
}
