<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TaxinvoiceController extends Controller
{
    public function tax_invoice()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Datatable"], ['name' => "Basic"]];
        return view('/content/09_Taxinvoice/taxinvoice', ['breadcrumbs' => $breadcrumbs]);
    }

    public function tax_invoiceadd()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/content/09_Taxinvoice/taxinvoiceadd', ['pageConfigs' => $pageConfigs]);
    }

    public function tax_invoiceshow()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/content/09_Taxinvoice/taxinvoiceshow', ['pageConfigs' => $pageConfigs]);
    }

    public function account_overview()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/content/09_Taxinvoice/overview', ['pageConfigs' => $pageConfigs]);
    }
}
