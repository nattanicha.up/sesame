<?php

namespace App\Http\Controllers;

use App\Models\Customers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class MemberController extends Controller
{
    public function member()
    {

        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Datatable"], ['name' => "Basic"]];
        return view('/content/07_Member/member', ['breadcrumbs' => $breadcrumbs]);
    }

    public function getTransectionbyMember()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "Datatable"], ['name' => "Basic"]];
        return view('/content/07_Member/transection', ['breadcrumbs' => $breadcrumbs]);
    }

    public function memberStore(Request $request)
    {
        try{
            $rules = [
                'id_card' => 'nullable|string|min:13|max:13|unique:300_CUSTOMERS',
                'phone' => 'required|unique:300_CUSTOMERS',
//                'title' => 'required',
                'name' => 'required',
//                'surname' => 'required',
//                'address' => 'required',
//                'province_id' => 'required',
//                'district_id' => 'required',
//                'subdistrict_id' => 'required',
//                'zipcode' => 'required',
            ];

            $message = [
                'id_card.numeric' => 'โปรดระบุเลขบัตรประชาชนให้ถูกต้อง',
                'id_card.min' => 'โปรดระบุเลขบัตรประชาชนให้ถูกต้อง',
                'id_card.max' => 'โปรดระบุเลขบัตรประชาชนให้ถูกต้อง',
                'id_card.unique' => 'เลขบัตรประชาชนซ้ำ',
                'phone.unique' => 'เบอร์โทรซ้ำ',
                '*.required' => 'โปรดระบุ',
            ];

            $validator = Validator::make($request->all(), $rules, $message);

            if ($validator->fails()) {
                return response()->json([
                    'error' => 1,
                    'messages' => $validator->messages(),
                ], 200);
            }

            $customers = Customers::create();
            $customers->id_card = $request->id_card;
            $customers->title = $request->title;
            $customers->name = $request->name;
            $customers->surname = $request->surname;
            $customers->title_en = $request->title_en;
            $customers->name_en = $request->name_en;
            $customers->surname_en = $request->surname_en;
            $customers->birthday = $request->birthday;
            $customers->gender = $request->gender;
            $customers->address = $request->address;
            $customers->subdistrict_id = $request->subdistrict_id;
            $customers->district_id = $request->district_id;
            $customers->province_id = $request->province_id;
            $customers->zipcode = $request->zipcode;
            $customers->issue = $request->issue;
            $customers->expire = $request->expire;
            $customers->phone = $request->phone;
            $customers->email = $request->email;
            $customers->line = $request->line;
            $customers->facebook = $request->facebook;
            $customers->user_id = Auth::user()->id;
            $customers->update_by = Auth::user()->id;
            $customers->company_id = 1;
            $customers->save();

//            $province = $customers->province->title_th;
//            $district = $customers->district->title_th;
//            $subdistrict = $customers->subdistrict->title_th;

            $data = Customers::query()
                ->with('province')
                ->with('district')
                ->with('subdistrict')
                ->whereId($customers->id)
                ->first();

            return response()->json([
                'error' => 0,
                'data' => $data,
                'messages' => "เพิ่มข้อมูลเรียบร้อย",
            ],200);

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'messages' => $e,
            ], 200);
        }
    }

    public function memberList(){
        try{
            $data = Customers::query()
                ->where('company_id', Auth::user()->company_id)
                ->where('dummy', null)
                ->get();
            return response()->json([
                'error' => 0,
                'data' => $data,
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }

    public function memberShow($id){
        try{
            $data = Customers::query()
                ->with('gender')
                ->with('title')
                ->with('province')
                ->with('district')
                ->with('subdistrict')
                ->where('id', $id)
                ->first();
            return response()->json([
                'error' => 0,
                'data' => $data,
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }

    public function memberShowOld(){
        try{
            $data = null;

            if(request()->filled('id') || request()->filled('phone')) {
                $data = Customers::query()
                    ->with('gender')
                    ->with('title')
                    ->with('province')
                    ->with('district')
                    ->with('subdistrict')
                    ->when(request()->filled('id'), function($q){
                        $q->where('id_card', request('id'));
                    })
                    ->when(request()->filled('phone'), function($q){
                        $q->where('phone', request('phone'));
                    })
                    ->first();
            }

            return response()->json([
                'error' => 0,
                'data' => $data,
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }

    public function memberUpdate(Request $request, $id)
    {
        try{
            $rules = [
//                'id_card' => 'required|string|min:13|max:13|unique:300_CUSTOMERS',
//                'phone' => 'required|unique:300_CUSTOMERS',
//                'title' => 'required',
                'name' => 'required',
//                'surname' => 'required',
//                'address' => 'required',
//                'province_id' => 'required',
//                'district_id' => 'required',
//                'subdistrict_id' => 'required',
//                'zipcode' => 'required',
            ];

            $message = [
//                'id_card.numeric' => 'โปรดระบุเลขบัตรประชาชนให้ถูกต้อง',
//                'id_card.min' => 'โปรดระบุเลขบัตรประชาชนให้ถูกต้อง',
//                'id_card.max' => 'โปรดระบุเลขบัตรประชาชนให้ถูกต้อง',
//                'id_card.unique' => 'เลขบัตรประชาชนซ้ำ',
//                'phone.unique' => 'เบอร์โทรซ้ำ',
                '*.required' => 'โปรดระบุ',
            ];

            $id_card = Customers::query()
                ->where('id', $id)
                ->where('id_card', $request->id_card)
                ->first();
            if($id_card == null) {
                $rules['id_card'] = 'required|string|min:13|max:13|unique:300_CUSTOMERS';
                $message['id_card.numeric'] = 'โปรดระบุเลขบัตรประชาชนให้ถูกต้อง';
                $message['id_card.min'] = 'โปรดระบุเลขบัตรประชาชนให้ถูกต้อง';
                $message['id_card.max'] = 'โปรดระบุเลขบัตรประชาชนให้ถูกต้อง';
                $message['id_card.unique'] = 'เลขบัตรประชาชนซ้ำ';
            }

            $phone = Customers::query()
                ->where('id', $id)
                ->where('phone', $request->phone)
                ->first();
            if($phone == null) {
                $rules['phone'] = 'required|unique:300_CUSTOMERS';
                $message['phone.unique'] = 'เบอร์โทรซ้ำ';
            }

            $validator = Validator::make($request->all(), $rules, $message);

            if ($validator->fails()) {
                return response()->json([
                    'error' => 1,
                    'messages' => $validator->messages(),
                ], 200);
            }

            $customers = Customers::query()->where('id', $id)->first();
            if($id_card == null) {
                $customers->id_card = $request->id_card;
            }
            $customers->title = $request->title;
            $customers->name = $request->name;
            $customers->surname = $request->surname;
            $customers->title_en = $request->title_en;
            $customers->name_en = $request->name_en;
            $customers->surname_en = $request->surname_en;
            $customers->birthday = $request->birthday;
            $customers->gender = $request->gender;
            $customers->address = $request->address;
            $customers->subdistrict_id = $request->subdistrict_id;
            $customers->district_id = $request->district_id;
            $customers->province_id = $request->province_id;
            $customers->zipcode = $request->zipcode;
            $customers->issue = $request->issue;
            $customers->expire = $request->expire;
            if($phone == null) {
                $customers->phone = $request->phone;
            }
            $customers->email = $request->email;
            $customers->line = $request->line;
            $customers->facebook = $request->facebook;
//            $customers->user_id = Auth::user()->id;
            $customers->update_by = Auth::user()->id;
//            $customers->company_id = 1;
            $customers->save();

            $data = Customers::query()
                ->with('province')
                ->with('district')
                ->with('subdistrict')
                ->whereId($customers->id)
                ->first();

            return response()->json([
                'error' => 0,
                'data' => $data,
                'messages' => "แก้ไขข้อมูลเรียบร้อย",
            ],200);

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'messages' => $e,
            ], 200);
        }
    }

    public function dummyStore(Request $request)
    {
        try{
            if($request->id_dummy == null) {
                $rules = [
                    'id_card' => 'required|string|min:13|max:13|unique:300_CUSTOMERS',
                    'phone' => 'required|unique:300_CUSTOMERS',
                ];

                $message = [
                    'id_card.numeric' => 'โปรดระบุเลขบัตรประชาชนให้ถูกต้อง',
                    'id_card.min' => 'โปรดระบุเลขบัตรประชาชนให้ถูกต้อง',
                    'id_card.max' => 'โปรดระบุเลขบัตรประชาชนให้ถูกต้อง',
                    'id_card.unique' => 'เลขบัตรประชาชนซ้ำ',
                    'phone.unique' => 'เบอร์โทรซ้ำ',
                    '*.required' => 'โปรดระบุ',
                ];
            }
            else {
                $rules = [];
                $message = ['*.required' => 'โปรดระบุ'];

                $id_card = Customers::query()
                    ->where('id', $request->id_dummy)
                    ->where('id_card', $request->id_card)
                    ->first();
                if ($id_card == null) {
                    $rules['id_card'] = 'required|string|min:13|max:13|unique:300_CUSTOMERS';
                    $message['id_card.numeric'] = 'โปรดระบุเลขบัตรประชาชนให้ถูกต้อง';
                    $message['id_card.min'] = 'โปรดระบุเลขบัตรประชาชนให้ถูกต้อง';
                    $message['id_card.max'] = 'โปรดระบุเลขบัตรประชาชนให้ถูกต้อง';
                    $message['id_card.unique'] = 'เลขบัตรประชาชนซ้ำ';
                }

                $phone = Customers::query()
                    ->where('id', $request->id_dummy)
                    ->where('phone', $request->phone)
                    ->first();
                if ($phone == null) {
                    $rules['phone'] = 'required|unique:300_CUSTOMERS';
                    $message['phone.unique'] = 'เบอร์โทรซ้ำ';
                }
            }

            $validator = Validator::make($request->all(), $rules, $message);

            if ($validator->fails()) {
                return response()->json([
                    'error' => 1,
                    'messages' => $validator->messages(),
                ], 200);
            }

            if($request->id_dummy == null) {
                $customers = Customers::create();
                $customers->id_card = $request->id_card;
                $customers->phone = $request->phone;
                $customers->name = $request->name;
                $customers->surname = $request->surname;
                $customers->user_id = Auth::user()->id;
                $customers->update_by = Auth::user()->id;
                $customers->company_id = 1;
                $customers->dummy = 1;
                $customers->save();
            }
            else {
                $customers = Customers::query()->where('id', $request->id_dummy)->first();
                if($id_card == null) {
                    $customers->id_card = $request->id_card;
                }
                if($phone == null) {
                    $customers->phone = $request->phone;
                }
                $customers->name = $request->name;
                $customers->surname = $request->surname;
                $customers->update_by = Auth::user()->id;
                $customers->save();
            }

            return response()->json([
                'error' => 0,
                'messages' => "แก้ไขข้อมูลเรียบร้อย",
            ],200);

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'messages' => $e,
            ], 200);
        }
    }

    public function dummyShow(){
        try{
            $data = Customers::query()
                ->where('dummy', '=', 1)
                ->first();
            return response()->json([
                'error' => 0,
                'data' => $data,
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }
}
