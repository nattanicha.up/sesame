<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    use HasFactory;

    protected $table = '300_CUSTOMERS';
    protected $fillable = [
        'id_card',
        'title',
        'name',
        'surname',
        'title_en',
        'name_en',
        'surname_en',
        'birthday',
        'gender',
        'address',
        'subdistrict_id',
        'district_id',
        'province_id',
        'zipcode',
        'issue',
        'expire',
        'phone',
        'email',
        'line',
        'facebook',
        'pic',
        'user_id',
        'update_by',
        'company_id',
        'dummy',
    ];

    public function gender(){
        return $this->hasOne(Gender::class,'id','gender');
    }

    public function title(){
        return $this->hasOne(Title::class,'id','title');
    }

    public function province(){
        return $this->hasOne(Province::class,'id','province_id');
    }

    public function district(){
        return $this->hasOne(District::class,'id','district_id');
    }

    public function subdistrict(){
        return $this->hasOne(Subdistrict::class,'id','subdistrict_id');
    }
}
