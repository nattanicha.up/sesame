<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    use HasFactory;
    protected $table = '900_BANK';
    protected $fillable = [
        'id',
        'title',
        'status',
        'created_at',
        'updated_at'
    ];
}
