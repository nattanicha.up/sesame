<?php

namespace App\Models\Gold;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GoldBuy extends Model
{
    use HasFactory;
    protected $table = '205_GOLDS_BUY';
    protected $fillable = [
        'title',
        'sku',
        'weight',
        'phone',
        'price_income',
        'price_goldsmith',
        'in_stock',
        'pic',
        'customer_idcard',
        'user_id',
        'subcategory_id',
        'unit_id',
    ];

    public function goldType(){
        return $this->hasOne(GoldType::class,'id','gold_type');
    }

    public function goldSubCategory(){
        return $this->hasOne(GoldSubCategory::class,'id','subcategory_id');
    }

    public function goldPercent(){
        return $this->hasOne(GoldPercent::class,'id','category_id');
    }
}
