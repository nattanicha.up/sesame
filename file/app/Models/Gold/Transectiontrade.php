<?php

namespace App\Models\Gold;

use App\Models\Customers;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transectiontrade extends Model
{
    use HasFactory;
    protected $table = '400_TRANSECTION_TRADE';
    protected $fillable = [
        'id',
        'transection_id',
        'type',
        'collect_id',
        'total',
        'discount',
        'discount_code',
        'received',
        'changed',
        'channel',
        'payment',
        'bank',
        'user_id',
        'update_by',
        'company_id',
        'customer_id',
        'created_at',
        'updated_at'
    ];

    public function customer(){
        return $this->hasOne(Customers::class,'id','customer_id');
    }

    public function goldbuy(){
        return $this->hasMany(GoldBuy::class,'transection_id','id');
    }

    public function userAdd(){
        return $this->hasOne(User::class,'id','user_id');
    }
}
