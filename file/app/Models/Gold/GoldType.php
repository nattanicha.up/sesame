<?php

namespace App\Models\Gold;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GoldType extends Model
{
    use HasFactory;

    protected $table = '201_GOLDS_CATEGORY';
    protected $fillable = [
        'title',
    ];
}
