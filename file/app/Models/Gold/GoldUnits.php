<?php

namespace App\Models\Gold;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GoldUnits extends Model
{
    use HasFactory;

    protected $table = '204_GOLDS_UNIT';
    protected $fillable = [
        'title',
    ];
}
