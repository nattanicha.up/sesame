<?php

namespace App\Models\Gold;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GoldCategory extends Model
{
    use HasFactory;

    protected $table = '206_GOLDS_PERCENT';
    protected $fillable = [
        'title',
    ];
}
