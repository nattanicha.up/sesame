<?php

namespace App\Models\ChartData;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MonthlyOrderAverage extends Model
{
    use HasFactory;

    protected $table = '805_MonthlyOrderAverage';
    protected $fillable = [
        'InvoiceDate',
        'net',
        'pawn',
        'collect',
        'sell',
        'buy',
    ];

    public function order(){
        return $this->hasOne(MonthlyTotalNumberOfOrder::class,'InvoiceDate','InvoiceDate');
    }
}
