<?php

namespace App\Models\ChartData;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LowHighSpender extends Model
{
    use HasFactory;

    protected $table = '815_LowHighSpender';
    protected $fillable = [
        'CustomerID',
        'Revenue',
    ];
}
